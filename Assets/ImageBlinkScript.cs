﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageBlinkScript : MonoBehaviour
{
	[HideInInspector]
	public Image image;

	public Color startClr;
	public Color endClr;
	public float speed;

	private float count;
	private void Awake( )
	{
		image = GetComponent<Image>( );
		image.color = startClr;
	}
	private void Update( )
	{
		count += speed * Time.deltaTime;
		image.color = Color.Lerp( startClr, endClr, count );
		if( count > 1f || count < 0f )
			speed *= -1;
	}
}