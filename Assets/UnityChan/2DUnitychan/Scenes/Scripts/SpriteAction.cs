﻿using UnityEngine;
using System.Collections;

public class SpriteAction : MonoBehaviour
{
	public bool isMove = true;
	int hashSpeed = Animator.StringToHash ("Speed");
	int hashFallSpeed = Animator.StringToHash ("FallSpeed");
	int hashGroundDistance = Animator.StringToHash ("GroundDistance");
	int hashIsCrouch = Animator.StringToHash ("IsCrouch");

	int hashDamage = Animator.StringToHash ("Damage");
	int hashIsDead = Animator.StringToHash ("IsDead");

	[SerializeField] private float characterHeightOffset = 0.2f;
	[SerializeField] LayerMask groundMask;

	[SerializeField, HideInInspector] Animator animator;
	[SerializeField, HideInInspector]SpriteRenderer spriteRenderer;
	[SerializeField, HideInInspector]Rigidbody2D rig2d;

	public int hp = 4;

	void Awake( )
	{
		animator = GetComponent<Animator>( );
		spriteRenderer = GetComponent<SpriteRenderer>( );
		rig2d = GetComponent<Rigidbody2D>( );
		stop = false;
		walk = false;
		play = false;
	}

	float rangeX = 2f;
	float curSpeed = 0.7f;
	float speed = 0f;

	bool walk;
	bool stop;
	bool play;

	IEnumerator Stop( )
	{
		stop = true;
		float time = Random.Range(3f,7f);
		yield return new WaitForSeconds( time );
		stop = false;
	}

	IEnumerator Walk( float go = 0f )
	{
		walk = true;
		float time = Random.Range(0.5f,2.5f);
		float ran = Random.Range(0f,1f);

		if( go != 0f )
		{
			ran = go;
		}
		speed = curSpeed * ( ran >= 0.5f ? 1 : -1 );
		while( walk && time > 0f )
		{

			time -= Time.deltaTime;
			yield return null;
		}
		speed = 0f;
		walk = false;
	}

	IEnumerator PlayAni( bool playi = true )
	{
		if( playi )
		{
			animator.SetTrigger( "Ani1" );
		}
		play = true;
		yield return new WaitForSeconds( 3f );
		play = false;
	}

	private void Update( )
	{
		if( play )
			return;
		if( !isMove )
		{
			float ran = Random.Range(0f,1f);
			if( ran < 0.3f )
			{
				StartCoroutine( PlayAni( ) );
			}
			else
			{
				StartCoroutine( PlayAni( false ) );
			}
			return;
		}

		if( stop )
			return;

		if( !walk )
		{
			float ran = Random.Range(0f,1f);
			if( ran < 0.25f )
			{
				StartCoroutine( Stop( ) );
			}
			else
			{
				StartCoroutine( Walk( ) );
			}
		}

		if( transform.localPosition.x > rangeX )
		{
			StopCoroutine( Walk( ) );
			StartCoroutine( Walk( -1f ) );
		}
		else if( transform.localPosition.x < -rangeX )
		{
			StopCoroutine( Walk( ) );
			StartCoroutine( Walk( 1f ) );
		}
		animator.SetFloat( hashSpeed, Mathf.Abs( speed ) );
		transform.position += Vector3.right * speed * Time.deltaTime;


		if( speed != 0 )
			spriteRenderer.flipX = speed < 0;
	}

	//void Update ()
	//{
	//	float axis = Input.GetAxisRaw ("Horizontal");
	//	bool isDown = Input.GetAxisRaw ("Vertical") < 0;

	//	if (Input.GetButtonDown ("Jump")) {
	//		rig2d.velocity = new Vector2 (rig2d.velocity.x, 5);
	//	}

	//	var distanceFromGround = Physics2D.Raycast (transform.position, Vector3.down, 1, groundMask);

	//	// update animator parameters
	//	animator.SetBool (hashIsCrouch, isDown);
	//	animator.SetFloat (hashGroundDistance, distanceFromGround.distance == 0 ? 99 : distanceFromGround.distance - characterHeightOffset);
	//	animator.SetFloat (hashFallSpeed, rig2d.velocity.y);
	//	animator.SetFloat (hashSpeed, Mathf.Abs (axis));

	//	// flip sprite
	//	if (axis != 0)
	//		spriteRenderer.flipX = axis < 0;
	//}
}
