﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPotal : ObjectInteraction
{
	public bool on;
	public GameObject explain;

	private float ttimer = 0.4f; // 키입력 재입력 대기 시간 (안하면 프레임당 호출됨)
	private float accc = 0f;

	protected override void Update( )
	{
		if( on )
			explain.SetActive( true );
		else
			explain.SetActive( false );
		if( flag == 1 )
			NpcInPutKey( );
		accc += Time.deltaTime;
	}
	private void NpcInPutKey( )
	{
		if( accc >= ttimer && GameMgr.Instance.Player.cs.pis.axies[(int)PlayerInputScript.Aixes.MoveVertical].GetAxis( ) > 0.7f )
		{
			accc = 0f;
			NpcSet( );
		}
	}
	protected override void NpcSet( )
	{
		if( !on )
			return;
	}

	protected override void HiddenSet( )
	{
		if( !on )
			return;
	}

	protected override void OutColli( )
	{
		if( !on )
			return;

	}
	protected override void OnTriggerEnter2D( Collider2D other )
	{

		if( other.tag == TargetTag )
		{
			this.other = other.gameObject;
			flag = 1;
			HiddenSet( );
		}

	}

	protected override void OnTriggerExit2D( Collider2D other )
	{
		if( other.tag == TargetTag )
		{
			this.other = null;
			flag = 0;

			OutColli( );

		}

	}
}