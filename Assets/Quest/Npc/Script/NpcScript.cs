﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum NpcType
{
	quest,
	upgrade,
	questDummy
}

public class NpcScript : ObjectInteraction
{
	public string npcName;
	public NpcType npcType;
	public Sprite npcSprite;

	public Text npcNameT;
	public miniTextBoxScript miniTextBox;

	public int[] questIdArr; //일반 퀘스트 목록
	public int[] questIdReArr; //반복 퀘스트 목록 (이중에 랜덤으로 뽑음)

	public string[] talkArr; //랜덤대사
	public string openingTalk; //대화창 열었을 때
	public string startTalk; //대사(퀘 없을 때)

	public int questIdRe; //반복 체크용

	private float talkDelay;
	private float talkTime = 3f;

	public void Awake( )
	{
		miniTextBox.miniTextBox.SetActive( false );
		talkDelay = Random.Range( 1f, 2f ) + Time.time;
		NpcInfoSet( );
	}

	protected override void Update( )
	{
		base.Update( );
		if( talkDelay <= Time.time )
		{
			talkDelay = Random.Range( 4f, 5f ) + Time.time;
			StartCoroutine( MiniTalk( ) );
		}
	}

	private IEnumerator MiniTalk( )
	{
		miniTextBox.talkT.text = talkArr[Random.Range( 0, talkArr.Length )];
		miniTextBox.miniTextBox.SetActive( true );
		yield return new WaitForSeconds( talkTime );
		miniTextBox.miniTextBox.SetActive( false );
	}

	public void NpcInfoSet( )
	{
		npcNameT.text = npcName;
		talk = false;
	}

	protected override void NpcSet( )
	{
		switch( npcType )
		{
		case NpcType.quest: Talk( ); break;
		case NpcType.upgrade: UpgradeOpen( ); break;
		}
	}

	public void Talk( )
	{
		if( !talk && GameMgr.Instance.Player.state == PlanetUtil.CharacterState.Idle )
		{
			talk = true;
			int qNum = QuestCheck();
			TalkMgr.Instance.TalkStart( this, qNum );
		}
	}

	public void UpgradeOpen( )
	{
		if( GameMgr.Instance.Player.state == PlanetUtil.CharacterState.Idle )
		{
			UIMgr.Instance.ToggleUpgradeMenu( );
		}
	}

	public void TalkEnd( )
	{
		talk = false;
		if( npcType == NpcType.questDummy )
		{
			PlayerData.Instance.playerDic["YukoSQuest"] = 1;
			gameObject.SetActive( false );
		}
	}

	public int QuestCheck( )
	{
		int qNum = 100;

		//일반퀘
		if( questIdArr.Length != 0 )
		{
			for( int i = 0 ; i < questIdArr.Length ; ++i )
			{
				if( PlayerData.Instance.questInfoList[questIdArr[i]].state == QuestState.enable ||
					PlayerData.Instance.questInfoList[questIdArr[i]].state == QuestState.ing ||
					PlayerData.Instance.questInfoList[questIdArr[i]].state == QuestState.clear )
				{
					qNum = questIdArr[i];
					break;
				}
			}
		}
		//반복퀘
		if( qNum == 100 )
		{
			if( questIdReArr.Length != 0 )
			{
				List<int> qt = new List<int>();
				foreach( var a in questIdReArr )
					if( PlayerData.Instance.questInfoList[a].state != QuestState.disable && PlayerData.Instance.questInfoList[a].state != QuestState.disable )
						qt.Add( a );

				if( qt.Count > 0 )
				{
					for( int i = 0 ; i < qt.Count + 1 ; ++i )
					{
						if( questIdRe != 100 )
						{
							qNum = questIdRe;
							break;
						}
						if( i == qt.Count )
						{
							qNum = qt[Random.Range( 0, qt.Count )];
							questIdRe = qNum;
							break;
						}
						else
						{
							if( PlayerData.Instance.questInfoList[qt[i]].state == QuestState.ing ||
								PlayerData.Instance.questInfoList[qt[i]].state == QuestState.clear )
							{
								qNum = qt[i];
								questIdRe = qNum;
								break;
							}
						}
					}
				}
			}
		}
		return qNum;
	}

	public void QuestAgree( int questId )
	{
		QuestMgr.Instance.QuestAgree( questId );
	}

	private bool miniTalk;
	private bool talk;
	protected override void OnTriggerEnter2D( Collider2D collision )
	{
		base.OnTriggerEnter2D( collision );
	}
	protected override void OnTriggerExit2D( Collider2D collision )
	{
		base.OnTriggerExit2D( collision );
	}
}