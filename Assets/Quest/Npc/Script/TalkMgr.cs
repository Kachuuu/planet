﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkMgr : MonoBehaviour
{
	[HideInInspector]

	public QuestInfo quest;
	public int questId;
	public static TalkMgr Instance;

	public GameObject talkPannel;
	public GameObject talkNpcPannel;
	public GameObject talkQuestPannel;

	public Image npcImage;
	public Sprite[] npcSprites;

	public Text npcNameT;
	public Text talkT;

	public GameObject questButton;
	public Text questButtonT;
	public GameObject talkEndButton;
	public Text talkEndButtonT;

	[Space]

	public Text typeT;
	public Text questNameT;
	public Text questT;
	public Text goalT;

	public GameObject acceptButton;
	public GameObject neggativeButton;
	public GameObject completeButton;
	public GameObject giveupButton;
	public GameObject talkEndButton2;

	public TalkMgr( )
	{
		Instance = this;
	}

	private NpcScript npc;

	public void TalkStart( NpcScript npc, int questId = 100 )
	{
		this.npc = npc;
		this.questId = questId;
		npcImage.sprite = npc.npcSprite;
		npcNameT.text = npc.npcName;

		talkT.text = npc.openingTalk;

		questButtonT.text = "혹시 나에게 퀘스트를 줄 수 있니?";
		talkEndButtonT.text = "너는 내 취향이 아니야 잘 지내~";

		Debug.Log( questId );

		if( npc.npcType == NpcType.questDummy )
		{
			TalkDummy( );
			talkPannel.SetActive( true );
			Time.timeScale = 0f;
			return;
		}
		if( questId != 100 )
		{
			quest = PlayerData.Instance.questInfoList[questId];
			switch( quest.state )
			{
			case QuestState.enable:
				TalkNormal( );
				break;
			case QuestState.ing:
				acceptButton.SetActive( false );
				neggativeButton.SetActive( false );
				completeButton.SetActive( false );
				giveupButton.SetActive( true );
				talkEndButton2.SetActive( true );
				questT.text = quest.ingTalk;
				QuestShow( true );
				break;
			case QuestState.clear:
				acceptButton.SetActive( false );
				neggativeButton.SetActive( false );
				completeButton.SetActive( true );
				giveupButton.SetActive( false );
				talkEndButton2.SetActive( true );
				questT.text = quest.completeTalk;
				QuestShow( true );
				break;
			case QuestState.complete:
				QuestShowFail( );
				break;
			case QuestState.disable:
				QuestShowFail( );
				break;
			}
		}
		else
		{
			TalkNormal( );
		}
		talkPannel.SetActive( true );
		Time.timeScale = 0f;
	}

	public void TalkNormal( )
	{
		questButton.SetActive( true );
		talkEndButton.SetActive( true );
		talkQuestPannel.SetActive( false );
		talkNpcPannel.SetActive( true );
	}

	public void TalkDummy( )
	{
		talkEndButtonT.text = "그래 얼른 들어가.";
		questButton.SetActive( false );
		talkEndButton.SetActive( true );
		talkQuestPannel.SetActive( false );
		talkNpcPannel.SetActive( true );
	}

	public void QuestShow( bool ing = false )
	{
		if( quest == null )
		{
			QuestShowFail( );
			return;
		}
		//Debug.Log( quest.questName );
		//Debug.Log( quest.questInfo );
		typeT.text = quest.reAble == 1 ? "Sub Quest" : "Main Quest";
		questNameT.text = quest.questName;
		goalT.text = quest.questInfo;
		if( !ing )
		{
			acceptButton.SetActive( true );
			neggativeButton.SetActive( true );
			completeButton.SetActive( false );
			giveupButton.SetActive( false );
			talkEndButton2.SetActive( false );
			questT.text = quest.startTalk;
		}
		talkNpcPannel.SetActive( false );
		talkQuestPannel.SetActive( true );
	}

	public void QuestShowFail( )
	{
		talkT.text = npc.startTalk;
		questButton.SetActive( false );
		talkEndButtonT.text = "없음 말고.";
	}

	public void QuestAgree( )
	{
		QuestMgr.Instance.QuestAgree( questId );
		TalkEnd( );
	}

	public void QuestGiveup( )
	{
		QuestMgr.Instance.QuestGiveup( questId );
		TalkEnd( );
	}

	public void QuestComplete( )
	{
		QuestMgr.Instance.QuestComplete( questId );
		npc.questIdRe = 100;
		TalkEnd( );
	}

	public void TalkEnd( )
	{
		quest = null;
		Time.timeScale = 1f;
		npc.TalkEnd( );
		talkNpcPannel.SetActive( false );
		talkQuestPannel.SetActive( false );
		talkPannel.SetActive( false );
	}
}