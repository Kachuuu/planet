﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SexyNPC : MonoBehaviour
{
	public bool questGive;
	public GameObject textBox;

	public int questId = 0;

	void Update( )
	{
		if( Player != null )
		{
			if( Input.GetKeyDown( KeyCode.Q ) && !questGive )
			{
				QuestAgree( );
				Debug.Log( "퀘스트 받음" );
			}
			else if( Input.GetKeyDown( KeyCode.Q ) && questGive )
			{
				Debug.Log( "이미 받음" );
			}
		}
	}

	public void QuestAgree( )
	{
		questGive = true;
		QuestMgr.Instance.QuestAgree( questId );
	}

	private Collider2D Player;
	public void OnTriggerEnter2D( Collider2D collision )
	{
		if( collision.tag == "Player" )
		{
			Player = collision;
			textBox.SetActive( true );
		}
	}
	public void OnTriggerExit2D( Collider2D collision )
	{
		if( collision.tag == "Player" )
		{
			Player = null;
			textBox.SetActive( false );
		}
	}
}