﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class Inventory
{
	public List<Item> items;
	public List<Item> backpack;
	public int mineral;
	public int dungoenMineral;

	public Inventory()
	{
		items = new List<Item>( );
		backpack = new List<Item>( );
	}

	public Inventory( Inventory _inven )
	{
		items = new List<Item>( );
		for( int i = 0 ; i < _inven.items.Count ; ++i )
			items.Add( _inven.items[i].Clone() );

		backpack = new List<Item>( );
		for( int i = 0 ; i < _inven.backpack.Count ; ++i )
			backpack.Add( _inven.backpack[i].Clone( ) );

		mineral = _inven.mineral;
		dungoenMineral = _inven.mineral;
	}

	public Inventory Clone()
	{
		return new Inventory( this );
	}

	public List<Item> sortedItemList
	{
		get
		{
			items.Sort( ( x, y ) => ItemComparison( x, y ) );
			return items;
		}
	}
	public List<Item> sortedBackpack
	{
		get
		{
			backpack.Sort( ( x, y ) => ItemComparison( x, y ) );
			return items;
		}
	}


	int ItemComparison( Item x, Item y )
	{
		if( x.useableLevel < y.useableLevel )
			return 1;
		else if( x.useableLevel > y.useableLevel )
			return -1;
		else
		{
			if( (int)x.type < (int)y.type )
				return 1;
			else if( (int)x.type > (int)y.type )
				return -1;
			else if( x.type == PlanetUtil.ItemType.Weapon )
			{
				if( (int)( (Weapon)x ).wpType < (int)( (Weapon)y ).wpType )
					return 1;
				else if( (int)( (Weapon)x ).wpType > (int)( (Weapon)y ).wpType )
					return -1;
			}
		}

		return string.Compare( x.name, y.name );
	}

	public bool AddItem( Item _item, bool _inDungeon = false )
	{
		_inDungeon = false;
		int count = _item.nowCount;
		for( int i = 0 ; i < items.Count ; ++i )
		{
			if( items[i].id == _item.id )
				if( items[i].nowCount < _item.maxCount )
				{
					if( !_inDungeon )           // 던전 속이라면 실제 인벤토리에 넣지 않는다.
					{
						items[i].nowCount += count;

						if( items[i].nowCount <= items[i].maxCount )
						{
							items.Add( _item );
							return true;
						}
						else
						{
							count = items[i].nowCount - items[i].maxCount;
							items[i].nowCount -= count;
						}
					}
					else		// 던전 속이라면 계산만 한다.
					{
						if( items[i].nowCount + count <= items[i].maxCount )
						{
							AddItemIndungeon( _item );
							return true;
						}
						else
							count = items[i].nowCount + count - items[i].maxCount;
					}
				}
		}

		if( items.Count < 100 )
		{
			if( !_inDungeon )                   // 던전 속이라면 실제 인벤토리에 넣지 않는다.
			{
				items.Add( _item );
			}
			else
			{
				AddItemIndungeon( _item );
			}
			return true;
		}

		return false;
	}

	public bool AddItemIndungeon( Item _item )
	{
		int count = _item.nowCount;
		for( int i = 0 ; i < backpack.Count ; ++i )
		{
			if( backpack[i].id == _item.id )
			{
				backpack[i].nowCount += count;
				if( backpack[i].nowCount <= backpack[i].maxCount )
				{
					return true;
				}
				else
				{
					count = backpack[i].nowCount - backpack[i].maxCount;
					backpack[i].nowCount -= count;
				}
			}
		}

		if( items.Count + backpack.Count < 100 )
		{
			backpack.Add( _item );
			return true;
		}
		return false;
	}

	public Item RemoveItem( int _idx, int count = 1 )
	{
		if( items.Count > _idx )
		{
			if( items[_idx].nowCount > count )
			{
				items[_idx].nowCount -= count;
				var item = items[_idx].Clone();
				item.nowCount = count;
				return item;
			}
		}
		return null;
	}

	public Item RemoveItem( Item _item, int count = 1 )
	{
		for( int i = 0 ; i < items.Count ; ++i )
		{
			if( items[i] == _item )
			{
				if( items[i].nowCount > count )
				{
					items[i].nowCount -= count;
					var it = items[i].Clone( );
					it.nowCount = count;
					return it;
				}
				else
				{
					items.RemoveAt( i );
					_item.nowCount = count;
					return _item;
				}
			}
		}

		for( int i = 0 ; i < backpack.Count ; ++i )
		{
			if( backpack[i] == _item )
			{
				if( backpack[i].nowCount > count )
				{
					backpack[i].nowCount -= count;
					var it = backpack[i].Clone( );
					it.nowCount = count;
					return it;
				}
				else
				{
					backpack.RemoveAt( i );
					_item.nowCount = count;
					return _item;
				}
			}
		}
		return null;
	}

	public Item RemoveItemId( int _id, int count = 1 )
	{
		for( int i = 0 ; i < items.Count ; ++i )
		{
			if( items[i].id == _id )
				return RemoveItem( i, count);
		}
		return null;
	}

	public void AddMineral( int _value, bool _inDungoen = false )
	{
		if( _inDungoen )
			dungoenMineral += _value;
		else
			mineral += _value;
	}

	public bool SubMineral( int _value )
	{
		if( _value > mineral )
			return false;
		mineral -= _value;
		return true;
	}

	public void Merge()
	{
		for(int i = 0 ; i < backpack.Count ; ++i )
			AddItem( backpack[i] );
		backpack.Clear();
		backpack.TrimExcess( );
		items.TrimExcess( );
		mineral += dungoenMineral;
		dungoenMineral = 0;
	}

}