﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQuestBtnScript : MonoBehaviour
{
	public QuestInfo quest = null;
	public Button questBtn;
	public Text questBtnText;
	public Sprite[] btnSprites; //0:선택안됨, 1:선택됨

	public void ButtonChange( int num = 0 )
	{
		questBtn.image.sprite = btnSprites[num];
	}

	public void QuestInsert( QuestInfo quest )
	{
		if( this.quest == null )
		{
			if( quest != null )
			{
				this.quest = quest;
				questBtnText.text = quest.questName;
			}
		}
	}

	public void QuestRemove( )
	{
		quest = null;
		questBtnText.text = "";
		Debug.Log( quest );
	}
}