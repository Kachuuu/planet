﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public enum DataKind //퀘스트, 보상, 인게임 보상 물품 등
{
	level,
	clear,
	kill,
	item,
	gold,
	exp,
	npc
}

public class PlayerData  //일단은 모노
{
	public static PlayerData Instance;
	public List<int> questList = new List<int>(); //진행중인 퀘스트에 바로 접근

	public Dictionary<string,int> playerDic = new Dictionary<string, int>(); //임시로 만든 dic

	/////////////////// 저장될 정보들 /////////////////////

	public List<QuestInfo> questInfoList = new List<QuestInfo>();

	public EquipItem[] items = new EquipItem[4]; // 장착된 아이템
	public Inventory inven;

	public int skillPoint;
	public ActiveSkill[,] skills = new ActiveSkill[(int)PlanetUtil.WeaponType.None, 3];
	public int[,] equipSkills = new int[(int)PlanetUtil.WeaponType.None, 2]
		{
			{-1,-1 },
			{-1,-1 },
			{-1,-1 },
			{-1,-1 },
		};

	public int level;
	public float nowExp;

	//////////////////////////////////////////////////
	
	[System.Serializable]
	public struct PlayerDataStruct
	{
		public QuestInfo[] questInfoList;
		public EquipItem[] items;
		public Inventory inven;
		public int skillPoint;
		public int[,] skills;
		public int[,] equipSkills;
		public int level;
		public float nowExp;

		public PlayerDataStruct( QuestInfo[] questInfoList, 
								EquipItem[] items, Inventory inven,
								int skillPoint, int[,] skills, int[,] equipSkills, 
								int level, float nowExp )
		{
			this.questInfoList = new QuestInfo[questInfoList.Length];
			//for(int i = 0 ; i < questInfoList.Length ; ++i )
			//	this.questInfoList[i] = questInfoList[i].Clone( );

			this.items = new EquipItem[items.Length];
			for( int i = 0 ; i < items.Length ; ++i )
				if( items[i] != null )
					this.items[i] = (EquipItem)items[i].Clone( );

			this.inven = inven.Clone( );

			this.skillPoint = skillPoint;

			this.skills = new int[(int)PlanetUtil.WeaponType.None, 3];
			for( int i = 0 ; i < skills.GetLength( 0 ) ; ++i )
				for( int j = 0 ; j < skills.GetLength( 1 ) ; ++j )
					this.skills[i, j] = skills[i, j];

			this.equipSkills = new int[(int)PlanetUtil.WeaponType.None, 2];
			for( int i = 0 ; i < equipSkills.GetLength( 0 ) ; ++i )
				for( int j = 0 ; j < equipSkills.GetLength( 1 ) ; ++j )
					this.equipSkills[i, j] = equipSkills[i, j];

			this.level = level;
			this.nowExp = nowExp;
		}

		public PlayerDataStruct( PlayerDataStruct _data )
		{
			this.questInfoList = new QuestInfo[_data.questInfoList.Length];
			//for(int i = 0 ; i < questInfoList.Length ; ++i )
			//	this.questInfoList[i] = questInfoList[i].Clone( );

			this.items = new EquipItem[_data.items.Length];
			for( int i = 0 ; i < _data.items.Length ; ++i )
				this.items[i] = (EquipItem)_data.items[i].Clone( );

			this.inven = _data.inven.Clone( );

			this.skillPoint = _data.skillPoint;

			this.skills = new int[(int)PlanetUtil.WeaponType.None, 3];
			for( int i = 0 ; i < _data.skills.GetLength( 0 ) ; ++i )
				for( int j = 0 ; j < _data.skills.GetLength( 1 ) ; ++j )
					this.skills[i, j] = _data.skills[i, j];

			this.equipSkills = new int[(int)PlanetUtil.WeaponType.None, 2];
			for( int i = 0 ; i < _data.equipSkills.GetLength( 0 ) ; ++i )
				for( int j = 0 ; j < _data.equipSkills.GetLength( 1 ) ; ++j )
					this.equipSkills[i, j] = _data.equipSkills[i, j];

			this.level = _data.level;
			this.nowExp = _data.nowExp;
		}

		public PlayerDataStruct Clone( )
		{
			return new PlayerDataStruct( this );
		}

		public void Save( string _path )
		{
			try
			{
				BinaryFormatter bf = new BinaryFormatter();
				FileStream stream = new FileStream(_path, FileMode.OpenOrCreate);
				bf.Serialize( stream, this );
				stream.Close( );
			}
			catch(Exception e)
			{
				Debug.Log( "파일 쓰기 실패 " + e);
			}
		}

		public static bool Load( string _path, out PlayerDataStruct _out )
		{
			BinaryFormatter bf = new BinaryFormatter();
			try
			{
				FileStream stream = new FileStream(_path, FileMode.Open);
				_out = ( PlayerDataStruct)bf.Deserialize( stream );
				stream.Close( );
				return true;
			}
			catch(Exception e)
			{
				Debug.Log( "파일 열기 실패 " + e.ToString( ) );
			}

			_out = new PlayerDataStruct( );
			return false;
		}
	}


	public PlayerData( )
	{
		if( Instance == null )
		{
			Instance = this;
			inven = new Inventory( );
			PlayerDataPath( );

			skills[(int)PlanetUtil.WeaponType.Handgun, 0] = new Active_Handgun_Freedom( null, 26, 0 );
			skills[(int)PlanetUtil.WeaponType.Handgun, 1] = new Active_Handgun_Guard( null, 27, 1 );
			skills[(int)PlanetUtil.WeaponType.Handgun, 2] = new Active_Handgun_Nansa( null, 28, 2 );

			skills[(int)PlanetUtil.WeaponType.Riple, 0] = new Active_Riple_ParalysisShoot( null, 31, 0 );
			skills[(int)PlanetUtil.WeaponType.Riple, 1] = new Active_Riple_Sniping( null, 30, 1 );
			skills[(int)PlanetUtil.WeaponType.Riple, 2] = new Active_Riple_Upgrade( null, 29, 2 );

			skills[(int)PlanetUtil.WeaponType.Shotgun, 0] = new Active_Shotgun_FireBomb( null, 32, 0 );
			skills[(int)PlanetUtil.WeaponType.Shotgun, 1] = new Active_Shotgun_FlashBomb( null, 33, 1 );
			skills[(int)PlanetUtil.WeaponType.Shotgun, 2] = new Active_Shotgun_Upgrade( null, 34, 2 );

			skills[(int)PlanetUtil.WeaponType.Scv, 0] = new Active_Scv_Dron( null, 35, 0 );
			skills[(int)PlanetUtil.WeaponType.Scv, 1] = new Active_Scv_Tower( null, 36, 1 );
			skills[(int)PlanetUtil.WeaponType.Scv, 2] = new Active_Scv_Upgrade( null, 37, 2 );

		}
	}

	internal bool ExistSaveFile( )
	{
		try
		{
			FileStream stream = new FileStream(Application.persistentDataPath + "/save.bat", FileMode.Open);
			stream.Close( );
			return true;
		}
		catch
		{
			return false;
		}
	}

	public void Save()
	{
		var sklvl = new int[(int)PlanetUtil.WeaponType.None, 3];
		for( int i = 0 ; i < skills.GetLength( 0 ) ; ++i )
			for( int j = 0 ; j < skills.GetLength( 1 ) ; ++j )
				sklvl[i, j] = skills[i, j].level;

		// 아이템 장착 정보 해제
		List<int> applyedItemIndex = new List<int>();
		for( int i = 0 ; i < items.Length ; ++i )
		{
			if( null != items[i] )
			{
				if( items[i].isApplyed )
				{
					items[i].isApplyed = false;
					applyedItemIndex.Add( i );
				}
			}
		}

		var pds = new PlayerDataStruct( questInfoList.ToArray( ), items, inven, skillPoint, sklvl, equipSkills, level, nowExp );
		pds.Save( Application.persistentDataPath + "/save.bat" );
		Debug.Log( Application.persistentDataPath + "/save.bat saved" );

		// 아이템 장착 정보 재설정
		while(applyedItemIndex.Count != 0)
		{
			items[applyedItemIndex[0]].isApplyed = true;
			applyedItemIndex.RemoveAt( 0 );
		}
	}

	public bool Load( )
	{
		PlayerDataStruct data;
		if (PlayerDataStruct.Load( Application.persistentDataPath + "/save.bat", out data ) )
		{

			questInfoList.AddRange( questInfoList );
			items = data.items;
			inven = new Inventory(data.inven);


			skillPoint = data.skillPoint;
			for( int i = 0 ; i < skills.GetLength( 0 ) ; ++i )
				for( int j = 0 ; j < skills.GetLength( 1 ) ; ++j )
					skills[i, j].level = data.skills[i, j];

			for( int i = 0 ; i < equipSkills.GetLength( 0 ) ; ++i )
				for( int j = 0 ; j < equipSkills.GetLength( 1 ) ; ++j )
					equipSkills[i, j] = data.equipSkills[i, j];

			level = data.level;
			nowExp = data.nowExp;

			return true;
		}

		return false;
	}

	public void UpdateSkillStat(StatusScript _stat)
	{
		for( int i = 0 ; i < skills.GetLength( 0 ) ; ++i )
			for( int j = 0 ; j < skills.GetLength( 1 ) ; ++j )
				skills[i, j].SetStat(_stat);
	}

	public void PlayerDataSet( DataKind kind, string key, int count )
	{
		int cnt;
		if( playerDic.TryGetValue( key, out cnt ) )
			playerDic[key] = cnt + count;
		else
			playerDic[key] = count;

		foreach( var a in questList )
			if( questInfoList[a].state == QuestState.ing ||
				questInfoList[a].state == QuestState.clear )
				questInfoList[a].isClear( );
	}

	public void PlayerDataPath( )
	{
		playerDic.Add( "Level", 1 );
		playerDic.Add( "Exp", 0 );
		playerDic.Add( "Gold", 0 );

		playerDic.Add( /*"NormalMonster"*/ PlanetUtil.CharacterTypes.monster.ToString( ), 0 );
		playerDic.Add( /*"BossMonster"*/PlanetUtil.CharacterTypes.bossmonster.ToString( ), 0 );

		playerDic.Add( "YukoSQuest", 0 ); //토코 만나기 퀘스트
		playerDic.Add( "RedReptailDrop", 0 ); //도마뱀 드랍 아이템
		playerDic.Add( "Stage3Monster", 0 ); //스테이지3 몬스터 처치수

		playerDic.Add( "RedReptail", 0 );
		playerDic.Add( "RedReptailBoss", 0 );

		playerDic.Add( "MaleNaga", 0 );
		playerDic.Add( "FemaleNaga", 0 );
		playerDic.Add( "MaleNagaBoss", 0 );
		playerDic.Add( "FemaleNagaBoss", 0 );
	}
}