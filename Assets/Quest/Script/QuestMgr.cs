﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class QuestMgr : MonoBehaviour
{
	public static QuestMgr Instance;
	//public List<string[]> questPathList = new List<string[]>(); //퀘스트 전체 데이터 만들 path
	//public List<QuestInfo> questInfoList = new List<QuestInfo>(); //퀘스트 전체 데이터

	public GameObject questPopUp;
	public UIQuestBtnScript[] UIQuestBtnScripts;
	public Text questInfoText;
	public Text questGoalText;
	public Text questRewardText;
	public UIQuestScript[] questScripts; //안씀

	private void Awake( )
	{
		//시작할 때 퀘스트창 초기화
		if( Instance == null )
		{
			Instance = this;
			Debug.Log( "????" );
		}
		foreach( var a in questScripts )
			a.UIRemove( false );
		foreach( var a in UIQuestBtnScripts )
			a.quest = null;
	}

	////퀘스트 데이터 생성. PlayerData 실행 이후 실행.
	//public void Quest( )
	//{
	//	QuestPath( );
	//	QuestInfoMake( );
	//	QuestEnableCheck( ); //퀘스트 활성화 체크
	//	Debug.Log( questInfoList[0].questName );
	//}

	//퀘스트 수락
	public void QuestAgree( int questId )
	{
		Debug.Log( "agree: " + questId );
		//퀘스트 리스트 추가, 상태 변경
		var a = PlayerData.Instance;
		a.questList.Add( questId );

		//UI 생성
		MakeQuestUI( a.questInfoList[questId] );

		//퀘스트 시작
		a.questInfoList[questId].QuestStart( );
	}

	//퀘스트 UI 생성
	public void MakeQuestUI( QuestInfo quest )
	{
		for( int i = 0 ; i < UIQuestBtnScripts.Length ; ++i )
		{
			if( UIQuestBtnScripts[i].quest == null )
			{
				Debug.Log( i + "번에 퀘스트 등록" );
				UIQuestBtnScripts[i].gameObject.SetActive( true );
				UIQuestBtnScripts[i].QuestInsert( quest );
				quest.uiId = i;
				break;
			}
		}
	}

	//퀘스트 UI 표시
	public void QuestUiEnable( )
	{
		foreach( var a in UIQuestBtnScripts )
			a.ButtonChange( 0 );
		questInfoText.text = "";
		questGoalText.text = "";
		questRewardText.text = "";
		questPopUp.SetActive( true );
	}

	public void QuestUiDisable( )
	{
		questPopUp.SetActive( false );
	}

	//퀘스트 UI 업데이트
	public void QuestTextUpdate( )
	{
		for( int i = 0 ; i < UIQuestBtnScripts.Length - 1 ; ++i )
		{
			if( UIQuestBtnScripts[i].quest == null && UIQuestBtnScripts[i + 1].quest != null )
			{
				UIQuestBtnScripts[i].QuestInsert( UIQuestBtnScripts[i + 1].quest );
				UIQuestBtnScripts[i + 1].quest = null;
			}
		}

		for( int i = 0 ; i < UIQuestBtnScripts.Length ; ++i )
		{
			if( UIQuestBtnScripts[i].quest == null )
				UIQuestBtnScripts[i].gameObject.SetActive( false );
			else
				UIQuestBtnScripts[i].gameObject.SetActive( true );
		}
	}

	//퀘스트 UI 삭제
	public void QuestTextRemove( int uiId )
	{
		if( uiId < UIQuestBtnScripts.Length )
		{
			UIQuestBtnScripts[uiId].quest = null;
			QuestTextUpdate( );
		}
	}

	//퀘스트 포기
	public void QuestGiveup( int QuestId )
	{
		var a = PlayerData.Instance;
		a.questList.Remove( QuestId );
		PlayerData.Instance.questInfoList[QuestId].QuestGiveup( );
	}

	//퀘스트 완료
	public void QuestComplete( int QuestId )
	{
		PlayerData.Instance.questInfoList[QuestId].QuestComplete( );
		//Debug.Log( "퀘스트완료" );

		//보상
		Debug.Log( "보상획득" );
		var rewards = PlayerData.Instance.questInfoList[QuestId].rewardList;
		for( int i = 0 ; i < rewards.Count ; ++i )
		{
			PlayerData.Instance.PlayerDataSet( DataKind.clear, rewards[i].targetKey, rewards[i].targetCount );
		}

		//퀘스트 클리어시 활성화 체크
		QuestEnableCheck( );
	}

	//퀘스트 활성화 체크
	public void QuestEnableCheck( )
	{
		//퀘스트 클리어 외에 레벨업 할때도 체크할 것
		foreach( var a in PlayerData.Instance.questInfoList )
			if( a.state == QuestState.disable )
				a.QuestEnableCheck( );
	}

	//퀘스트 버튼 클릭
	public void QuestBtnClick( int btnId )
	{
		foreach( var a in UIQuestBtnScripts )
			a.ButtonChange( 0 );
		UIQuestBtnScripts[btnId].ButtonChange( 1 );

		var q = UIQuestBtnScripts[btnId].quest;

		StringBuilder sb = new StringBuilder( );
		sb.Append( "-퀘스트 내용-\n" );
		sb.Append( q.questInfo );
		questInfoText.text = sb.ToString( );

		sb = new StringBuilder( );
		sb.Append( "-퀘스트 목표-\n" );
		for( int i = 0 ; i < q.goalList.Count ; i++ )
		{
			switch( q.goalList[i].kind )
			{
			//case DataKind.gold:
			//	sb.Append( q.goalList[i].targetName );
			//	sb.Append( " 획득" );
			//	sb.Append( " : " );
			//	sb.Append( PlayerData.Instance.playerDic[q.goalList[i].targetKey] - q.goalList[i].targetCurrCount );
			//	sb.Append( " / " );
			//	sb.Append( q.goalList[i].targetCount );
			//	sb.Append( "\n" );
			//	break;
			case DataKind.kill:
				sb.Append( q.goalList[i].targetName );
				sb.Append( " 처치" );
				sb.Append( " : " );
				sb.Append( PlayerData.Instance.playerDic[q.goalList[i].targetKey] - q.goalList[i].targetCurrCount );
				sb.Append( " / " );
				sb.Append( q.goalList[i].targetCount );
				sb.Append( "\n" );
				break;
			case DataKind.item:
				sb.Append( q.goalList[i].targetName );
				sb.Append( " 획득" );
				sb.Append( " : " );
				sb.Append( PlayerData.Instance.playerDic[q.goalList[i].targetKey] - q.goalList[i].targetCurrCount );
				sb.Append( " / " );
				sb.Append( q.goalList[i].targetCount );
				sb.Append( "\n" );
				break;
			//case DataKind.clear:
			//	sb.Append( q.goalList[i].targetName );
			//	sb.Append( " 클리어" );
			//	sb.Append( " : " );
			//	sb.Append( PlayerData.Instance.playerDic[q.goalList[i].targetKey] > 0 ? "미달성" : "달성" );
			//	sb.Append( "\n" );
			//	break;
			//case DataKind.level:
			//	sb.Append( q.goalList[i].targetName );
			//	sb.Append( " 달성" );
			//	sb.Append( " : " );
			//	sb.Append( PlayerData.Instance.playerDic[q.goalList[i].targetKey] - q.goalList[i].targetCurrCount );
			//	sb.Append( " / " );
			//	sb.Append( q.goalList[i].targetCount );
			//	sb.Append( "\n" );
			//	break;
			case DataKind.npc:
				sb.Append( q.goalList[i].targetName );
				sb.Append( " 찾기" );
				sb.Append( " : " );
				sb.Append( PlayerData.Instance.playerDic[q.goalList[i].targetKey] > 0 ? "미달성" : "달성" );
				sb.Append( "\n" );
				break;
			}
		}
		questGoalText.text = sb.ToString( );

		sb = new StringBuilder( );
		sb.Append( "-퀘스트 보상-\n" );
		for( int i = 0 ; i < q.rewardList.Count ; i++ )
		{
			sb.Append( q.rewardList[i].targetName );
			sb.Append( " : " );
			sb.Append( q.rewardList[i].targetCount );
			sb.Append( "\n" );
		}
		questRewardText.text = sb.ToString( );
	}


	//////////////////




	////퀘스트 패스 임시로 만듬
	//public void QuestPath( )
	//{
	//	for( int i = 0 ; i < 3 ; ++i )
	//	{
	//		string[] a = new string[3];
	//		//a[0] = QuestInfo
	//		//a[1] = GoalInfo
	//		//a[2] = Reward (미구현)

	//		if( i == 0 )
	//		{
	//			a[0] = i + ";나쁜 도마뱀(1);" + "마을을 어지럽히는 나쁜 도마뱀들을 죽이자.;" + "나쁜 도마뱀들 좀 죽여줄래? 더도말고 덜도말고 딱 1마리만."
	//				+ ";1;100;0";
	//			a[1] = "kill;나쁜도마뱀 처치;나쁜도마뱀;RedReptail;1";
	//			a[2] = "5;100";
	//		}
	//		if( i == 1 )
	//		{
	//			a[0] = i + ";나쁜 도마뱀(2);" + "도마뱀들을 통솔하는 보스 도마뱀을 죽이자.;" + "흠흠 우리 도마뱀쨩들에게는 아무 잘못도 없다능. 도마뱀쨩들을 통솔하는 보스도마뱀이 문제인거라능! 어서 죽여달라능!"
	//				+ ";1;0;0"; ;
	//			a[1] = "kill;도마뱀보스 처치;도마뱀보스;RedReptailBoss;1";
	//			a[2] = "5;100";
	//		}
	//		if( i == 2 )
	//		{
	//			a[0] = i + ";나쁜 도마뱀(3);" + "도마뱀과 보스도마뱀을 죽이자.;" + "도마뱀이건 도마뱀보스건 싹다 죽여버려! 나는 파충류는 딱 질색이라고!"
	//				+ ";1;1;0"; ;
	//			a[1] = "kill;나쁜도마뱀 처치;나쁜도마뱀;RedReptail;5" + "*"
	//				+ "kill;도마뱀보스 처치;도마뱀보스;RedReptailBoss;1";
	//			a[2] = "5;100";
	//		}

	//		//if( i == 3 )
	//		//{
	//		//	a[0] = i + ";나쁜 도마뱀(1);" + "마을을 어지럽히는 나쁜 도마뱀들을 죽이자.;" + "나쁜 도마뱀들 좀 죽여줄래? 더도말고 덜도말고 딱 2마리만."
	//		//		+ "0;1;1";
	//		//	a[1] = "kill;도마뱀 왕 처치;도마뱀 왕;RedReptailBoss;1";
	//		//	a[2] = "골드;Gold;5" + "*" + "경험치;Exp;150";
	//		//}
	//		//if( i == 4 )
	//		//{
	//		//	a[0] = i + ";나쁜 도마뱀(1);" + "마을을 어지럽히는 나쁜 도마뱀들을 죽이자.;" + "나쁜 도마뱀들 좀 죽여줄래? 더도말고 덜도말고 딱 3마리만."
	//		//		+ ";1;3;1";
	//		//	a[1] = "kill;나가의 왕 처치;도마뱀 왕;RedReptail;3";
	//		//	a[2] = "골드;Gold;5" + "*" + "경험치;Exp;300";
	//		//}
	//		//if( i == 5 )
	//		//{
	//		//	a[0] = i + ";나쁜 도마뱀(1);" + "마을을 어지럽히는 나쁜 도마뱀들을 죽이자.;" + "나쁜 도마뱀들 좀 죽여줄래? 더도말고 덜도말고 딱 3마리만."
	//		//		+ ";1;;1";
	//		//	a[1] = "kill;나가의 여왕 처치;나쁜도마뱀;RedReptail;3";
	//		//	a[2] = "골드;Gold;5" + "*" + "경험치;Exp;450";
	//		//}
	//		questPathList.Add( a );
	//	}
	//}

	//public void QuestInfoMake( )
	//{
	//	foreach( var a in questPathList )
	//		QuestInfoMake( a[0], a[1], a[2] );
	//}

	//public void QuestInfoMake( string questPath = null, string goalPath = null, string rewardPath = null )
	//{
	//	//퀘스트 인포
	//	string[] questPaths = questPath.Split(';');
	//	int questInfoCount = 0;
	//	int needLevel = 0;
	//	int needQuest = 0;
	//	int reAble = 0;
	//	System.Int32.TryParse( questPaths[0], out questInfoCount );
	//	System.Int32.TryParse( questPaths[4], out needLevel );
	//	System.Int32.TryParse( questPaths[5], out needQuest );
	//	System.Int32.TryParse( questPaths[6], out reAble );
	//	QuestInfo questInfo = new QuestInfo( questInfoCount, questPaths[1], questPaths[2],questPaths[3],needLevel,needQuest,reAble);

	//	//골 인포
	//	string[] goalPaths = goalPath.Split('*');
	//	questInfo.goalList = GoalInfoMake( goalPaths );

	//	//리워드 인포
	//	string[] rewardPaths = rewardPath.Split('*');
	//	questInfo.rewardList = RewardMake( rewardPaths );

	//	questInfoList.Add( questInfo );
	//}

	//public List<GoalInfo> GoalInfoMake( string[] path = null )
	//{
	//	List<GoalInfo> goalList = new List<GoalInfo>();
	//	for( int i = 0 ; i < path.Length ; ++i )
	//	{
	//		string[] paths = path[i].Split(';');
	//		int targetCount = 0;
	//		System.Int32.TryParse( paths[4], out targetCount );
	//		GoalInfo goalInfo = new GoalInfo(i,paths[0],paths[1],paths[2],paths[3],targetCount);
	//		goalList.Add( goalInfo );
	//	}
	//	return goalList;
	//}

	//public List<Reward> RewardMake( string[] path = null )
	//{
	//	List<Reward> rewardList = new List<Reward>();
	//	for( int i = 0 ; i < path.Length ; ++i )
	//	{
	//		string[] paths = path[i].Split(';');
	//		int targetCount = 0;
	//		System.Int32.TryParse( paths[2], out targetCount );
	//		Reward rewardInfo = new Reward(paths[0],paths[1],targetCount);
	//		rewardList.Add( rewardInfo );
	//	}
	//	return rewardList;
	//}
}