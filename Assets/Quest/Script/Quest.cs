﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestInfo
{
	public int id;
	public int uiId;
	public string questName;
	public string questInfo;
	public string startTalk;
	public string ingTalk;
	public string completeTalk;

	public int reAble;
	public QuestState state;
	public List<GoalInfo> goalList;
	public List<Reward> rewardList;

	public int needLevel;
	public int needQuest;

	public void QuestEnableCheck( )
	{
		if( state == QuestState.disable )
			if( needLevel <= PlayerData.Instance.playerDic["Level"] )
				if( needQuest >= 100 )
					this.state = QuestState.enable;
				else if( PlayerData.Instance.questInfoList[needQuest].state == QuestState.complete )
					QuestStateSet( QuestState.enable );
	}

	public QuestInfo( int id, string questName, string questInfo, string startTalk, string ingTalk, string completeTalk, int needLevel, int needQuest, int reAble )
	{
		uiId = 100;
		this.id = id;
		this.questName = questName;
		this.questInfo = questInfo;
		this.startTalk = startTalk;
		this.ingTalk = ingTalk;
		this.completeTalk = completeTalk;
		this.state = QuestState.disable;
		this.goalList = new List<GoalInfo>( );
		this.rewardList = new List<Reward>( );
		this.needLevel = needLevel;
		this.needQuest = needQuest;
		this.reAble = reAble;
		//Debug.Log( id + " " + this.questName );
	}

	public void QuestStateSet( QuestState state )
	{
		this.state = state;
		switch( state )
		{
		case QuestState.enable:
			break;
		case QuestState.ing:
			break;
		case QuestState.clear:
			Debug.Log( "퀘스트 조건 달성!" );
			break;
		case QuestState.complete:
			if( reAble == 1 )
			{
				foreach( var a in goalList )
					a.GoalSetDefault( );
				this.state = QuestState.enable;
			}
			break;
		}
	}

	public void QuestStart( )
	{
		Debug.Log( "퀘스트 시작!" + uiId );
		foreach( var a in goalList )
			a.GoalSetDefault( );
		QuestStateSet( QuestState.ing );
	}

	public void QuestGiveup( )
	{
		Debug.Log( "퀘스트 포기!" + uiId );
		QuestMgr.Instance.QuestTextRemove( uiId );
		foreach( var a in goalList )
			a.GoalSetDefault( );
		QuestStateSet( QuestState.enable );
	}

	public void QuestComplete( )
	{
		Debug.Log( "퀘스트 완료!" + uiId );
		QuestMgr.Instance.QuestTextRemove( uiId );
		QuestStateSet( QuestState.complete );
	}

	public void isClear( )
	{
		QuestState k = QuestState.clear;
		foreach( var a in goalList )
			if( !isGoalClear( a ) )
				k = QuestState.ing;
		QuestStateSet( k );
	}

	public bool isGoalClear( GoalInfo goal )
	{
		switch( goal.kind )
		{
		case DataKind.kill:
			if( PlayerData.Instance.playerDic[goal.targetKey] - goal.targetCurrCount >= goal.targetCount )
				return true;
			break;
		case DataKind.item:
			if( PlayerData.Instance.playerDic[goal.targetKey] >= goal.targetCount )
				return true;
			break;
		case DataKind.clear:
			if( PlayerData.Instance.playerDic[goal.targetKey] >= goal.targetCount )
				return true;
			break;
		case DataKind.level:
			if( PlayerData.Instance.playerDic[goal.targetKey] >= goal.targetCount )
				return true;
			break;
		default: break;
		}
		return false;
	}
}

public class GoalInfo
{
	public int id;
	public DataKind kind;
	public string goalName;
	public string targetName;
	public string targetKey;
	public int targetCount;
	public int targetCurrCount;

	public void GoalSetDefault( )
	{
		switch( kind )
		{
		case DataKind.kill:
			Debug.Log( targetKey + "확인" );
			targetCurrCount = PlayerData.Instance.playerDic[targetKey];
			break;
		case DataKind.item: break;
		case DataKind.clear: break;
		case DataKind.level: break;
		default: break;
		}
	}

	public GoalInfo( int id, string kind, string goalName, string targetName, string targetKey, int targetCount )
	{
		this.targetCurrCount = 0;

		this.id = id;
		this.goalName = goalName;
		this.targetName = targetName;
		this.targetKey = targetKey;
		this.targetCount = targetCount;

		switch( kind )
		{
		case "kill":
			this.kind = DataKind.kill; break;
		case "item":
			this.kind = DataKind.item; break;
		case "clear":
			this.kind = DataKind.clear; break;
		case "level":
			this.kind = DataKind.level; break;
		case "gold":
			this.kind = DataKind.gold; break;
		case "exp":
			this.kind = DataKind.exp; break;
		case "npc":
			this.kind = DataKind.npc; break;
		default:
			this.kind = DataKind.clear; break;
		}
	}
}

public struct Reward
{
	public string targetName;
	public string targetKey;
	public int targetCount;

	public Reward( string targetName, string targetKey, int targetCount )
	{
		//switch( kind )
		//{
		//case "kill":
		//	this.kind = DataKind.kill; break;
		//case "item":
		//	this.kind = DataKind.item; break;
		//case "clear":
		//	this.kind = DataKind.clear; break;
		//case "level":
		//	this.kind = DataKind.level; break;
		//case "gold":
		//	this.kind = DataKind.gold; break;
		//case "exp":
		//	this.kind = DataKind.exp; break;
		//default:
		//	this.kind = DataKind.clear; break;
		//}
		this.targetName = targetName;
		this.targetKey = targetKey;
		this.targetCount = targetCount;
	}
}

public enum QuestState
{
	disable,
	enable,
	ing,
	clear,
	complete
}

//public class Goal
//{
//	public GoalInfo goalInfo;
//	public int num;
//	public string info;
//	public string targetCount;
//	public int goalCount;

//	public Goal( int num, string info, string targetCount, int goalCount )
//	{
//		this.num = num;
//		this.info = info;
//		this.targetCount = targetCount;
//		this.goalCount = goalCount;
//	}

//	public bool isClear( )
//	{
//		if( PlayerData.Instance.killCount[targetCount] >= goalCount )
//			return true;
//		else
//			return false;
//	}
//}