﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class UIQuestGoalScript : MonoBehaviour
{
	public GoalInfo goal;
	public Text nameT;
	public Text resultT;

	public void UiSetting( GoalInfo goal )
	{
		this.goal = goal;
		nameT.text = goal.goalName;
		TextUpdate( );
	}

	public void TextUpdate( )
	{
		if( goal.kind == DataKind.kill )
		{
			StringBuilder sb = new StringBuilder( );
			sb.Append( PlayerData.Instance.playerDic[goal.targetKey] - goal.targetCurrCount );
			sb.Append( " / " );
			sb.Append( goal.targetCount );
			resultT.text = sb.ToString( );
		}
	}
}