﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQuestScript : MonoBehaviour
{
	public QuestInfo quest;
	public UIQuestGoalScript[] goalScripts;
	public Text QuestNameT;

	public GameObject GoalParent;

	private UIQuestScript( )
	{
		quest = null;
	}

	public void UISetting( QuestInfo quest )
	{
		this.quest = quest;
		QuestNameT.text = quest.questName;
		foreach( var a in goalScripts )
			a.gameObject.SetActive( false );
		for( int i = 0 ; i < quest.goalList.Count ; ++i )
		{
			goalScripts[i].gameObject.SetActive( true );
			goalScripts[i].UiSetting( quest.goalList[i] );
		}
		gameObject.SetActive( true );
	}

	public void TextUpdate( )
	{
		foreach( var a in goalScripts )
			if( a.gameObject.activeSelf )
				a.TextUpdate( );
	}

	public void UIRemove( bool uiIdDefault = true )
	{
		if( uiIdDefault && quest != null )
			quest.uiId = 100;
		quest = null;
		foreach( var a in goalScripts )
			a.gameObject.SetActive( false );
		gameObject.SetActive( false );
	}
}