﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageCtrl : MonoBehaviour
{
	public Image image;
	public void Awake( )
	{
		image = GetComponent<Image>( );
	}
	public IEnumerator CoImageCtrl( float x, float y, float alphaBf, float alphaAf, float time = 0.5f, bool revert = false )
	{
		var cCount = 0f;
		Vector4 cClr = new Vector4( image.color.r, image.color.g, image.color.b, alphaBf);
		var cTran = image.rectTransform.position;

		if( revert )
			cTran = image.rectTransform.position + new Vector3( x, y, 0f );
		else
			image.rectTransform.position += new Vector3( x, y, 0f );
		image.color = cClr;
		while( time > cCount )
		{
			cCount += Time.deltaTime;
			image.rectTransform.position = Vector3.Lerp( image.rectTransform.position, cTran, cCount / time );
			if( revert )
				image.color = Vector4.Lerp( image.color, new Vector4( cClr.x, cClr.y, cClr.z, alphaAf ), cCount / time );
			else
				image.color = Vector4.Lerp( cClr, new Vector4( cClr.x, cClr.y, cClr.z, alphaAf ), cCount / time );
			yield return null;
		}
	}

	public IEnumerator CoImageColor( Vector4 clr, float time = 0.5f )
	{
		var cCount = 0f;
		Vector4 cClr = image.color;
		while( time > cCount )
		{
			cCount += Time.deltaTime;
			image.color = Vector4.Lerp( cClr, clr, cCount / time );
			yield return null;
		}
		yield return 0;
	}
}