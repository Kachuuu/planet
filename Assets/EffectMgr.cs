﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public enum iTweenType
{
	shake,
	move,
	rotate,
	scale,
	punchPos,
	punchRot,
	punchScale
}

public class EffectMgr : MonoBehaviour
{
	public static EffectMgr Instance;
	public Image fadeinout;

	private bool effPlus;
	private float effTime, count;

	private void Awake( )
	{
		if( Instance == null )
			Instance = this;
		effTime = 0f;
	}

	public IEnumerator Fade( bool isIn, int rule = 0, float time = 2f )
	{
		if( isIn )//흰색
		{
			float count = time;
			while( count > 0 )
			{
				fadeinout.color = new Color( 0f, 0f, 0f, count / time );
				count -= Time.fixedDeltaTime;
				yield return 0;
			}
			fadeinout.color = new Color( 0f, 0f, 0f, 0f );
			fadeinout.gameObject.SetActive( false );
		}
		else if( !isIn )//검은색
		{
			fadeinout.gameObject.SetActive( true );
			float count = 0f;
			while( count < time )
			{
				fadeinout.color = new Color( 0f, 0f, 0f, count / time );
				count += Time.fixedDeltaTime;
				yield return 0;
			}
			fadeinout.color = new Color( 0f, 0f, 0f, 1f );
		}
		yield return 0;
	}

	public IEnumerator FadeIn( Image image, float fadeTime )
	{
		float count = 0f;
		image.color = new Vector4( 1f, 1f, 1f, 1f );
		Color clr = image.color;
		while( fadeTime > count )
		{
			count += Time.deltaTime;
			clr.a -= Time.deltaTime / fadeTime;
			image.color = clr;
			yield return null;
		}
		clr.a = 0f;
		image.color = clr;
	}

	public IEnumerator FadeOut( Image image, float fadeTime )
	{
		float count = 0f;
		image.color = new Vector4( 1f, 1f, 1f, 0f );

		Color clr = image.color;
		clr.a = 0f;
		image.color = clr;
		while( fadeTime > count )
		{
			count += Time.deltaTime;
			clr.a += Time.deltaTime / fadeTime;
			image.color = clr;
			yield return null;
		}
		clr.a = 1f;
		image.color = clr;
	}

	/*
     public IEnumerator Fade( string inout, int rule = 0, float time = 2f )
     {
         if ( inout == "in" )
         {
             screentrans.maskValue = 0;
             screentrans.maskInvert = true;
         }
         else if ( inout == "out" )
         {
             screentrans.maskValue = 0;
             screentrans.maskInvert = false;
         }

         screentrans.maskTexture = tex[rule];

         float count = 0f;
         while ( count < time / 1.5f )
         {
             //Debug.Log( screentrans.maskValue );
             screentrans.maskValue = count / time;
             count += Time.fixedDeltaTime;
             yield return 0;
         }
         while ( count < time )
         {
             //Debug.Log( screentrans.maskValue );
             screentrans.maskValue = count / time;
             count += Time.fixedDeltaTime * 1.3f;
             yield return 0;
         }
         screentrans.maskValue = 1f;
         yield return 0;
     } */

	public IEnumerator iTweenCtrl( GameObject target, iTweenType iType,
		float x, float y, float z, float time, float delay = 0f, string easeType = "", bool islocal = true )
	{
		if( iType == iTweenType.rotate )
		{
			x /= 360f; y /= 360f; z /= 360f;
			Debug.Log( x );
			Debug.Log( y );
			Debug.Log( z );
		}
		else if( iType == iTweenType.punchRot )
		{
		}

		if( target.GetComponent<RectTransform>( ) && iType != iTweenType.scale && iType != iTweenType.punchScale )
		{
			x *= 0.01667f; y *= 0.01667f; z *= 0.01667f;
		}
		var hash = iTween.Hash( "x", x, "y", y, "z", z, "time", time, "delay", delay, "islocal", true );
		switch( easeType )
		{
		case "": break;
		default:
			hash = iTween.Hash( "x", x, "y", y, "z", z, "time", time, "easeType", easeType, "delay", delay, "islocal", true ); break;
		}
		yield return StartCoroutine( PlayingEffect( target, iType, hash ) );
	}

	IEnumerator PlayingEffect( GameObject target, iTweenType iType, Hashtable hash )
	{
		yield return 0;
		switch( iType )
		{
		case iTweenType.shake:
			iTween.ShakePosition( target, hash ); break;
		case iTweenType.move:
			iTween.MoveBy( target, hash ); break;
		case iTweenType.rotate:
			iTween.RotateBy( target, hash ); break;
		case iTweenType.scale:
			iTween.ScaleTo( target, hash ); break;
		case iTweenType.punchPos:
			iTween.PunchPosition( target, hash ); break;
		case iTweenType.punchRot:
			iTween.PunchRotation( target, hash ); break;
		case iTweenType.punchScale:
			iTween.PunchScale( target, hash ); break;
		}
	}

	IEnumerator TimeCheck( )
	{
		while( effTime > -0.05f )
		{
			effTime -= Time.fixedDeltaTime;
			yield return 0;
		}
	}
}