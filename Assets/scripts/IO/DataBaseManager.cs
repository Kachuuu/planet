﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBaseManager : MonoBehaviour
{
	public static DataBaseManager Instance;
	public AudioClip[] audios;
	public AudioClip[] bgms;

	public void Awake( )
	{
		if( Instance == null )
			Instance = this;
	}

	#region Stat 

	/// <summary>
	/// Database Structrue
	/// 
	///			A   B     C        ...
	///  1  - id, atk, moveSpeed, ....	- 목차(순서는 CharacterStat과 동일하다 )
	///  n  - 0 , 10 , 13       , ....	- 항목
	/// ... - ...
	/// 
	/// </summary>

	public Dictionary<int, float[]> statDatas = null;

	public void InitStat( )
	{
		CSVReader file = new CSVReader( PlanetUtil.statsDataFileName );
		statDatas = new Dictionary<int, float[]>( );


			for( int r = PlanetUtil.statsStartLine ; r < file.GetRow( ) ; ++r )
			{
				try
				{
					float[] dummy = new float[(int)PlanetUtil.CharacterStat.None];

					var id = Convert.ToInt32( file[0, r] );
					for( int c = 1 ; c < file.GetColumn( r ) ; ++c )
						dummy[c - 1] = Convert.ToSingle( file[c, r] );
					statDatas[id] = dummy;
				}
				catch
				{
				break;
				}
			}
	}

	public Stat[] GetStats( int id )
	{
		if( statDatas == null )
			InitStat( );

		float[] dummy;
		if( statDatas.TryGetValue( id, out dummy ) )
		{
			Stat[] result = new Stat[(int)PlanetUtil.CharacterStat.None];
			for( int i = 0 ; i < result.Length ; ++i )
				result[i] = new Stat( );

			for( int j = 0 ; j < dummy.Length ; ++j )
			{
				result[j].type = (PlanetUtil.CharacterStat)j;
				result[j].def = dummy[j];
			}

			return result;
		}

		Debug.LogError( id + "에 해당하는 Stat이 존재하지 않습니다." );
		return null;
	}
	#endregion

	#region Sprite

	/// <summary>
	/// Database Struct
	/// 
	///			A   B		C
	/// 1 - id, path	index	- 목차(path는 이미지 파일의 경로 명, index는 아틀라스의 스프라이트 인덱스)
	/// n - 0 , aaa.png	 0		- 항목
	/// ...  - ...
	/// 
	/// </summary>

	public struct SpriteDataInfo
	{
		public string path;
		public int index;
	}

	public Dictionary<int, SpriteDataInfo> spriteDatas = null;
	public Dictionary<string, Sprite[]> spritePathList = null;
	public Dictionary<int, Sprite> sprites = null;

	public void InitSprite( )
	{
		CSVReader file = new CSVReader( PlanetUtil.spriteDataFileName );
		spriteDatas = new Dictionary<int, SpriteDataInfo>( );
		spritePathList = new Dictionary<string, Sprite[]>( );
		sprites = new Dictionary<int, Sprite>( );

		for( int r = PlanetUtil.statsStartLine ; r < file.GetRow( ) ; ++r )
		{
			try
			{
				var id = Convert.ToInt32( file[0, r] );
				var data = new SpriteDataInfo()
				{
					path = file[1, r],
					index = Convert.ToInt32( file[2, r] )
				};
				spriteDatas[id] = data;
			}
			catch
			{
				break;
			}
		}
	}


	public Sprite GetSprite( int id ) // 기능단위로 쪼갤것
	{
		if( spriteDatas == null )
			InitSprite( );

		//return sprites[0]; // Todo 임시임. 반드시 삭제

		Sprite result;
		if( sprites.TryGetValue( id, out result ) )
			return result;

		SpriteDataInfo info;
		if( spriteDatas.TryGetValue( id, out info ) )
		{
			Sprite[] sps;
			if( spritePathList.TryGetValue( info.path, out sps ) )
			{
				if( sps.Length <= info.index )
				{
					Debug.LogError( info.path + "의 스프라이트는 " + info.index + "보다 적습니다." );
					return null;
				}
				sprites[id] = sps[info.index];
				return sprites[id];
			}

			sps = Resources.LoadAll<Sprite>( info.path );
			spritePathList[info.path] = sps;
			if( sps.Length <= info.index )
			{
				Debug.LogError( info.path + "의 스프라이트는 " + info.index + "보다 적습니다." );
				return null;
			}
			sprites[id] = sps[info.index];

			return sprites[id];
		}

		Debug.LogError( id + "에 해당하는 sprite 존재하지 않습니다." );
		return null;
	}

	#endregion

	#region Item

	/// <summary>
	/// Database Structrue
	/// 
	///			A   B        C       D	   E		  F		G		H		I		J			K	 ,  L      , ...
	///  1  - id, type,    subtype, name, Level, spriteID, disc, defPfmCnt,  pfm1.type, pfm1.add, pfm1.scale, ....	- 목차( pfmCnt만큼 반복 )
	///  n  - 0 ,"Weapon", "HandGun","a", 5,        4	,"설명",	 2	,    "Attack" ,	20.0	 , 1.0  , ....	- 항목
	/// ... - ...
	/// 
	/// </summary>

	public Dictionary<int, Item> itemDatas = null;

	public void InitItem( )
	{
		CSVReader file = new CSVReader( PlanetUtil.itemDataFileName );
		itemDatas = new Dictionary<int, Item>( );

		for( int r = PlanetUtil.itemStartLine ; r < file.GetRow( ) ; ++r )
		{
			try
			{ 
				var id = Convert.ToInt32( file[0, r] );
				Item dummy = GetItem( file[1, r], file[2, r] );
				dummy.name = file[3, r];
				dummy.useableLevel = Convert.ToInt32( file[4, r] );
				dummy.spriteid = Convert.ToInt32( file[5, r] );
				dummy.disc = file[6, r];
				dummy.pfm = GetPerformence( file, 7, r );
				//dummy.rate = (PlanetUtil.ItemRate)UnityEngine.Random.Range( 0, (int)PlanetUtil.ItemRate.None );

				itemDatas[id] = dummy;
			}
			catch
			{
				break;
			}
		}
	}

	Item GetItem( string _type, string _subType )
	{
		Item item;
		switch( _type )
		{
		case "Weapon":
			item = GetWeapon( _subType );
			item.maxCount = 1;
			return item;
		case "Armor":
			item = new Armor( );
			item.maxCount = 1;
			return item;
		case "Raw":
			item = new DummyItem( );
			item.maxCount = 99;
			return item;
		}
		Debug.LogError( _type + " 존재하지 않는 아이템 타입입니다." );
		return null;
	}

	Item GetWeapon( string _subType )
	{
		switch( _subType )
		{
		case "Handgun": return new Handgun( );
		case "Sniper": return new Riple( );
		case "Shotgun": return new Shotgun( );
		case "HavyMachinegun": return new SCV( );
		}
		Debug.Log( _subType + " 존재하지 않는 무기 타입입니다." );
		return null;
	}

	Performence[] GetPerformence( CSVReader file, int c, int r )
	{
		int cnt = Convert.ToInt32( file[c++,r] );
		List<Performence> dummy = new List<Performence>();
		for( int i = 0 ; i < cnt ; ++i )
		{
			var data = new Performence( )
			{
				type = GetCharacterStat( file[c + ( i * 3 ) + 0, r] ),
				add = Convert.ToSingle( file[c + ( i * 3 ) + 1, r] ),
				scale = Convert.ToSingle( file[c + ( i * 3 ) + 2, r] )
			};
			if( data.add != 0 || data.scale != 0 )
				dummy.Add( data );
		}
		return dummy.ToArray( );
	}

	PlanetUtil.CharacterStat GetCharacterStat( string _name )
	{
		switch( _name )
		{
		case "Level": return PlanetUtil.CharacterStat.Level;
		case "MaxHp": return PlanetUtil.CharacterStat.MaxHp;
		case "Attack": return PlanetUtil.CharacterStat.Attack;
		case "Armor": return PlanetUtil.CharacterStat.Armor;
		case "CriticalRate": return PlanetUtil.CharacterStat.CriticalRate;
		case "Avoid": return PlanetUtil.CharacterStat.Avoid;
		case "Acc": return PlanetUtil.CharacterStat.Acc;
		case "ReHitTime": return PlanetUtil.CharacterStat.ReHitTime;
		case "AttackPrevDelay": return PlanetUtil.CharacterStat.AttackPrevDelay;
		case "AttackDelay": return PlanetUtil.CharacterStat.AttackDelay;
		case "AttackPostDelay": return PlanetUtil.CharacterStat.AttackPostDelay;
		case "DashCount": return PlanetUtil.CharacterStat.DashCount;
		case "DashSpeed": return PlanetUtil.CharacterStat.DashSpeed;
		case "DashOpTime": return PlanetUtil.CharacterStat.DashOpTime;
		case "DashTime": return PlanetUtil.CharacterStat.DashTime;
		case "MoveSpeed": return PlanetUtil.CharacterStat.MoveSpeed;
		case "MoveRotate": return PlanetUtil.CharacterStat.MoveRotate;
		case "JumpCount": return PlanetUtil.CharacterStat.JumpCount;
		case "JumpPower": return PlanetUtil.CharacterStat.JumpPower;
		case "SecJumpPower": return PlanetUtil.CharacterStat.SecJumpPower;
		case "MaxBullet": return PlanetUtil.CharacterStat.MaxBullet;
		case "MaxMagazine": return PlanetUtil.CharacterStat.MaxMagazine;
		case "ReloadCnt": return PlanetUtil.CharacterStat.ReloadCnt;
		case "ReloadDelay": return PlanetUtil.CharacterStat.ReloadDelay;
		case "BulletRange": return PlanetUtil.CharacterStat.BulletRange;
		case "Penetrate": return PlanetUtil.CharacterStat.Penetrate;
		default:
			Debug.LogError( "존재하지 않는 Performence입니다. " + _name );
			return PlanetUtil.CharacterStat.None;
		}
	}

	public Item GetItem( int id )
	{
		if( itemDatas == null )
			InitItem( );

		Item item;
		if( itemDatas.TryGetValue( id, out item ) )
		{
			Item dummy = item.Clone();
			var r = UnityEngine.Random.value;
			if( r < 0.4f )
				dummy.rate = PlanetUtil.ItemRate.Normal;
			else if( r < 0.7f )
				dummy.rate = PlanetUtil.ItemRate.Rare;
			else if( r < 0.9f )
				dummy.rate = PlanetUtil.ItemRate.Epic;
			else 
				dummy.rate = PlanetUtil.ItemRate.Legendary;
			if( dummy is EquipItem )
				SetAdditialOption( (EquipItem)dummy );
			return dummy;
		}

		//Debug.Log( id + "의 아이템 못찾음" );
		return null;
	}


	#endregion

	#region Item Additial Option

	/// <summary>
	/// Database Structrue
	/// 
	///			A   B        C    D
	///  1  - id, type,    add, scale	- 목차( pfmCnt만큼 반복 )
	///  n  - 0 ,"Attack", 0,"	1.5		- 항목
	/// ... - ...
	/// 
	/// </summary>

	private List<Performence> optionDatas = null;

	public void InitOption( )
	{
		CSVReader file = new CSVReader( PlanetUtil.itemOptionDataFileName );
		optionDatas = new List<Performence>( );

		for( int r = PlanetUtil.itemOptionStartLine ; r < file.GetRow( ) ; ++r )
		{
			try
			{
				//var id = Convert.ToInt32( file[0, r] );
				var pfm = new Performence()
				{
					type =  GetCharacterStat( file[1, r] ),
					add = Convert.ToSingle(file[2, r] ),
					scale = Convert.ToSingle(file[3, r] ),
				};
				//optionDatas[id] = pfm;
				optionDatas.Add( pfm );
			}
			catch
			{
				break;
			}
		}
	}

	void SetAdditialOption( EquipItem _item )
	{
		if( null == optionDatas )
			InitOption( );

		var rate = (int)_item.rate;
		if( rate == 0 )
			return;

		Performence[] pfm = new Performence[rate];

		for( int i = 0 ; i < rate ; ++i )
			pfm[i] = optionDatas[UnityEngine.Random.Range( 0, optionDatas.Count )].Clone( );

		_item.addPfm = pfm;
		return;
	}

	#endregion

	#region RewardItemList  // 리워드 공식 임시로 작업함

	/// <summary>
	/// Database Structrue
	/// 
	///			A   B        C			 D		 E		  F	    , ...
	///  1  - id, Mineral, RewardCnt,  ListCnt,	(id,    Percent), 반복...	- 목차( pfmCnt만큼 반복 )
	///  n  - 0 ,, 5,			4	,	3	  ,  601,	0.3		,	 ....	- 항목
	/// ... - ...
	/// 
	/// </summary>

	public struct RewardItem
	{
		public int id;
		public float percent;

		public RewardItem( int id, float percent )
		{
			this.id = id;
			this.percent = percent;
		}
	}
	public struct RewardData
	{
		public int id;
		public int Mineral;
		public RewardItem[] items;

		public RewardData( int id, int mineral, int rewardCnt, RewardItem[] items )
		{
			this.id = id;
			Mineral = mineral;

			if( items != null )
			{
				this.items = new RewardItem[items.Length];
				for( int i = 0 ; i < items.Length ; ++i )
					items[i] = items[i];
			}
			else
			{ this.items = null; }
		}
	}

	public Dictionary<int, RewardData> rewardlistDatas = null;

	public void InitRewardList( )
	{
		CSVReader file = new CSVReader( PlanetUtil.rewardListDataFileName );
		rewardlistDatas = new Dictionary<int, RewardData>( );

		for( int r = PlanetUtil.rewardListStartLine ; r < file.GetRow( ) ; ++r )
		{
			try
			{
				RewardData dummy;
				dummy.id = Convert.ToInt32( file[0, r] );
				dummy.Mineral = Convert.ToInt32( file[2, r] );
				dummy.items = new RewardItem[Convert.ToInt32( file[3, r] )];
				for( int i = 0 ; i < dummy.items.Length ; ++i )
				{
					var id = Convert.ToInt32( file[4 + (i*2) + 0, r] );
					var pnt = Convert.ToSingle( file[4 + (i*2) + 1, r] );
					dummy.items[i] = new RewardItem( id, pnt );
				}

				rewardlistDatas[dummy.id] = dummy;
			}
			catch
			{
				break;
			}
		}
	}

	public Item[] GetDropItems( int id, out int _mineral )
	{
		if( rewardlistDatas == null )
			InitRewardList( );

		RewardData data;
		if( rewardlistDatas.TryGetValue( id, out data ) )
		{
			_mineral = data.Mineral;
			List<Item> items = new List<Item>();

			var max = CalcRewardMaxRand(data);
			//for( int i = 0 ; i < data.RewardCnt ; ++i )
			{
				var dummy = GetRandomItem( data, max );
				if( dummy != null )
					items.Add( dummy );
			}

			return items.ToArray( );
		}

		Debug.Log( "그런 보상테이블이 없다." + id );
		_mineral = 0;
		return null;
	}
	float CalcRewardMaxRand( RewardData _data )
	{
		var percent = 0f;
		for( int i = 0 ; i < _data.items.Length ; ++i )
			percent += _data.items[i].percent;

		return percent;
	}

	Item GetRandomItem( RewardData _data, float _max )
	{
		var rand = UnityEngine.Random.Range(0f, _max );

		for( int i = 0 ; i < _data.items.Length - 1 ; ++i )
		{
			if( rand < _data.items[i].percent )
				return GetItem( _data.items[i].id );
			else
				rand -= _data.items[i].percent;
		}
		return GetItem( _data.items[_data.items.Length - 1].id );
	}

	#endregion

	#region Quest

	/// <summary>
	/// Database Structrue
	/// 
	///			A   B     C		D	 E		F		G
	///  1  - id, name, info, talk, level, proced, reAble	- 목차
	///  n  - 0 , "a" , "asd", asd", 5	 ,  -1	,	1		- 항목
	/// ... - ...
	/// 
	/// </summary>

	public Dictionary<int, QuestInfo> questDatas = null;

	public void InitQuset( )
	{
		CSVReader file = new CSVReader( PlanetUtil.QuestDataFileName );
		questDatas = new Dictionary<int, QuestInfo>( );

		for( int r = PlanetUtil.questStartLine ; r < file.GetRow( ) ; ++r )
		{
			try
			{
				//퀘스트
				int     id      = Convert.ToInt32( file[0, r] );
				string  name    = file[1, r];
				string  info    = file[2, r];
				string  talk1    = file[3, r];
				string  talk2    = file[4, r];
				string  talk3    = file[5, r];
				int     level   = Convert.ToInt32( file[6, r] );
				int     preced  = Convert.ToInt32( file[7, r] );
				int     reAble  = Convert.ToInt32( file[8, r] );
				QuestInfo dummy = new QuestInfo(id, name, info, talk1,talk2,talk3, level, preced, reAble);

				//목표
				string  gkind   = file[9,r];
				string  ginfo   = file[10,r];
				string  gname   = file[11,r];
				string  gkeyid  = file[12,r];
				int     gcount  = Convert.ToInt32( file[13, r] );
				GoalInfo g      = new GoalInfo(0,gkind,ginfo,gname,gkeyid,gcount);

				//보상
				int     rgold   = Convert.ToInt32( file[14, r] );
				int     rexp    = Convert.ToInt32( file[15, r] );

				Reward  r1      = new Reward();
				r1.targetKey = "Gold";
				r1.targetName = "골드";
				r1.targetCount = rgold;

				Reward  r2      = new Reward();
				r2.targetKey = "Exp";
				r2.targetName = "경험치";
				r2.targetCount = rexp;

				dummy.goalList.Add( g );
				dummy.rewardList.Add( r1 );
				dummy.rewardList.Add( r2 );
				questDatas[id] = dummy;
			}
			catch
			{
				break;
			}
		}
	}

	public QuestInfo GetQuest( int id )
	{
		if( questDatas == null )
			InitQuset( );

		QuestInfo dummy;
		if( questDatas.TryGetValue( id, out dummy ) )
		{
			return dummy;
		}

		Debug.LogError( id + "에 해당하는 Stat이 존재하지 않습니다." );
		return null;
	}
	#endregion

	#region PlayerData // 작성 중
	#endregion

	#region Buff // 작성중

	/// <summary>
	/// Database Structrue
	/// 
	///			A   B		C          D		E		F	  G		  F			H	  I...
	///  1  - id, type,		spriteID, DataCnt,PfmCnt, Data1, Data2, 데이터타입, Add, Scale  ....	- 목차(순서는 CharacterStat과 동일하다 )
	///  n  - 0 , "Passive" ,	13  ,  2	 ,	2	,  1.0,	1.0,	Attack,		0,  1.1     , ....	- 항목
	/// ... - ...
	/// 
	/// </summary>

	public Dictionary<int, Buff> buffDatas = null;

	public void InitBuff( )
	{
		CSVReader file = new CSVReader( PlanetUtil.buffDataFileName );
		buffDatas = new Dictionary<int, Buff>( );

		for( int r = PlanetUtil.buffStartLine ; r < file.GetRow( ) ; ++r )
		{
			//try
			//{
				var id = Convert.ToInt32( file[0, r] );
				Buff dummy = GetBuffType( file[1, r] );
				dummy.id = id;
				dummy.spriteID = Convert.ToInt32( file[2, r] );
				dummy.datas = new float[Convert.ToInt32( file[3, r] )];
				dummy.buff = new Performence[Convert.ToInt32( file[4, r] )];
				for( int i = 0 ; i < dummy.datas.Length ; ++i )
				{
					dummy.datas[i] = Convert.ToSingle( file[5+i, r] );
				}

				int n = 5 + dummy.datas.Length;
				for( int i = 0 ; i < dummy.buff.Length ; ++i )
				{
					var data = new Performence( )
					{
						type = GetCharacterStat( file[ n+(i*3)+0, r] ),
						add = Convert.ToSingle( file[ n+(i*3)+1, r] ),
						scale = Convert.ToSingle( file[ n+(i*3)+2, r] )
					};
					if( data.add != 0 || data.scale != 0 )
						dummy.buff[i] = data;
				}
				buffDatas[id] = dummy;
			//}
			//catch
			//{
			//	break;
			//}
			
		}
	}

	Buff GetBuffType( string _type )
	{
		switch( _type )
		{
		case "Pasive":		return new PassiveBuff( );
		case "TimeLimit":	return new TimeLimitBuff( );
		case "CountLimit":	return new CountLimitBuff( );
		case "Accumulate":	return new AccumulateBuff( );
		case "Tick":		return new TickBuff( );
		case "Heal":		return new HealBuff( );
		}
		Debug.LogError( _type + "이라는 버프는 존재하지 않는 타입입니다." );
		return null;
	}

	public Buff GetBuff( int id )
	{
		if( buffDatas == null )
			InitBuff( );

		Buff dummy;
		if( buffDatas.TryGetValue( id, out dummy ) )
		{
			return dummy;
		}

		Debug.LogError( id + "에 해당하는 Stat이 존재하지 않습니다." );
		return null;
	}
	#endregion
}
