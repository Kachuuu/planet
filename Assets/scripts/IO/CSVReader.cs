﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class CSVReader
{
	string[][] data;
	public string this[int x, int y]
	{ get { return data[y][x]; } } // 행을 먼저 가져오기때문에 x, y가 뒤집힌다.

	// 해당 행의 컬럼 수
	public int GetColumn(int r)
	{
		return data[r].Length;
	}

	// 총 행 수
	public int GetRow()
	{
		return data.Length;
	}

	// 생성자
	public CSVReader( string _file )
	{
		Init( _file );
	}

	// 초기화
	// 특수 문자 처리 및 분할
	private void Init( string _file )
	{
		string[] stt = new string[]{ "\r\n" };
		string txt = Resources.Load<TextAsset>( _file ).text;

		StringBuilder sb = new StringBuilder();
		bool s = false;
		int oldIndex = 0;
		for(int i = 0 ; i< txt.Length ; ++i )
		{
			if( txt[i] == '\"' )
				s = !s;

			if( s && txt[i] == '\r' )
			{
				sb.Append( txt.Substring( oldIndex, i - oldIndex ) );
				++i;
				oldIndex = i;
			}
		}
		sb.Append( txt.Substring( oldIndex, txt.Length - 1 - oldIndex ) );

		string[] str = sb.ToString().Split(stt,System.StringSplitOptions.None);
		//string[] str = _file.Split(stt,System.StringSplitOptions.RemoveEmptyEntries);

		data = new string[str.Length][];

		for( int i = 0 ; i < str.Length ; ++i )
		{
			int pos = 0;
			bool openComma = false;
			List<int> positions = new List<int>();

			positions.Add( -1 );

			while( pos < str[i].Length )
			{
				if( openComma == false )
				{
					if( str[i][pos] == ',' )
						positions.Add( pos );
					else if( str[i][pos] == '"' )
						openComma = true;
				}
				else if( str[i][pos] == '"' )
				{
					if( str[i][pos + 1] == '"' )
						++pos;
					else
						openComma = false;
				}
				++pos;
			}
			positions.Add( str[i].Length );

			data[i] = new string[positions.Count - 1];
			for( int j = 0 ; j < positions.Count - 1 ; ++j )
				data[i][j] = str[i].Substring( positions[j] + 1, ( positions[j + 1] - 1 ) - ( positions[j] ) );
		}

		//for( int i = 0 ; i < data.Length ; ++i )
		//{
		//	string ss = data[i][0];
		//	for( int j = 1 ; j < data[i].Length ; ++j )
		//	{
		//		ss += "," + data[i][j];
		//	}
		//	Debug.Log( ss );
		//}
	}
}
