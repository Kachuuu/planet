﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CommandButton : Button
{
	public PlayerInputScript.Inputs input;
	[HideInInspector]
	public PlayerInputScript.KeyMapping keymapping;

	[HideInInspector]
	public int touchID;

	public override void OnPointerDown( PointerEventData eventData )
	{
		base.OnPointerDown( eventData );
		// 버튼 터치 중인 터치 인덱스 배제를 위해
#if UNITY_ANDROID
		touchID = PlayerInputScript.GetNowTouchID( eventData.position );
		if ( input == PlayerInputScript.Inputs.Move )
			PlayerInputScript.AddExclude( touchID );
#endif

		if( keymapping != null )
			keymapping.OnDown( );
	}

	private void LateUpdate( )
	{
		if( keymapping != null )
			keymapping.OffToggle( );
	}

	public override void OnPointerUp( PointerEventData eventData )
	{
		base.OnPointerUp( eventData );

		// 배제된 인덱스 삭제
#if UNITY_ANDROID
		PlayerInputScript.RemoveExclude( touchID );
#endif
		if( keymapping != null )
			keymapping.OnUp( );
	}
}
