﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StatusScript : MonoBehaviour
{
	public ControlScript   cs;
	[HideInInspector]
	public SpriteRenderer   sr;
	[HideInInspector]
	public Animator         am;
	[HideInInspector]
	public AudioSource      ado;
	public Rigidbody2D      rb;
	public Transform        shotPos;
	public Transform        centerPos;
	public WeaponSwapEffectScript swapEffect;

	[Header( "Info" )]
	public int id;
	public new string name;
	public PlanetUtil.CharacterTypes type;
	public GameObject listStandingBlock;
	public Collider2D col;
	public GameObject Rf_Target;
	public GameObject Hg_Target;
	public Transform Scv_Target;


	[Header("EquipItem")]
	private EquipItem[] items = new EquipItem[(int)PlanetUtil.ItemType.Consume];
	public int[] nowBullet = new int[(int)PlanetUtil.ItemType.Armor];
	public int[] nowMagazine = new int[(int)PlanetUtil.ItemType.Armor];
	public int nowWeapon = 0;
	public PlanetUtil.WeaponType nowWeaponType;

	[Header("Skills")]
	public List<PassiveSkill>[] passiveSkills = new List<PassiveSkill>[(int)PlanetUtil.WeaponType.None];
	public ActiveSkill[,] playerActiveSkill = new ActiveSkill[(int)PlanetUtil.WeaponType.None,2];
	//public Skill[,] skills = new Skill[PlanetUtil.WeaponType.None, 3](); // 3개??? 2개???
	//public Skill[PlanetUtil.WeaponType _type,int n] Skills
	//{
	//	get { return skills[(int)_type, n]; }
	//	set { skills[(int)_type, n] = value; }
	//}

	[Header("Buff/Debuff")]
	[SerializeField]
	public List<Buff> buffList = new List<Buff>();


	[Header( "Status" )]
	public Stat[] stats;
	public float[] status = new float[(int)PlanetUtil.CharacterStatus.None];
	//public int nowHp=1; // stats[(int)Stats.MaxHp]을 사용한다.
	//public int GurugiCnt; // stats[(int)Stats.GurugiCount]을 사용한다.
	//public int JumpCnt; // stats[(int)Stats.JumpCount]을 사용한다.
	//public int AttackCombo = 0;
	//public int RemainBullet; // 남은 총알
	public Vector2 velocity;
	public Vector2 velocityAcc;

	public bool nowStanding = false; // 땅에 서 있는가?
	public bool NowStanding
	{
		get { return nowStanding; }
		set
		{
			//if( type == PlanetUtil.CharacterTypes.bossmonster )
			//Debug.Log( value );
			nowStanding = value;
		}
	}
	public bool lookLeft = false;
	public bool bHit = false; // 데미지를 받으면 활성화. Hit 동작이 끝나면 false
	public bool bAttack; // BT와 공격에서 제어

	public Coroutine coSkill = null;
	public Coroutine coAttack = null;
	public Coroutine coReload = null;
	public Coroutine coStuck = null;

	public float this[PlanetUtil.CharacterStat _type]
	{
		get
		{
			return stats[(int)_type].final;
		}
	}

	public float HpRate { get { return status[(int)PlanetUtil.CharacterStatus.NowHp] / stats[(int)PlanetUtil.CharacterStat.MaxHp].final; } }

	[HideInInspector]
	internal Statable[] statable = new Statable[(int)PlanetUtil.CharacterStatables.None];
	[BitMask(typeof(PlanetUtil.CharacterState))]
	public PlanetUtil.CharacterState state = PlanetUtil.CharacterState.Idle;
	public PlanetUtil.CharacterState State
	{
		get { return state; }
		set
		{
			state = value;
			//if( name == "Player" )
			//	Debug.Log( state );
			switch( state )
			{
			case PlanetUtil.CharacterState.Idle:
				break;
			case PlanetUtil.CharacterState.Move:
				break;
			case PlanetUtil.CharacterState.Dash:
				break;
			case PlanetUtil.CharacterState.Attack:
				break;
			case PlanetUtil.CharacterState.Jump:
				break;
			case PlanetUtil.CharacterState.Reload:
				break;
			}
		}
	}
	public int StateInt
	{
		get { return (int)State; }
		set { State = (PlanetUtil.CharacterState)value; }
	}


	//////////////////////////Methods//////////////////////

	public void Init( Stat[] _stats )
	{
		stats = new Stat[_stats.Length];
		for( int i = 0 ; i < stats.Length ; ++i )
			stats[i] = _stats[i].Clone( );
		Init( );
	}

	/*public virtual void Init( )
	{
		if( type == PlanetUtil.CharacterTypes.player )
			CalcLevelData( );

		CalcStat( );
		StatReset( );
		StatusReset( );
		ResetBullet( );
		if( name == "Player" )
			UpdateBulletUI( );

		// 버프 클리어
		buffList.Clear( );

		state = PlanetUtil.CharacterState.Idle;
	}*/

	public virtual void Init( )
	{
		CalcStat( );
		StatusReset( );
		ResetBullet( );
		if( type == PlanetUtil.CharacterTypes.player )
		{
			CalcLevelData( );
			UpdateBulletUI( );
			UpdateExpUI( );
		}
		CalcStat( );
		StatReset( );
		
		


		// 버프 클리어
		buffList.Clear( );

		state = PlanetUtil.CharacterState.Idle;
	}

	void CalcLevelData( )
	{
		int lvl = (int)this[PlanetUtil.CharacterStat.Level];
		stats[(int)PlanetUtil.CharacterStat.MaxHp].def = 200 * Mathf.Pow( 1.2f, lvl );
		stats[(int)PlanetUtil.CharacterStat.Attack].def = 35 + lvl * 5;
		stats[(int)PlanetUtil.CharacterStat.CriticalRate].def = lvl * 0.02f;
		var exp = 100;
		var f = 100;
		for( int i = 1 ; i < lvl ; ++i )
		{
			exp += f;
			f += 100;
		}
		stats[(int)PlanetUtil.CharacterStat.MaxExp].def = exp;
	}

	public void StatReset( )
	{
		status[(int)PlanetUtil.CharacterStatus.NowHp] = (int)stats[(int)PlanetUtil.CharacterStat.MaxHp].final;
		status[(int)PlanetUtil.CharacterStatus.NowGurugiableCount] = (int)stats[(int)PlanetUtil.CharacterStat.DashCount].final;
		status[(int)PlanetUtil.CharacterStatus.NowJumpableCount] = (int)stats[(int)PlanetUtil.CharacterStat.JumpCount].final;
	}

	public void StatusReset( )
	{
		if( statable[0] == null )
			for( int i = 0 ; i < statable.Length ; ++i )
				statable[i] = new Statable( );

		for( int i = 0 ; i < statable.Length ; ++i )
			statable[i].Reset( true );
	}

	public void ResetBullet( )
	{
		for( int i = 0 ; i < (int)PlanetUtil.ItemType.Armor ; ++i )
			DeApplyItemPower( items[i] );

		for( int i = 0 ; i < (int)PlanetUtil.ItemType.Armor ; ++i )
		{
			ApplyItemPower( items[i] );
			CalcStat( );
			nowBullet[i] = (int)this[PlanetUtil.CharacterStat.MaxBullet];
			nowMagazine[i] = (int)this[PlanetUtil.CharacterStat.MaxMagazine];
			DeApplyItemPower( items[i] );
		}

		ApplyItemPower( items[nowWeapon] );
		CalcStat( );
	}

	public void UpdateBulletUI( bool _Immediate = false )
	{
		var percent = nowBullet[nowWeapon] / this[PlanetUtil.CharacterStat.MaxBullet];
		GameMgr.Instance.uiMgr.bltBar.SetParam( percent, nowBullet[nowWeapon], nowMagazine[nowWeapon], _Immediate );
	}

	public void UpdateExpUI( bool _Immediate = false )
	{
		var percent = status[(int)PlanetUtil.CharacterStatus.NowExp] / this[PlanetUtil.CharacterStat.MaxExp];
		GameMgr.Instance.uiMgr.expBar.SetParam( percent, (int)status[(int)PlanetUtil.CharacterStatus.NowExp], (int)this[PlanetUtil.CharacterStat.MaxExp], _Immediate );
	}

	private void Awake( )
	{
		cs = GetComponent<ControlScript>( );
		ado = GetComponent<AudioSource>( );
		sr = transform.GetComponentInChildren<SpriteRenderer>( );
		am = transform.GetComponentInChildren<Animator>( );
		rb = GetComponent<Rigidbody2D>( );

		for( int i = 0 ; i < passiveSkills.Length ; ++i )
			passiveSkills[i] = new List<PassiveSkill>( );

		for( int i = 0 ; i < statable.Length ; ++i )
			statable[i] = new Statable( );
	}

	private void Start( )
	{
		// 임시
		for( int i = 0 ; i < buffList.Count ; ++i )
			buffList[i].Init( this );

		//CalcStat( );
		StatReset( );
	}

	public void CalcStat( )
	{
		var oldHpRate = HpRate;

		//foreach( var it in stats )
		//	it.Reset( );

		foreach( var it in stats )
			it.Calc( );

		status[(int)PlanetUtil.CharacterStatus.NowGurugiableCount] = (int)( stats[(int)PlanetUtil.CharacterStat.DashCount].final );
		status[(int)PlanetUtil.CharacterStatus.NowHp] = (int)( oldHpRate * stats[(int)PlanetUtil.CharacterStat.MaxHp].final );
	}

	public bool EquipSkill( PlanetUtil.WeaponType _type, int _slot, int _index = -1 )
	{
		if( _index == -1 )
		{
			playerActiveSkill[(int)_type, _slot] = null;
			GameMgr.Instance.playerData.equipSkills[(int)_type, _slot] = -1;
			UpdateSkillButton( );
			return true;
		}

		var skill = GameMgr.Instance.playerData.skills[(int)_type, _index];
		//if( skill.level == 0 )
		//	return false;
		skill.SetStat( this );
		playerActiveSkill[(int)_type, _slot] = skill;
		GameMgr.Instance.playerData.equipSkills[(int)_type, _slot] = _index;
		UpdateSkillButton( );
		return true;
	}

	private void UpdateSkillButton( )
	{
		// 스킬 교체
		for( int i = 0 ; i < 2 ; ++i )
		{
			if( playerActiveSkill[(int)( (Weapon)items[nowWeapon] ).wpType, i] != null )
			{
				GameMgr.Instance.uiMgr.skill[i].SetSkill( true, playerActiveSkill[(int)( (Weapon)items[nowWeapon] ).wpType, i].icon );
				GameMgr.Instance.uiMgr.skill[i].SetCoolTime( playerActiveSkill[(int)( (Weapon)items[nowWeapon] ).wpType, i].curCoolTime[playerActiveSkill[(int)( (Weapon)items[nowWeapon] ).wpType, i].level],
															 playerActiveSkill[(int)( (Weapon)items[nowWeapon] ).wpType, i].coolTime - Time.time, true );
			}
			else
			{
				GameMgr.Instance.uiMgr.skill[i].SetSkill( false, null );
			}
		}
	}

	public bool Equip( PlanetUtil.ItemType _type, EquipItem _item ) // 벗을 땐 _item을 null로
	{
		if( _type >= PlanetUtil.ItemType.Consume )
			return false;

		var oldItem = items[(int)_type];
		items[(int)_type] = _item;

		if( oldItem == _item ) // 같은거면 패쓰
			return false;

		if( _item != null ) // 플레이어 정보 갱신
		{
			GameMgr.Instance.playerData.items[(int)_type] = _item;
		}

		// 능력치 적용
		if( ( _type == (PlanetUtil.ItemType)nowWeapon || _type == PlanetUtil.ItemType.Armor ) )
		{
			if( oldItem != null )
			{
				DeApplyItemPower( oldItem );
			}
			if( _item != null )
			{
				ApplyItemPower( _item );
			}
			else
			{
				// 자동 무기 교체
				if( null != items[( nowWeapon + 1 ) % (int)PlanetUtil.ItemType.Armor] )
				{
					nowWeapon = ( nowWeapon + 1 ) % (int)PlanetUtil.ItemType.Armor;
					ApplyItemPower( items[nowWeapon] );
				}
				else if( null != items[( nowWeapon + 2 ) % (int)PlanetUtil.ItemType.Armor] )
				{
					nowWeapon = ( nowWeapon + 2 ) % (int)PlanetUtil.ItemType.Armor;
					ApplyItemPower( items[nowWeapon] );
				}
				GameMgr.Instance.uiMgr.weaponUI.SwapWeapon( nowWeapon );
			}

		}

		// 메인 무기일때 패시브 스킬 처리
		if( _type == (PlanetUtil.ItemType)nowWeapon )
		{
			if( oldItem != null )
				DeApplyPassiveSkill( ( (Weapon)oldItem ).wpType );
			if( _item != null )
			{
				//cs.ChangeAttackCallback( ( (Weapon)_item ).AttackCallback ); // 기본 공격 콜백
				ApplyPassiveSkill( ( (Weapon)_item ).wpType );
			}

			// 스킬 교체
			UpdateSkillButton( );
		}

		//무기일때의 업데이트
		if( _type <= PlanetUtil.ItemType.DefaultWeapon )
		{
			if( _item != null )
			{
				GameMgr.Instance.uiMgr.weaponUI.ChangeWaeapon( _type, ( (Weapon)_item ).wpType );
				nowWeapon = ( (int)_type + 2 ) % (int)PlanetUtil.ItemType.Armor;
				SwapWeapon( );
			}
			else
				GameMgr.Instance.uiMgr.weaponUI.ChangeWaeapon( _type, ( (PlanetUtil.WeaponType)( -1 ) ) );

			ResetBullet( );

			// UI 업데이트
			UpdateBulletUI( );
		}

		CalcStat( );

		return true;
	}

	public void SwapWeapon( )
	{
		var oldWeapon = nowWeapon;
		if( null != items[( nowWeapon + 1 ) % (int)PlanetUtil.ItemType.Armor] )
			nowWeapon = ( nowWeapon + 1 ) % (int)PlanetUtil.ItemType.Armor;
		else if( null != items[( nowWeapon + 2 ) % (int)PlanetUtil.ItemType.Armor] )
			nowWeapon = ( nowWeapon + 2 ) % (int)PlanetUtil.ItemType.Armor;
		else
			return; // 교체 불가

		// 능력치 적용
		DeApplyItemPower( items[oldWeapon] );
		ApplyItemPower( items[nowWeapon] );
		nowWeaponType = ( (Weapon)( items[nowWeapon] ) ).wpType;
		CalcStat( );

		// UI 적용
		UpdateBulletUI( );

		GameMgr.Instance.uiMgr.weaponUI.SwapWeapon( nowWeapon );
		swapEffect.ShowSwapEffect( items[nowWeapon].name );

		UpdateSkillButton( );
	}

	void ApplyItemPower( EquipItem _item )
	{
		if( _item != null )
		{
			if( _item.isApplyed )
				return;
			_item.isApplyed = true;

			if( _item.pfm != null )
			{
				foreach( var it in _item.pfm )
				{
					stats[(int)it.type].ApplyAdd( it.add );
					stats[(int)it.type].ApplyScale( it.scale );
				}
			}
			if( _item.addPfm != null )
			{
				foreach( var it in _item.addPfm )
				{
					stats[(int)it.type].ApplyAdd( it.add );
					stats[(int)it.type].ApplyScale( it.scale );
				}
			}
		}
	}

	void DeApplyItemPower( EquipItem _item )
	{
		if( _item != null )
		{
			if( !_item.isApplyed )
				return;
			_item.isApplyed = false;

			if( _item.pfm != null )
			{
				foreach( var it in _item.pfm )
				{
					stats[(int)it.type].DeApplyAdd( it.add );
					stats[(int)it.type].DeApplyScale( it.scale );
				}
			}
			if( _item.addPfm != null )
			{
				foreach( var it in _item.addPfm )
				{
					stats[(int)it.type].DeApplyAdd( it.add );
					stats[(int)it.type].DeApplyScale( it.scale );
				}
			}
		}
	}

	void ApplyPassiveSkill( PlanetUtil.WeaponType _wptype )
	{
		foreach( var skill in passiveSkills[(int)_wptype] )
		{
			foreach( var it in skill.buff.buff )
			{
				stats[(int)it.type].ApplyAdd( it.add );
				stats[(int)it.type].ApplyScale( it.scale );
			}
		}
	}

	void DeApplyPassiveSkill( PlanetUtil.WeaponType _wptype )
	{
		foreach( var skill in passiveSkills[(int)_wptype] )
		{
			foreach( var it in skill.buff.buff )
			{
				stats[(int)it.type].DeApplyAdd( it.add );
				stats[(int)it.type].DeApplyScale( it.scale );
			}
		}
	}

	public bool AddBuff( Buff _buff )
	{
		foreach( var it in buffList )
			if( it.id == _buff.id )
				return false;

		_buff.Init( this );
		buffList.Add( _buff );

		CalcStat( );

		return true;
	}

	public bool AddBuff( int _id )
	{
		if( _id == 0 )
			return true;
		//Todo 데이터베이스 매니저에게 데이터 가져오게 하기
		return AddBuff( GameMgr.Instance.GetBuff( _id ) );
	}

	// 버프가 지워질때 셀프로 쓴다.
	public bool RemoveBuff( Buff _buff )
	{
		return buffList.Remove( _buff );
	}

	// 버프를 지울땐 반트시 이것이 이용할 것
	public bool RemoveBuff( int _id )
	{
		for( int i = 0 ; i < buffList.Count ; ++i )
			if( buffList[i].id == _id )
			{
				buffList[i].Uninit( );
				CalcStat( );
				return true;
			}
		return false;
	}

	public EquipItem GetItem( int _idx )
	{
		return items[_idx];
	}

	public bool AddMagazine( int _value )
	{
		if( nowWeapon == (int)PlanetUtil.ItemType.DefaultWeapon )
			return false;

		nowMagazine[nowWeapon] += _value;
		nowMagazine[nowWeapon] = Mathf.Min( (int)this[PlanetUtil.CharacterStat.MaxMagazine], nowMagazine[nowWeapon] );
		UpdateBulletUI( );

		return true;
	}

	public void AddExp( int _value )
	{
		status[(int)PlanetUtil.CharacterStatus.NowExp] += _value;
		if( status[(int)PlanetUtil.CharacterStatus.NowExp] >= this[PlanetUtil.CharacterStat.MaxExp] )
			LevelUp( );
		GameMgr.Instance.playerData.nowExp = status[(int)PlanetUtil.CharacterStatus.NowExp];
		GameMgr.Instance.uiMgr.expBar.SetParam( status[(int)PlanetUtil.CharacterStatus.NowExp] / this[PlanetUtil.CharacterStat.MaxExp],
												(int)status[(int)PlanetUtil.CharacterStatus.NowExp],
												(int)this[PlanetUtil.CharacterStat.MaxExp] );
	}

	void LevelUp( )
	{
		// Todo 레벨업 효과 추가
		stats[(int)PlanetUtil.CharacterStat.Level].def += 1;
		status[(int)PlanetUtil.CharacterStatus.NowExp] = 0f;
		CalcLevelData( );
		CalcStat( );

		GameMgr.Instance.playerData.level = (int)this[PlanetUtil.CharacterStat.Level];
		++GameMgr.Instance.playerData.skillPoint;

		GameMgr.Instance.uiMgr.level.text = GameMgr.Instance.playerData.level.ToString();
	}

	public virtual void Demage( float _dmg )
	{
		var hp = status[(int)PlanetUtil.CharacterStatus.NowHp];
		status[(int)PlanetUtil.CharacterStatus.NowHp] = Mathf.Clamp( hp - _dmg, 0, (int)stats[(int)PlanetUtil.CharacterStat.MaxHp].final );

		if( name == "Player" ) // HP UI 업데이트
		{
			GameMgr.Instance.uiMgr.hpBar.SetParam( HpRate, (int)status[(int)PlanetUtil.CharacterStatus.NowHp],
															(int)this[PlanetUtil.CharacterStat.MaxHp] );
		}

		// 사망 시
		if( HpRate == 0f )
		{
			State = PlanetUtil.CharacterState.Death;
			UIMgr.Instance.Die( );
		}

		bHit = true; // brhavior Tree 에서 false 한다.
	}

	public void StatableResetAll( bool _set = true )
	{
		for( int i = 0 ; i < statable.Length ; ++i )
			statable[i].Reset( _set );
	}

	public void SetStatable( PlanetUtil.StatableSet _statableSet, bool _set = true )
	{
		//idle모드
		if( ( _statableSet & PlanetUtil.StatableSet.IdleMode ) != 0 )
		{
			Debug.Log( "idle모드" );
			State = PlanetUtil.CharacterState.Idle;

			statable[(int)PlanetUtil.CharacterStatables.Move].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Attack].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Jump].Reset( true );
			statable[(int)PlanetUtil.CharacterStatables.DownJump].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Hit].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Dash].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Skill].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Stuck].Reset( true );
			return;
		}

		//스킬무브애블
		if( ( _statableSet & PlanetUtil.StatableSet.SkillMoveAble ) != 0 )
		{
			State = PlanetUtil.CharacterState.Skill;

			statable[(int)PlanetUtil.CharacterStatables.Move].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Attack].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Jump].Reset( false );
			statable[(int)PlanetUtil.CharacterStatables.DownJump].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Hit].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Dash].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Skill].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Stuck].Reset( true );
		}

		//스킬돈컨트롤
		if( ( _statableSet & PlanetUtil.StatableSet.SkillDontCtrl ) != 0 )
		{
			State = PlanetUtil.CharacterState.Skill;

			statable[(int)PlanetUtil.CharacterStatables.Move].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Attack].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Jump].Reset( false );
			statable[(int)PlanetUtil.CharacterStatables.DownJump].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Hit].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Dash].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Skill].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Stuck].Reset( true );
		}

		//가드모드
		if( ( _statableSet & PlanetUtil.StatableSet.GuardMode ) != 0 )
		{
			State = PlanetUtil.CharacterState.Guard;

			statable[(int)PlanetUtil.CharacterStatables.Move].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Attack].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Jump].Reset( false );
			statable[(int)PlanetUtil.CharacterStatables.DownJump].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Hit].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Dash].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Skill].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Stuck].Reset( true );
		}

		//슈퍼모드
		if( ( _statableSet & PlanetUtil.StatableSet.SuperMode ) != 0 )
		{
			State = PlanetUtil.CharacterState.SuperArmor;

			statable[(int)PlanetUtil.CharacterStatables.Move].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Attack].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Jump].Reset( false );
			statable[(int)PlanetUtil.CharacterStatables.DownJump].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Hit].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Dash].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Skill].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Stuck].Reset( false );
		}

		//딜레이모드
		if( ( _statableSet & PlanetUtil.StatableSet.DelayMode ) != 0 )
		{
			state = PlanetUtil.CharacterState.Idle;

			statable[(int)PlanetUtil.CharacterStatables.Move].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Attack].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Jump].Reset( false );
			statable[(int)PlanetUtil.CharacterStatables.DownJump].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Hit].Reset( true );

			statable[(int)PlanetUtil.CharacterStatables.Dash].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Skill].Reset( false );

			statable[(int)PlanetUtil.CharacterStatables.Stuck].Reset( true );
		}


		if( ( _statableSet & PlanetUtil.StatableSet.Movement ) != 0 )
		{
			statable[(int)PlanetUtil.CharacterStatables.Move].Reset( _set );
			statable[(int)PlanetUtil.CharacterStatables.Jump].Reset( _set );
			statable[(int)PlanetUtil.CharacterStatables.DownJump].Reset( _set );
			statable[(int)PlanetUtil.CharacterStatables.Dash].Reset( _set );
		}
		if( ( _statableSet & PlanetUtil.StatableSet.Attack ) != 0 )
		{
			statable[(int)PlanetUtil.CharacterStatables.Attack].Reset( _set );
			statable[(int)PlanetUtil.CharacterStatables.Skill].Reset( _set );
		}
		if( ( _statableSet & PlanetUtil.StatableSet.Util ) != 0 )
		{
			statable[(int)PlanetUtil.CharacterStatables.Reload].Reset( _set );
			statable[(int)PlanetUtil.CharacterStatables.Guard].Reset( _set );
		}
		if( ( _statableSet & PlanetUtil.StatableSet.Hit ) != 0 )
		{
			statable[(int)PlanetUtil.CharacterStatables.Hit].Reset( _set );
		}
		if( ( _statableSet & PlanetUtil.StatableSet.HitAction ) != 0 )
		{
			statable[(int)PlanetUtil.CharacterStatables.Stuck].Reset( _set );
		}

		if( type == PlanetUtil.CharacterTypes.player )
			GameMgr.Instance.uiMgr.hpBar.SetParam( HpRate, (int)status[(int)PlanetUtil.CharacterStatus.NowHp], (int)this[PlanetUtil.CharacterStat.MaxHp] );

	}
}
