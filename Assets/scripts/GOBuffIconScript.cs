﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GOBuffIconScript : GOScript
{
	BuffIconUIScript root;
	Image icon;
	public Image fill;
	public Text timer;
	public float time;
	public float acc;

	public override bool Init( )
	{
		return true;
	}

	protected override void Awake( )
	{
		icon = GetComponent<Image>( );
		icon.sprite = null;
		icon.color = new Color( 1f, 1f, 1f, 0f );
		type = ObjectPoolManager.PoolType.BuffIcon;
	}

	public void Init( Sprite _sp, float _time , BuffIconUIScript _root)
	{
		root = _root;
		icon.sprite = _sp;
		icon.color = new Color( 1f, 1f, 1f, 1f );
		time = _time;
		acc = 0f;
		fill.fillAmount = 0f;
	}

	private void Update( )
	{
		if( time != -1f )
		{
			if( acc >= time )
				root.DestroyIcon( this );

			acc += Time.deltaTime;
			fill.fillAmount = acc / time;
			timer.text = ( time - acc ).ToString( "0.0" );
		}
	}
}
