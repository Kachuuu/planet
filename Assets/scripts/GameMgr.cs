﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMgr : MonoBehaviour
{
	[Header("임시")]
	public Sprite[] skillIcon;

	[Space]
	[Header("로드될 씬 인덱스")]
	public int nowScene = 0;

	public static GameMgr Instance;

	public ObjectPoolManager objMgr;
	public DataBaseManager dbMgr;
	public UIMgr uiMgr;

	public DevelopMgr developMgr;

	public PlayerData playerData;
	public bool gamePause = false; //false로 초기화
	//public UIMenuScript testUI;
	//public Text touchIDs;

	public Text uiPlayerState;
	public CanvasGroup panelBlind;
	public Text endPanelText;

	public AudioSource ado;

	public float seVolume = 1f;
	public float bgmVolume = 1f;

	public GameObject exGo;

	[HideInInspector] public StatusScript Player;
	[HideInInspector] public StatusScript Monster;

	void Start( )
	{
		Instance = this;
		if( developMgr == null )
		{
			DontDestroyOnLoad( gameObject );
			DontDestroyOnLoad( Camera.main );
			DontDestroyOnLoad( GameObject.Find( "Canvas" ) );
			DontDestroyOnLoad( GameObject.Find( "EventSystem" ) );

			SceneManager.LoadScene( nowScene );

			objMgr.ClearPool( );
			// 임시
			//Screen.orientation = ScreenOrientation.LandscapeLeft;
		}
		playerData = new PlayerData( ); // 이니셜라이즈로 생성 시 씬이 디스트로이될때 이 게임오브젝트만 '재생성'된다.
										// 그래서 playerData는 기존의 것(Instance)가 있기때문에 재대로 생성치 않고, 게임매니저는 그걸 들고 있는다.
		for( int i = 0 ; i < 6 ; ++i )
		{
			PlayerData.Instance.questInfoList.Add( DataBaseManager.Instance.GetQuest( i ) );
		}
		QuestMgr.Instance.QuestEnableCheck( );
	}

	public GameObject MakePlayer( Vector3 _pos, bool _load = true )
	{
		var player = objMgr.BringObject( ObjectPoolManager.PoolType.Player, _pos, Quaternion.identity);

		Player = player.GetComponent<StatusScript>( );
		player.GetComponent<PlayerInputScript>( ).InitButtons( );
		playerData.UpdateSkillStat( Player );

		Camera.main.GetComponent<SimpleFollowCam>( ).tg = player.transform;

		// 로드 생성
		if( _load )
		{
			if( playerData.Load( ) ) // 저장된 캐릭터 생성
			{
				LoadPlayer( );
				Debug.Log( "Loaded Player" );
				return player;
			}
		}
		NewPlayer( );
		Debug.Log( "Create New Player" );
		return player;
	}

	public void NewPlayer()
	{
		// 새로운 캐릭터 생성
		// 무기 장착
		Performence[] perf1 = new Performence[]{
		new Performence { type = PlanetUtil.CharacterStat.Attack,           add = 20f   , scale = 1f },
		new Performence { type = PlanetUtil.CharacterStat.MaxBullet,        add = 6f    , scale = 1f },
		new Performence { type = PlanetUtil.CharacterStat.MaxMagazine,      add = -1f   , scale = 1f }, // 핸드건은 반드시 0 미만이여야 한다.
		new Performence { type = PlanetUtil.CharacterStat.ReloadCnt,        add =  6f   , scale = 1f },
		new Performence { type = PlanetUtil.CharacterStat.ReloadDelay,      add = 0.5f  , scale = 1f },
		new Performence { type = PlanetUtil.CharacterStat.AttackPrevDelay,  add = 0.4f  , scale = 1f },
		new Performence { type = PlanetUtil.CharacterStat.AttackDelay,      add = 0.5f  , scale = 1f },
		new Performence { type = PlanetUtil.CharacterStat.AttackPostDelay,  add = 0.2f  , scale = 1f },
		new Performence { type = PlanetUtil.CharacterStat.BulletRange,      add = 8f    , scale = 1f },
		new Performence { type = PlanetUtil.CharacterStat.Penetrate,        add = 0.0f  , scale = 1f },
		new Performence { type = PlanetUtil.CharacterStat.CriticalRate,     add = 0.0f  , scale = 1f }
		};

		Weapon Hg = new Handgun( perf1 );
		Hg.name = "꾸린 권총";
		Hg.useableLevel = 1;
		Hg.spriteid = 1;
		Player.Equip( PlanetUtil.ItemType.DefaultWeapon, Hg );
		Player.Init( );

		playerData.skillPoint = 4;
		GameMgr.Instance.playerData.level = (int)Player[PlanetUtil.CharacterStat.Level];
		GameMgr.Instance.uiMgr.level.text = GameMgr.Instance.playerData.level.ToString( );
		var max = Player[PlanetUtil.CharacterStat.MaxExp];
		var now = Player.status[(int)PlanetUtil.CharacterStatus.NowExp];
		uiMgr.expBar.SetParam( now / max, (int)now, (int)max, true );
	}

	public void LoadPlayer()
	{
		for(int i = 0 ; i < playerData.items.Length ; ++i )	
			Player.Equip( (PlanetUtil.ItemType)i, playerData.items[i] );

		for( int i = 0 ; i < playerData.equipSkills.GetLength( 0 ) ; ++i )
			for( int j = 0 ; j < playerData.equipSkills.GetLength( 1 ) ; ++j )
				Player.EquipSkill( (PlanetUtil.WeaponType)i, j, playerData.equipSkills[i, j] );

		Player.stats[(int)PlanetUtil.CharacterStat.Level].def = GameMgr.Instance.playerData.level;
		GameMgr.Instance.uiMgr.level.text = GameMgr.Instance.playerData.level.ToString( );
		Player.Init( );

		Player.status[(int)PlanetUtil.CharacterStatus.NowExp] = playerData.nowExp;
		var max = Player[PlanetUtil.CharacterStat.MaxExp];
		var now = Player.status[(int)PlanetUtil.CharacterStatus.NowExp];
		uiMgr.expBar.SetParam( now / max, (int)now, (int)max, true );
	}
/*
	public GameObject MakePlayer( Vector3 _pos )
	{
		var player = objMgr.BringObject( ObjectPoolManager.PoolType.Player, _pos, Quaternion.identity);

		Player = player.GetComponent<StatusScript>();
		player.GetComponent<PlayerInputScript>( ).InitButtons( );
		playerData.UpdateSkillStat( Player );

		Camera.main.GetComponent<SimpleFollowCam>( ).tg = player.transform;


		// 무기 장착
		Performence[] perf1 = new Performence[]{
			new Performence { type = PlanetUtil.CharacterStat.Attack,           add = 20f   },
			new Performence { type = PlanetUtil.CharacterStat.MaxBullet,        add = 6f    },
			new Performence { type = PlanetUtil.CharacterStat.MaxMagazine,      add = -1f   }, // 핸드건은 반드시 0 미만이여야 한다.
			new Performence { type = PlanetUtil.CharacterStat.ReloadCnt,        add =  6f   },
			new Performence { type = PlanetUtil.CharacterStat.ReloadDelay,      add = 0.5f  },
			new Performence { type = PlanetUtil.CharacterStat.AttackPrevDelay,  add = 0.4f  },
			new Performence { type = PlanetUtil.CharacterStat.AttackDelay,      add = 0.5f  },
			new Performence { type = PlanetUtil.CharacterStat.AttackPostDelay,  add = 0.2f  },
			new Performence { type = PlanetUtil.CharacterStat.BulletRange,      add = 8f    },
			new Performence { type = PlanetUtil.CharacterStat.Penetrate,        add = 0.0f  },
			new Performence { type = PlanetUtil.CharacterStat.CriticalRate,     add = 0.0f  }
		};
		Weapon Hg = new Handgun( perf1 );
		Hg.name = "꾸린 권총";

		Performence[] perf2 = new Performence[]{
			new Performence { type = PlanetUtil.CharacterStat.Attack,           add = 100f  },
			new Performence { type = PlanetUtil.CharacterStat.MaxBullet,        add = 500f  },
			new Performence { type = PlanetUtil.CharacterStat.MaxMagazine,      add = 15f   },
			new Performence { type = PlanetUtil.CharacterStat.ReloadCnt,        add = 15f   },
			new Performence { type = PlanetUtil.CharacterStat.ReloadDelay,      add = 1.5f  },
			new Performence { type = PlanetUtil.CharacterStat.AttackPrevDelay,  add = 0.5f  },
			new Performence { type = PlanetUtil.CharacterStat.AttackDelay,      add = 0.4f  },
			new Performence { type = PlanetUtil.CharacterStat.AttackPostDelay,  add = 0.3f  },
			new Performence { type = PlanetUtil.CharacterStat.BulletRange,      add = 15f   },
			new Performence { type = PlanetUtil.CharacterStat.Penetrate,        add = 1.0f  },
			new Performence { type = PlanetUtil.CharacterStat.CriticalRate,     add = 0.3f  }
		};
		Weapon sniper = new Riple( perf2 );
		sniper.name = "꾸린 스나이퍼";

		Performence[] perf3 = new Performence[]{
			new Performence { type = PlanetUtil.CharacterStat.Attack,           add = 30f   },
			new Performence { type = PlanetUtil.CharacterStat.MaxBullet,        add = 7f    },
			new Performence { type = PlanetUtil.CharacterStat.MaxMagazine,      add = 30f   },
			new Performence { type = PlanetUtil.CharacterStat.ReloadCnt,        add = 1f    },
			new Performence { type = PlanetUtil.CharacterStat.ReloadDelay,      add = 0.6f  },
			new Performence { type = PlanetUtil.CharacterStat.AttackPrevDelay,  add = 0.5f  },
			new Performence { type = PlanetUtil.CharacterStat.AttackDelay,      add = 0f    },
			new Performence { type = PlanetUtil.CharacterStat.AttackPostDelay,  add = 0.5f  },
			new Performence { type = PlanetUtil.CharacterStat.BulletRange,      add = 15f   },
			new Performence { type = PlanetUtil.CharacterStat.Penetrate,        add = 1.0f  },
			new Performence { type = PlanetUtil.CharacterStat.CriticalRate,     add = 0.3f  }
		};
		Weapon shot = new Shotgun( perf3 );
		shot.name = "개좋은 샷건";

		Performence[] perf4 = new Performence[]{
			new Performence { type = PlanetUtil.CharacterStat.Attack,           add = 30f   },
			new Performence { type = PlanetUtil.CharacterStat.MaxBullet,        add = 30f   },
			new Performence { type = PlanetUtil.CharacterStat.MaxMagazine,      add = 100f  }, // 핸드건은 반드시 0 미만이여야 한다.
			new Performence { type = PlanetUtil.CharacterStat.ReloadCnt,        add = 30f   },
			new Performence { type = PlanetUtil.CharacterStat.ReloadDelay,      add = 2f    },
			new Performence { type = PlanetUtil.CharacterStat.AttackPrevDelay,  add = 0.4f  },
			new Performence { type = PlanetUtil.CharacterStat.AttackDelay,      add = 0.2f  },
			new Performence { type = PlanetUtil.CharacterStat.AttackPostDelay,  add = 0.2f  },
			new Performence { type = PlanetUtil.CharacterStat.BulletRange,      add = 12f   },
			new Performence { type = PlanetUtil.CharacterStat.Penetrate,        add = 0.0f  },
			new Performence { type = PlanetUtil.CharacterStat.CriticalRate,     add = 0.0f  }
		};
		Weapon hmg = new SCV( perf4 );
		hmg.name = "안나오는 머신건";
		//stat.Equip( PlanetUtil.ItemType.DefaultWeapon, hmg );

		Player.playerActiveSkill[(int)PlanetUtil.WeaponType.Handgun, 0] = new Active_Handgun_Guard( Player ) { icon = dbMgr.GetSprite( 8 ) };
		//Player.playerActiveSkill[(int)PlanetUtil.ItemType.DefaultWeapon, 1] = new Active_Handgun_Nansa( stat );
		Player.playerActiveSkill[(int)PlanetUtil.WeaponType.Handgun, 1] = new Active_Handgun_Freedom( Player ) { icon = dbMgr.GetSprite( 1 ) };
		Player.Equip( PlanetUtil.ItemType.DefaultWeapon, Hg );
		Hg.useableLevel = 1;
		Hg.spriteid = 1;

		Player.Equip( PlanetUtil.ItemType.SubWeapon, sniper );
		sniper.useableLevel = 1;
		sniper.spriteid = 3;

		Player.Equip( PlanetUtil.ItemType.Weapon, shot );
		shot.useableLevel = 1;
		shot.spriteid = 4;

		Player.playerActiveSkill[(int)PlanetUtil.WeaponType.HavyMachinegun, 0] = new Active_Scv_Dron( Player ) { icon = dbMgr.GetSprite( 6 ) };
		Player.playerActiveSkill[(int)PlanetUtil.WeaponType.HavyMachinegun, 1] = new Active_Scv_Tower( Player ) { icon = dbMgr.GetSprite( 7 ) };

		//	EquipItem armor = new Armor();
		//	stat.Equip( PlanetUtil.ItemType.Armor, armor );
		//	armor.useableLevel = 1;
		Player.Init( );

		return player;
	}
*/

	Coroutine coSupVoid;
	internal void SuperVoid( float startScale = 0.5f, float time = 0.5f )
	{
		if( coSupVoid != null )
			StopCoroutine( coSupVoid );
		coSupVoid = StartCoroutine( CoSupVoid( startScale, time ) );
	}

	public IEnumerator CoSupVoid( float startScale, float time )
	{
		float acc = 0f;

		while( acc < time )
		{
			acc += Time.deltaTime;
			Time.timeScale = Mathf.Lerp( startScale, 1f, acc / time );
			yield return null;
		}

		Time.timeScale = 1f;
		coSupVoid = null;
	}

	// 임시
	public Buff GetBuff( int _id )
	{
		return new PassiveBuff( _id, null );
	}

	public void LoadSceneOnFade( int _sceneNum )
	{
		playerData.inven.Merge( );
		playerData.Save( );
		StartCoroutine( CoSceneFading( panelBlind, 0.8f, 0f, 0.5f, _sceneNum ) );
	}

	public void LoadSceneOnFadeAndNoSave( int _sceneNum )
	{
		StartCoroutine( CoSceneFading( panelBlind, 0.8f, 0f, 0.5f, _sceneNum ) );
		uiMgr.ClearUI( );
	}

	public void LoadScene( int _sceneNum )
	{
		objMgr.ClearPool( );
		nowScene = _sceneNum;
		SceneManager.LoadScene( _sceneNum );
		uiMgr.ClearUI( );
		Time.timeScale = 1f;
	}

	internal void HitEffect( float value )
	{
		panelBlind.alpha = Mathf.Lerp( 0.3f, 0.8f, value );
		endPanelText.text = string.Empty;
		panelBlind.GetComponent<Image>( ).color = Color.red;
		StartCoroutine( CoHitEffect( panelBlind ) );
	}

	IEnumerator CoHitEffect( CanvasGroup _pnael )
	{
		while( _pnael.alpha > 0f )
		{
			_pnael.alpha -= 0.02f;
			yield return null;
		}
		_pnael.alpha = 0f;
	}

	private void Update( )
	{
		//if( Monster != null )
		//{
		//	if( Monster.HpRate <= 0f )
		//	{
		//		Monster = null;
		//		Debug.Log( "Monster Dead" );
		//		endPanelText.text = "E N D";
		//		panelBlind.GetComponent<Image>( ).color = Color.black;
		//		StartCoroutine( CoSceneFading( panelBlind, 5f, 3 ) );
		//	}
		//}

		//if( Player != null )
		//{
		//	uiPlayerState.text = Player.status[(int)PlanetUtil.CharacterStatus.JumpCount] + " State : " + Player.State +
		//						 Player.am.GetFloat( "Speed" ).ToString( "0.00" );
		//	if( Player.HpRate <= 0f )
		//	{
		//		Player = null;
		//		Debug.Log( "Player Dead" );
		//		endPanelText.text = "YOU DIED";
		//		panelBlind.GetComponent<Image>( ).color = Color.black;
		//		StartCoroutine( CoSceneFading( panelBlind, 5f, 3 ) );
		//	}
		//}

		if( Input.GetKeyDown( KeyCode.O ) )
			playerData.Save( );

		if( Input.GetKeyDown( KeyCode.P ) )
			playerData.inven.mineral += 100;
	}

	public void SceneFade(bool _in = true)
	{
		StartCoroutine( CoSceneFade( _in, panelBlind, 0.7f ) );
	}

	public IEnumerator CoSceneFade( bool _in, CanvasGroup _cg, float _time )
	{
		panelBlind.GetComponent<Image>( ).color = Color.black;
		float src = 0f, dst = 0f;
		if( _in )	src = 1f;
		else		dst = 1f;

		float acc = 0f;
		while( acc < _time )
		{
			acc += Time.deltaTime;
			_cg.alpha = Mathf.Lerp( src, dst, acc / _time );
			yield return null;
		}

		_cg.alpha = dst;
	}

	public IEnumerator CoSceneFading( CanvasGroup _cg, float _time, float _prevTime = 0f, float postTime = 0f, int _loadScene = -1 )
	{
		panelBlind.GetComponent<Image>( ).color = Color.black;
		yield return new WaitForSeconds( _prevTime );
		float acc = 0;
		while( acc < _time )
		{
			acc += Time.deltaTime;
			if( _loadScene != -1 )
				_cg.alpha = acc / _time;
			else
				_cg.alpha = 1f - acc / _time;
			yield return null;
		}

		_cg.alpha = ( _loadScene == -1 ) ? 0f : 1f;

		yield return new WaitForSeconds( postTime );

		//씬 전환
		if( _loadScene != -1 )
			LoadScene( _loadScene );
	}

	public static void CreateDropItem( Vector3 _pos, Item[] _items, int mineral, bool _boss )
	{
		// 아이템 생성
		if( _items != null )
		{
			for( int i = 0 ; i < _items.Length ; ++i )
			{
				var go = GameMgr.Instance.objMgr.BringObject( ObjectPoolManager.PoolType.DropItem, _pos );
				go.GetComponent<Rigidbody2D>( ).AddForce( new Vector2( UnityEngine.Random.Range( -200f, 200f ), 500f ) );
				var drop = go.GetComponent<GODropItem>();
				var rand = UnityEngine.Random.value;
				drop.SetDropItem( GODropItem.Subtype.Item, 0, _items[i] );
			}
		}

		var mCnt = UnityEngine.Random.Range(3, 7);
		if( _boss )
			mCnt = UnityEngine.Random.Range( 15, 18 );
		mineral /= mCnt;
		for( int i = 0 ; i < mCnt ; ++i )
		{
			var go = GameMgr.Instance.objMgr.BringObject( ObjectPoolManager.PoolType.DropItem, _pos );
			go.GetComponent<Rigidbody2D>( ).AddForce( new Vector2( UnityEngine.Random.Range( -200f, 200f ), 500f ) );
			var drop = go.GetComponent<GODropItem>();
			var rand = mineral * UnityEngine.Random.Range(0.9f, 1.1f);
			drop.SetDropItem( GODropItem.Subtype.Gold, rand );
		}
	}

	Coroutine coBgmChange = null;
	public void PlayBGM(int num) // num == -1 이면 BGM를 멈춘다.
	{
		if( coBgmChange != null )
			StopCoroutine( coBgmChange );
		if( dbMgr.bgms.Length < num || num < 0 )
		{
			Debug.Log( num + "는 BGM이 존재하지 않는 인덱스입니다." );
			return;
		}
		PlayBGM( dbMgr.bgms[num] );
	}

	public void PlayBGM( AudioClip clip ) // num == -1 이면 BGM를 멈춘다.
	{
		if( coBgmChange != null )
			StopCoroutine( coBgmChange );

		coBgmChange = StartCoroutine( CoPlayBGMFade( clip ) );
	}

	IEnumerator CoPlayBGMFade( AudioClip clip )
	{
		if( ado.isPlaying )
		{
			var v = ado.volume;
			var time = 0.5f;
			float acc = 0f;
			while( acc < time )
			{
				acc += Time.deltaTime;
				ado.volume = Mathf.Lerp( v, 0f, acc / time );
				yield return null;
			}
		}
		if( clip == null )
			yield break;
		
		ado.clip = clip;
		ado.Play( );
		{
			var v = bgmVolume;
			var time = 0.5f;
			float acc = 0f;
			while( acc < time )
			{
				acc += Time.deltaTime;
				ado.volume = Mathf.Lerp( 0f, v, acc / time );
				yield return null;
			}
		}
	}
}
