﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageSeletUIScript : MonoBehaviour {

	public Button[] StageButtons; // 3스테이지 언락???
	public GameObject[] infoPanels;

	public void Show()
	{
		gameObject.SetActive( true );
		infoPanels[0].SetActive( true );
		for( int i  = 1 ; i < infoPanels.Length ; ++i )
			infoPanels[i].SetActive( false );
	}

	public void Hide()
	{
		gameObject.SetActive( false );
	}

	public void ShowInfoPanel(int num )
	{
		for( int i = 0 ; i < infoPanels.Length ; ++i )
			infoPanels[i].SetActive( false );
		infoPanels[num].SetActive( true );
	}

}
