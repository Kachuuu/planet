﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponUIScript : MonoBehaviour {

	public Image[] Weapon = new Image[(int)PlanetUtil.ItemType.Armor];
	public int[] WeaponIdx = new int[(int)PlanetUtil.ItemType.Armor];
	public Sprite[] sprite = new Sprite[(int)PlanetUtil.WeaponType.None*2];

	public int nowWeapon;

	public void SwapWeapon( int _idx )
	{
		nowWeapon = _idx;
		for(int i = 0 ; i < Weapon.Length ; ++i )
		{
			if( WeaponIdx[i] != -1 )
			{
				if( i == _idx )
					Weapon[i].sprite = sprite[WeaponIdx[i]*2 + 0];
				else
					Weapon[i].sprite = sprite[WeaponIdx[i]*2 + 1];
			}
			else
				Weapon[i].sprite = null;
		}
	}

	public void ChangeWaeapon( PlanetUtil.ItemType _idx, PlanetUtil.WeaponType _wp )
	{
		if( (int)_wp != -1 )
		{
			WeaponIdx[(int)_idx] = (int)_wp;
			if( nowWeapon == (int)_idx )
				Weapon[(int)_idx].sprite = sprite[WeaponIdx[(int)_idx] * 2 + 0];
			else
				Weapon[(int)_idx].sprite = sprite[WeaponIdx[(int)_idx] * 2 + 1];
			Weapon[(int)_idx].color = Color.white;
		}
		else
		{
			WeaponIdx[(int)_idx] = -1;
			Weapon[(int)_idx].sprite = null;
			Weapon[(int)_idx].color = new Color(0f,0f,0f,0f);
		}
	}
}
