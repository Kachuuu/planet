﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUIScript : MonoBehaviour
{
	public GameObject uiDungoenInven;
	public Transform contentRoot;
	public Transform contentBackpackRoot;
	public ItemSlotScript weapon1;
	public ItemSlotScript weapon2;
	public ItemSlotScript armor;
	public Text mineral;
	[HideInInspector]	public List<InvenItemScript> itemList = new List<InvenItemScript>();
	[HideInInspector]	public List<InvenItemScript> itemList_Indungoen = new List<InvenItemScript>();

	public bool inDungoen;

	public void Show( bool _indungoen = false )
	{
		gameObject.SetActive( true );

		inDungoen = _indungoen;
		//uiDungoenInven.SetActive( inDungoen );
		//if( inDungoen )
		//{
		//	var bp = GameMgr.Instance.playerData.inven.backpack;//sortedBackpack;
		//	for( int i = 0 ; i < bp.Count ; ++i )
		//	{
		//		var item = CreateInvenItem( bp[i] );
		//		item.transform.SetParent( contentBackpackRoot );
		//		itemList_Indungoen.Add( item );
		//	}
		//}

		var items = GameMgr.Instance.playerData.inven.items;
		for( int i = 0 ; i < items.Count ; ++i )
		{
			var item = CreateInvenItem( items[i] );
			item.transform.SetParent( contentRoot, false );
			itemList.Add( item );
		}

		weapon1.idx = PlanetUtil.ItemType.Weapon;
		weapon1.SetItem( GameMgr.Instance.Player.GetItem((int)weapon1.idx) );

		weapon2.idx = PlanetUtil.ItemType.SubWeapon;
		weapon2.SetItem( GameMgr.Instance.Player.GetItem((int)weapon2.idx) );

		armor.idx = PlanetUtil.ItemType.Armor;
		armor.SetItem( GameMgr.Instance.Player.GetItem((int)armor.idx) );

		mineral.text = GameMgr.Instance.playerData.inven.mineral.ToString();

	}

	public void Hide( )
	{
		uiDungoenInven.SetActive( false );
		for( int i = 0 ; i < itemList.Count ; ++i )
			itemList[i].GetComponent<GOScript>( ).DoDistroy( );
		itemList.Clear( );

		for( int i = 0 ; i < itemList_Indungoen.Count ; ++i )
			itemList_Indungoen[i].GetComponent<GOScript>( ).DoDistroy( );
		itemList_Indungoen.Clear( );
		
		gameObject.SetActive( false );

		if( false == GameMgr.Instance.uiMgr.inDungoen )
			GameMgr.Instance.playerData.Save( );
	}


	public void AddItem( Item _item )
	{
		var item = CreateInvenItem( _item );
		item.transform.SetParent( contentRoot, false );
		itemList.Add( item );
	}
	public void RemoveItem ( Item _item )
	{
		for( int i = 0 ; i < itemList.Count ; ++i )
		{
			if( _item == itemList[i].item)
			{
				itemList[i].GetComponent<GOScript>( ).DoDistroy( );
				itemList.RemoveAt( i );
				return;
			}
		}

		for( int i = 0 ; i < itemList_Indungoen.Count ; ++i )
		{
			if( _item == itemList_Indungoen[i].item )
			{
				itemList_Indungoen[i].GetComponent<GOScript>( ).DoDistroy( );
				itemList_Indungoen.RemoveAt( i );
				return;
			}
		}
	}

	InvenItemScript CreateInvenItem( Item _item )
	{
		var iis = GameMgr.Instance.objMgr.BringObject( ObjectPoolManager.PoolType.InvenItem ).GetComponent<InvenItemScript>();
		iis.SetItemInfo( _item );
		return iis;
	}
}
