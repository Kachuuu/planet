﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SkillSlotScript : MonoBehaviour, IDropHandler
{
	[HideInInspector] public ActiveSkill skill;
	private SkillIconScript icon;

	public SkillSlotScript another;
	[HideInInspector] public PlanetUtil.WeaponType type;
	public int idx; 

	private void Awake( )
	{
		icon = transform.GetChild( 0 ).GetComponent<SkillIconScript>( );
	}

	public virtual void OnDrop( PointerEventData eventData )
	{
		if( null == SkillIconScript.s_skillBeingDragged )
			return;

		var skill = SkillIconScript.s_skillBeingDragged.GetComponent<SkillIconScript>( ).skill;
		var slot = SkillIconScript.parrent.GetComponent<SkillSlotScript>( );          // 슬롯에서 왔는지 검사

		if( null == skill ) 
			return;

		if( false == ChkItemUseAble( skill ) ) // 장비할 수 없다면 Do Not
			return;

		var oldSkill = SetSkill( skill );
		GameMgr.Instance.uiMgr.UI_SE( );

		if( slot != null ) // 다른 슬롯에서 왔다면
			slot.SetSkill( oldSkill );

		if( ChkAnotherSame( ) )
			another.SetSkill( null );

	}

	bool ChkItemUseAble( ActiveSkill _skill )
	{
		if( _skill.level == 0 )
			return false;
		return true;
	}

	public ActiveSkill SetSkill( ActiveSkill _skill )
	{
		var old = skill;
		skill = _skill;

		icon.SetSkillInfo( _skill );

		if( skill != null )
			GameMgr.Instance.Player.EquipSkill( type, idx, _skill.idx );
		else
			GameMgr.Instance.Player.EquipSkill( type, idx, -1 );

		return old;
	}

	public bool ChkAnotherSame()
	{
		if( skill == another.skill )
			return true;
		return false;
	}
}
