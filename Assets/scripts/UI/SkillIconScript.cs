﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SkillIconScript : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public static GameObject s_skillBeingDragged;
	public static Transform parrent;

	private Image image;

	public ActiveSkill skill;

	private void Awake( )
	{
		image = GetComponent<Image>( );
	}

	public void SetSkillInfo( ActiveSkill _skill )
	{
		skill = _skill;

		if ( skill != null )
		{
			image.sprite = skill.icon;
			image.color = new Color( 1f, 1f, 1f, 1f );
		}
		else
		{
			image.sprite = null;
			image.color = new Color( 1f, 1f, 1f, 0f );
		}
	}

	#region Drag Routine

	public void OnBeginDrag( PointerEventData eventData )
	{
		//if( GameMgr.Instance.uiMgr.inDungoen )
		//	eventData.Reset( );
		s_skillBeingDragged = gameObject;
		parrent = transform.parent;
		transform.parent = GameMgr.Instance.uiMgr.transform;
		image.raycastTarget = false;
	}

	public void OnDrag( PointerEventData eventData )
	{
#if Unity_ANDROID
		transform.position = Input.touches[0].position;
#else
		transform.position = Input.mousePosition;
#endif
	}

	public void OnEndDrag( PointerEventData eventData )
	{
		s_skillBeingDragged = null;
		image.raycastTarget = true;

		if( transform.parent == GameMgr.Instance.uiMgr.transform )
			transform.parent = parrent;

		transform.localPosition = new Vector3( );
	}

	#endregion
}
