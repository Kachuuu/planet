﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelFailedScript : MonoBehaviour
{
	public Text guidText;
	public string txt = "터치해 서 재시작해  주세요.";
	public float delayTime = 3f;
	public bool touchable = false;

	public void Show()
	{
		gameObject.SetActive( true );
		touchable = false;
		guidText.text = "";
		Invoke( "SetTouchable", delayTime );
	}

	public void Hide()
	{
		gameObject.SetActive( false );
		touchable = false;
		guidText.text = "";
	}

	public void Toggle()
	{
		if( gameObject.activeSelf )
			Hide( );
		else
			Show( );
	}

	public void SetTouchable()
	{
		touchable = true;
		guidText.text = txt;
	}

	public void ResetScene()
	{
		if( touchable )
		{
			touchable = false;
			//GameMgr.Instance.LoadSceneOnFadeAndNoSave( PlanetUtil.TownSceneNumber );
			GameMgr.Instance.LoadSceneOnFade( PlanetUtil.TownSceneNumber );
		}
	}
}
