﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DangerPerUIScript : MonoBehaviour
{
	private void Start( )
	{
		if( GameMgr.Instance.nowScene == 3 )
		{
			gameObject.SetActive( true );
		}
		else
		{
			gameObject.SetActive( false );
		}
	}

	private void Update( )
	{

		if( GameMgr.Instance.nowScene == 3 )
		{
			gameObject.SetActive( true );
		}
		else
		{
			gameObject.SetActive( false );
		}
	}
}
