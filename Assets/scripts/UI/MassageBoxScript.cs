﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MassageBoxScript : MonoBehaviour
{
	public Text msg;
	public GameObject cancel;
	PlanetUtil.MsgBoxCallBack cb;

	public void Show(string _msg, PlanetUtil.MsgBoxCallBack _callback = null )
	{
		msg.text = _msg;
		cb = _callback;
		cancel.SetActive( null != cb );
		gameObject.SetActive( true );
	}

	public void Hide()
	{
		cb = null;
		msg.text = string.Empty;
		gameObject.SetActive( false );
	}

	public void OnCollect()
	{
		if( cb != null )
			cb( );
		Hide( );

	}

	public void OnCancel()
	{
		Hide( );
	}

}
