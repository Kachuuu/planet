﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuffIconUIScript : MonoBehaviour
{
	List<GOBuffIconScript> iconList =new List<GOBuffIconScript>();


	public void AddBuffIcon(Buff _buff)
	{
		Debug.Log( "aa" );
		Sprite sp =  GameMgr.Instance.dbMgr.GetSprite(_buff.spriteID);
		float time = -1;
		if( _buff is TimeLimitBuff )
			time = _buff.datas[(int)TimeLimitBuff.Datas.Time];

		var go = GameMgr.Instance.objMgr.BringObject( ObjectPoolManager.PoolType.BuffIcon );
		go.transform.SetParent( transform, false );
		var bi = go.GetComponent<GOBuffIconScript>( );
		bi.Init( sp, time ,this );

		iconList.Add( bi );
	}

	public void DestroyIcon( GOBuffIconScript _tg )
	{
		iconList.Remove( _tg );
		_tg.DoDistroy( );
	}

	public void Clear()
	{
		for ( int i = 0 ; i < iconList.Count ; ++i )
			iconList[i].DoDistroy( );
		iconList.Clear( );
	}

}
