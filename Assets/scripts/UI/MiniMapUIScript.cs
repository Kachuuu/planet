﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MiniMapUIScript : MonoBehaviour
{
	private Image minimap1;
	private Image minimap2;
	private Image minimap3;

	private int clickCheck = 0;
	private Text noText;

	private void Start( )
	{
		minimap1 = UIMgr.Instance.stage1MiniMap;
		minimap2 = UIMgr.Instance.stage2MiniMap;
		minimap3 = UIMgr.Instance.stage3MiniMap;
		noText = UIMgr.Instance.upCenterText;
	}

	public void OnClickMap()
	{
		if( clickCheck == 0 )
		{
			if( SceneManager.GetActiveScene( ).buildIndex == 1 )
			{
				minimap1.gameObject.SetActive( true );			
				clickCheck = 1;
			}
			if( SceneManager.GetActiveScene( ).buildIndex == 2 )
			{
				minimap2.gameObject.SetActive( true );
				clickCheck = 1;
			}
			if( SceneManager.GetActiveScene( ).buildIndex == 3 )
			{
				minimap3.gameObject.SetActive( true );
				clickCheck = 1;
			}
		}

		else if( clickCheck == 1 )
		{
			if( SceneManager.GetActiveScene( ).buildIndex == 1 )
			{
				minimap1.gameObject.SetActive( false );
				clickCheck = 0;
			}
			if( SceneManager.GetActiveScene( ).buildIndex == 2 )
			{
				minimap2.gameObject.SetActive( false );
				clickCheck = 0;
			}
			if( SceneManager.GetActiveScene( ).buildIndex == 3 )
			{
				minimap3.gameObject.SetActive( false );
				clickCheck = 0;
			}
		}

		if(SceneManager.GetActiveScene().buildIndex == 4)
		{
			if(noText == null)
			{ noText = UIMgr.Instance.upCenterText; }

			noText.gameObject.SetActive( true );
			noText.text = "해당 지역은 미니맵을 지원하지않습니다";
			Invoke( "TextNull", 3f );
		}

	}

	private void TextNull()
	{
		noText.text = null;
		noText.gameObject.SetActive( false );
	}

}
