﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIParameterScript : MonoBehaviour
{
	public Image under;
	public Image over;
	public Text text;

	private bool Up = false;

	public float time = 2f;
	private float acc = 0f;

	void Update( )
	{
		if( acc <= time )
		{
			acc += Time.deltaTime;
			if( !Up )	under.fillAmount = Mathf.Lerp( under.fillAmount, over.fillAmount, acc/time);
			else		over.fillAmount  = Mathf.Lerp( over.fillAmount, under.fillAmount, acc/time);
		}
	}

	public void SetParam( float _percent, int _now, int _max ,bool _Immediate = false )
	{
		if( over.fillAmount < _percent )
		{
			Up = true;
			under.fillAmount = _percent;
		}
		else
		{
			Up = false;
			over.fillAmount = _percent;
		}
		if( _Immediate )
			acc = time;
		else
			acc = 0f;

		if( text != null )
		{
			if( _max < 0 )
				text.text = _now + "/inf"; // 권총(무한탄창)일때
			else
				text.text = _now + "/" + _max;
		}
	}
}
