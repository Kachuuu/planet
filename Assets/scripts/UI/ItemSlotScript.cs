﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemSlotScript : MonoBehaviour, IDropHandler
{
	private EquipItem item = null;
	private ItemIconScript icon;

	private void Awake( )
	{
		icon = transform.GetChild( 0 ).GetComponent<ItemIconScript>( );
	}
	public PlanetUtil.ItemType type; // 담을 수 있는 아이템 타입

	public PlanetUtil.ItemType idx; // 실제 인덱스

	public virtual void OnDrop( PointerEventData eventData )
	{
		var newitem = ItemIconScript.s_itemBeingDragged.GetComponent<ItemIconScript>( ).item as EquipItem;
		var slot = ItemIconScript.parrent.GetComponent<ItemSlotScript>( );          // 슬롯에서 왔는지 검사

		if( null == newitem ) // 장비 아이템이 아니라면 Do Not
			return;

		if( false == ChkItemUseAble( newitem ) ) // 장비할 수 없다면 Do Not
			return;

		var oldItem = SetItem( newitem );
		GameMgr.Instance.uiMgr.UI_SE( );

		if( slot != null ) // 다른 슬롯에서 왔다면
		{
			slot.SetItem( oldItem );
		}
		else
		{
			GameMgr.Instance.uiMgr.inven.RemoveItem( newitem );
			GameMgr.Instance.playerData.inven.RemoveItem( newitem );
			if( oldItem != null ) // 기존의 장비가 있었다면
			{
				GameMgr.Instance.uiMgr.inven.AddItem( oldItem );
				GameMgr.Instance.playerData.inven.AddItem( oldItem );
			}
		}
	}


	bool ChkItemUseAble( Item _item )
	{
		if( _item.useableLevel > GameMgr.Instance.Player[PlanetUtil.CharacterStat.Level] )
		{
			GameMgr.Instance.uiMgr.ShowMsgBox( "아이템 사용 가능한 레벨이 아닙니다." );
			return false;
		}

		if( GameMgr.Instance.uiMgr.inDungoen ) // Todo 던전 중일때 무조건 false
		{
			GameMgr.Instance.uiMgr.ShowMsgBox( "던전에서는 장비를 변경할 수 없습니다." );
			return false;
		}

		if( _item.type != type )
		{
			GameMgr.Instance.uiMgr.ShowMsgBox( "아이템 타입이 맞지 않습니다." );
			return false;
		}

		return true;
	}

	public EquipItem SetItem( EquipItem _item )
	{
		var oldItem = item;

		// 아이템 설정 새로운 아이콘 설정
		item = _item;
		if( item != null )	icon.SetItemInfo( item );
		else				icon.SetItemInfo( null );

		GameMgr.Instance.Player.Equip( idx, item );

		// 기존 아이템 반환
		return oldItem;
	}
}
