﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InvenTrashScript : MonoBehaviour, IDropHandler
{
	public void OnDrop( PointerEventData eventData )
	{
		var newitem = ItemIconScript.s_itemBeingDragged.GetComponent<ItemIconScript>( ).item;
		var slot = ItemIconScript.parrent.GetComponent<ItemSlotScript>( );          // 슬롯에서 왔는지 검사

		if( slot != null ) // 다른 슬롯에서 왔다면
		{
			if( !GameMgr.Instance.uiMgr.inDungoen )
				slot.SetItem( null );
		}
		else // 아이템 삭제
		{
			GameMgr.Instance.uiMgr.UI_SE( );
			GameMgr.Instance.uiMgr.inven.RemoveItem( newitem );
			GameMgr.Instance.playerData.inven.RemoveItem( newitem, newitem.nowCount );
		}
	}
}
