﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemIconScript : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public static GameObject s_itemBeingDragged;
	public static Transform parrent;

	private Image image;
	[HideInInspector]
	public Item item = null;
	public bool Dragable = true;

	private void Awake( )
	{
		image = GetComponent<Image>( );
	}

	public void SetItemInfo( Item _item )
	{
		item = _item;
		if( image == null )
			image = GetComponent<Image>( );

		if( item != null )
		{
			image.sprite = GameMgr.Instance.dbMgr.GetSprite( item.spriteid );
			image.color = new Color( 1f, 1f, 1f, 1f );
		}
		else
		{
			image.sprite = null;
			image.color = new Color( 1f, 1f, 1f, 0f );
		}
	}

	#region Drag Routine

	public void OnBeginDrag( PointerEventData eventData )
	{
		if( !Dragable )
			return;
		//if( GameMgr.Instance.uiMgr.inDungoen )
		//	eventData.Reset( );
		s_itemBeingDragged = gameObject;
		parrent = transform.parent;
		transform.parent = GameMgr.Instance.uiMgr.transform;
		image.raycastTarget = false;
	}

	public void OnDrag( PointerEventData eventData )
	{
#if Unity_ANDROID
		transform.position = Input.touches[0].position;
#else
		transform.position = Input.mousePosition;
#endif
	}

	public void OnEndDrag( PointerEventData eventData )
	{
		s_itemBeingDragged = null;
		image.raycastTarget = true;

		if( transform.parent == GameMgr.Instance.uiMgr.transform )
			transform.parent = parrent;

		transform.localPosition = new Vector3( );
	}

	#endregion
}
