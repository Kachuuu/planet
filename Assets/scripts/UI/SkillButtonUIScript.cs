﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillButtonUIScript : MonoBehaviour {

	bool equip;
	public Image fillImg;
	public Image icon;
	public Text coolTimeText;
	public float coolTime;
	private float acc;
	public bool cooling;

	void Start ()
	{
		
	}
	
	void Update ()
	{
		if( equip && acc < coolTime )
		{
			acc += Time.deltaTime;
			fillImg.fillAmount = acc / coolTime;
			coolTimeText.text = (coolTime - acc).ToString( "0.0" );
			icon.color = new Color( 0.3f, 0.3f, 0.3f );

			if( acc > coolTime )
			{
				cooling = false;
				fillImg.fillAmount = 0f;
				coolTimeText.text = string.Empty;
				icon.color = Color.white;
			}
		}
		else
		{
			cooling = false;
		}
	}

	public void SetCoolTime( float _time, float _acc, bool _forcing = false )
	{
		if( cooling == true && _forcing == false )
			return;

		coolTime = _time;
		acc = coolTime - _acc;
		cooling = acc < coolTime;
		fillImg.fillAmount = acc / coolTime;
	}

	// 스킬장착되었는지 아닌지 설정해줘야 한다.
	public void SetSkill( bool _b, Sprite _icon ) // true - equip, false - empty
	{
		equip = _b;
		if( equip )
		{
			coolTimeText.text = string.Empty;
			icon.sprite = _icon;
			icon.color = Color.white;
		}
		else
		{
			cooling = false;
			coolTimeText.text = "Skill";
			fillImg.fillAmount = 0f;
			icon.sprite = null;
			icon.color = new Color( 1f, 1f, 1f, 0f );
		}

	}
}
