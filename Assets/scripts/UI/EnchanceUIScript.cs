﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnchanceUIScript : MonoBehaviour
{
	public Transform contentRoot;
	[HideInInspector]   public List<EnchanceContentScript> itemList = new List<EnchanceContentScript>();

	public EnchanceSlotScript ItemBefore;
	public EnchanceSlotScript ItemAfter;

	public Text percent;
	public Text cost;

	public void Show()
	{
		Time.timeScale = 0f;
		gameObject.SetActive( true );
		UpdateList( );
		UpdateInfo( null );
	}

	public void Hide( )
	{
		for( int i = 0 ; i < itemList.Count ; ++i )
			itemList[i].GetComponent<GOScript>( ).DoDistroy( );
		itemList.Clear( );

		ItemBefore.SetItemInfo( null );
		ItemAfter.SetItemInfo( null );
		gameObject.SetActive( false );
		Time.timeScale = 1f;
	}

	public void SetNextEnchanceInfo( Item _item )
	{
		if( _item == null )
		{
			ItemBefore.SetItemInfo( null );
			ItemAfter.SetItemInfo( null );
			return;
		}

		if( _item.enchanceCount < 10 )
		{
			EquipItem next = _item.Clone() as EquipItem;
			Upgrade( next );
			ItemAfter.SetItemInfo( next );
		}
	}

	public void TryUpgrade()
	{
		if( ItemBefore.item == null )
			return;

		if(	ItemBefore.item.enchanceCount >= 10 )
		{
			GameMgr.Instance.uiMgr.UI_SE( 17 );
			GameMgr.Instance.uiMgr.ShowMsgBox( "강화할 수 없습니다!\n<size=6>잭 : 더 이상은 안돼.</size>" );
			return;
		}
		if(	GameMgr.Instance.playerData.inven.mineral <
			PlanetUtil.GetEnchanceCost( ItemBefore.item.enchanceCount ) )
		{
			GameMgr.Instance.uiMgr.UI_SE( 17 );
			GameMgr.Instance.uiMgr.ShowMsgBox( "자원이 부족합니다!\n<size=6>잭 : 미네랄 가 온나</size>" );
			return;
		}

		GameMgr.Instance.playerData.inven.mineral -= PlanetUtil.GetEnchanceCost( ItemBefore.item.enchanceCount );

		if( Enchancing( (EquipItem)ItemBefore.item ) )
		{
			Upgrade( (EquipItem)ItemBefore.item );
			GameMgr.Instance.uiMgr.UI_SE( );
			var clr = PlanetUtil.itemNameClrText[(int)ItemBefore.item .rate];
			GameMgr.Instance.uiMgr.ShowMsgBox( "강화 성공!\n<size=6>잭 : 역시 나라니까!</size> \n\n<color=#"+clr+">"
						+ ItemBefore.item.name +" +" + ItemBefore.item.enchanceCount + "</color> 획득!" );
		}
		else
		{
			Downgrade( (EquipItem)ItemBefore.item );
			GameMgr.Instance.uiMgr.UI_SE( 17);
			GameMgr.Instance.uiMgr.ShowMsgBox( "강화 실패\n<size=6>잭 : 괘안타. 또 강화하믄 된다. 미네랄만 가 온나</size>  \n\n" + ItemBefore.item.name + " +" + ItemBefore.item.enchanceCount );
		}

		GameMgr.Instance.playerData.Save( );
		UpdateInfo( ItemBefore.item );
		UpdateList( );
	}


	public void UpdateList( )
	{
		if( itemList.Count == 0 )
		{
			for( int i = 0 ; i < itemList.Count ; ++i )
				itemList[i].GetComponent<GOScript>( ).DoDistroy( );
			itemList.Clear( );

			var items = GameMgr.Instance.playerData.inven.items;
			for( int i = 0 ; i < items.Count ; ++i )
			{
				var item = CreateEnchanceItem( items[i] );
				if( item != null )
				{
					item.transform.SetParent( contentRoot, false );
					itemList.Add( item );
				}
			}
		}
		else
		{
			for( int i = 0 ; i < itemList.Count ; ++i )
				itemList[i].SetItemInfo((EquipItem)itemList[i].item);
		}
	}

	public void UpdateInfo( Item _item )
	{
		ItemBefore.SetItemInfo( _item );
		SetNextEnchanceInfo( _item );

		if( _item != null )
		{
			percent.text = PlanetUtil.GetEnchancePercent( _item.enchanceCount ).ToString( "0" ) + "%";
			cost.text = PlanetUtil.GetEnchanceCost( _item.enchanceCount ).ToString( ) + " / " + GameMgr.Instance.playerData.inven.mineral;
			cost.color = ( GameMgr.Instance.playerData.inven.mineral < PlanetUtil.GetEnchanceCost( ItemBefore.item.enchanceCount ) )
							? Color.red : Color.white;
		}
		else
		{
			percent.text = "0%";
			cost.text = "0 / " + GameMgr.Instance.playerData.inven.mineral;
			cost.color = Color.white;
		}
	}

	EnchanceContentScript CreateEnchanceItem( Item _item )
	{
		if( _item is EquipItem )
		{
			var iis = GameMgr.Instance.objMgr.BringObject( ObjectPoolManager.PoolType.EncanceItem ).GetComponent<EnchanceContentScript>();
			iis.SetItemInfo( _item as EquipItem );
			return iis;
		}
		return null;
	}

	bool Enchancing( EquipItem _item )
	{
		if( Random.value * 100f < PlanetUtil.GetEnchancePercent( _item.enchanceCount ) )
			return true;
		return false;
	}

	bool Upgrade(EquipItem _item)
	{
		if( _item.enchanceCount >= 10 )
			return false;
		switch( _item.type )
		{
		case PlanetUtil.ItemType.Weapon:
			switch( ( (Weapon)_item ).wpType )
			{
			case PlanetUtil.WeaponType.Handgun:
				SetItemPerformence( _item, PlanetUtil.CharacterStat.Attack, 10 );
				break;
			case PlanetUtil.WeaponType.Riple:
				SetItemPerformence( _item, PlanetUtil.CharacterStat.Attack, 30 );
				break;
			case PlanetUtil.WeaponType.Shotgun:
				SetItemPerformence( _item, PlanetUtil.CharacterStat.Attack, 8 );
				break;
			case PlanetUtil.WeaponType.Scv:
				SetItemPerformence( _item, PlanetUtil.CharacterStat.Attack, 10 );
				break;
			default:
				return false;
			}
			break;

		case PlanetUtil.ItemType.Armor:
			SetItemPerformence( _item, PlanetUtil.CharacterStat.Armor, 5 );
			break;

		default:
			return false;
		}

		return true;
	}

	bool Downgrade( EquipItem _item )
	{
		switch( _item.type )
		{
		case PlanetUtil.ItemType.Weapon:
			switch( ( (Weapon)_item ).wpType )
			{
			case PlanetUtil.WeaponType.Handgun:
				SetItemPerformence( _item, PlanetUtil.CharacterStat.Attack, 10, true );
				break;
			case PlanetUtil.WeaponType.Riple:
				SetItemPerformence( _item, PlanetUtil.CharacterStat.Attack, 30, true );
				break;
			case PlanetUtil.WeaponType.Shotgun:
				SetItemPerformence( _item, PlanetUtil.CharacterStat.Attack, 8, true );
				break;
			case PlanetUtil.WeaponType.Scv:
				SetItemPerformence( _item, PlanetUtil.CharacterStat.Attack, 10, true );
				break;
			}
			return false;

		case PlanetUtil.ItemType.Armor:
			SetItemPerformence( _item, PlanetUtil.CharacterStat.Armor, 5, true );
			break;

		default:
			return false;
		}

		return true;
	}

	void SetItemPerformence(Item _item, PlanetUtil.CharacterStat _type, float _add , bool _dwon = false )
	{
		for( int i = 0 ; i < _item.pfm.Length ; ++i )
		{
			if( _item.pfm[i].type == _type )
			{
				if( !_dwon )
				{
					_item.pfm[i].add += _add;
					++_item.enchanceCount;
				}
				else
				{
					_item.pfm[i].add -= _add * _item.enchanceCount;
					_item.enchanceCount = 0;
				}
				return;
			}
		}
	}


}
