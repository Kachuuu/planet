﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillUIScript : MonoBehaviour
{
	public Sprite[] BtImg;
	public Image[] weaponBts;
	public SkillUIContentScript[] skillContents;
	public SkillSlotScript[] skillSlots;
	public Text skillPoint;

	private void Awake( )
	{
		for( int i = 0 ; i < skillContents.Length ; ++i )
			skillContents[i].root = this;
	}

	public void Show( )
	{
		gameObject.SetActive( true );

		var nowWpType = (int)((Weapon)GameMgr.Instance.Player.GetItem(GameMgr.Instance.Player.nowWeapon) ).wpType;
		SetWeapon( nowWpType );
	}

	public void Hide( )
	{
		gameObject.SetActive( false );
		if( false == GameMgr.Instance.uiMgr.inDungoen )
			GameMgr.Instance.playerData.Save( );
	}

	public void SetWeapon( int _type )
	{
		for( int i = 0 ; i < weaponBts.Length ; ++i )
		{
			if( _type == i )
				weaponBts[i].sprite = BtImg[0]; // 하이라이트
			else
				weaponBts[i].sprite = BtImg[1];
		}

		UpdateSkillPoint( );

		var skills = GameMgr.Instance.playerData.skills;
		for( int i = 0 ; i < skills.GetLength( 1 ) ; ++i )
			skillContents[i].SetInfo( skills[_type, i] );

		var equips = GameMgr.Instance.playerData.equipSkills;

		for( int i = 0 ; i < equips.GetLength( 1 ) ; ++i )
		{
			skillSlots[i].type = (PlanetUtil.WeaponType)_type;
			if( equips[_type, i] == -1 )
				skillSlots[i].SetSkill( null );
			else
			{
				Debug.Log( skills[_type, equips[_type, i]] );
				skillSlots[i].SetSkill( skills[_type, equips[_type, i]] );
			}
			
		}
	}


	public void UpdateSkillPoint( )
	{
		skillPoint.text = GameMgr.Instance.playerData.skillPoint.ToString( "0" );
	}
}
