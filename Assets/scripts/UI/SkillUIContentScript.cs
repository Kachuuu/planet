﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class SkillUIContentScript : MonoBehaviour
{
	public SkillUIScript root;
	public SkillIconScript icon;
	public new Text name;
	public Text level;
	public Text disc;
	public Text nextLevelDisc;
	public Button lvlUpButton;

	public void SetInfo( ActiveSkill _skill )
	{
		icon.SetSkillInfo( _skill );
		level.text = _skill.level.ToString( );
		name.text = _skill.skillName;
		disc.text = _skill.skillExplain;
		nextLevelDisc.text = GetNextLevelDisc( _skill );

		//Todo 버튼 활성화/비활성화
	}

	private string GetNextLevelDisc( ActiveSkill skill )
	{
		if( skill.level == skill.maxLevel )
			return "최고 레벨";

		StringBuilder sb = new StringBuilder();
		int nextLevel = skill.level;

		if( skill.damage[nextLevel] != 0f )
		{
			sb.Append( "공격력 : " );
			sb.Append( ( skill.damage[nextLevel] ).ToString( "0" ) );
			sb.Append( "\n" );
		}

		if( skill.playTime[nextLevel] != 0f )
		{
			sb.Append( "시전시간 : " );
			sb.Append( ( skill.playTime[nextLevel] ).ToString( "0.0" ) );
			sb.Append( "초\n" );
		}

		if( skill.curCoolTime[nextLevel] != 0f )
		{
			sb.Append( "재사용 시간 : " );
			sb.Append( ( skill.curCoolTime[nextLevel] ).ToString( "0.0" ) );
			sb.Append( "초\n" );
		}

		return sb.ToString( );

	}

	public void LevelUp( )
	{
		if( icon.skill.level >= icon.skill.maxLevel ||
			GameMgr.Instance.playerData.skillPoint <= 0 )
			return;

		++icon.skill.level;
		--GameMgr.Instance.playerData.skillPoint;
		SetInfo( icon.skill );
		root.UpdateSkillPoint( );
	}
}
