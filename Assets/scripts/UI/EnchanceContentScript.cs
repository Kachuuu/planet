﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnchanceContentScript : MonoBehaviour
{
	public Item item = null;

	public ItemIconScript icon;
	public new Text name;
	public Text type;
	public Text enchanceCount;
	public Text option;
	
	public void SetItemInfo(EquipItem _item)
	{
		if( _item != null)
		{
			item = _item;
			icon.SetItemInfo( _item );
			name.text = _item.name + "+" + _item.enchanceCount;
			name.color = PlanetUtil.itemNameClr[(int)_item.rate];
			type.text = PlanetUtil.GetItemTypeString( _item );
			enchanceCount.text = _item.enchanceCount.ToString();
			option.text = GetOptionText( _item.type, _item.pfm, true );
		}
	}

	string GetOptionText( PlanetUtil.ItemType _type, Performence[] _pfm, bool _bulletInfo = false )
	{
		switch( _type )
		{
		case PlanetUtil.ItemType.Weapon:
			for( int i = 0 ; i < _pfm.Length ; ++i )
				if( _pfm[i].type == PlanetUtil.CharacterStat.Attack )
					return "공격력 : " + ( (int)_pfm[i].add ).ToString( );
			break;

		case PlanetUtil.ItemType.Armor:
			for( int i = 0 ; i < _pfm.Length ; ++i )
				if( _pfm[i].type == PlanetUtil.CharacterStat.Armor )
					return "방어력 : " + ( (int)_pfm[i].add ).ToString( );
			break;
		}
		return "missing";
	}
}
