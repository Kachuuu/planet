﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMgr : MonoBehaviour
{
	private AudioSource ado;

	public static UIMgr Instance;
	[Header("PlayerInfo")]
	public Text level;
	public UIParameterScript hpBar;
	public UIParameterScript expBar;
	public UIParameterScript bltBar;
	public WeaponUIScript weaponUI;
	public Text centerText;
	
	
	public GameObject numCheck;
	public Image stage1MiniMap;
	public Image stage2MiniMap;
	public Image stage3MiniMap;
	public Text upCenterText;

	[Header("Controller")]
	public GameObject controllerRoot;
	public SkillButtonUIScript[] skill;

	private float curTimeScale;

	public Button menuBtn;
	public Button mapBtn;

	[Header("UIs")]
	public GameObject pauseMenu;
	public GameObject giveUpBt;
	public InventoryUIScript inven;
	public QuestMgr quest;
	public SkillUIScript skillui;
	public StageSeletUIScript stageUI;
	public BuffIconUIScript buffIconUI;
	public GameObject clearPanel;
	public PanelFailedScript panelFailed;
	public EnchanceUIScript enchanceUI;
	public MassageBoxScript msgBox;
	public Slider dangerPercent;
	public Text starText;
	public Text lavorText;
	public bool inDungoen;

	private void Update( )
	{
		if( !pauseMenu.activeSelf && Input.GetKeyDown( KeyCode.Escape ) )
		{
			TogglePauseMenu( );
		}
	}

	public void Awake( )
	{
		if( Instance == null )
			Instance = this;

		ado = GetComponent<AudioSource>( );
	}

	public void UpdateBulletState( int _max, int _now, int _magazine )
	{
		bltBar.SetParam( (float)_now / _max, _now, _magazine );
	}

	public void BtnsSetEnabled( bool b )
	{
		menuBtn.enabled = b;
		mapBtn.enabled = b;
		if( b )
		{
			Time.timeScale = curTimeScale;
		}
		else
		{
			curTimeScale = Time.timeScale;
			Time.timeScale = 0f;
		}
	}

	public void TogglePauseMenu( )
	{
		UI_SE( 17 );
		giveUpBt.SetActive( inDungoen );
		GameMgr.Instance.gamePause = !pauseMenu.activeSelf;
		pauseMenu.SetActive( !pauseMenu.activeSelf );
		BtnsSetEnabled( !pauseMenu.activeSelf );
	}

	public void ToggleInventory( )
	{
		UI_SE( );
		if( inven.gameObject.activeSelf )
			inven.Hide( );
		else
			inven.Show( inDungoen );
	}

	public void ToggleQuestMenu( )
	{
		UI_SE( );
		if( quest.questPopUp.activeSelf )
			quest.QuestUiDisable( );
		else
			quest.QuestUiEnable( );
	}

	public void ToggleSettingMenu( )
	{
		UI_SE( );
	}

	public void ToggleSkillMenu( )
	{
		UI_SE( );
		if( skillui.gameObject.activeSelf )
			skillui.Hide( );
		else
			skillui.Show( );
	}

	public void ToggleGiveUpMenu( )
	{
		UI_SE( );
		Time.timeScale = 1f;
		ShowMsgBox( "획득한 아이템과 미네랄을 모두 잃습니다.\n그래도 포기하시겠습니까?",
					() => GameMgr.Instance.LoadSceneOnFadeAndNoSave( PlanetUtil.TownSceneNumber ) );
	}

	public void ToggleStageUI( )
	{
		UI_SE( );
		if( stageUI.gameObject.activeSelf )
			stageUI.Hide( );
		else
			stageUI.Show( );
	}

	public void ToggleUpgradeMenu( )
	{
		UI_SE( );
		if( !enchanceUI.gameObject.activeSelf )
			enchanceUI.Show( );
		//else
		//	enchanceUI.Hide( );
	}

	public void Die( )
	{
		panelFailed.Show( );
		Time.timeScale = 0.5f;
	}



	public void ShowMsgBox( string _txt = "Debug Check", PlanetUtil.MsgBoxCallBack _cb = null )
	{
		msgBox.Show( _txt, _cb );
	}
	public void HideMsgBox()
	{
		msgBox.Hide( );
	}


	public void UI_SE( int num = 18)
	{
		ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[num] );
	}

	internal void ClearUI( )
	{
		inven.Hide( );
		skillui.Hide( );
		stageUI.Hide( );
		enchanceUI.Hide( );
		panelFailed.Hide( );
		buffIconUI.Clear( );
		quest.QuestUiDisable( );

		pauseMenu.SetActive( false );
		GameMgr.Instance.gamePause = true;
		BtnsSetEnabled( true );

		giveUpBt.SetActive( false );
		clearPanel.SetActive( false );
	}
}
