﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EnchanceSlotScript : MonoBehaviour, IDropHandler
{
	public EnchanceUIScript root;
	public Item item = null;

	public bool dropable;
	public ItemIconScript icon;
	public new Text name;
	public Text enchance;
	public Text type;
	public Text disc;
	public Text option;
	public Text additialOption;

	public virtual void OnDrop( PointerEventData eventData )
	{
		if( !dropable || null == ItemIconScript.s_itemBeingDragged )
			return;

		var newitem = ItemIconScript.s_itemBeingDragged.GetComponent<ItemIconScript>( ).item as EquipItem;

		if( false == ChkItemUseAble( newitem ) )
			return;

		SetItem( newitem );
		GameMgr.Instance.uiMgr.UI_SE( );
	}

	bool ChkItemUseAble( Item _item )
	{
		if( _item.enchanceCount <= 10 ||
			false == GameMgr.Instance.uiMgr.inDungoen ) // Todo 던전 중일때 무조건 false
			return true;

		return false;
	}

	public void SetItem( EquipItem _item )
	{
		var oldItem = item;

		// 아이템 설정 새로운 아이콘 설정
		item = _item;
		if( item != null )
		{
			SetItemInfo( item );
			root.UpdateInfo( item );
		}
		else
		{
			SetItemInfo( null );
			root.UpdateInfo( null );
		}
	}

	public void SetItemInfo( Item _item )
	{
		if( null != _item )
		{
			item = _item;
			icon.SetItemInfo( _item );
			name.text = _item.name;
			enchance.text = (_item.enchanceCount < 10 ) ? "+" + _item.enchanceCount : "MAX";
			name.color = PlanetUtil.itemNameClr[(int)_item.rate];
			type.text = PlanetUtil.GetItemTypeString( _item );
			disc.text = _item.disc;

			if( _item.type < PlanetUtil.ItemType.Consume )
			{
				option.text = PlanetUtil.GetPerfomenceString( _item.pfm, true );
				additialOption.text = PlanetUtil.GetPerfomenceString(  _item.addPfm, false );
			}
			else
			{
				option.text = "x" + _item.nowCount;
				additialOption.text = "";
			}
		}
		else
		{
			item = null;
			icon.SetItemInfo( null );
			name.text = "";
			enchance.text = "";
			type.text = "";
			disc.text = "";

			option.text = "";
			additialOption.text = "";
		}
	}
}
