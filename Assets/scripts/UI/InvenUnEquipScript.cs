﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InvenUnEquipScript : MonoBehaviour, IDropHandler
{
	public virtual void OnDrop( PointerEventData eventData )
	{
		if( ItemIconScript.s_itemBeingDragged == null )
			return;

		if( GameMgr.Instance.uiMgr.inDungoen )
		{
			GameMgr.Instance.uiMgr.ShowMsgBox( "던전에서는 장비를 변경할 수 없습니다." );
			return;
		}
		var icon = ItemIconScript.s_itemBeingDragged.GetComponent<ItemIconScript>( );
		if( icon == null )
			return;

		var newitem = icon.item;
		var slot = ItemIconScript.parrent.GetComponent<ItemSlotScript>( );          // 슬롯에서 왔는지 검사


		if( newitem == null ) // 아이템이 없다면
			return;

		if( slot != null ) // 다른 슬롯에서 왔다면
		{
			slot.SetItem( null );
			GameMgr.Instance.uiMgr.inven.AddItem( newitem );
			GameMgr.Instance.playerData.inven.AddItem( newitem );
		}
	}
}
