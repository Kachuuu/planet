﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpUIScript : MonoBehaviour
{
	private int index;
	public GameObject[] help;
	public string playerPefsKey;

	private void Awake( )
	{
		if(PlayerPrefs.GetInt( playerPefsKey, 0 ) != 0 )
		{
			Off( );
		}
		else
		{
			index = help.Length;
			ToggleHelp( );
			PlayerPrefs.SetInt( playerPefsKey, 1 );
			PlayerPrefs.Save( );
		}
	}

	public void ToggleHelp()
	{
		index = ( index + 1 ) % ( help.Length + 1 );
		for( int i = 0 ; i < help.Length ; ++i )
			help[i].SetActive( index == i );
	}
	
	public void Off()
	{
		for( int i = 0 ; i < help.Length ; ++i )
			help[i].SetActive( false );
		index = help.Length;
	}
	
}
