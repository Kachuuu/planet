﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage1UIScript : MonoBehaviour
{

	private void Start( )
	{
		if( GameMgr.Instance.nowScene == 1 )
		{
			gameObject.SetActive( true );
		}
		else
		{
			gameObject.SetActive( false );
		}
	}

	private void Update( )
	{

		if( GameMgr.Instance.nowScene == 1 )
		{
			gameObject.SetActive( true );
		}
		else
		{
			gameObject.SetActive( false );
		}
	}

}
