﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class InvenItemScript : MonoBehaviour
{
	public ItemIconScript icon;
	public new Text name;
	public Text type;
	public Text disc;
	public Text option;
	public Text additialOption;
	public Item item = null;

	public void SetItemInfo( Item _item )
	{
		if( null != _item )
		{
			item = _item;
			icon.SetItemInfo( _item );
			name.text = _item.name + "+" +  _item.enchanceCount;
			name.color = PlanetUtil.itemNameClr[(int)_item.rate];
			type.text = PlanetUtil.GetItemTypeString( _item );
			disc.text = _item.disc;

			if( _item.type < PlanetUtil.ItemType.Consume )
			{
				option.text = GetOptionText( _item.type, _item.pfm, true );
				additialOption.text = GetOptionText( _item.type, _item.addPfm );
			}
			else
			{
				option.text = "x" + _item.nowCount;
				additialOption.text = "";
			}
		}
		else
		{
			item = null;
			icon.SetItemInfo( null );
			name.text = "";
			type.text = "";
			disc.text = "";
			option.text = "";
			additialOption.text = "";
		}
	}

	string GetOptionText( PlanetUtil.ItemType _type, Performence[] _pfm , bool _bulletInfo = false )
	{
		switch( _type )
		{
		case PlanetUtil.ItemType.Weapon:
		case PlanetUtil.ItemType.Armor:
			return PlanetUtil.GetPerfomenceString( _pfm, _bulletInfo );
		default:
			return "";
		}
	}
}
