﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlanetMonster
{

	public static StatusScript CreateMonster(ObjectPoolManager.PoolType _type, Vector3 _pos )
	{
		switch( _type )
		{
		case ObjectPoolManager.PoolType.damage:		return CreateRedRepTail( _pos );
		case ObjectPoolManager.PoolType.RedReptailBoss:	return CreateRedRepTailBoss( _pos );
		case ObjectPoolManager.PoolType.MaleNaga:		return CreateMaleNaga( _pos );
		case ObjectPoolManager.PoolType.FemaleNaga:		return CreateFemaleNaga( _pos );
		case ObjectPoolManager.PoolType.MaleNagaBoss:	return CreateMaleNagaBoss( _pos );
		case ObjectPoolManager.PoolType.MaleNagaBlack:	return CreateMaleNagaBlack( _pos );
		case ObjectPoolManager.PoolType.FemaleNagaBlack:return CreateFemaleNagaBlack( _pos );
		case ObjectPoolManager.PoolType.FemaleNagaBoss:	return CreateFemaleNagaBoss( _pos );
		case ObjectPoolManager.PoolType.HellHound:		return CreateHellHound( _pos );
		case ObjectPoolManager.PoolType.Wizard:			return CreateWizard( _pos );
		case ObjectPoolManager.PoolType.Skeleton:		return CreateSkeleton( _pos );
		default:
			Debug.LogError( "Not Found type " + _type );
			return null;
		}
	}

	public static StatusScript CreateRedRepTail( Vector3 _pos )
	{
		var monster = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.damage, _pos, Quaternion.identity);
		var stat = monster.GetComponent<MonsterStatusScript>();
		stat.Init( );  //  왜 호출이 안될까
		BoxCollider2D col = (BoxCollider2D)stat.AttackRange;

		MonsterDefSkill.Datas[] Datas1 = new MonsterDefSkill.Datas[]
		{
			new MonsterDefSkill.Datas(1.0f, 3, 2.0f, 3.0f),
		};
		MonsterDefSkill skill1 = new MonsterNormalSkill(col, Datas1, 0.5f, 0.2f, 0.4f, new Vector2(0.56f, 1.4f), new Vector2(2.5f, 2.5f));

		MonsterDefSkill.Datas[] Datas2 = new MonsterDefSkill.Datas[]
		{
			new MonsterDefSkill.Datas(1.2f, 1111, 7.0f,12.0f),
		};
		Vector2[] Data2sub = new Vector2[1] // 대쉬 시 이동량 배율
		{
			new Vector2(10f,1f),
		};
		MonsterDefSkill skill2 = new MonsterDashSkill(col, Datas2, Data2sub, 1.2f, 0.1f, 0.8f, new Vector2(1.05f, 1.3f), new Vector2(3.35f, 2.6f));

		stat.AddSkill( skill2 );
		stat.AddSkill( skill1 );

		stat.AddPhaseData( -1.0f, 1.0f, 1.0f, 2, 0, Color.white );
		stat.keyCategory = "NormalMonster";
		stat.keyName = "RedReptail";

		stat.exp = 10;
		stat.RewardId = 2101;

		return stat;
	}

	public static StatusScript CreateMaleNaga( Vector3 _pos )
	{
		var monster = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.MaleNaga, _pos, Quaternion.identity);
		var stat = monster.GetComponent<MonsterStatusScript>();
		stat.Init( );
		BoxCollider2D col = (BoxCollider2D)stat.AttackRange;

		MonsterDefSkill.Datas[] Datas1 = new MonsterDefSkill.Datas[]
		{   new MonsterDefSkill.Datas(1.0f, 3, 1.5f, 3.0f) };
		MonsterDefSkill skill1 = new MonsterNormalSkill(col, Datas1, 0.3f, 0.1f, 0.5f, new Vector2(0.02f, 1.4f), new Vector2(2.5f, 2.5f));

		//MonsterDefSkill.Datas[] Datas2 = new MonsterDefSkill.Datas[]
		//{   new MonsterDefSkill.Datas(1.0f, 1, 3.0f, 3.0f) };
		//var subDatas2 = new MonsterGoingSkill.SubData[]
		//{   new MonsterGoingSkill.SubData( 1.2f, 5 ) };
		//MonsterDefSkill skill2 = new MonsterGoingSkill(col, Datas1, subDatas2, 0.2f, 0.3f, 0.2f, new Vector2(0.02f, 1.4f), new Vector2(2.5f, 2.5f));

		// 원거리 발사 공격
		MonsterDefSkill.Datas[] Datas2 = new MonsterDefSkill.Datas[]
		{   new MonsterDefSkill.Datas(1.0f, 3, 10f, 3.0f) };
		var subDatas2 = new MonsterShotSkill.SubData[]
		{   new MonsterShotSkill.SubData( 10f, 10f, 1, false, true, 0f, 0f, 1 ) };
		MonsterDefSkill skill2 = new MonsterShotSkill(Datas2, subDatas2, ObjectPoolManager.PoolType.NagaBullet, 0.3f, 0.3f, 0.4f );

		stat.AddSkill( skill1 );
		stat.AddSkill( skill2 );

		stat.AddPhaseData( -1.0f, 1.0f, 1.0f, 2, 0, Color.white );

		stat.keyCategory = "NormalMonster";
		stat.keyName = "MaleNaga";

		stat.exp = 20;
		stat.RewardId = 2102;

		return stat;
	}

	public static StatusScript CreateFemaleNaga( Vector3 _pos )
	{
		var monster = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.FemaleNaga, _pos, Quaternion.identity);
		var stat = monster.GetComponent<MonsterStatusScript>();
		stat.Init( );
		BoxCollider2D col = (BoxCollider2D)stat.AttackRange;

		// 일반 공격
		MonsterDefSkill.Datas[] Datas1 = new MonsterDefSkill.Datas[]
		{   new MonsterDefSkill.Datas(1.0f, 3, 1.5f, 3.0f) };
		MonsterDefSkill skill1 = new MonsterNormalSkill(col, Datas1, 0.5f, 0.1f, 0.4f, new Vector2(0.9f, 1.0f), new Vector2(1.25f, 0.9f));

		// 방사 공격
		MonsterDefSkill.Datas[] Datas2 = new MonsterDefSkill.Datas[]
		{   new MonsterDefSkill.Datas(1.0f, 3, 5f, 3.0f) };
		var subDatas2 = new MonsterShotSkill.SubData[]
		{   new MonsterShotSkill.SubData( 10f, 10f, 1, false, true, 0f, 20f, 5 ) };
		MonsterDefSkill skill2 = new MonsterShotSkill(Datas2, subDatas2, ObjectPoolManager.PoolType.NagaBullet, 0.5f, 0.4f, 0.4f );

		stat.AddSkill( skill1 );
		stat.AddSkill( skill2 );

		stat.AddPhaseData( -1.0f, 1.0f, 1.0f, 2, 0, Color.white );

		stat.keyCategory = "NormalMonster";
		stat.keyName = "FemaleNaga";

		stat.exp = 40;
		stat.RewardId = 2103;

		return stat;
	}

	public static StatusScript CreateMaleNagaBlack( Vector3 _pos )
	{
		var monster = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.MaleNagaBlack, _pos, Quaternion.identity);
		var stat = monster.GetComponent<MonsterStatusScript>();
		stat.Init( );
		BoxCollider2D col = (BoxCollider2D)stat.AttackRange;

		MonsterDefSkill.Datas[] Datas1 = new MonsterDefSkill.Datas[]
		{   new MonsterDefSkill.Datas(1.0f, 3, 1.5f, 3.0f) };
		MonsterDefSkill skill1 = new MonsterNormalSkill(col, Datas1, 0.3f, 0.1f, 0.5f, new Vector2(0.02f, 1.4f), new Vector2(2.5f, 2.5f));

		MonsterDefSkill.Datas[] Datas2 = new MonsterDefSkill.Datas[]
		{   new MonsterDefSkill.Datas(1.0f, 1, 3.0f, 3.0f) };
		var subDatas2 = new MonsterGoingSkill.SubData[]
		{   new MonsterGoingSkill.SubData( 2f, 5 ) };
		MonsterDefSkill skill2 = new MonsterGoingSkill(col, Datas2, subDatas2, 0.2f, 0.3f, 0.2f, new Vector2(0.02f, 1.4f), new Vector2(2.5f, 2.5f));

		stat.AddSkill( skill1 );
		stat.AddSkill( skill2 );

		stat.AddPhaseData( -1.0f, 1.0f, 1.0f, 2, 0, Color.white );

		stat.keyCategory = "NormalMonster";
		stat.keyName = "MaleNagaBlack";

		stat.exp = 10;
		stat.RewardId = 2104;

		return stat;
	}

	public static StatusScript CreateFemaleNagaBlack( Vector3 _pos )
	{
		var monster = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.FemaleNagaBlack, _pos, Quaternion.identity);
		var stat = monster.GetComponent<MonsterStatusScript>();
		stat.Init( );
		BoxCollider2D col = (BoxCollider2D)stat.AttackRange;

		// 일반 공격
		MonsterDefSkill.Datas[] Datas1 = new MonsterDefSkill.Datas[]
		{   new MonsterDefSkill.Datas(1.0f, 3, 1.5f, 3.0f) };
		MonsterDefSkill skill1 = new MonsterNormalSkill(col, Datas1, 0.5f, 0.1f, 0.4f, new Vector2(0.9f, 1.0f), new Vector2(1.25f, 0.9f));

		// 원거리 발사 공격
		MonsterDefSkill.Datas[] Datas2 = new MonsterDefSkill.Datas[]
		{   new MonsterDefSkill.Datas(1.0f, 3, 10f, 3.0f) };
		var subDatas2 = new MonsterShotSkill.SubData[]
		{   new MonsterShotSkill.SubData( 10f, 10f, 1, false, true, 0f, 0f, 1 ) };
		MonsterDefSkill skill2 = new MonsterShotSkill(Datas2, subDatas2, ObjectPoolManager.PoolType.NagaBullet, 0.3f, 0.3f, 0.4f );

		// 방사 공격
		MonsterDefSkill.Datas[] Datas3 = new MonsterDefSkill.Datas[]
		{   new MonsterDefSkill.Datas(1.0f, 3, 5f, 3.0f) };
		var subDatas3 = new MonsterShotSkill.SubData[]
		{   new MonsterShotSkill.SubData( 10f, 10f, 1, false, true, 0f, 20f, 5 ) };
		MonsterDefSkill skill3 = new MonsterShotSkill(Datas3, subDatas3, ObjectPoolManager.PoolType.NagaBullet, 0.5f, 0.4f, 0.4f );

		stat.AddSkill( skill1 );
		stat.AddSkill( skill2 );
		stat.AddSkill( skill3 );

		stat.AddPhaseData( -1.0f, 1.0f, 1.0f, 3, 0, Color.white );

		stat.keyCategory = "NormalMonster";
		stat.keyName = "FemaleNagaBlack";

		stat.exp = 40;
		stat.RewardId = 2105;

		return stat;
	}

	public static StatusScript CreateHellHound( Vector3 _pos )
	{
		var monster = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.HellHound, _pos, Quaternion.identity);
		var stat = monster.GetComponent<MonsterStatusScript>();
		stat.Init( );
		BoxCollider2D col = (BoxCollider2D)stat.AttackRange;

		// 일반 공격
		MonsterDefSkill.Datas[] Datas1 = new MonsterDefSkill.Datas[]
		{   new MonsterDefSkill.Datas(1.0f, 9999, 1.05f, 1.0f) };
		MonsterDefSkill skill1 = new MonsterNormalSkill(col, Datas1, 0.16f, 0.8f, 0.16f, new Vector2(0.7f, 1.3f), new Vector2(1.4f, 1.7f));

		stat.AddSkill( skill1 );

		stat.AddPhaseData( -1.0f, 1.0f, 1.0f, 1, 0, Color.white );

		stat.keyCategory = "NormalMonster";
		stat.keyName = "HellHound";

		stat.exp = 10;
		stat.RewardId = 2102;

		return stat;
	}

	public static StatusScript CreateWizard( Vector3 _pos )
	{
		var monster = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.Wizard, _pos, Quaternion.identity);
		var stat = monster.GetComponent<MonsterStatusScript>();
		stat.Init( );
		BoxCollider2D col = (BoxCollider2D)stat.AttackRange;

		MonsterDefSkill.Datas[] Datas2 = new MonsterDefSkill.Datas[]
		{   new MonsterDefSkill.Datas(1.0f, 9999, 10.0f, 2.0f) };
		var subDatas2 = new MonsterShotSkill.SubData[]
		{   new MonsterShotSkill.SubData( 15f, 10f, 1, false, true, 0f, 0f, 1 ) };
		MonsterDefSkill skill2 = new MonsterShotSkill( Datas2, subDatas2, ObjectPoolManager.PoolType.NagaBullet, 0.2f, 0.3f, 0.2f );

		stat.AddSkill( skill2 );

		stat.AddPhaseData( -1.0f, 1.0f, 1.0f, 1, 0, Color.white );

		stat.keyCategory = "NormalMonster";
		stat.keyName = "Wizard";

		stat.exp = 10;
		stat.RewardId = 2102;

		return stat;
	}
	
	public static StatusScript CreateSkeleton( Vector3 _pos )
	{
		var monster = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.Skeleton, _pos, Quaternion.identity);
		var stat = monster.GetComponent<MonsterStatusScript>();
		stat.Init( );
		BoxCollider2D col = (BoxCollider2D)stat.AttackRange;

		MonsterDefSkill.Datas[] Datas1 = new MonsterDefSkill.Datas[]
		{   new MonsterDefSkill.Datas(1.0f, 9999, 1.5f, 3.0f) };
		MonsterDefSkill skill1 = new MonsterNormalSkill(col, Datas1, 0.56f, 0.08f, 0.8f, new Vector2(0.8f, 0.85f), new Vector2(1.5f, 1.7f));

		stat.AddSkill( skill1 );

		stat.AddPhaseData( -1.0f, 1.0f, 1.0f, 1, 0, Color.white );

		stat.keyCategory = "NormalMonster";
		stat.keyName = "Skeleton";

		stat.exp = 10;
		stat.RewardId = 2102;

		return stat;
	}

	public static StatusScript CreateRedRepTailBoss( Vector3 _pos )
	{
		var monster = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.RedReptailBoss, _pos, Quaternion.identity);
		var stat = monster.GetComponent<MonsterStatusScript>();
		stat.Init( );
		var col = (BoxCollider2D)stat.AttackRange;

		MonsterDefSkill.Datas[] Datas1 = new MonsterDefSkill.Datas[4]
		{
			new MonsterDefSkill.Datas(1.0f, 3, 3.0f, 3.0f),
			new MonsterDefSkill.Datas(1.2f, 3, 2.0f, 3.0f),
			new MonsterDefSkill.Datas(1.0f, 3, 2.0f, 3.0f),
			new MonsterDefSkill.Datas(1.0f, 3, 2.0f, 3.0f)
		};
		MonsterDefSkill skill1 = new MonsterNormalSkill(col, Datas1, 0.5f, 0.2f, 0.4f, new Vector2(0.7f, 1.47f), new Vector2(2.25f, 2.39f));

		MonsterDefSkill.Datas[] Datas2 = new MonsterDefSkill.Datas[4]
		{
			new MonsterDefSkill.Datas(1.2f, 2, 9.0f,12.0f),
			new MonsterDefSkill.Datas(1.6f, 2, 9.0f,12.0f),
			new MonsterDefSkill.Datas(1.8f, 2, 9.0f,12.0f),
			new MonsterDefSkill.Datas(1.4f, 2, 9.0f,12.0f)
		};
		Vector2[] Data2sub = new Vector2[4] // 대쉬 시 이동량 배율
		{
			new Vector2(10f,1f),
			new Vector2(10f,1f),
			new Vector2(10f,1f),
			new Vector2(10f,1f),
		};
		MonsterDefSkill skill2 = new MonsterDashSkill(col, Datas2, Data2sub, 1.2f, 0.1f, 0.8f, new Vector2(1.05f, 1.3f), new Vector2(3.35f, 2.6f));

		MonsterDefSkill.Datas[] Datas3 = new MonsterDefSkill.Datas[4]
		{
			new MonsterDefSkill.Datas(1.0f, 1, 3.0f, 3.0f),
			new MonsterDefSkill.Datas(1.1f, 1, 3.0f, 3.0f),
			new MonsterDefSkill.Datas(1.2f, 1, 3.0f, 3.0f),
			new MonsterDefSkill.Datas(1.4f, 1, 3.0f, 3.0f)
		};
		int[] Data3sub = new int[4] // 연속 공격 횟수
		{
			3,3,4,6
		};
		MonsterDefSkill skill3 = new MonsterMultiSkill(col, Datas3, Data3sub, new Vector2(0.7f, 1.47f), new Vector2(2.25f, 2.39f));

		MonsterDefSkill.Datas[] Datas4 = new MonsterDefSkill.Datas[4]
		{
			new MonsterDefSkill.Datas(1.0f, 1, 9.0f, 6.0f),
			new MonsterDefSkill.Datas(1.1f, 1, 9.0f, 6.0f),
			new MonsterDefSkill.Datas(1.2f, 1, 9.0f, 6.0f),
			new MonsterDefSkill.Datas(1.4f, 5, 9.0f, 6.0f)
		};
		int[] Data4sub = new int[4] // 연속 던지기 횟수
		{
			1,1,2,6
		};
		MonsterDefSkill skill4 = new MonsterThrowSkill(ObjectPoolManager.PoolType.ThrowRock, Datas4, Data4sub);

		stat.AddSkill( skill2 );
		stat.AddSkill( skill1 );
		stat.AddSkill( skill3 );
		stat.AddSkill( skill4 );

		//					Hp	 Atk	Mov	Skil sp	Clr
		stat.AddPhaseData( 0.7f, 1.0f, 1.0f, 4, 0, Color.white );
		stat.AddPhaseData( 0.3f, 1.4f, 1.2f, 4, 3, new Color( 1f, 0.8f, 0.8f ) );
		stat.AddPhaseData( -1.0f, 1.5f, 1.4f, 4, 3, new Color( 1f, 0.4f, 0.4f ) );

		stat.keyCategory = "BossMonster";
		stat.keyName = "RedRepTailBoss";

		stat.exp = 100;
		stat.RewardId = 2201;

		return stat;
	}

	public static IEnumerator CoRedRepTailBossSpeacialPattern( MonsterStatusScript _mstat )
	{
		yield return null;
	}

	public static StatusScript CreateMaleNagaBoss( Vector3 _pos )
	{
		var monster = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.MaleNagaBoss, _pos, Quaternion.identity);
		var stat = monster.GetComponent<MonsterStatusScript>();
		stat.Init( );
		BoxCollider2D col = (BoxCollider2D)stat.AttackRange;

		MonsterDefSkill.Datas[] Datas1 = new MonsterDefSkill.Datas[]
		{
			new MonsterDefSkill.Datas(1.0f, 3, 3f, 3.0f),
			new MonsterDefSkill.Datas(1.2f, 3, 3f, 3.0f),
			new MonsterDefSkill.Datas(1.4f, 3, 3f, 3.0f),
			new MonsterDefSkill.Datas(2.0f, 3, 3f, 3.0f)
		};
		MonsterDefSkill skill1 = new MonsterNormalSkill(col, Datas1, 0.3f, 0.1f, 0.5f, new Vector2(0.02f, 1.4f), new Vector2(2.5f, 2.5f));

		MonsterDefSkill.Datas[] Datas2 = new MonsterDefSkill.Datas[]
		{
			new MonsterDefSkill.Datas(1.0f, 1, 6.0f, 3.0f),
			new MonsterDefSkill.Datas(1.0f, 1, 6.0f, 3.0f),
			new MonsterDefSkill.Datas(1.0f, 1, 6.0f, 3.0f),
			new MonsterDefSkill.Datas(3.0f, 5, 999.0f, 3.0f)
		};
		var subDatas2 = new MonsterGoingSkill.SubData[]
		{
			new MonsterGoingSkill.SubData( 1.2f, 5 ),
			new MonsterGoingSkill.SubData( 1.2f, 5 ),
			new MonsterGoingSkill.SubData( 1.2f, 5 ),
			new MonsterGoingSkill.SubData( 1.2f, 999 )
		};
		MonsterDefSkill skill2 = new MonsterGoingSkill(col, Datas2, subDatas2, 0.2f, 0.3f, 0.2f, new Vector2(0.02f, 1.4f), new Vector2(2.5f, 2.5f));

		// 따라가는 불 공격
		MonsterDefSkill.Datas[] Datas3 = new MonsterDefSkill.Datas[]
		{
			new MonsterDefSkill.Datas(0.9f, 1, 10f, 3.0f),
			new MonsterDefSkill.Datas(0.9f, 1, 10f, 3.0f),
			new MonsterDefSkill.Datas(1.2f, 1, 10f, 3.0f),
			new MonsterDefSkill.Datas(1.5f, 1, 10f, 3.0f)
		};
		var subDatas3 = new MonsterShotSkill.SubData[]
		{
			new MonsterShotSkill.SubData( 10f, 5f, 1, true, true, 2f,0f, 1 ),
			new MonsterShotSkill.SubData( 10f, 5f, 1, true, true, 3f,0f, 1 ),
			new MonsterShotSkill.SubData( 15f, 5f, 1, true, true, 3f,0f, 1 ),
			new MonsterShotSkill.SubData( 20f, 7f, 15, true, true, 3f,0f, 1 )
		};
		MonsterDefSkill skill3 = new MonsterShotSkill(Datas3, subDatas3, ObjectPoolManager.PoolType.NagaBullet, 0.3f, 0.3f, 0.4f );

		stat.AddSkill( skill1 );
		stat.AddSkill( skill2 );
		stat.AddSkill( skill3 );

		stat.AddPhaseData( 0.7f, 1.0f, 1.0f, 2, 0, Color.white );
		stat.AddPhaseData( 0.3f, 1.0f, 1.0f, 3, 1, new Color( 1f, 0.7f, 0.7f ) );
		stat.AddPhaseData( -1.0f, 1.0f, 1.0f, 3, 1, new Color( 1f, 0.4f, 0.4f ) );

		stat.phaseAction = CoMaleNagaBossSpeacialPattern;

		stat.keyCategory = "BossMonster";
		stat.keyName = "MaleNagaBoss";

		stat.exp = 300;
		stat.RewardId = 2202;

		return stat;
	}

	public static IEnumerator CoMaleNagaBossSpeacialPattern( MonsterStatusScript _mstat )
	{
		// 할 수 있는지 검사
		RaycastHit2D[] hitInfo = new RaycastHit2D[1];
		var rayPos = _mstat.transform.position + new Vector3( 0f, 0.2f, 0f );
		var rayDir = new Vector2( 1f, 0f );
		var filter = new ContactFilter2D();
		filter.layerMask = LayerMask.GetMask( "Ground" );
		filter.useLayerMask = true;

		if( _mstat.lookLeft )
			rayDir *= -1;
		var cnt = Physics2D.Raycast( rayPos, rayDir, filter, hitInfo );
		if( cnt == 0 ) // 할 수 없다면 스킵
			yield break;

		var tgPos = hitInfo[0].point - rayDir;
		var oldSort = _mstat.sr.sortingOrder;

		_mstat.hpBar.Show( false );
		_mstat.sr.sortingOrder = 0; // 그라운드에 가려지도록
		_mstat.statable[(int)PlanetUtil.CharacterStatables.Hit].Reset( false );
		_mstat.State |= PlanetUtil.CharacterState.Attack;
		_mstat.rb.isKinematic = true;

		var acc = 0f;
		var time = 2f;
		var srcPos = _mstat.transform.position;
		var dstPos = srcPos + new Vector3(0f,-5f,0f);
		while( acc < time ) // 땅으로 꺼지기
		{
			acc += Time.deltaTime;
			_mstat.transform.position = Vector3.Lerp( srcPos, dstPos, acc / time );
			yield return null;
		}

		_mstat.cs.FlipXToggle( );
		acc = 0;
		srcPos = tgPos + new Vector2( 0f, -5.2f );
		dstPos = tgPos + new Vector2( 0f, -0.2f );
		while( acc < time ) // 솟아오르기
		{
			acc += Time.deltaTime;
			_mstat.transform.position = Vector3.Lerp( srcPos, dstPos, acc / time );
			yield return null;
			_mstat.cs.FlipX( _mstat.lookLeft ); // 회전되는 것을 막기 위해....
		}

		_mstat.sr.sortingOrder = oldSort;
		_mstat.statable[(int)PlanetUtil.CharacterStatables.Hit].Reset( true );
		_mstat.State &= ~PlanetUtil.CharacterState.Attack;
		_mstat.rb.isKinematic = false;
		_mstat.hpBar.Show( true );

		yield return null;
	}

	public static StatusScript CreateFemaleNagaBoss( Vector3 _pos )
	{
		var monster = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.FemaleNagaBoss, _pos, Quaternion.identity);
		var stat = monster.GetComponent<MonsterStatusScript>();
		stat.Init( );
		BoxCollider2D col = (BoxCollider2D)stat.AttackRange;

		// 원거리 공격
		MonsterDefSkill.Datas[] Datas1 = new MonsterDefSkill.Datas[]
		{
			new MonsterDefSkill.Datas(1.0f, 2, 10f, 3.0f),
			new MonsterDefSkill.Datas(1.1f, 2, 10f, 3.0f),
			new MonsterDefSkill.Datas(1.2f, 2, 10f, 3.0f),
			new MonsterDefSkill.Datas(2.0f, 2, 10f, 3.0f),
		};
		var subDatas1 = new MonsterShotSkill.SubData[]
		{
			new MonsterShotSkill.SubData( 10f, 8f, 1, false, true, 0f, 0f, 1 ),
			new MonsterShotSkill.SubData( 10f, 8f, 1, false, true, 0f, 0f, 1 ),
			new MonsterShotSkill.SubData( 10f, 8f, 1, false, true, 0f, 0f, 1 ),
			new MonsterShotSkill.SubData( 10f, 8f, 1, false, true, 0f, 0f, 1 ),
		};
		MonsterDefSkill skill1 = new MonsterShotSkill(Datas1, subDatas1, ObjectPoolManager.PoolType.NagaBullet, 0.5f, 0.4f, 0.4f );

		// 방사형 공격
		MonsterDefSkill.Datas[] Datas2 = new MonsterDefSkill.Datas[]
		{
			new MonsterDefSkill.Datas(1.0f, 2, 10f, 3.0f),
			new MonsterDefSkill.Datas(1.1f, 2, 10f, 3.0f),
			new MonsterDefSkill.Datas(1.2f, 2, 10f, 3.0f),
			new MonsterDefSkill.Datas(2.0f, 2, 10f, 3.0f),
		};
		var subDatas2 = new MonsterShotSkill.SubData[]
		{
			new MonsterShotSkill.SubData( 5f, 6f, 1, false, true, 0f, 20f, 4 ),
			new MonsterShotSkill.SubData( 5f, 6f, 1, false, true, 0f, 30f, 6 ),
			new MonsterShotSkill.SubData( 5f, 6f, 1, false, true, 0f, 40f, 8 ),
			new MonsterShotSkill.SubData( 5f, 6f, 5, false, true, 0f, 90f, 15 ),
		};
		MonsterDefSkill skill2 = new MonsterShotSkill(Datas2, subDatas2, ObjectPoolManager.PoolType.NagaBullet, 0.5f, 0.4f, 0.4f );

		// 추적
		MonsterDefSkill.Datas[] Datas3 = new MonsterDefSkill.Datas[]
		{
			new MonsterDefSkill.Datas(1.0f, 3, 1.5f, 3.0f),
			new MonsterDefSkill.Datas(1.2f, 3, 1.5f, 3.0f),
			new MonsterDefSkill.Datas(1.4f, 3, 1.5f, 3.0f),
			new MonsterDefSkill.Datas(2.0f, 3, 1.5f, 3.0f),
		};
		var subDatas3 = new MonsterShotSkill.SubData[]
		{
			new MonsterShotSkill.SubData( 5f, 6f, 1, true, true, 2f, 0f, 1 ),
			new MonsterShotSkill.SubData( 5f, 6f, 1, true, true, 4f, 0f, 1 ),
			new MonsterShotSkill.SubData( 5f, 6f, 1, true, true, 4f, 0f, 1 ),
			new MonsterShotSkill.SubData( 5f, 6f, 1, true, true, 4f, 0f, 1 ),
		};
		MonsterDefSkill skill3 = new MonsterShotSkill( Datas3, subDatas3, ObjectPoolManager.PoolType.NagaBullet, 0.5f, 0.4f, 0.4f );

		stat.AddSkill( skill1 );
		stat.AddSkill( skill2 );
		stat.AddSkill( skill3 );

		stat.AddPhaseData( 0.7f, 1.0f, 1.0f, 3, 0, Color.white );
		stat.AddPhaseData( 0.3f, 1.0f, 1.0f, 3, 1, new Color( 1f, 0.7f, 0.7f ) );
		stat.AddPhaseData( -1.0f, 1.0f, 1.0f, 3, 1, new Color( 1f, 0.4f, 0.4f ) );

		stat.phaseAction = CoFemaleNagaBossSpeacialPattern;

		stat.keyCategory = "BossMonster";
		stat.keyName = "FemaleNagaBoss";

		stat.exp = 600;
		stat.RewardId = 2203;

		return stat;
	}

	public static IEnumerator CoFemaleNagaBossSpeacialPattern( MonsterStatusScript _mstat )
	{
		if( _mstat.specialSkill == null || false == ( _mstat.specialSkill is MonsterShotSkill ) )
		{
			Debug.Log( "특수 패턴 불가" );
			yield break;
		}

		_mstat.cs.StartCoroutine( _mstat.specialSkill.CoSkill( _mstat ) );
		var list = ((MonsterShotSkill)_mstat.specialSkill).blts;

		while( list.Count != 0 )
		{
			for( int i = 0 ; i < list.Count ; ++i )
			{
				if( list[i].activeSelf )
					break;
				else
					list.RemoveAt( i-- );
			}
			yield return new WaitForSeconds( 0.5f );
		}

		_mstat.specialSkill = null;
	}

}
