﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DevelopMgr : MonoBehaviour
{
	public BoxCollider2D camRect;
	public Transform startTrans;

	public Transform monsterTrans;
	private StatusScript monStat;
	public void Start( )
	{
		//		StartCoroutine( DevelopMode( ) );
		StartCoroutine( Tutorial( ) );
	}

	bool box;
	bool monsterzen;
	bool monsterkill;
	public IEnumerator Tutorial( )
	{
		yield return 0;
		GameMgr.Instance.MakePlayer( startTrans.position );
		Camera.main.GetComponent<SimpleFollowCam>( ).tg = GameMgr.Instance.Player.transform;
		Camera.main.GetComponent<SimpleFollowCam>( ).CalcRect( camRect );
		while( true )
		{
			if( Input.GetKeyDown( KeyCode.Alpha8 ) )
			{
				PlanetMonster.CreateMonster( ObjectPoolManager.PoolType.Skeleton, monsterTrans.position );
			}
			yield return 0;
		}
	}
	public IEnumerator DevelopMode( )
	{
		yield return 0;
		GameMgr.Instance.MakePlayer( new Vector3( 5f, 1f, 0f ) );
		Camera.main.GetComponent<SimpleFollowCam>( ).tg = GameMgr.Instance.Player.transform;
		Camera.main.GetComponent<SimpleFollowCam>( ).CalcRect( camRect );
		//yield return 0;

		//for( int i = 0 ; i < 6 ; ++i )
		//{
		//	PlayerData.Instance.questInfoList.Add( DataBaseManager.Instance.GetQuest( i ) );
		//}
		//QuestMgr.Instance.QuestEnableCheck( );

		while( true )
		{
			if( Input.GetKeyDown( KeyCode.Alpha8 ) )
			{
				PlanetMonster.CreateRedRepTail(
					new Vector3( UnityEngine.Random.Range( -32f, 32f ),
					UnityEngine.Random.Range( 2f, 20f ), 0f ) );
			}
			if( Input.GetKeyDown( KeyCode.Alpha9 ) )
			{
				PlanetMonster.CreateRedRepTailBoss(
					new Vector3( UnityEngine.Random.Range( -32f, 32f ),
					UnityEngine.Random.Range( 2f, 20f ), 0f ) );
			}
			yield return 0;
		}
	}
}