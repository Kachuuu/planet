﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterControlScript : ControlScript
{
	MonsterStatusScript mstat;
	float idleMoveDelay = 0f; // idle 상태에서 갔다 멈췄다를 하기 위한 변수

	protected override void Awake( )
	{
		base.Awake( );
		mstat = GetComponent<MonsterStatusScript>( );
	}

	public bool IsFollowing( )
	{
		if( mstat.Tg == null )
			return false;

		var dir = transform.position - mstat.Tg.transform.position;
		if( dir.magnitude > mstat.FollowDistance )
		{
			mstat.Tg = null;
			return false;
		}

		return true;
	}

	public bool ChkPhase( )
	{
		if( mstat.HpRate <= mstat.phaseData[mstat.nowPhase].trigerHpRate )
		{
			++mstat.nowPhase;
			for( int i = 0 ; i < mstat.skills.Count ; ++i )
			{
				mstat.skills[i].level = mstat.nowPhase;
				sr.color = mstat.phaseData[mstat.nowPhase].clr;
			}
			mstat.specialSkill = mstat.skills[mstat.phaseData[mstat.nowPhase].specialSkillIndex];
			mstat.skills[mstat.phaseData[mstat.nowPhase].specialSkillIndex].level = 3; // 스페셜 레벨

			// 포효 이펙트
			Camera.main.GetComponent<SimpleFollowCam>( ).ShakeCamera( 0.5f, 0.5f, 0f, 2.5f, true, 0.5f );

			return true;
		}
		return false;
	}

	public bool PhaseAction( )
	{
		if( mstat.phaseAction != null )
		{
			StartCoroutine( mstat.phaseAction( mstat ) );
			return true;
		}
		return false;
	}


	public override void Death( )
	{
		base.Death( );

		int mineral;
		var items = GameMgr.Instance.dbMgr.GetDropItems( mstat.RewardId, out mineral );
		GameMgr.CreateDropItem( mstat.centerPos.position, items, mineral, (stat.type == PlanetUtil.CharacterTypes.bossmonster) );

		GameMgr.Instance.Player.AddExp( mstat.exp );
	}

	public override bool Attack( )
	{
		if( stat.coAttack == null )
		{
			// Y 축 무시
			if( stat.type != PlanetUtil.CharacterTypes.bossmonster &&                   // 일반 몬스터가
				( stat.transform.position.y - mstat.Tg.transform.position.y ) > 0.5f )  // Y축 0.5 이상 차이 나면
				return false;                                                           // 공격 안함

			var dist = (transform.position - mstat.Tg.transform.position).magnitude;

			// 스페셜 공격
			if( mstat.specialSkill != null )
			{
				if( ( (MonsterDefSkill)mstat.specialSkill ).Able( dist ) )
				{
					stat.coAttack = StartCoroutine( mstat.specialSkill.CoSkill( mstat ) );
					mstat.specialSkill = null;
					Debug.Log( "Speacial" );
					return true;
				}
			}
			else
			{
				var ables = new List<int>( mstat.skills.Count );

				// 사용가능한 스킬 색출
				for( int i = 0 ; i < mstat.phaseData[mstat.nowPhase].ableSkillCount ; ++i )
				{
					if( ( (MonsterDefSkill)mstat.skills[i] ).Able( dist ) )
						ables.Add( i );
				}

				if( ables.Count != 0 )
				{
					// 랜덤한 스킬 사용
					var idx = ables[Random.Range( 0, ables.Count )];
					//Debug.Log( idx + " " + mstat.nowPhase );
					stat.coAttack = StartCoroutine( mstat.skills[idx].CoSkill( mstat ) );

					// 나머지 리셋
					for( int i = 0 ; i < mstat.skills.Count ; ++i )
					{
						if( i == idx )
							continue;

						( (MonsterDefSkill)mstat.skills[i] ).RepeatReset( );
					}
					return true;
				}
			}
		}
		return false;
	}


	public override bool Jump( )
	{
		stat.velocity.y = stat[PlanetUtil.CharacterStat.JumpPower] * stat[PlanetUtil.CharacterStat.JumpCount];
		stat.State |= PlanetUtil.CharacterState.Air;
		stat.statable[(int)PlanetUtil.CharacterStatables.Jump].SetTimer( 3f );
		jumping = 2;
		stat.NowStanding = false;
		return true;
	}

	public override bool Move( )
	{
		if( idleMoveDelay < Time.time )
		{
			idleMoveDelay = Time.time + Random.Range( 0f, 5f );
			stat.State |= PlanetUtil.CharacterState.Idle;
			stat.State &= ~PlanetUtil.CharacterState.Move;
			return false;
		}

		if( WallChkRangeIn( ) || !MovableChkRangeIn( ) )
		{
			idleMoveDelay = Time.time + Random.Range( 0f, 5f );
			stat.State |= PlanetUtil.CharacterState.Idle;
			stat.State &= ~PlanetUtil.CharacterState.Move;
			FlipXToggle( );
			return false;
		}

		stat.velocity.x = transform.right.x * stat[PlanetUtil.CharacterStat.MoveSpeed] * ( mstat.phaseData[mstat.nowPhase] ).movespeedScale;

		if( Mathf.Abs( stat.velocity.x ) > 0.1 )
			FlipX( stat.velocity.x < 0 );

		return true;
	}


	public override void Idle( )
	{
		if( idleMoveDelay < Time.time )
		{
			idleMoveDelay = Time.time + Random.Range( 0f, 5f );
			stat.State |= PlanetUtil.CharacterState.Move;
			stat.State &= ~PlanetUtil.CharacterState.Idle;
		}
	}

	// 적용된다면 true. 아니라면 false
	// Todo  피격 안된느 경우 때문에 임시로 오버라이딩 함. 반드시 오버라이딩 없이 되게 할 것
	public override bool Demage( StatusScript _stat, float add = 0f, float scale = 1f, PlanetUtil.AttackType type = PlanetUtil.AttackType.Melee, bool stuck = false, float _stun = 0f )
	{
		if( stat.HpRate == 0f )
			return false;
		if( !stat.statable[(int)PlanetUtil.CharacterStatables.Hit].Able( ) )
			return false;

		mstat.Tg = _stat;
		mstat.State |= PlanetUtil.CharacterState.Trace;
		//stat.State |= PlanetUtil.CharacterState.Hit;

		// Todo 데미지 공식
		bool cri = Random.Range(0f,1f) < _stat[PlanetUtil.CharacterStat.CriticalRate];
		var dmg = (_stat[PlanetUtil.CharacterStat.Attack] + add) * scale * Random.Range(0.935f, 1.065f)
					* ((cri) ? 2f : 1f );

		PlayMonsterHitSound( );

		stat.Demage( dmg );
		DemagePrint( dmg, new Color( 0.1f, 0.9f, 0.9f ), cri );

		if( stat.HpRate == 0f )
		{
			AllStopCoroutine( );
			GetComponentInChildren<StunEffectScript>( ).SetStun( 0f );
			return true;
		}

		// if Stun?
		if( _stun != 0f && stat.type != PlanetUtil.CharacterTypes.bossmonster ) // 보스몬스터는 제외
		{
			AllStopCoroutine( );
			stat.State = PlanetUtil.CharacterState.Stun;
			stat.statable[(int)PlanetUtil.CharacterStatables.Stun].SetTimer( _stun );
			GetComponentInChildren<StunEffectScript>( ).SetStun( _stun );
		}
		// if not Stuck?
		else if( ( stat.State & ( PlanetUtil.CharacterState.Attack | PlanetUtil.CharacterState.Skill | PlanetUtil.CharacterState.Reload ) ) != 0 &&  // 스킬 또는 공격, 페이징 전환 중일때
			( ( stat.State & PlanetUtil.CharacterState.SuperArmor ) != 0 || stuck == false ) )            // 슈퍼아머가 있거나
		{                                                                                                   // stuck 공격이 아닐때
			return true;                                                                                    // 경직 없음
		}



		if( null != stat.coStuck )
			StopCoroutine( stat.coStuck );
		if( stat.HpRate != 0f )
			stat.coStuck = StartCoroutine( CoStuck( _stat ) );

		return true;
	}

	public override IEnumerator CoStuck( StatusScript _stat )
	{
		stat.State |= PlanetUtil.CharacterState.Stuck;
		am.SetTrigger( "Damage" );

		stat.velocity = new Vector2( 0.2f, 0.2f );
		//col.enabled = false;
		//yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.ReHitTime] );
		//col.enabled = true;
		yield return null;

		stat.State &= ~PlanetUtil.CharacterState.Stuck;

		stat.coStuck = null;
	}

	public bool AwaRangeIn( )
	{
		var go = RangeIn( mstat.awaRange , "Player" );
		if( null != go )
		{
			mstat.Tg = go.GetComponent<StatusScript>( );
			return true;
		}
		return false;
	}

	public bool AtkRangeIn( )
	{
		//return RangeIn( mstat.atkRange, "Player" ) != null;
		return false;
	}

	public bool WallChkRangeIn( )
	{
		return RangeIn( mstat.wallChkRange, "Ground" ) != null;
	}

	public bool MovableChkRangeIn( )
	{
		return RangeIn( mstat.MovableChkRange, "Ground" ) != null;
	}

	Collider2D[] col2ds = new Collider2D[10];   // 충돌 중인 모든 콜리더들
	public GameObject RangeIn( Collider2D _col, string _tag )
	{
		int cnt = _col.GetContacts( col2ds );
		if( cnt != 0 )
			for( int i = 0 ; i < cnt ; ++i )
				if( col2ds[i].tag == _tag )
					return col2ds[i].gameObject;

		return null;
	}

	public virtual void Trace( )
	{
		if( mstat.State != PlanetUtil.CharacterState.Trace )
			mstat.State = PlanetUtil.CharacterState.Trace;

		var tgTf = mstat.Tg.transform;
		var x =  stat[PlanetUtil.CharacterStat.MoveSpeed] * (mstat.phaseData[mstat.nowPhase]).movespeedScale;
		var distX = tgTf.position.x - transform.position.x;

		if( ( tgTf.position - transform.position ).x < 0f )
			x *= -1f;

		if( Mathf.Abs( x ) > 0.1 ) // 회전
			FlipX( x < 0 );

		if( Mathf.Abs( distX ) < mstat.dontFollowDist ) //너무 가까우면 움직이지 않는다.
			return;

		stat.velocity.x = x;
	}


	public void PlayMonsterHitSound()
	{
		switch( GetComponent<GOScript>( ).Type )
		{
		case ObjectPoolManager.PoolType.damage:		
		case ObjectPoolManager.PoolType.RedReptailBoss:
			stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_M_RedTail_Hit] );
			return;
		case ObjectPoolManager.PoolType.FemaleNaga:
		case ObjectPoolManager.PoolType.MaleNagaBoss:
		case ObjectPoolManager.PoolType.FemaleNagaBoss:
		case ObjectPoolManager.PoolType.MaleNagaBlack:
		case ObjectPoolManager.PoolType.FemaleNagaBlack:
		case ObjectPoolManager.PoolType.HellHound:
		case ObjectPoolManager.PoolType.Wizard:
		case ObjectPoolManager.PoolType.Skeleton:
			stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_M_M_Naga_Hit] );
			return;
		}
	}

	public void PlayMonsterAttackSound( )
	{
		switch( GetComponent<GOScript>( ).Type )
		{
		case ObjectPoolManager.PoolType.damage:
		case ObjectPoolManager.PoolType.RedReptailBoss:
		case ObjectPoolManager.PoolType.HellHound:
		case ObjectPoolManager.PoolType.Skeleton:
			stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_M_RedTail_Atk] );
			return;
		case ObjectPoolManager.PoolType.FemaleNaga:
		case ObjectPoolManager.PoolType.MaleNagaBoss:
		case ObjectPoolManager.PoolType.FemaleNagaBoss:
		case ObjectPoolManager.PoolType.MaleNagaBlack:
		case ObjectPoolManager.PoolType.FemaleNagaBlack:
		case ObjectPoolManager.PoolType.Wizard:
			stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_M_M_Naga_Atk] );
			return;
		}
	}


	protected override void Update( )
	{
		var distanceFromGround = Physics2D.Raycast (transform.position- new Vector3(0f,-0.1f,0f), Vector3.down, 2, LayerMask.GetMask("Ground"));
		var jumpDist = distanceFromGround.collider == null ? 99 : distanceFromGround.distance;

		if( jumpDist < 0.1f && rb.velocity.y < 0.05f )
			stat.State &= ~PlanetUtil.CharacterState.Air;
	}
}
