﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOBullet : GOScript
{
	private TrailRenderer tr;
	public ObjectPoolManager.PoolType _type;


	public override bool Init( )
	{
		tr.Clear( );
		return true;
	}

	public void OnDisable( )
	{
		tr.Clear( );
	}

	protected override void Awake( )
	{
		tr = GetComponent<TrailRenderer>( );
		type = _type;
	}
}
