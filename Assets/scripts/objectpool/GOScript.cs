﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Object Pool에서 관리하기위해 반드시 사용되어야하는 것.
/// </summary>
public abstract class GOScript : MonoBehaviour
{
	public int id;
	public StatusScript stat; // 생성할 때 넣어줘야 한다.
	protected ObjectPoolManager.PoolType type; // 스크립트에서 설정
	public virtual ObjectPoolManager.PoolType Type { get { return type; } }

	// 최초 생성시 단 한번 호출 
	protected abstract void Awake( );

	// Bring 할 때마다 호출
	public abstract bool Init( );

	private void OnEnable( )
	{
		if( false == Init( ) )
			Debug.LogError( gameObject.name + "초기화 실패하였습니다." );
	}

	public virtual void DoDistroy( float _time = 0f )
	{
		if( _time != 0f )
			Invoke( "Dest", _time );
		else
			Dest( );
	}

	private void Dest( )
	{
		CancelInvoke( );
		ObjectPoolManager.Instance.ReturnObject( type, gameObject );
	}
}
