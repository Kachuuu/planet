﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOHitEffect : GOScript
{
	public ObjectPoolManager.PoolType _Type; // 인스펙터에서 설정되도록

	public float animationTime = 0.2f;

	private Animator anim;
	private float acc = 0f;

	protected override void Awake( )
	{
		type = _Type;
		anim = GetComponent<Animator>( );
	}
	public override bool Init( )
	{
		acc = 0f;
		return true;
	}

	
	private void Update( )
	{
		acc += Time.deltaTime;
		if( acc > animationTime )
			DoDistroy( );
	}
}
