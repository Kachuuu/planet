﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GODropItem : GOScript
{
	public enum Subtype
	{
		Gold, // nothing
		Item,
		Posion,
		Magazine
	}

	public Subtype subtype = Subtype.Gold;
	private CapsuleCollider2D col;
	private SpriteRenderer sr;
	private Rigidbody2D rb;
	private Transform tf;
	private TextMesh tm;
	public Item item;
	public AudioSource ado;
	public new string name; // 접근시 뜨는 이름 
	public float value; // 골드, 포션 또는 탄창에서 사용되는 수치

	private Coroutine co;

	public override bool Init( )
	{
		tm.text = "";
		rb.isKinematic = false;
		co = null;
		item = null;
		return true;
	}

	protected override void Awake( )
	{
		type = ObjectPoolManager.PoolType.DropItem;
		col = GetComponent<CapsuleCollider2D>( );
		ado = GetComponent<AudioSource>( );
		rb = GetComponent<Rigidbody2D>( );
		tf = transform;
		sr = GetComponent<SpriteRenderer>( );
		tm = GetComponentInChildren<TextMesh>( );
		tm.text = "";
		tm.GetComponent<MeshRenderer>( ).sortingOrder = 10;
	}

	private void Update( )
	{
		transform.rotation = Quaternion.identity;
	}

	private void OnTriggerStay2D( Collider2D collision )
	{
		if( co != null )
			return;

		if( collision.tag == "Player")
		{
			if(  GameMgr.Instance.Player.cs.pis.axies[(int)PlayerInputScript.Aixes.MoveVertical].GetAxis( ) > 0.7f )
			{
				switch( subtype )
				{
				case Subtype.Gold:
					//Nothing
					break;

				case Subtype.Item:
					co = StartCoroutine( CoFollowing( collision.GetComponent<StatusScript>().centerPos ) );
					break;

				case Subtype.Posion:
					GameMgr.Instance.Player.Demage( -value );
					DoDistroy( );
					return;
				case Subtype.Magazine:
					GameMgr.Instance.Player.AddMagazine( (int)value );
					DoDistroy( );
					break;
				}
				
			}
		}
	}

	private void OnTriggerEnter2D( Collider2D other )
	{
		if( co != null )
			return;

		if( other.tag == "Player" )
		{
			switch( subtype )
			{
			case Subtype.Gold:
				co = StartCoroutine( CoFollowing( other.GetComponent<StatusScript>( ).centerPos ) );
				return;

			case Subtype.Item:
			case Subtype.Posion:
			case Subtype.Magazine:
				PrintName( true );
				break;
			}
		}
	}

	private void OnTriggerExit2D( Collider2D other )
	{
		if( co != null )
			return;

		if( other.tag == "Player" )
		{
			switch( subtype )
			{
			case Subtype.Item:
			case Subtype.Posion:
			case Subtype.Magazine:
				PrintName( false );
				break;
			}
		}
	}

	public void SetDropItem( Subtype _type, float _value = 0f, Item _item = null )
	{
		subtype = _type;
		value = _value;
		item = _item;

		sr.flipX = Random.Range( 0, 2 ) < 1;

		switch( _type )
		{
		case Subtype.Gold:
			col.size = new Vector2(0.6f, 0.4f);
			var i = Random.Range(0,5);
			sr.color = Random.ColorHSV( i * 0.1f, ( i + 1 ) * 0.1f, 0.5f, 0.6f, 0.6f, 0.7f );
			sr.sprite = GameMgr.Instance.dbMgr.GetSprite( 15 );
			break;

		case Subtype.Item:
			col.size = new Vector2( 1f, 0.7f );
			sr.color = Color.white;
			sr.sprite = GameMgr.Instance.dbMgr.GetSprite( item.spriteid );
			break;

		case Subtype.Posion:
			col.size = new Vector2( 1f, 0.7f );
			sr.color = Color.red;
			sr.sprite = GameMgr.Instance.dbMgr.GetSprite( 15 );
			break;

		case Subtype.Magazine:
			col.size = new Vector2( 0.5f, 0.5f );
			sr.color = Color.blue;
			sr.sprite = GameMgr.Instance.dbMgr.GetSprite( 15 );
			break;
		}
	}

	void PrintName(bool _on = true )
	{
		var text = "";
		var clr = Color.white;
		if( _on )
		{
			switch( subtype )
			{
			case Subtype.Item:
				text = item.name;
				clr = PlanetUtil.itemNameClr[(int)item.rate];
				break;
			case Subtype.Posion:
				text = value + " 회복";
				break;
			case Subtype.Magazine:
				text = value + " 탄환";
				break;
			}
		}
		tm.text = text;
		tm.color = clr;
	}

	IEnumerator CoFollowing(Transform _tg , float _time = 0.2f )
	{
		tm.text = "";
		var dir = _tg.position - tf.position;
		rb.AddForce( dir * 50f );

		yield return new WaitForSeconds( _time );
		rb.isKinematic = true;
		float t = 0.8f;
		float acc = 0f;
		while( acc < t && Vector3.Distance(tf.position, _tg.position) > 0.3f ) 
		{
			acc += Time.deltaTime;
			tf.position = Vector3.Lerp( tf.position, _tg.position, acc / t );
			yield return null;
		}

		switch( subtype )
		{
		case Subtype.Gold:
			GameMgr.Instance.playerData.inven.mineral += (int)value;
			GameMgr.Instance.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_EF_GetItem] );
			DoDistroy( );
			break;

		case Subtype.Item:
			if( GameMgr.Instance.playerData.inven.AddItem( item, GameMgr.Instance.uiMgr.inDungoen ) )
			{
				GameMgr.Instance.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_EF_GetItem] );
				DoDistroy( );
			}
			// 못 먹는다면
			rb.AddForce( ( tf.position - _tg.position ).normalized * 300f );
			co = null;
			break;

		case Subtype.Posion:
			break;
		case Subtype.Magazine:
			break;
		}



	}
}
