﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GODefScript : GOScript
{
	public ObjectPoolManager.PoolType _Type; // 인스펙터에서 설정되도록

	protected override void Awake( )
	{
		type = _Type;
	}

	public override bool Init( )
	{
		if( stat != null )
			stat.Init( );
		return true;
	}

}
