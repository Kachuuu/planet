﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GORock : GOScript
{
	public SpriteRenderer sr;
	public Collider2D col;
	public Rigidbody2D rb;
	public ParticleSystem ps;
	public float scale;

	public override bool Init( )
	{
		sr.enabled = true;
		col.enabled = true;
		rb.isKinematic = false;
		ps.Stop( );
		ps.Clear( );
		return true;
	}

	protected override void Awake( )
	{
		type = ObjectPoolManager.PoolType.ThrowRock;
	}

	private void OnTriggerEnter2D( Collider2D collision )
	{
		col.enabled = false;
		sr.enabled = false;
		rb.velocity = Vector2.zero;
		rb.angularVelocity = 0f;
		rb.isKinematic = true;
		ps.Play( );
		DoDistroy( 5f );

		if( collision.tag == "Player" )
		{
			var playerCtrl = collision.GetComponent<ControlScript>( );
			playerCtrl.Demage( stat , 0, scale );

		}
	}

}
