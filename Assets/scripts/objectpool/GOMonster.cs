﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOMonster : GOScript
{
	public ObjectPoolManager.PoolType poolType;

	public override ObjectPoolManager.PoolType Type { get { return poolType; } }

	public override bool Init( )
	{
		if( stat == null )
			stat = GetComponent<MonsterStatusScript>( );
		stat.Init( );
		stat.CalcStat( );
		return true;
	}

	protected override void Awake( )
	{
		type = poolType;
	}
}
