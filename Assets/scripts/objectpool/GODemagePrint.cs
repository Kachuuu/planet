﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GODemagePrint : GOScript
{
	public int sortingOrder;
	public float speed = 1f;
	private Transform tf;
	[HideInInspector]public TextMesh tm;

	public override bool Init( )
	{
		tm.color += new Color( 0f, 0f, 0f, 1f );
		return true;
	}

	protected override void Awake( )
	{
		GetComponent<MeshRenderer>( ).sortingOrder = sortingOrder;
		tm = GetComponent<TextMesh>( );
		tf = transform;
		type = ObjectPoolManager.PoolType.DemagePrint;
	}

	private void Update( )
	{
		tf.position += new Vector3( 0f, speed ) * Time.deltaTime;
		tm.color -= new Color( 0f, 0f, 0f, speed ) * Time.deltaTime;
		if( tm.color.a <= 0f )
		{
			DoDistroy( );
		}
	}
}
