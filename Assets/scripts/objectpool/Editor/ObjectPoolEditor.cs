﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;


// 실제 사용하는 구조체( 또는 클래스 )
//[System.Serializable]  // 반드시 이렇게 직렬화를 해주어야 가능하다.
//public class ObjectPool
//{
//	private Stack<GameObject> pool = new Stack<GameObject>( );

//	public ObjectPoolManager.PoolType  type;
//	public GameObject   prefab;
//	public Transform    parent;
//	public uint         count = 4; // 기본적으로 인스펙터에서 설정되며, 개수가 모자를때 2배씩 커진다.
//}

[CanEditMultipleObjects]
[CustomEditor( typeof( ObjectPoolManager ) )]
public class ObjectPoolEditor : Editor
{
	private ReorderableList list;

	public override void OnInspectorGUI( )
	{
		DrawDefaultInspector( );
		serializedObject.Update( );
		list.DoLayoutList( );
		serializedObject.ApplyModifiedProperties( );
	}

	private void OnEnable()
	{
		list = new ReorderableList( serializedObject,
				serializedObject.FindProperty( "pools" ),
				true, true, true, true );

		list.drawHeaderCallback = ( Rect rect ) =>
		{ EditorGUI.LabelField( rect, "ObjectPools (type)   prefab    parrent   count" ); };

		list.drawElementCallback =
		(Rect rect, int index, bool isActive, bool isFocused) =>
		{
			var element = list.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;
			EditorGUI.PropertyField( new Rect( rect.x,							rect.y, 60,						EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "type" ), GUIContent.none );
			EditorGUI.PropertyField( new Rect( rect.x + 60,						rect.y, (rect.width - 90) / 2,	EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "prefab" ), GUIContent.none );
			EditorGUI.PropertyField( new Rect( rect.x + 60 + (rect.width-90)/2,	rect.y, (rect.width - 90) / 2,	EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "parent" ), GUIContent.none );
			EditorGUI.PropertyField( new Rect( rect.x + rect.width - 30,		rect.y, 30,						EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "count" ), GUIContent.none );
		};
	}
}