﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoRedReptailBoss : GOScript
{
	public override bool Init( )
	{
		var mstat = GetComponent<MonsterStatusScript>();
		mstat.Init( );
		return true;
	}

	protected override void Awake( )
	{
		type = ObjectPoolManager.PoolType.RedReptailBoss;
	}
}
