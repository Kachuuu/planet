﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOPlayer : GOScript
{
	PlayerInputScript pis;
	StatusScript    stat;

	protected override void Awake( )
	{
		type = ObjectPoolManager.PoolType.Player;
		pis = GetComponent<PlayerInputScript>( );
		stat = GetComponent<StatusScript>( );
	}

	public override bool Init( )
	{
		pis.InitButtons( );
		Camera.main.GetComponent<SimpleFollowCam>( ).tg = transform;
		return true;
	}


	public override void DoDistroy( float _time = 0 )
	{
		Camera.main.GetComponent<SimpleFollowCam>( ).tg = null;
		base.DoDistroy( _time );
	}


}
