﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class ObjectPool
{

	public static readonly int MAX_COUNT = 1023;


	private List<GameObject> tempList = new List<GameObject>();
	private Stack<GameObject> pool = new Stack<GameObject>( );

	public ObjectPoolManager.PoolType  type;
	public GameObject   prefab;
	public Transform    parent;
	public int          count = 4; // 기본적으로 인스펙터에서 설정되며, 개수가 모자를때 2배씩 커진다.


	public bool Init()
	{
		if (null == prefab || MAX_COUNT <= count)
		{
			Debug.LogError( "NULL GameObject or Count Over" );
			return false;
		}
		if( count == 0 )
			count = 4;

		CreatePool();
		return true;
	}

	// pool 동적 생성
	public bool Init( ObjectPoolManager.PoolType _type, GameObject _prefab, int _count = 4, Transform _parent = null )
	{
		if( null == _prefab || MAX_COUNT <= _count )
		{
			Debug.Log( ( _prefab == null ) + " " + _count );
			Debug.LogError( "NULL GameObject or Count Over" );
			return false;
		}
		this.type = _type;
		this.prefab = _prefab;
		this.count = _count;
		this.parent = _parent;

		CreatePool( );
		return true;
	}

	private bool IncObject( )
	{
		if( count == MAX_COUNT || count == 0 )
			return false;

		var incCnt = ( ( ( count * 2 ) > MAX_COUNT ) ? MAX_COUNT : ( count * 2 ) ) - count;
		CreateObject( incCnt );
		return true;
	}

	private void CreateObject( int cnt)
	{
		if( cnt + count > MAX_COUNT )
			return;

		var oldCnt = count;
		count = ( ( ( count + cnt ) > MAX_COUNT ) ? MAX_COUNT : ( count + cnt) );

		for( int i = oldCnt ; i < count ; ++i )
		{
			int id = ((int)(MAX_COUNT + 1) * (int)type) + i;
			var go = GameObject.Instantiate( prefab, parent );
			go.name += i;
			go.GetComponent<GOScript>( ).id = id;
			go.SetActive( false );
			pool.Push( go );
			tempList.Insert( (int)i, go);
		}
	}

	private void CreatePool()
	{
		ClearPool();
		var cnt = count;
		count = 0;
		CreateObject( cnt );
	}

	public void ClearPool()
	{
		foreach (var i in pool)
			GameObject.Destroy( i );
		pool.Clear();
		tempList.Clear( );
	}

	public GameObject BringObject()
	{
		if( 0 == pool.Count )
			if( false == IncObject( ) )
			{
				Debug.LogError( type + " GameObject를 더이상 생성 할 수 없습니다!" );
				return null;
			}
		var result = pool.Pop();
		result.SetActive( true );
		return result;
	}

	public GameObject BringObject(Vector3 _v, Quaternion _q = new Quaternion())
	{
		if( 0 == pool.Count )
			if( false == IncObject( ) )
			{
				Debug.LogError( type + " GameObject를 더이상 생성 할 수 없습니다!" );
				return null;
			}
		var result = pool.Pop();
		
		result.transform.SetPositionAndRotation( _v, _q );
		result.SetActive( true );
		return result;
	}

	public bool ReturnObject(GameObject _obj)
	{
		if( type != _obj.GetComponent<GOScript>( ).Type )
		{
			Debug.LogError( type + "과 다른 GameObject반환을 시도 하였습니다. -" + _obj.GetComponent<GOScript>( ).Type );
			return false;
		}

		var rb = _obj.GetComponent<Rigidbody>( ) as Rigidbody;
		if( null != rb )
		{
			rb.velocity = Vector3.zero;
			rb.angularVelocity = Vector3.zero;
		}
		_obj.SetActive( false );
		pool.Push( _obj );
		return true;
	}

	// 추가
	public GameObject GetObject(int _id)
	{
		var idx = ( MAX_COUNT & _id);
		return tempList[(int)idx];
	}
}

