﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour
{
	public static ObjectPoolManager Instance;

	public GameObject[] prefabs;
	[HideInInspector]
	private GameObject[] instanceObjs;

	private GameObject instanceRoot;

	public enum PoolType // 필요한 것이 있다면 여기에 추가한 후 사용
	{
		Player,
		damage,
		Bullet,
		BulletBig,
		BulletMany,
		DemagePrint,
		RedReptailBoss,
		ThrowRock,
		DropItem,
		ParalysisBullet,
		ReplectBullet,
		FreedomBullet,
		FlashBomb,
		FireBomb,
		MaleNaga,
		FemaleNaga,
		NagaBullet,
		MaleNagaBoss,
		FemaleNagaBoss,
		MaleNagaBlack,
		FemaleNagaBlack,
		Tower,
		Rocket,
		DronBlt,
		HitEffect,
		Dron,
		InvenItem,
		InvenIcon,
		HellHound,
		Wizard,
		Skeleton,
		BuffIcon,
		EncanceItem,
		none // 반드시 마지막에 둘것. ( 이것이 ID Count 역할을 한다.)
	}
	//public readonly GameObject[] prefabs; // 인스펙터 창에서 해당 오브젝트 프레팹을 설정해야 한다.
	public List<ObjectPool> pools = new List<ObjectPool>( );

	private void Awake( )
	{
		//Todo 임시로 지움
		//if( null == Instance )
		Instance = this;
		//else
		//	Debug.LogError( "이미 Instance가 존재 합니다." );

		instanceRoot = new GameObject( "Instances" );
		instanceRoot.transform.parent = transform;

		var root = new GameObject("Instances(private)");
		root.transform.parent = transform;
		//Debug.Log( "obj" );
		instanceObjs = new GameObject[prefabs.Length];
		for( int i = 0 ; i < instanceObjs.Length ; ++i )
		{
			instanceObjs[i] = Instantiate( prefabs[i], root.transform );
			instanceObjs[i].SetActive( false );
		}

		//인스펙터에서 기본 설정을 안하기 때문에 불필요
		foreach( var it in pools )
			it.Init( );
	}

	// 동적 생성
	public ObjectPool AddPool( PoolType _type, GameObject _prefab, int _count, Transform _parent = null )
	{
		var pool = GetPool( _type );
		if( pool != null )
			return pool;

		var result = new ObjectPool();
		result.Init( _type, _prefab, _count, _parent );
		pools.Add( result );
		return result;
	}

	public ObjectPool GetPool( PoolType _type )
	{
		for( int i = 0 ; i < pools.Count ; ++i )
			if( pools[i].type == _type )
				return pools[i];

		//Debug.Log( "Not Find ObjectPool ID" + _type );
		return null;
	}

	private ObjectPool FindInstance( PoolType type )
	{
		for( int i = 0 ; i < instanceObjs.Length ; ++i )
		{
			if( type == instanceObjs[i].GetComponent<GOScript>( ).Type )
			{
				return AddPool( type, instanceObjs[i], 1, instanceRoot.transform );
			}
		}
		return null;
	}

	public void RemovePool( PoolType _type ) // 다 지움
	{
		var pool = GetPool( _type );
		if( pool != null )
		{
			pool.ClearPool( );
			pools.Remove( pool );
		}

	}

	public void ClearPool( ) // 싹다 지움
	{
		for( int i = 0 ; i < pools.Count ; ++i )
			pools[i].ClearPool( );
		pools.Clear( );

		if( instanceRoot != null )
			Destroy( instanceRoot );

		instanceRoot = new GameObject( "Instances" );
		instanceRoot.transform.parent = transform;

		// Todo Collect();

	}


	public GameObject BringObject( PoolType _type )
	{
		return BringObject( _type, Vector3.zero );
	}

	public GameObject BringObject( PoolType _type, Vector3 _v, Quaternion _q = new Quaternion( ) )
	{
		var pool = GetPool(_type);
		if( null == pool )
		{
			pool = FindInstance( _type );
			if( null == pool )
			{
				Debug.LogError( _type + "의 Pool과 Instance가 존재하지 않습니다." );
				return null;
			}
		}

		var go = pool.BringObject(_v, _q);
		if( go != null )
			SetRegistID( go );
		return go;
	}

	public bool ReturnObject( PoolType _type, GameObject _obj )
	{
		var pool = GetPool(_type);
		if( null == pool )
		{
			Debug.LogError( _type + "의 Pool이 존재하지 않습니다." );
			return false;
		}

		if( pool.ReturnObject( _obj ) )
		{
			UnregistID( _obj );
			return true;
		}
		else
			return false;
	}



	//추가

	public GameObject BringObject( int _id, PoolType _type, Vector3 _v, Quaternion _q = new Quaternion( ) )
	{
		var pool = GetPool(_type);

		var go = pool.BringObject(_v, _q);
		if( go != null )
			RegistID( _id, go );
		return go;
	}


	// object ID 관리

	Dictionary<int,GameObject> set = new Dictionary<int, GameObject>();
	int listEmpty = 0;
	List<int> emptyIndexes = new List<int>();
	public void SetRegistID( GameObject _go )
	{
		if( emptyIndexes.Count == 0 )
		{
			set[listEmpty] = _go;
			_go.GetComponent<GOScript>( ).id = listEmpty++;
		}
		else
		{
			var idx = emptyIndexes[0];
			emptyIndexes.RemoveAt( 0 );
			set[idx] = _go;
			_go.GetComponent<GOScript>( ).id = idx;
		}
	}

	public void UnregistID( GameObject _go )
	{
		var id = _go.GetComponent<GOScript>().id;
		set[id] = null;
		emptyIndexes.Add( id );
	}

	public void RegistID( int _id, GameObject _go )
	{
		_go.GetComponent<GOScript>( ).id = -1;
		GameObject dummy;

		if( set.TryGetValue( _id, out dummy ) )
		{
			if( dummy == null )
				emptyIndexes.Remove( _id );
		}
		else if( _id > listEmpty )
			return;
		else if( _id == listEmpty )
			++listEmpty;

		_go.GetComponent<GOScript>( ).id = _id;
		set[_id] = _go;
	}

	public GameObject GetObject( int _id )
	{
		GameObject dummy;
		if( set.TryGetValue( _id, out dummy ) )
			return dummy;
		return null;
	}

}
