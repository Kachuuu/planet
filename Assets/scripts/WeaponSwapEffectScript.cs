﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwapEffectScript : MonoBehaviour
{
	public StatusScript stat;
	public int sortingOrder = 10;
	private TextMesh tm;
	private Transform tf;
	private Vector3 sPos, oPos, ePos;
	public float time = 1.5f;
	private float inT = 0.3f;
	private float outT = 0.3f;
	private float acc = 0f;

	private void Awake( )
	{
		tm = GetComponent<TextMesh>( );
		GetComponent<MeshRenderer>( ).sortingOrder = sortingOrder;
		tf = transform;

		sPos = tf.localPosition - new Vector3( -0.5f, 0f );
		oPos = tf.localPosition;
		ePos = tf.localPosition - new Vector3(  0.5f, 0f );
	}

	private void Update( )
	{
		if( stat.lookLeft )
		{
			tf.localRotation = Quaternion.Euler( 0f, 180f, 0f );
			sPos = oPos - new Vector3( -0.5f, 0f );
			ePos = oPos - new Vector3( 0.5f, 0f );
		}
		else
		{
			tf.localRotation = Quaternion.Euler( 0f, 0f, 0f );
			sPos = oPos - new Vector3( -0.5f, 0f );
			ePos = oPos - new Vector3( 0.5f, 0f );
		}

		if ( acc < time )
		{
			acc += Time.deltaTime;

			if( acc < inT )
			{
				tf.localPosition = Vector3.Lerp( sPos, oPos, acc / inT );
				tm.color = new Color( 1f, 1f, 1f, acc / inT );
			}
			else if( acc > time - outT )
			{
				tf.localPosition = Vector3.Lerp( oPos, ePos, (acc - (time - outT)) / inT );
				tm.color = new Color( 1f, 1f, 1f, 1f - (( acc - ( time - outT ) ) / inT) );
			}
		}
		else
		{
			gameObject.SetActive( false );
		}
	}

	public void ShowSwapEffect( string _name )
	{
		gameObject.SetActive( true );
		tm.text = _name;
		acc = 0f;
	}
}
