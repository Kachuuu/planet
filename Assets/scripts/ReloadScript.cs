﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadScript : MonoBehaviour
{

	private bool bRunning = false;
	private float time;
	private float acc;
	private SpriteRenderer sr;

	public Transform bar;
	public Transform startPos;
	public Transform endPos;

	private Vector3 oldRight;
	void Start( )
	{
		sr = GetComponent<SpriteRenderer>( );
		oldRight = new Vector3( 1f, 0f, 0f );
		bar.SetAsLastSibling( );
	}

	void Update( )
	{
		if( bRunning )
		{
			acc += Time.deltaTime;
			bar.position = Vector3.Lerp( startPos.position, endPos.position, acc / time );
			if( acc >= time )
				Cancel( );
			if( transform.right != oldRight )
			{
				transform.rotation *= Quaternion.Euler( 0f, 180f, 0f );
				oldRight = transform.right;
			}

		}
	}

	public void Reload( float _time )
	{
		gameObject.SetActive( true );
		time = _time;
		acc = 0f;
		bRunning = true;
	}

	public void Cancel( )
	{
		bRunning = false;
		gameObject.SetActive( false );
	}
}
