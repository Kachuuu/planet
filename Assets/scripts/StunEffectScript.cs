﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunEffectScript : MonoBehaviour
{
	private float x = 0.5f;		// 가로축 움직임
	//public float y;			// 세로축 움직임
	private float time = 0.5f;	// 한 싸이클 시간

	private float runningTime; // 이만큼 동작하고 사라진다.
	private SpriteRenderer sr;
	private Transform tf;
	private Vector3 originPos;
	private Vector3 min, max;
	private float half;
	private float acc;

	private void Start( )
	{
		sr = GetComponent<SpriteRenderer>( );
		tf = transform;
		originPos = tf.localPosition;
		min = new Vector3( originPos.x - x, originPos.y/* - y */);
		max = new Vector3( originPos.x + x, originPos.y/* + y */);
		half = time * 0.5f;
		acc = 0f;
	}

	private void Update( )
	{
		if( 0f < runningTime )
		{
			runningTime -= Time.deltaTime;

			acc += Time.deltaTime;
			acc %= time * 2f;

			if( acc < time )
				tf.localPosition = Vector3.Lerp( min, max, acc / time );
			else
				tf.localPosition = Vector3.Lerp( min, max, 2f - acc / time );
		}
		else
			sr.enabled = false;
	}

	public void SetStun( float _time )
	{
		runningTime = _time;
		sr.enabled = true;
	}
}
