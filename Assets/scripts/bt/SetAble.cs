﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "Tg 를 time 만큼 설정한다.( 0일 경우 기본 값이로 적용" )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class SetAble : Action
	{
		private StatusScript stat;
		public PlanetUtil.CharacterStatables tg;
		public float time = 0f;

		public override void OnAwake( )
		{
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			stat.statable[(int)tg].Reset(false);
			return TaskStatus.Success;
		}
	}
}