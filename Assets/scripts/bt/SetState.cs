﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "Log is a simple task which will output the specified text and return success. It can be used for debugging." )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class SetState : Action
	{
		private StatusScript stat;
		public PlanetUtil.CharacterState state;

		public override void OnAwake( )
		{
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			stat.State = state;
			//stat.State = ~state;

			return TaskStatus.Success;
		}
	}
}