﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "Log is a simple task which will output the specified text and return success. It can be used for debugging." )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class Hit : Action
	{
		private ControlScript cs;

		public override void OnAwake( )
		{
			cs = GetComponent<ControlScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			return TaskStatus.Running;
		}

		public override void OnTriggerEnter( Collider other )
		{
			Debug.Log( "adsasdasdasdwdqwdwrq" );
		}
	}
}