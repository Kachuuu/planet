﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "Gurugi" )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class Dash : Action
	{
		private ControlScript cs;
		private StatusScript stat;

		public override void OnAwake( )
		{
			cs = GetComponent<ControlScript>( );
			stat = GetComponent<StatusScript>( );
		}


		public override TaskStatus OnUpdate( )
		{
			if(stat.statable[(int)PlanetUtil.CharacterStatables.Dash].Able( ) )
				if(cs.Dash( ))
					return TaskStatus.Success;
			return TaskStatus.Failure;
		}
	}
}