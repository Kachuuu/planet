﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "몬스터가 앞이 벽인지를 검사" )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class WallChkRangeIn : Action
	{
		private MonsterControlScript msc;

		public override void OnAwake( )
		{
			msc = GetComponent<MonsterControlScript>( );
		}


		public override TaskStatus OnUpdate( )
		{
			if( msc.WallChkRangeIn( ) )
				return TaskStatus.Success;
			else
				return TaskStatus.Failure;
		}
	}
}