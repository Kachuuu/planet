﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "특정 상태가 끌날때까지 대기한다" )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class WaitState : Action
	{
		private StatusScript stat;
		public PlanetUtil.CharacterState state;

		public override void OnAwake( )
		{
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if( ( stat.State & state ) != 0 )
				return TaskStatus.Running;
			return TaskStatus.Success;
		}


	}
}