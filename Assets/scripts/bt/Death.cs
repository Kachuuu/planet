﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "사망 애니메이션 후 삭제한다. ( Call ContorolScript.Death() ) " )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class Death : Action
	{
		private ControlScript cs;
		private StatusScript stat;

		public override void OnAwake( )
		{
			cs = GetComponent<ControlScript>( );
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			//GameMgr.Instance.cmdMgr.ExcuteAndPush( new DestroyCommand( gameObject ) );
			stat.State = PlanetUtil.CharacterState.Death;
			cs.Death( );
			return TaskStatus.Success;
		}
	}
}