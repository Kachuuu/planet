﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
//namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables

namespace myBT
{
	[TaskCategory( "MyBT/Conditions" )]
	[TaskDescription( "몬스터 뒤에 플레이어가 있을때" )]
	public class IfBehindTg : Conditional
	{

		private MonsterStatusScript mstat;

		public override void OnAwake( )
		{
			mstat = GetComponent<MonsterStatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if( mstat.Tg == null )
				return TaskStatus.Failure;

			if( ( mstat.Tg.transform.position.x - mstat.transform.position.x > 0 && mstat.transform.right.x < 0 ) ||
				( mstat.Tg.transform.position.x - mstat.transform.position.x < 0 && mstat.transform.right.x > 0 ) )
				return TaskStatus.Success;
			else
				return TaskStatus.Failure;
		}
	}
}