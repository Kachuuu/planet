﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
//namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables

namespace myBT
{
	[TaskCategory( "MyBT/Conditions" )]
	[TaskDescription( "해당 축이 0인지 아닌지를 검사한다. (0이라면 false)" )]
	public class InputAxis : Conditional
	{
		private PlayerInputScript pis;
		public PlayerInputScript.Aixes axisName1;
		public PlayerInputScript.Aixes axisName2;

		public override void OnAwake( )
		{
			pis = GetComponent<PlayerInputScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if( 0f == pis.axies[(int)axisName1].GetAxis( ) &&
				0f == pis.axies[(int)axisName2].GetAxis( ) )
				return TaskStatus.Failure;
			return TaskStatus.Success;
		}
	}
}