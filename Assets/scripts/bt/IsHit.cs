﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
//namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables

namespace myBT
{
	[TaskCategory( "MyBT/Conditions" )]
	[TaskDescription( "tag가 같은 개체와 충돌 시 True - 작업중" )]
	public class IsHit : Conditional
	{
		public string tag;

		private StatusScript stat;

		public override void OnAwake( )
		{
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if( stat.statable[(int)PlanetUtil.CharacterStatables.Hit].Able( ) )
				return TaskStatus.Success;
			else
				return TaskStatus.Failure;
		}
	}
}