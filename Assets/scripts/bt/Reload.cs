﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "재장전" )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class Reload : Action
	{
		private ControlScript cs;
		private StatusScript stat;

		public override void OnAwake( )
		{
			cs = GetComponent<ControlScript>( );
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if( stat.statable[(int)PlanetUtil.CharacterStatables.Reload].Able( ) )
				if( cs.Reload( ) )
					return TaskStatus.Success;
			return TaskStatus.Failure;
		}
	}
}