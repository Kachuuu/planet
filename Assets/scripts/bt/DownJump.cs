﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "다운 점프. 불가할 때는 Failure를 반환" )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class DownJump : Action
	{
		private ControlScript cs;
		private StatusScript stat;
		private PlayerInputScript pis;

		public override void OnAwake( )
		{
			stat = GetComponent<StatusScript>( );
			cs = GetComponent<ControlScript>( );
			pis = GetComponent<PlayerInputScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
#if UNITY_ANDROID
			if( pis.axies[(int)PlayerInputScript.Aixes.MoveVertical].GetAxis( ) < -0.7f &&
				stat.listStandingBlock.tag == "GroundPlatform" )
			{
				cs.DownJump( );
				return TaskStatus.Success;
			}
#else
			if( pis.axies[(int)PlayerInputScript.Aixes.MoveVertical].GetAxis( ) < -0.2f &&
				stat.listStandingBlock.tag == "GroundPlatform" )
			{
				if ( cs.DownJump( ) )
					return TaskStatus.Success;
			}
#endif
			return TaskStatus.Failure;
		}
	}
}