﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
//namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables

namespace myBT
{
	[TaskCategory( "MyBT/Conditions" )]
	[TaskDescription( "tg중 하나라도 현재 상태에 포함되는지 검사" )]
	public class IsState : Conditional
	{
		private StatusScript stat;
		[BitMask(typeof(PlanetUtil.CharacterState))]
		public PlanetUtil.CharacterState tg;

		public override void OnAwake( )
		{
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if( (stat.State & tg) != 0 )
				return TaskStatus.Success;
			return TaskStatus.Failure;
		}
	}
}