﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
//namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables

namespace myBT
{
	public enum Compaprer
	{
		equl,
		notequl,
		less,
		grate,
		none
	}

	[TaskCategory( "MyBT/Conditions" )]
	[TaskDescription( "Hp 수치와 비교 한 결과를 반환한다." )]
	public class isHp : Conditional
	{
		public float vlaue;
		public Compaprer compaprer;
		private float HpRate;

		private StatusScript stat;

		public override void OnStart( )
		{
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			HpRate = stat.HpRate;
			switch( compaprer )
			{
			case Compaprer.equl: return vlaue == HpRate ? TaskStatus.Success : TaskStatus.Failure;
			case Compaprer.notequl: return vlaue != HpRate ? TaskStatus.Success : TaskStatus.Failure;
			case Compaprer.less: return vlaue > HpRate ? TaskStatus.Success : TaskStatus.Failure;
			case Compaprer.grate: return vlaue < HpRate ? TaskStatus.Success : TaskStatus.Failure;
			}
			return TaskStatus.Failure;
		}
	}
}