﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
//namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables

namespace myBT
{
	[TaskCategory( "MyBT/Conditions" )]
	[TaskDescription( "able이 true면 true, 아니면 false" )]
	public class ItsAble : Conditional
	{
		private PlayerInputScript pis;
		private StatusScript stat;
		public PlanetUtil.CharacterStatables tg;


		public override void OnAwake( )
		{
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if( stat.statable[(int)tg].Able( ) )
			{
				return TaskStatus.Success;
			}
			return TaskStatus.Failure;
		}
	}
}