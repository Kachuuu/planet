﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "Log is a simple task which will output the specified text and return success. It can be used for debugging." )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class Idle : Action
	{
		private ControlScript cs;
		private StatusScript stat;

		public override void OnAwake( )
		{
			cs = GetComponent<ControlScript>( );
			stat = GetComponent<StatusScript>( );
		}


		public override TaskStatus OnUpdate( )
		{
			cs.Idle( );
			return TaskStatus.Success;
		}
	}
}