﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "Log is a simple task which will output the specified text and return success. It can be used for debugging." )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class Crouch : Action
	{
		private ControlScript cs;
		public bool on;

		public override void OnAwake( )
		{
			cs = GetComponent<ControlScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			//cs.Crouch( on );

			return TaskStatus.Success;
		}
	}
}