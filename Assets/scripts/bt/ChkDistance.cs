﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
//namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables

namespace myBT
{
	[TaskCategory( "MyBT/Monster/Conditions" )]
	[TaskDescription( "타겟과의 거리가 dist 이하일때 True(Only Monster)" )]
	public class ChkDistance : Conditional
	{
		private MonsterStatusScript mstat;
		public SharedFloat dist;

		public override void OnAwake( )
		{
			mstat = GetComponent<MonsterStatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if( mstat.Tg == null )
				return TaskStatus.Failure;

			if( dist.Value >= (mstat.Tg.transform.position - transform.position).magnitude )
				return TaskStatus.Success;
			else
				return TaskStatus.Failure;
		}
	}
}