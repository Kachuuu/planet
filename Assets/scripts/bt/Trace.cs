﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "타겟을 따라간다." )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Monster/Actions" )]
	public class Trace : Action
	{
		private MonsterControlScript cs;

		public override void OnAwake( )
		{
			cs = GetComponent<MonsterControlScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			cs.Trace( );
			return TaskStatus.Success;
		}
	}
}