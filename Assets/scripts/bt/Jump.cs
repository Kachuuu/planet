﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "점프을 한다면 true, 안한다면 false" )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class Jump : Action
	{
		private ControlScript cs;
		private StatusScript stat;

		public override void OnAwake( )
		{
			cs = GetComponent<ControlScript>( );
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if( stat.statable[(int)PlanetUtil.CharacterStatables.Jump].Able( ) )
				if( cs.Jump( ) )
					return TaskStatus.Success;
			return TaskStatus.Failure;
		}
	}
}