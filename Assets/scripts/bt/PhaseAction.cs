﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "Phase Action(스페셜 공격)을 시작한다." )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Monster/Actions" )]
	public class PhaseAction : Action
	{
		private MonsterControlScript cs;
		private StatusScript stat;

		public override void OnAwake( )
		{
			cs = GetComponent<MonsterControlScript>( );
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			cs.PhaseAction( );
			return TaskStatus.Success;
		}
	}
}