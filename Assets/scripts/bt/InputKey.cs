﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
//namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables

namespace myBT
{
	[TaskCategory( "MyBT/Conditions" )]
	[TaskDescription( "해당 버튼이 눌렸는지 검사한다." )]
	public class InputKey : Conditional
	{
		private PlayerInputScript pis;
		public PlayerInputScript.Inputs keyName;
		public bool GetKeyDown;
		public bool GetKey;
		public bool GetKeyUp;

		public override void OnAwake( )
		{
			pis = GetComponent<PlayerInputScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if( GetKeyDown && pis.inputs[(int)keyName].GetKeyDown( ) )
				return TaskStatus.Success;
			else if( GetKey && pis.inputs[(int)keyName].GetKey( ) )
				return TaskStatus.Success;
			else if( GetKeyUp && pis.inputs[(int)keyName].GetKeyUp( ) )
				return TaskStatus.Success;

			return TaskStatus.Failure;
		}
	}
}