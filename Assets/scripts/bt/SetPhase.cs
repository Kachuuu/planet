﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "몬스터의 페이즈를 설정한다.(공격/이동 속도 및 페이즈 단계, 스프라이트 컬러)" )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Monster/Actions" )]
	public class SetPhase : Action
	{
		private MonsterControlScript cs;
		private MonsterStatusScript mstat;
		public int phase;
		public float attackSpeed;
		public float moveSpeed;
		public Color clr;

		public override void OnAwake( )
		{
			mstat = GetComponent<MonsterStatusScript>( );
			cs = GetComponent<MonsterControlScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if ( true == cs.ChkPhase() )
			{
				cs.col.enabled = true;
				cs.AllStopCoroutine( ); // 페이지 변화 상태를 Reload로 대체한다.
				mstat.State =	PlanetUtil.CharacterState.Trace |
								PlanetUtil.CharacterState.Reload | 
								PlanetUtil.CharacterState.SuperArmor;
				mstat.velocity.x = 0f;
				mstat.am.SetTrigger( "NextPhase" );
				mstat.statable[(int)PlanetUtil.CharacterStatables.Reload].SetTimer( 5f );
				return TaskStatus.Success;
			}
			return TaskStatus.Failure;
		}
	}
}