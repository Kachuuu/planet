﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
//namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables

namespace myBT
{
	[TaskCategory( "MyBT/Monster/Conditions" )]
	[TaskDescription( "현재 페이즈 값과 일치하는지 검사" )]
	public class IsPhase : Conditional
	{
		private MonsterStatusScript mstat;
		public int num;

		public override void OnAwake( )
		{
			mstat = GetComponent<MonsterStatusScript>( );
		}


		public override TaskStatus OnUpdate( )
		{
			if( mstat.nowPhase == num )
				return TaskStatus.Success;
			return TaskStatus.Failure;
		}
	}
}