﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
//namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables

namespace myBT
{
	[TaskCategory( "MyBT/Conditions" )]
	[TaskDescription( "플레이어 인식 여부" )]
	public class IsTrue : Conditional
	{
		public SharedBool target;

		public override TaskStatus OnUpdate( )
		{
			if( target.Value == true )
				return TaskStatus.Success;

			return TaskStatus.Failure;
		}
	}
}