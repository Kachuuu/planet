﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "움직일 수 있을 때, 조작 가능할때 움직인다.(안움직이면 false)" )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class Move : Action
	{
		private ControlScript cs;
		private StatusScript stat;

		public override void OnAwake( )
		{
			cs = GetComponent<ControlScript>( );
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if( stat.statable[(int)PlanetUtil.CharacterStatables.Move].Able( ) )
			{
				if( cs.Move( ) )
				{
					stat.State &= ~PlanetUtil.CharacterState.Idle;
					stat.State |= PlanetUtil.CharacterState.Move;
					return TaskStatus.Success;
				}
			}
			cs.FlipX( stat.lookLeft );
			stat.State &= ~PlanetUtil.CharacterState.Move;
			stat.State |= PlanetUtil.CharacterState.Idle;
			return TaskStatus.Failure;
		}
	}
}