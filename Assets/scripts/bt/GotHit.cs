﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
//namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables

namespace myBT
{
	[TaskCategory( "MyBT/Conditions" )]
	[TaskDescription( "맞았을때" )]
	public class GotHit : Conditional
	{
		private StatusScript stat;

		public override void OnAwake( )
		{
			stat = GetComponent<StatusScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if ( stat.bHit == true )
			{
				stat.bHit = false;
				return TaskStatus.Success;
			}
			return TaskStatus.Failure;
		}

	}
}