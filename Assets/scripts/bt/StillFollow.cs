﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;
//namespace BehaviorDesigner.Runtime.Tasks.Basic.SharedVariables

namespace myBT
{
	[TaskCategory( "MyBT/Monster/Conditions" )]
	[TaskDescription( "타겟과의 거리가 최대 인식 범위를 벗어났는지 검사 및 설정" )]
	public class StillFollow : Conditional
	{
		private MonsterStatusScript mstat;

		public override void OnAwake( )
		{
			mstat = GetComponent<MonsterStatusScript>( );
		}


		public override TaskStatus OnUpdate( )
		{
			if( mstat.Tg != null &&
				( mstat.Tg.transform.position - transform.position ).magnitude < mstat.FollowDistance )
				return TaskStatus.Success;

			mstat.Tg = null;
			return TaskStatus.Failure;
		}
	}
}