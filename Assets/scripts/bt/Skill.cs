﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "Player의 스킬 사용 성공 시 true, 실패시 false" )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class Skill : Action
	{
		private StatusScript stat;
		private ControlScript cs;
		public int Index;

		public override void OnAwake( )
		{
			stat = GetComponent<StatusScript>( );
			cs = GetComponent<ControlScript>( );
		}

		public override TaskStatus OnUpdate( )
		{
			if( stat.statable[(int)PlanetUtil.CharacterStatables.Skill].Able( ) )
			{
				if( ( cs.pis.inputs[(int)PlayerInputScript.Inputs.Skill1].GetKeyDown( ) && cs.Skill( 0 ) ) ||
					( cs.pis.inputs[(int)PlayerInputScript.Inputs.Skill2].GetKeyDown( ) && cs.Skill( 1 ) ) )
				{
					return TaskStatus.Success;
				}

				var wpType = (int)( (Weapon)stat.GetItem(stat.nowWeapon) ).wpType;

				for( int i = 0 ; i < 2 ; ++i )
					if( stat.playerActiveSkill[(int)( (Weapon)stat.GetItem( stat.nowWeapon ) ).wpType, i] != null )
					{
						GameMgr.Instance.uiMgr.skill[i].SetCoolTime(
							stat.playerActiveSkill[wpType, i].curCoolTime[stat.playerActiveSkill[wpType, i].level],
							stat.playerActiveSkill[wpType, i].coolTime - Time.time );
					}

				return TaskStatus.Failure;
			}

			//for( int i = 0 ; i < 2 ; ++i )
			//	GameMgr.Instance.uiMgr.skill[1].SetCoolTime( 999f, 0f );

			return TaskStatus.Failure;
		}
	}
}