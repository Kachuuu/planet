﻿using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using UnityEngine;

namespace myBT
{
	[TaskDescription( "Log is a simple task which will output the specified text and return success. It can be used for debugging." )]
	[TaskIcon( "{SkinColor}LogIcon.png" )]
	[TaskCategory( "MyBT/Actions" )]
	public class AwkRangeIn : Action
	{
		private MonsterControlScript cs;

		public override void OnAwake( )
		{
			cs = GetComponent<MonsterControlScript>( );
		}


		public override TaskStatus OnUpdate( )
		{
			if( cs.AtkRangeIn( ) )
				return TaskStatus.Success;
			else
				return TaskStatus.Failure;
		}
	}
}