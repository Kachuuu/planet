﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Command
{
	protected GameObject obj; // 동작할때만 사용된다.
	protected int objID=-1; // 행동의 주체가 되는 오브젝트 ID
							// 오브젝트 관리자에게서 객체을 얻어와야한다.

	public GameObject GetObject( )
	{
		return obj;
	}

	protected Command( int _id, GameObject _go )
	{
		objID = _id;
		obj = _go;
	}

	protected Command( GameObject _go )
	{
		if( _go != null )
			objID = _go.GetComponent<GOScript>( ).id;
		obj = _go;
	}

	public abstract Command Clone( );

	public abstract void Excute( );
}


public class MoveCommand : Command
{
	Vector3 pos;

	public MoveCommand( GameObject _go, Vector3 _pos )
	: base( _go )
	{
		pos = _pos;
	}

	public MoveCommand( Transform _tf, Vector3 _pos )
		: base( _tf.gameObject )
	{
		pos = _pos;
	}

	public override void Excute( )
	{
		if( obj.transform.position != pos )
		{
			//obj.GetComponent<ControlScript>( ).Move( pos );
			//obj.transform.position = pos;
			//Todo
			//방향에 따른 애니메이션
		}
		else
		{
			//정지애니메이션
		}
	}

	public override Command Clone( )
	{
		return new MoveCommand( obj, pos );
	}
}


public class RotateCommand : Command
{
	Quaternion rot;

	public RotateCommand( GameObject _go, Quaternion _rot )
	: base( _go )
	{
		rot = _rot;
	}

	public RotateCommand( Transform _tf, Quaternion _rot )
		: base( _tf.gameObject )
	{
		rot = _rot;
	}

	public override void Excute( )
	{
		obj.transform.rotation = rot;
		//Todo
		//방향에 따라 스프라이트 결정
	}

	public override Command Clone( )
	{
		return new RotateCommand( obj, rot );
	}
}


public class ShootCommand : Command
{
	int parentID;
	ObjectPoolManager.PoolType type;
	Vector3 pos;
	Quaternion rot;


	public ShootCommand( int _pid, ObjectPoolManager.PoolType _type, Vector3 _pos, Quaternion _rot = new Quaternion( ) )
	: base( null )
	{
		parentID = _pid;
		type = _type;
		pos = _pos;
		rot = _rot;
	}

	public ShootCommand( Transform _tf, ObjectPoolManager.PoolType _type )
		: base( null )
	{
		parentID = _tf.GetComponent<GOScript>( ).id;
		type = _type;
		pos = _tf.position;
		rot = _tf.rotation;
	}

	public ShootCommand( Transform _tf, ObjectPoolManager.PoolType _type, Vector3 _pos, Quaternion _rot = new Quaternion( ) )
	: base( null )
	{
		if( _tf != null )
			parentID = _tf.GetComponent<GOScript>( ).id;

		type = _type;
		pos = _pos;
		rot = _rot;
	}

	public override void Excute( )
	{
		if( objID == -1 )
		{
			obj = GameMgr.Instance.objMgr.BringObject( type, pos, rot );
			objID = (int)obj.GetComponent<GOScript>( ).id;
			var stat = obj.GetComponent<StatusScript>( );
			Debug.Log( obj.name + " " + objID );
		}
		else
		{
			Debug.Log( "asdads" + objID );
			obj = GameMgr.Instance.objMgr.BringObject( objID, type, pos, rot );
		}
	}

	public override Command Clone( )
	{
		return new ShootCommand( parentID, type, pos, rot );
	}
}

public class CreateCommand : Command
{
	ObjectPoolManager.PoolType type;
	Vector3 pos;
	Quaternion rot;

	public CreateCommand( ObjectPoolManager.PoolType _type, Vector3 _pos, Quaternion _rot )
		: base( null )
	{
		type = _type;
		pos = _pos;
		rot = _rot;
	}

	public CreateCommand( Transform _tf, ObjectPoolManager.PoolType _type )
		: base( null )
	{
		type = _type;
		pos = _tf.position;
		rot = _tf.rotation;
	}

	public override void Excute( )
	{
		if( objID == -1 )
		{
			obj = GameMgr.Instance.objMgr.BringObject( type, pos, rot );
			objID = (int)obj.GetComponent<GOScript>( ).id;
			var stat = obj.GetComponent<StatusScript>( );
			if( stat != null )
				stat.id = objID;
		}
		else
		{
			obj = GameMgr.Instance.objMgr.BringObject( objID, type, pos, rot );
		}
	}

	public override Command Clone( )
	{
		return new CreateCommand( type, pos, rot );
	}
}


public class DestroyCommand : Command
{

	public DestroyCommand( int _id )
		: base( _id, GameMgr.Instance.objMgr.GetObject( _id ) )
	{
	}
	public DestroyCommand( GameObject _obj )
		: base( _obj )
	{
	}

	public override void Excute( )
	{
		obj.GetComponent<GOScript>( ).DoDistroy( );
	}

	public override Command Clone( )
	{
		return new DestroyCommand( objID );
	}
}