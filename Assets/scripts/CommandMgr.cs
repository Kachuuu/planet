﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandMgr : MonoBehaviour
{
	Queue<Command> commands = new Queue<Command>();

	public void Push( Command _cmd )
	{
		commands.Enqueue( _cmd );
	}

	public void ExcuteAndPush( Command _cmd )
	{
		_cmd.Excute( );
		commands.Enqueue( _cmd );
	}
}
