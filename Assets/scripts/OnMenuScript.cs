﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMenuScript : MonoBehaviour
{
	public GameObject menuPanel;

	public void OnToggleAiming( )
	{
		menuPanel.SetActive( !( menuPanel.activeInHierarchy ) );
	}
}
