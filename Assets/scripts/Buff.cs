﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuffType
{
	Pasive,
	TimeLimit,
	CountLimit,
	Accumulate,
	Tick,
	None,
}

//dasdasd

[System.Serializable]
public class Buff
{
	public int id;
	public int spriteID;
	public BuffType type;

	protected StatusScript tgStat;
	public Performence[] buff; // 이 데이터는 어태치 되는 동시에 플레이어에게 적용된다.
	protected bool bEffected; // 버프의 효과가 적용 되었을때 True.

	public float[] datas; // 버프 내에서 필요한 데이터 들

	public Buff()
	{
	}

	public Buff( int _id, BuffType _type, Performence[] _buff, float[] _datas = null )
	{
		id = _id;
		type = _type;
		if( _buff != null )
		{
			buff = new Performence[_buff.Length];
			for( int i = 0 ; i < buff.Length ; ++i )
				buff[i] = _buff[i];
		}
		if( _datas != null )
		{
			datas = new float[_datas.Length];
			for( int i = 0 ; i < datas.Length ; ++i )
				datas[i] = _datas[i];
		}
	}

	public Buff( Buff _bf )
	{
		id = _bf.id;
		type = _bf.type;
		if( _bf.buff != null )
		{
			buff = new Performence[_bf.buff.Length];
			for( int i = 0 ; i < buff.Length ; ++i )
				buff[i] = _bf.buff[i];
		}
		if( _bf.datas != null )
		{
			datas = new float[_bf.datas.Length];
			for( int i = 0 ; i < datas.Length ; ++i )
				datas[i] = _bf.datas[i];
		}
	}

	public virtual Buff Clone( )
	{
		Buff clone = new Buff( this );
		return clone;
	}

	// Attech될때 호출된다. 이것으로 동작을 시작한다.
	public virtual void Init( StatusScript _stat )
	{
		tgStat = _stat;
		tgStat.StartCoroutine( CoBuffing( ) );
	}

	public void Uninit( )
	{
		if( tgStat.RemoveBuff( this ) )
			DeApplyBuff( );
	}

	protected virtual void ApplyBuff( )
	{
		if( bEffected )
			return;
		bEffected = true;

		foreach( var it in buff )
		{
			tgStat.stats[(int)it.type].ApplyAdd( it.add );
			tgStat.stats[(int)it.type].ApplyScale( it.scale );
		}
	}

	protected virtual void DeApplyBuff( ) // 디태칭 시 호출되며, 어태칭 시 적용된 것을 해제한다.
	{
		if( !bEffected )
			return;
		bEffected = false;

		foreach( var it in buff )
		{
			tgStat.stats[(int)it.type].DeApplyAdd( it.add );
			tgStat.stats[(int)it.type].DeApplyScale( it.scale );
		}
		tgStat.CalcStat( );
	}

	protected virtual IEnumerator CoBuffing( )
	{
		yield break;
	}
}



public class PassiveBuff : Buff
{
	public PassiveBuff( )
		:base()
	{

	}
	public PassiveBuff( int _id, Performence[] _buff, float[] _datas = null )
		: base( _id, BuffType.Pasive, _buff, _datas )
	{
	}

	public PassiveBuff( PassiveBuff _bf )
		: base(_bf)
	{

	}

	public override Buff Clone( )
	{
		Buff clone = new PassiveBuff( this );
		return clone;
	}

	protected override IEnumerator CoBuffing( )
	{
		ApplyBuff( );
		if( tgStat.name == "Player" )
			GameMgr.Instance.uiMgr.buffIconUI.AddBuffIcon( this );
		yield break; // 외부에서 제어한다.
	}
}



public class TimeLimitBuff : Buff
{
	public enum Datas
	{
		Time,
	}

	public TimeLimitBuff()
		:base()
	{

	}

	public TimeLimitBuff( int _id, Performence[] _buff, float[] _datas )
		: base( _id, BuffType.TimeLimit, _buff, _datas )
	{
	}

	public TimeLimitBuff( TimeLimitBuff _bf )
		: base(_bf)
	{

	}

	public override Buff Clone( )
	{
		Buff clone = new TimeLimitBuff( this );
		return clone;
	}

	protected override IEnumerator CoBuffing( )
	{
		ApplyBuff( );
		if( tgStat.name == "Player" )
			GameMgr.Instance.uiMgr.buffIconUI.AddBuffIcon( this );
		Debug.Log( datas[0] );
		yield return new WaitForSeconds( datas[0] );
		Debug.Log( datas[0] );
		Uninit( ); // 버프 삭제
	}
}



public class CountLimitBuff : Buff
{
	private enum Datas
	{
		Target,
		Count
	}
	public CountLimitBuff()
		:base()
	{

	}
	public CountLimitBuff( int _id, Performence[] _buff, float[] _datas )
		: base( _id, BuffType.CountLimit, _buff, _datas )
	{
	}

	public CountLimitBuff( CountLimitBuff _bf )
		: base(_bf)
	{

	}

	public override Buff Clone( )
	{
		Buff clone = new CountLimitBuff( this );
		return clone;
	}

	protected override IEnumerator CoBuffing( )
	{
		ApplyBuff( );

		var tg = tgStat.status[(int)datas[(int)Datas.Target]] + datas[(int)Datas.Count];

		while( tg > tgStat.status[(int)datas[(int)Datas.Target]] )
			yield return null;

		Uninit( ); // 버프 삭제
	}
}



public class AccumulateBuff : Buff
{
	private enum Datas
	{
		Time,
		Target,
		Count
	}

	int Count = 0; // 누적 카운트

	public AccumulateBuff(  )
		: base()
	{
		Count = 0;
	}

	public AccumulateBuff( int _id, Performence[] _buff, float[] _datas )
		: base( _id, BuffType.Accumulate, _buff, _datas )
	{
		Count = 0;
	}

	public AccumulateBuff( AccumulateBuff _bf )
		: base(_bf)
	{
		Count = 0;
	}

	public override Buff Clone( )
	{
		Buff clone = new AccumulateBuff( this );
		return clone;
	}

	public override void Init( StatusScript _stat )
	{
		Count = 0;
		base.Init( _stat );
	}

	protected override void ApplyBuff( )
	{
		++Count;
		foreach( var it in buff )
		{
			tgStat.stats[(int)it.type].ApplyAdd( it.add );
			tgStat.stats[(int)it.type].ApplyScale( it.scale );
		}
	}

	protected override void DeApplyBuff( ) // 싹 다 지운다
	{
		for( int i = 0 ; i < Count ; ++i )
		{
			foreach( var it in buff )
			{
				tgStat.stats[(int)it.type].DeApplyAdd( it.add );
				tgStat.stats[(int)it.type].DeApplyScale( it.scale );
			}
		}
		Count = 0;
	}

	protected override IEnumerator CoBuffing( )
	{
		var acc = Time.time + datas[(int)Datas.Time];

		var tg = tgStat.status[(int)datas[(int)Datas.Target]] + datas[(int)Datas.Count];

		while( acc > Time.time )
		{
			if( tg > tgStat.status[(int)datas[(int)Datas.Target]] )
			{
				tg = tgStat.status[(int)datas[(int)Datas.Target]] + datas[(int)Datas.Count];
				ApplyBuff( );
			}
			yield return null;
		}

		Uninit( ); // 버프 삭제
	}
}



public class TickBuff : Buff
{
	private enum Datas
	{
		Time,
		Target,
		Tick,
		Damege
	}

	public TickBuff( )
	: base(  )
	{
	}

	public TickBuff( int _id, float[] _datas )
		: base( _id, BuffType.Tick, null, _datas )
	{
	}

	public TickBuff( TickBuff _bf )
	: base( _bf )
	{
	}

	public override Buff Clone( )
	{
		Buff clone = new TickBuff( this );
		return clone;
	}


	protected override void ApplyBuff( )
	{
		tgStat.status[(int)datas[(int)Datas.Target]] += tgStat.status[(int)datas[(int)Datas.Damege]];
		tgStat.cs.DemagePrint( -tgStat.status[(int)datas[(int)Datas.Damege]], new Color( 0.3f, 0.9f, 0.3f ), false );
	}

	protected override void DeApplyBuff( )
	{
	}

	protected override IEnumerator CoBuffing( )
	{
		if( tgStat.name == "Player" )
			GameMgr.Instance.uiMgr.buffIconUI.AddBuffIcon( this );

		var acc = Time.time + datas[(int)Datas.Time];

		var tg = Time.time + datas[(int)Datas.Tick];

		while( acc > Time.time )
		{
			if( tg > Time.time )
			{
				tg = Time.time + datas[(int)Datas.Tick];
				ApplyBuff( );
			}
			yield return null;
		}

		Uninit( ); // 버프 삭제
	}

}

class HealBuff : Buff
{
	private enum Datas
	{
		Add,
		Scale,
	}

	public HealBuff( )
		:base()
	{
	}

	public HealBuff( Buff _bf ) 
		: base( _bf )
	{
	}

	public override Buff Clone( )
	{
		return new HealBuff( this );
	}

	protected override IEnumerator CoBuffing( )
	{
		var heal = datas[(int)Datas.Add] +
			tgStat[PlanetUtil.CharacterStat.MaxHp] * datas[(int)Datas.Scale];

		tgStat.Demage( -heal );
		tgStat.cs.DemagePrint( heal, new Color( 0.1f, 1.0f, 0.1f ), true );

		//tgStat.status[(int)PlanetUtil.CharacterStatus.NowHp];
		yield break;
	}
}