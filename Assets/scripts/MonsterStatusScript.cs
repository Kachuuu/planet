﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterStatusScript : StatusScript
{
	public Collider2D awaRange;
	public Collider2D wallChkRange;
	public Collider2D MovableChkRange;
	public Collider2D AttackRange;

	public HpbarScript hpBar;

	public List<ActiveAttackSkill> skills = new List<ActiveAttackSkill>();
	public ActiveAttackSkill specialSkill = null;

	public int RewardId = 2201;
	public int exp = 1;

	// 페이즈 관련
	public int nowPhase = 0;                // 페이즈 단계
	public List<PhaseData> phaseData = new List<PhaseData>();
	public delegate IEnumerator PhaseAction( MonsterStatusScript _mstat );
	public PhaseAction phaseAction;

	// 플레이어 데이터 관련 키, 몬스터마다 소환할 때 따로 지정
	public string keyName; //= "RedReptail";
	public string keyCategory;// = "NormalMonster";

	public struct PhaseData
	{
		public float trigerHpRate;
		public float attackspeedScale;     // 공격 속도
		public float movespeedScale;       // 이동 속도
		public int ableSkillCount;         // SetPhase Skill
		public int specialSkillIndex;       // 특수 공격 인덱스
		public Color clr;

		public PhaseData( float _hpRate, float attackspeedScale, float movespeedScale, int ableSkillCount, int specialSkillIndex, Color _clr )
		{
			this.trigerHpRate = _hpRate;
			this.attackspeedScale = attackspeedScale;
			this.movespeedScale = movespeedScale;
			this.ableSkillCount = ableSkillCount;
			this.specialSkillIndex = specialSkillIndex;
			this.clr = _clr;
		}
	}



	public void AddSkill( MonsterDefSkill _skill )
	{
		skills.Add( _skill );
	}

	public void AddPhaseData( 
		float _hpRate,		// 다음 페이지로 넘어가기위한 체력 퍼센트 (다음 페이즈가 없다면 음수여야 한다.)
		float _atkSpd,		// 현재 페이즈의 공격 속도 배율
		float _movSpd,		// 현재 페이즈의 이동 속도 배율
		int _skillCnt,		// 사용 가능한 스킬 개수 ( 3 이라면 0 ~ 2 인덱스의 스킬만 사용할 수 있다.) 
		int specialSkillIndex, //현재 페이즈 진입시 동작할 스페셜 스킬의 인덱스.(해당 스킬의 4레벨로 동작한다.)
		Color _clr )
	{
		var dummy = new PhaseData( _hpRate, _atkSpd, _movSpd, _skillCnt, specialSkillIndex, _clr );
		phaseData.Add( dummy );
	}

	private StatusScript tg;
	public StatusScript Tg
	{
		get { return tg; }
		set
		{
			tg = value;
			awaRange.enabled = ( tg == null );
		}
	}
	public bool Target { get { return tg != null; } }

	public float dontFollowDist = 1.5f;
	public float followDistance = 10f;
	public float FollowDistance { get { return followDistance; } }

	public override void Init( )
	{
		base.Init( );
		awaRange.enabled = true;
		GetComponentInChildren<SpriteRenderer>( ).color = Color.white;
		nowPhase = 0;
		phaseData.Clear( );
		skills.Clear( );
		hpBar.SetHp( HpRate );
	}

	public override void Demage( float _dmg )
	{
		var hp = status[(int)PlanetUtil.CharacterStatus.NowHp];
		status[(int)PlanetUtil.CharacterStatus.NowHp] = Mathf.Clamp( hp - _dmg, 0, (int)stats[(int)PlanetUtil.CharacterStat.MaxHp].final );

		hpBar.SetHp( HpRate );

		// 사망 시
		if( HpRate == 0f && State != PlanetUtil.CharacterState.Death )
		{
			State = PlanetUtil.CharacterState.Death;
			//Debug.Log( keyName + "" + keyCategory );			
			PlayerData.Instance.PlayerDataSet( DataKind.kill, keyName, 1 );
			PlayerData.Instance.PlayerDataSet( DataKind.kill, keyCategory, 1 );
		}

		bHit = true; // brhavior Tree 에서 false 한다.

	}
}
