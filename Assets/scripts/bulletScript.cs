﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class bulletScript : MonoBehaviour
{

	public float speed = 500f;
	public StatusScript stat;
	public ControlScript.DefCallback callback;

	private bool enemy; // 적이 쏜것인가?
	private Transform target; // 따라갈 타겟
	private float followSpeed;

	public float range = 10f;
	private float acc = 0f;
	public int Penetrate = 0;
	public float stunTime = 0f;

	public float dmgAdd = 0f;
	public float dmgScale = 1f;

	RaycastHit2D[] hits = new RaycastHit2D[10];
	ContactFilter2D filter = new ContactFilter2D();

	List<GameObject> allHitList = new List<GameObject>();


	private void Awake( )
	{
		filter.layerMask = LayerMask.GetMask( "Ground" ) | LayerMask.GetMask( "Enemy" );
		filter.useLayerMask = true;
	}

	public void Init( bool _enemy, StatusScript _stat, Transform _tg = null, float _range = 10f, float _speed = 20f, float _followSpeed = 1f, int _Penetrate = 0, float _stunTime = 0f, ControlScript.DefCallback _callback = null )
	{
		stat = _stat;
		callback = _callback;
		target = _tg;
		range = _range;
		speed = _speed;
		followSpeed = _followSpeed;
		Penetrate = _Penetrate;
		stunTime = _stunTime;
		allHitList.Clear( );

		enemy = _enemy;
		if( enemy ) filter.layerMask = /*LayerMask.GetMask( "Ground" ) |*/ LayerMask.GetMask( "Player" );
		else filter.layerMask = LayerMask.GetMask( "Ground" ) | LayerMask.GetMask( "Enemy" );
	}

	private void OnEnable( )
	{
		allHitList.Clear( );

		dmgAdd = 0f;
		dmgScale = 1f;
		acc = 0f;
		target = null;
		enemy = false;
		if( enemy ) filter.layerMask = /*LayerMask.GetMask( "Ground" ) |*/ LayerMask.GetMask( "Player" );
		else filter.layerMask = LayerMask.GetMask( "Ground" ) | LayerMask.GetMask( "Enemy" );
	}

	private void Update( )
	{
		if( acc > range )
		{
			GetComponent<GOScript>( ).DoDistroy( );
			return;
		}
		if( target != null )
		{
			FollowCurve( );
		}

		FireBullet( );
	}

	private void FireBullet( )
	{
		var dir = transform.right * speed * Time.deltaTime;

		int cnt = Physics2D.Raycast(transform.position, transform.right, filter, hits, dir.magnitude );
		if( cnt != 0 )
		{
			// 거리순 정렬
			List<RaycastHit2D> hitList = new List<RaycastHit2D>();
			for( int i = 0 ; i < cnt ; ++i )
				hitList.Add( hits[i] );
			hitList.OrderBy( h => h.distance ).ToArray( );

			// 충돌한 적 찾기
			float listDist = dir.magnitude;
			for( int i = 0 ; i < cnt ; ++i )
			{
				if( hitList[i].collider.tag == "Ground" ||
					hitList[i].collider.tag == "GroundPlatform" )
				{
					GetComponent<GOScript>( ).DoDistroy( );
					listDist = hitList[i].distance;
					Penetrate = 0;
					break;
				}

				if( allHitList.Find( obj => obj == hitList[i].collider.gameObject ) )
					continue;

				allHitList.Add( hitList[i].collider.gameObject );
				hitList[i].collider.GetComponent<ControlScript>( ).Demage( stat, dmgAdd, dmgScale, PlanetUtil.AttackType.Melee, false, stunTime );
				GameMgr.Instance.objMgr.BringObject( ObjectPoolManager.PoolType.HitEffect, hitList[i].point );
				if( null != callback )
					callback( hitList[i].collider.GetComponent<StatusScript>( ) );

				--Penetrate;
				if( Penetrate <= 0 )
				{
					GetComponent<GOScript>( ).DoDistroy( );
					break;
				}
			}
			dir *= listDist / dir.magnitude;
		}

		gameObject.transform.Translate( dir, Space.World );
		acc += dir.magnitude;
	}

	void FollowCurve( )
	{
		var tgPos = target.position;
		var bltPos = transform.position;

		float digree = Mathf.Atan2( tgPos.y - bltPos.y, tgPos.x - bltPos.x ) * Mathf.Rad2Deg;
		float nowDig = transform.eulerAngles.z;

		digree = Mathf.LerpAngle( nowDig, digree, Time.deltaTime * followSpeed );

		transform.rotation = Quaternion.Euler( 0f, 0f, digree );
	}

}
