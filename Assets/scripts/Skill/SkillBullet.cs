﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBulletSkill : ActiveAttackSkill
{
	public BigBulletSkill( )
	{
		prevDelay = 0f;
		playTime = 0.4f;
		nextDelay = 0.2f;
	}
	public override IEnumerator CoSkill( StatusScript stat, Rigidbody2D rigid = null )
	{
		//스킬 레벨에 따라 스킬이 달라져야함
		stat.State = PlanetUtil.CharacterState.Skill;
		yield return stat.StartCoroutine( CoSkillPrevDelay( ) );

		yield return stat.StartCoroutine( SkillEffect( stat, rigid ) );

		yield return stat.StartCoroutine( CoSkillNextDelay( ) );
		stat.State = PlanetUtil.CharacterState.Idle;
	}
	public override IEnumerator SkillEffect( StatusScript stat, Rigidbody2D rigid = null )
	{
		stat.am.SetTrigger( "Attack1" );
		float count = playTime;
		Vector3 euler = Vector3.up * (stat.sr.flipX ? 0f : 180f);
		GameMgr.Instance.objMgr.BringObject( ObjectPoolManager.PoolType.BulletBig, stat.transform.position, Quaternion.Euler( euler ) );

		while( count > 0f )
		{
			stat.statable[(int)PlanetUtil.CharacterStatables.Jump].Reset( false );
			stat.statable[(int)PlanetUtil.CharacterStatables.Move].Reset( false );
			count -= Time.deltaTime;
			stat.transform.position += Vector3.left * 1f * Time.deltaTime * ( stat.sr.flipX ? -1f : 1f );
			yield return 0;
		}

		Debug.Log( "BigBullet !!" );
		stat.statable[(int)PlanetUtil.CharacterStatables.Jump].Reset( true );
		stat.statable[(int)PlanetUtil.CharacterStatables.Move].Reset( true );
		yield return 0;
	}
}




public class SpecialSkill : ActiveAttackSkill
{
	public SpecialSkill( )
	{
		prevDelay = 0f;
		playTime = 1.5f;
		nextDelay = 0.5f;
	}

	public override IEnumerator CoSkill( StatusScript stat, Rigidbody2D rigid = null )
	{
		//스킬 레벨에 따라 달라져야함
		stat.State = PlanetUtil.CharacterState.Skill;

		//선딜 
		yield return stat.StartCoroutine( CoSkillPrevDelay( ) );

		//발동
		yield return stat.StartCoroutine( SkillEffect( stat, rigid ) );

		//후딜
		yield return stat.StartCoroutine( CoSkillNextDelay( ) );

		stat.State = PlanetUtil.CharacterState.Idle;
	}

	public override IEnumerator SkillEffect( StatusScript stat, Rigidbody2D rigid = null )
	{
		var rigidScale = rigid.gravityScale;
		rigid.gravityScale = 0f;
		stat.am.SetTrigger( "Attack3" );

		float count = 0f;
		float speed1 = 1f;
		float speed2 = 0.5f;
		float targetY = 0.2f;
		float currY = stat.transform.position.y;

		while( stat.transform.position.y <= currY + targetY )
		{
			count += Time.deltaTime / 2f;
			stat.transform.position += Vector3.up * count * speed1;
			yield return 0;
		}
		Vector3 euler = Vector3.up * (stat.sr.flipX ? 180f : 0f);
		GameMgr.Instance.objMgr.BringObject( ObjectPoolManager.PoolType.BulletMany, stat.transform.position, Quaternion.Euler( euler ) );

		count = 0f;
		while( count < playTime )
		{
			stat.statable[(int)PlanetUtil.CharacterStatables.Jump].Reset( false );
			stat.statable[(int)PlanetUtil.CharacterStatables.Move].Reset( false );
			count += Time.deltaTime;
			stat.transform.position += Vector3.up * Time.deltaTime * speed2;
			yield return 0;
		}
		Debug.Log( "SpecialBullet !!" );
		stat.statable[(int)PlanetUtil.CharacterStatables.Jump].Reset( true );
		stat.statable[(int)PlanetUtil.CharacterStatables.Move].Reset( true );
		rigid.gravityScale = rigidScale;
		yield return 0;
	}
}