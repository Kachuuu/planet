﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ActiveAttackSkill
{
	public int level;
	public float prevDelay;
	public float nextDelay;
	public float playTime;

	public virtual IEnumerator CoSkill( StatusScript stat, Rigidbody2D rigid = null )
	{
		yield return 0;
	}

	public virtual IEnumerator SkillEffect( StatusScript stat, Rigidbody2D rigid = null )
	{
		yield return 0;
	}

	public virtual IEnumerator CoSkillPrevDelay( )
	{
		yield return new WaitForSeconds( prevDelay );
	}

	public virtual IEnumerator CoSkillNextDelay( )
	{
		yield return new WaitForSeconds( nextDelay );
	}
}


public class MonsterDefSkill : ActiveAttackSkill
{
	public struct Datas
	{
		public float demageScale;   // 데미지 배율
		public int repeat;          // 최대 연속 반복 횟수
		public float distance;      // 사거리
		public float coolTime;      // 재사용 대기 시간

		public Datas( float demageScale, int repeat, float distance, float coolTime )
		{
			this.demageScale = demageScale;
			this.repeat = repeat;
			this.distance = distance;
			this.coolTime = coolTime;
		}
	}

	public Datas[] datas;
	public Vector2 offset;
	public Vector2 size;
	public BoxCollider2D col;
	public string animName;
	public float coolAcc;
	protected int nowRepeat;

	public MonsterDefSkill( float _prevD, float _actD, float _postD, Vector2 _offset, Vector2 _size, BoxCollider2D _col, string _animName, Datas[] _datas )
	{
		prevDelay = _prevD;
		playTime = _actD;
		nextDelay = _postD;
		coolAcc = 0f;
		offset = _offset;
		size = _size;
		col = _col;
		animName = _animName;
		level = 0;
		nowRepeat = 0;

		if( _datas != null )
		{
			datas = new Datas[_datas.Length];
			for( int i = 0 ; i < datas.Length ; ++i )
				datas[i] = _datas[i];
		}
	}

	public virtual bool Able( float _dist )
	{
		if( nowRepeat < datas[level].repeat && datas[level].distance >= _dist && coolAcc < Time.time )
			return true;
		return false;
	}

	public void RepeatReset( )
	{
		nowRepeat = 0;
	}

	public override IEnumerator CoSkill( StatusScript stat, Rigidbody2D rigid = null )
	{
		stat.State |= PlanetUtil.CharacterState.Attack;
		coolAcc = Time.time + datas[level].coolTime;
		++nowRepeat;

		if( level == 3 )
			stat.SetStatable( PlanetUtil.StatableSet.Hit, false );

		stat.am.speed = ( (MonsterStatusScript)stat ).phaseData[( (MonsterStatusScript)stat ).nowPhase].attackspeedScale;
		stat.am.SetTrigger( animName );
		yield return new WaitForSeconds( prevDelay / stat.am.speed );
		if( col != null )
		{
			col.offset = offset;
			col.size = size;
		}

		yield return stat.StartCoroutine( CoAttack_Core( stat ) );

		if( col != null )
		{
			col.offset = Vector2.zero;
			col.size = size;
		}

		yield return new WaitForSeconds( nextDelay / stat.am.speed );
		if( level == 3 )
		{
			stat.SetStatable( PlanetUtil.StatableSet.Hit, true );
			level = ( (MonsterStatusScript)stat ).nowPhase;
		}
		stat.am.speed = 1f;
		stat.State &= ~PlanetUtil.CharacterState.Attack;
		stat.coAttack = null;
	}

	public virtual IEnumerator CoAttack_Core( StatusScript stat )
	{
		yield return null;
	}
}


public class MonsterNormalSkill : MonsterDefSkill
{

	public MonsterNormalSkill( BoxCollider2D _col, Datas[] _datas, float _prev, float _delay, float _post, Vector2 _offset, Vector2 _size )
		: base( _prev, _delay, _post, _offset, _size, _col, "Attack1", _datas )
	{
	}

	public override IEnumerator CoAttack_Core( StatusScript stat )
	{
		( (MonsterControlScript)stat.cs ).PlayMonsterAttackSound( );

		float acc = 0f;
		float atkTime = playTime / stat.am.speed;
		while( acc < atkTime )
		{
			acc += Time.deltaTime;
			if( col.enabled == true )
			{
				var player = PlanetUtil.RangeIn( col, "Player" );
				if( null != player )
				{
					yield return new WaitForSeconds( 0.1f );
					player.GetComponent<ControlScript>( ).Demage( stat, 0, datas[level].demageScale );
					break;
				}
			}
			yield return null;
		}
		yield return new WaitForSeconds( atkTime - acc );
	}
}


public class MonsterDashSkill : MonsterDefSkill
{
	public Vector2[] dashScale;


	public MonsterDashSkill( BoxCollider2D _col, Datas[] _datas, Vector2[] _dashScales, float _prev, float _delay, float _post, Vector2 _offset, Vector2 _size )
		: base( _prev, _delay, _post, _offset, _size, _col, "Attack2", _datas )
	{
		dashScale = new Vector2[_dashScales.Length];
		for( int i = 0 ; i < _dashScales.Length ; ++i )
			dashScale[i] = _dashScales[i];
	}

	public override IEnumerator CoAttack_Core( StatusScript stat )
	{
		( (MonsterControlScript)stat.cs ).PlayMonsterAttackSound( );
		stat.velocity.y = stat[PlanetUtil.CharacterStat.JumpPower] * dashScale[level].y;

		float x = stat[PlanetUtil.CharacterStat.MoveSpeed] * dashScale[level].x * stat.transform.right.x * stat.am.speed;
		float acc = 0f;
		float atkTime = playTime / stat.am.speed;
		bool bHit = false;
		while( acc < atkTime )
		{
			acc += Time.deltaTime;
			stat.velocity.x = x;
			if( col.enabled == true )
			{
				var player = PlanetUtil.RangeIn( col, "Player" );
				if( null != player && false == bHit )
				{
					player.GetComponent<ControlScript>( ).Demage( stat, 0, datas[level].demageScale );
					bHit = true;
				}
			}
			yield return null;
		}
	}
}

// 앞으로 걸어가며 공격 // 애니메이션이 시작, 종료가 있어야함.(시작과 종료는 트리거로 설정된다)
public class MonsterGoingSkill : MonsterDefSkill
{
	public struct SubData
	{
		public float moveScale; // 공격시 이동 속도 배율
		public int repeatCnt;   // 공격 반복 횟수

		public SubData( float moveScale, int repeatCnt )
		{
			this.moveScale = moveScale;
			this.repeatCnt = repeatCnt;
		}
	}

	public SubData[] subDatas;
	public string animEndName;

	public MonsterGoingSkill( BoxCollider2D _col, Datas[] _datas, SubData[] _subDatas, float _prev, float _delay, float _post, Vector2 _offset, Vector2 _size )
		: base( _prev, _delay, _post, _offset, _size, _col, "Attack3", _datas )
	{
		subDatas = new SubData[_subDatas.Length];
		for( int i = 0 ; i < _subDatas.Length ; ++i )
			subDatas[i] = _subDatas[i];
		animEndName = "Attack3_End";
	}

	public override IEnumerator CoAttack_Core( StatusScript stat )
	{
		float x = stat[PlanetUtil.CharacterStat.MoveSpeed] * subDatas[level].moveScale * stat.transform.right.x * stat.am.speed;
		float atkTime = ( playTime ) / stat.am.speed;

		for( int i = 0 ; i < subDatas[level].repeatCnt ; ++i )
		{
			( (MonsterControlScript)stat.cs ).PlayMonsterAttackSound( );

			bool bHit = false;
			float acc = 0f;

			while( acc < atkTime )
			{
				acc += Time.deltaTime;
				stat.velocity.x = x;
				if( col.enabled == true )
				{
					var player = PlanetUtil.RangeIn( col, "Player" );
					if( null != player && false == bHit )
					{
						yield return new WaitForSeconds( 0.1f );
						player.GetComponent<ControlScript>( ).Demage( stat, 0, datas[level].demageScale );
						bHit = true;
					}
				}
				yield return null;
			}

			if( null != PlanetUtil.RangeIn( ( (MonsterStatusScript)stat ).wallChkRange, "Ground" ) )
				break;
		}
		stat.am.SetTrigger( animEndName );
	}
}


// 걍 평타 연속 ///애니메이션이 시작, 공격, 종료가 있어야함.(시작과 종료는 트리거로 설정된다)
public class MonsterMultiSkill : MonsterDefSkill
{
	public int[] counts;
	public string animEndName;

	public MonsterMultiSkill( BoxCollider2D _col, Datas[] _datas, int[] _counts, Vector2 _offset, Vector2 _size )
		: base( 0.9f, 0.6f, 0.6f, _offset, _size, _col, "Attack3", _datas )
	{
		counts = new int[_counts.Length];
		for( int i = 0 ; i < _counts.Length ; ++i )
			counts[i] = _counts[i];
		animEndName = "Attack3_End";
	}


	public override IEnumerator CoAttack_Core( StatusScript stat )
	{
		for( int i = 0 ; i < counts[level] ; ++i )
		{
			( (MonsterControlScript)stat.cs ).PlayMonsterAttackSound( );
			col.enabled = true;

			float acc = 0f;
			float atkTime = playTime / stat.am.speed;
			while( acc < atkTime )
			{
				acc += Time.deltaTime;

				if( col.enabled == true )
				{
					var player = PlanetUtil.RangeIn( col, "Player" );
					if( null != player )
					{
						player.GetComponent<ControlScript>( ).Demage( stat, 0, datas[level].demageScale );
						break;
					}
				}
				yield return null;
			}
			yield return new WaitForSeconds( atkTime - acc );
		}

		stat.am.SetTrigger( animEndName );
	}
}


// 투사체 스킬 // 전체 모션이 리핏된다.
public class MonsterThrowSkill : MonsterDefSkill
{
	public ObjectPoolManager.PoolType objType;
	public int[] throwCnt;
	public int acc;

	public MonsterThrowSkill( ObjectPoolManager.PoolType _objType, Datas[] _datas, int[] _throwCnt )
		: base( 1.1f, 0.4f, 0.9f, Vector2.zero, Vector2.zero, null, "Attack2", _datas )
	{
		objType = _objType;
		throwCnt = new int[_throwCnt.Length];
		for( int i = 0 ; i < throwCnt.Length ; ++i )
			throwCnt[i] = _throwCnt[i];

		acc = 0;
	}



	public override IEnumerator CoSkill( StatusScript stat, Rigidbody2D rigid = null )
	{
		( (MonsterControlScript)stat.cs ).PlayMonsterAttackSound( );
		stat.State |= PlanetUtil.CharacterState.Attack;
		coolAcc = Time.time + datas[level].coolTime;
		++nowRepeat;
		stat.am.speed = ( (MonsterStatusScript)stat ).phaseData[( (MonsterStatusScript)stat ).nowPhase].attackspeedScale;

		if( level == 3 )
			stat.SetStatable( PlanetUtil.StatableSet.Hit, false );

		for( int i = 0 ; i < throwCnt[level] ; ++i )
		{
			stat.am.SetTrigger( animName );
			yield return new WaitForSeconds( prevDelay / stat.am.speed );

			if( col != null )
			{
				col.offset = offset;
				col.size = size;
			}

			yield return stat.StartCoroutine( CoAttack_Core( stat ) );
			++acc;

			if( col != null )
			{
				col.offset = Vector2.zero;
				col.size = size;
			}

			yield return new WaitForSeconds( nextDelay / stat.am.speed );
		}

		if( level == 3 )
		{
			stat.SetStatable( PlanetUtil.StatableSet.Hit, true );
			level = ( (MonsterStatusScript)stat ).nowPhase;
		}

		stat.am.speed = 1f;
		stat.State &= ~PlanetUtil.CharacterState.Attack;
		stat.coAttack = null;
	}


	public override IEnumerator CoAttack_Core( StatusScript stat )
	{
		( (MonsterControlScript)stat.cs ).PlayMonsterAttackSound( );
		var go = GameMgr.Instance.objMgr.BringObject( objType, stat.shotPos.position );
		go.GetComponent<GORock>( ).stat = stat;
		go.GetComponent<GORock>( ).scale = datas[level].demageScale;
		var rb = go.GetComponent<Rigidbody2D>( );
		rb.velocity = Vector2.zero;
		rb.angularVelocity = 30f;
		rb.AddForce( new Vector2( stat.transform.right.x * 1500f, 90f ) );

		yield return new WaitForSeconds( playTime );
	}
}

// 투사체 스킬 // 공격 모션만 모션이 리핏된다. // 애니메이션이 시작, 종료가 있어야함.(시작과 종료는 트리거로 설정된다)
public class MonsterShotSkill : MonsterDefSkill
{
	public struct SubData
	{
		public float shotThrowSpeed;    // 탄환 속도
		public float AliveTime;         // 탄환 생존 시간
		public int  shotCount;          // 연속 발사 횟수
		public bool bFollowing;         // 추적 여부
		public bool bTargetting;        // 타겟을 향해 발사하는지 여부 ( false면 정면으로 발사 )
		public float followSpeed;       // 추적시 회전 속도
		public float angle;             // 발사 범위 앵글
		public int bltCount;            // 1회 발사시 발사되는 탄환 수

		public SubData( float AliveTime, float shotThrowSpeed, int shotCount, bool bFollowing, bool bTargetting, float followSpeed, float angle, int bltCount )
		{
			this.AliveTime = AliveTime;
			this.shotThrowSpeed = shotThrowSpeed;
			this.shotCount = shotCount;
			this.bFollowing = bFollowing;
			this.bTargetting = bTargetting;
			this.followSpeed = followSpeed;
			this.angle = angle;
			this.bltCount = bltCount;
		}
	}

	public SubData[] subDatas;
	public string animEndName;
	public ObjectPoolManager.PoolType objType;

	public List<GameObject> blts = new List<GameObject>(); // 나가퀸의 특수 패턴 전용. 탄환이 소멸했는지 판단 여부

	public MonsterShotSkill( Datas[] _datas, SubData[] _subDatas, ObjectPoolManager.PoolType _type, float _prev, float _delay, float _post )
		: base( _prev, _delay, _post, Vector2.zero, Vector2.zero, null, "Attack3", _datas )
	{
		subDatas = new SubData[_subDatas.Length];
		for( int i = 0 ; i < _subDatas.Length ; ++i )
			subDatas[i] = _subDatas[i];
		animEndName = "Attack3_End";
		objType = _type;
	}

	public override IEnumerator CoAttack_Core( StatusScript stat )
	{
		var tg = subDatas[level].bFollowing ? GameMgr.Instance.Player.centerPos : null;
		blts.Clear( );

		for( int i = 0 ; i < subDatas[level].shotCount ; ++i )
		{
			( (MonsterControlScript)stat.cs ).PlayMonsterAttackSound( );

			var ang = -subDatas[level].angle * 0.5f;
			var angAcc = subDatas[level].angle / subDatas[level].bltCount;

			for( int j = 0 ; j < subDatas[level].bltCount ; ++j )
			{
				// 탄환 생성
				var blt = GameMgr.Instance.objMgr.BringObject( objType, stat.shotPos.position );
				var bs = blt.GetComponent<bulletScript>( );

				if( level == 4 ) // 나가퀸의 특수 패턴이라면
					blts.Add( blt ); // 리스트에 등록

				//플레이어를 향해 발사
				if( subDatas[level].bTargetting )
				{
					var tgPos =  GameMgr.Instance.Player.centerPos.position;
					var bltPos = bs.transform.position;
					float digree = Mathf.Atan2( tgPos.y - bltPos.y, tgPos.x - bltPos.x ) * Mathf.Rad2Deg;
					bs.transform.rotation = Quaternion.Euler( 0f, 0f, digree + ang );
				}
				else // 정면으로 발사
				{
					bs.transform.rotation = Quaternion.Euler( 0f, 0f, stat.transform.eulerAngles.y + ang );
				}

				// 탄환에 설정 값 세팅
				bs.Init( true, stat, tg, subDatas[level].AliveTime, subDatas[level].shotThrowSpeed, subDatas[level].followSpeed );
				bs.dmgScale = datas[level].demageScale;

				ang += angAcc;
			}

			yield return new WaitForSeconds( playTime / stat.am.speed );
		}
		stat.am.SetTrigger( animEndName );
	}
}