﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//1.포탑소환
//2.드론소환
//3.강화

//1.포탑설치
public class Active_Scv_Tower : ActiveSkill
{
	public float Tspeed;
	public float Tdelay;
	public int TbltCount;

	public List<Skill_TowerScript> TowerList;

	public int towerCount;
	public int towerMaxCount;

	public Active_Scv_Tower( StatusScript stat, int spriteId, int _idx ) : base( stat, spriteId, PlanetUtil.WeaponType.Scv, _idx )
	{
		skillName = "포탑설치";
		skillExplain = "적을 향해 자동 공격하는 포탑을 설치합니다. 포탑의 체력이 0이되면 사라집니다.";

		ActiveSkillTower( );
		TowerList = new List<Skill_TowerScript>( );

		coolTime = 0f;

		Tspeed = 15f;
		TbltCount = 10;
		Tdelay = 1f;

		towerMaxCount = 3;
		towerCount = towerMaxCount;

		SkilPathSetting( GetFloat( 12f, 4 ), GetFloat( 20f, 4 ), GetFloat( 0f, 4 ) );
		curCoolTime[2] += -2f;
		curCoolTime[3] += -4f;
		damage[2] += +10f;
		damage[3] += +10f;
		playTime[2] += +0f;
		playTime[3] += +0f;
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
	}

	public override IEnumerator CoSkill( float time = 0.3f )    //public IEnumerator Shoot( float time = 0.2f )
	{
		//사운드 
		//stat.State |= PlanetUtil.CharacterState.Skill;
		//stat.StartCoroutine( TowerDelay( ) );
		//while( time > 0f && !cancel )
		//{
		//	time -= Time.deltaTime;
		//	yield return 0;
		//	if( stat.bHit )
		//		cancel = true;
		//}

		if( !cancel )
		{
			stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_Gun_V_T] );
			if( TowerList.Count >= towerMaxCount )
			{
				stat.StartCoroutine( TowerList[0].TowerBreak( ) );
			}
			stat.bAttack = true;
			var pos = stat.shotPos.position;
			var rot = stat.shotPos.rotation;

			var cmd = GameMgr.Instance.objMgr.BringObject( ObjectPoolManager.PoolType.Tower, pos, rot );
			var bs = cmd.GetComponent<Skill_TowerScript>( );
			TowerList.Add( bs );
			bs.Tower( damage[level - 1], Tspeed, Tdelay, TbltCount, this );
			yield return 0;
			stat.StartCoroutine( CoEnd( ) );
		}
		else
		{
			//			stat.am.SetBool( "Attack", false );
			//stat.State &= ~PlanetUtil.CharacterState.Skill;
			stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
		}
		ResetCoolTime( );
		yield return 0;
	}

	public IEnumerator CoEnd( float time = 0f )
	{
		//		stat.am.SetBool( "Attack", false );
		stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
		yield return 0;
	}
}

//2.드론소환
public class Active_Scv_Dron : ActiveSkill
{
	public float Tspeed;
	public float Tdelay;
	public float Ttime;

	public List<Skill_TowerScript> TowerList;

	public Active_Scv_Dron( StatusScript stat, int spriteId, int _idx ) : base( stat, spriteId, PlanetUtil.WeaponType.Scv, _idx )
	{
		skillName = "드론소환";
		skillExplain = "일정 시간동안 유니티쨩을 따라 다니며 공격하는 드론을 소환합니다. 지속시간이 0이 되면 사라집니다.";
		ActiveSkillTower( );

		TowerList = new List<Skill_TowerScript>( );

		coolTime = 0f;

		Tspeed = 15f;
		Ttime = 20f;
		Tdelay = 0.5f;
		SkilPathSetting( GetFloat( 15f, 4 ), GetFloat( 5f, 4 ), GetFloat( 9f, 4 ) );
		curCoolTime[2] += -1f;
		curCoolTime[3] += -2f;
		damage[2] += +2f;
		damage[3] += +4f;
		playTime[2] += +1f;
		playTime[3] += +2f;
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
	}

	public override IEnumerator CoSkill( float time = 0.3f )    //public IEnumerator Shoot( float time = 0.2f )
	{
		//사운드 
		stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_Gun_V_D] );
		cancel = false;
		finish = false;
		//stat.State |= PlanetUtil.CharacterState.Skill;

		//stat.StartCoroutine( TowerDelay( ) );
		//while( time > 0f && !cancel )
		//{
		//	time -= Time.deltaTime;
		//	yield return 0;
		//	if( stat.bHit )
		//		cancel = true;
		//}
		yield return 0;
		if( !cancel )
		{
			var b = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.Dron, GameMgr.Instance.Player.Scv_Target.transform.position);
			Debug.Log( b );
			var a =b.GetComponent<Skill_DronScript>( );
			a.gameObject.SetActive( true );
			a.DronStart( damage[level - 1], Tspeed, Tdelay, playTime[level - 1] );
			stat.StartCoroutine( CoEnd( ) );
		}
		else
			stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
		ResetCoolTime( );
	}

	public IEnumerator CoEnd( float time = 0f )
	{
		stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
		yield return 0;
	}
}

//3.Upgrede!!!
public class Active_Scv_Upgrade : ActiveSkill
{
	public float delayCount;
	public Active_Scv_Upgrade( StatusScript stat, int spriteId, int _idx ) : base( stat, spriteId, PlanetUtil.WeaponType.Scv, _idx )
	{
		skillName = "Upgrede!!!";
		skillExplain = "일정 시간 동안 포탑과 드론을 강화합니다. 포탑과 드론의 공격속도가 대폭 상승합니다.";

		ActiveSkillTower( );

		coolTime = 0f;
		delayCount = 0.5f;
		SkilPathSetting( GetFloat( 20f, 4 ), GetFloat( 4f, 4 ), GetFloat( 0f, 4 ) );
		curCoolTime[2] += -3f;
		curCoolTime[3] += -6f;
		damage[2] += +2f;
		damage[3] += +4f;
		playTime[2] += +0f;
		playTime[3] += +0f;
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
	}

	public override IEnumerator CoSkill( float time = 0.3f )    //public IEnumerator Shoot( float time = 0.2f )
	{
		stat.SetStatable( PlanetUtil.StatableSet.SkillMoveAble );
		cancel = false;
		finish = false;
		UpgradeStart( );
		yield return null;
		if( stat.state == PlanetUtil.CharacterState.Skill )
			stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
	}
	public void UpgradeStart( )
	{
		stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_Gun_V_U] );
		var towers = GameObject.FindGameObjectsWithTag("Tower");
		var drons = GameObject.FindGameObjectsWithTag("Dron");
		foreach( var a in towers )
			a.GetComponent<Skill_TowerScript>( ).Upgrade( 1, delayCount );
		foreach( var a in drons )
			a.GetComponent<Skill_DronMoveScript>( ).Upgrade( 1, delayCount );
		ResetCoolTime( );
	}
	//public IEnumerator CoEnd( float time = 0f )
	//{
	//	stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
	//	yield return 0;
	//}
}