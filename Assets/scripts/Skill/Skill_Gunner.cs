﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//1.가드
//2.난사
//3.프리덤


//1.가드
public class Active_Handgun_Guard : ActiveSkill
{
	public bool GuardSucces;

	public float GuardAttackScale;
	public float GuardAttackTime;
	public float GuardAttackStuck;
	public override bool Able( )
	{
		if( Time.time > coolTime && finish && stat.state == PlanetUtil.CharacterState.Idle )
		{
			finish = false;
			cancel = false;
			return true;
		}
		else
		{
			//Debug.Log( coolTime - Time.time + "초 남음" );
			return false;
		}
	}
	public Active_Handgun_Guard( StatusScript stat, int spriteId, int _idx ) : base( stat, spriteId, PlanetUtil.WeaponType.Handgun, _idx )
	{
		skillName = "가드";
		skillExplain = "적의 공격으로부터 유니티쨩을 보호합니다. 가드에 성공했을 경우 적에게 데미지를 돌려줍니다";

		ActiveSkillHandgun( );
		coolTime = 0f;

		//스킬
		GuardSucces = false;
		GuardAttackTime = 0.5f;
		GuardAttackStuck = 1f;

		SkilPathSetting( GetFloat( 7f, 4 ), GetFloat( 10f, 4 ), GetFloat( 0.4f, 4 ) );
		curCoolTime[2] += -0.5f;
		curCoolTime[3] += -1f;
		damage[2] += +2f;
		damage[3] += +5f;
		playTime[2] += +0.05f;
		playTime[3] += +0.1f;
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
		stat.StartCoroutine( GuardAttack( stat, obj ) );
	}

	public override IEnumerator CoSkill( float time = 0.0f )
	{
		//stat.SetStatable( PlanetUtil.StatableSet.DelayMode ); //아무런 움직임을 할 수 없다.
		//while( time > 0f )
		//{
		//	if( stat.bHit )
		//	{
		//		cancel = true;
		//		break;
		//	}
		//	time -= Time.deltaTime;
		//	yield return 0;
		//}
		//if( !cancel )
		Debug.Log( "실행" );
		stat.StartCoroutine( Guard( playTime[level - 1] ) );
		yield return 0;
	}

	public IEnumerator Guard( float time )
	{
		GuardSucces = false;
		stat.SetStatable( PlanetUtil.StatableSet.GuardMode );
		GameMgr.Instance.ado.PlayOneShot( DataBaseManager.Instance.audios[19] );
		stat.am.speed = 3.0f;
		stat.am.Play( "GuardAttack" );
		stat.am.SetBool( "GuardAttack", true );  //가드애니
		while( time > 0f && !GuardSucces )
		{
			if( stat.bHit ) //논가드 공격 받았을 때 true
			{
				cancel = true;
				break;
			}
			time -= Time.deltaTime;
			yield return 0;
		}
		stat.am.speed = 1f;
		stat.am.SetBool( "GuardAttack", false );  //가드애니 취소
		if( !GuardSucces )
		{
			stat.StartCoroutine( GuardCancel( ) );
			ResetCoolTime( );
		}
		//cancel 이면 히트에 위임
	}

	public IEnumerator GuardAttack( StatusScript _stat, object _dmg ) //가드공격을 시작할 때
	{
		stat.am.SetBool( "GuardAttack", false );
		stat.StartCoroutine( Camera.main.GetComponent<SimpleFollowCam>( ).CameraScale( true, 0.45f, 0.2f ) );
		stat.SetStatable( PlanetUtil.StatableSet.SuperMode );
		GuardSucces = true;
		stat.am.speed = 2f;
		Time.timeScale = 0.7f;
		stat.am.SetBool( "Attack", true ); //반격애니
		_stat.cs.Demage( stat, 0f, 0f, PlanetUtil.AttackType.NonGuard, true, GuardAttackTime * 2f );
		stat.am.speed = 1f;
		GameMgr.Instance.ado.PlayOneShot( DataBaseManager.Instance.audios[20] );
		yield return stat.StartCoroutine( ShootMove( GuardAttackTime ) );
		float dmg = (float)_dmg;
		Time.timeScale = 1f;
		_stat.cs.Demage( stat, dmg, damage[level - 1], PlanetUtil.AttackType.NonGuard, true, 4f );
		GameMgr.Instance.ado.PlayOneShot( DataBaseManager.Instance.audios[21] );
		stat.am.SetBool( "Attack", false ); //반격애니 스탑
		stat.am.speed = 1f;
		stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
		stat.StartCoroutine( Camera.main.GetComponent<SimpleFollowCam>( ).CameraScale( false, 0.6f, 0.7f ) );
		ResetCoolTime( );
	}
	public IEnumerator ShootMove( float time = 1f, float speed = 1f )
	{
		while( time > 0f && !cancel )
		{
			time -= Time.deltaTime;
			stat.transform.position -= ( stat.lookLeft ? Vector3.left : Vector3.right ) * Time.deltaTime * speed;
			yield return 0;
		}
	}
	public IEnumerator GuardCancel( float time = 0.2f ) //가드공격을 성공하지 못하고 끝났을 때
	{
		stat.SetStatable( PlanetUtil.StatableSet.DelayMode );
		while( time > 0f )
		{
			if( stat.bHit )
			{
				cancel = true;
				break;
			}
			time -= Time.deltaTime;
			yield return 0;
		}
		if( !cancel )
			stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
	}
}


//2.연속 타격
public class Active_Handgun_Nansa : ActiveSkill
{
	public Active_Handgun_Nansa( StatusScript stat, int spriteId, int _idx ) : base( stat, spriteId, PlanetUtil.WeaponType.Handgun, _idx )
	{
		skillName = "연속 타격";
		skillExplain = "빠른 속도로 전방에 연속적인 공격을 가합니다. 빠바방!";

		ActiveSkillHandgun( );
		coolTime = 0f;
		SkilPathSetting( GetFloat( 6f, 4 ), GetFloat( 0f, 4 ), GetFloat( 0f, 4 ) );
		curCoolTime[2] += -0.5f;
		curCoolTime[3] += -1f;
		damage[2] += +0f;
		damage[3] += +0f;
		playTime[2] += +0f;
		playTime[3] += +0f;
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
	}

	public override IEnumerator CoSkill( float time = 0.2f )
	{
		//stat.State |= PlanetUtil.CharacterState.Skill;
		Debug.Log( stat.state );
		stat.SetStatable( PlanetUtil.StatableSet.SkillMoveAble );
		Debug.Log( stat.state );
		yield return 0;
		while( stat.nowBullet[stat.nowWeapon] != 0f )
		{
			Debug.Log( stat.nowBullet[stat.nowWeapon] );
			if( stat.bHit ) //논가드 공격 받았을 때 true
			{
				cancel = true;
				break;
			}
			else if( stat.coAttack == null )
				stat.coAttack = stat.coAttack = stat.StartCoroutine( ( (Weapon)stat.GetItem( stat.nowWeapon ) ).CoAttack( stat, 0.3f, false ) );
			yield return 0;
		}
		//if( !cancel )
		//	stat.StartCoroutine( NansaEnd( ) );
		ResetCoolTime( );
		Debug.Log( stat.state );
		stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
		Debug.Log( stat.state );
	}

	public IEnumerator NansaEnd( float time = 0f )
	{
		yield return 0;
		Debug.Log( "★난사 종료★" );
	}
}


//3.풀 버스트
public class Active_Handgun_Freedom : ActiveSkill
{
	private Skill_Gunner_FreedomTargetScript scr;
	public override bool Able( )
	{
		if( Time.time > coolTime && finish && stat.state != PlanetUtil.CharacterState.Attack
			&& stat.state != PlanetUtil.CharacterState.Guard && stat.state != PlanetUtil.CharacterState.Skill )
		{
			finish = false;
			return true;
		}
		else if( Time.time > coolTime && !finish && !immediatlyCancel )
		{
			immediatlyCancel = true;
			return false;
		}
		else
		{
			Debug.Log( coolTime - Time.time + "초 남음" );
			return false;
		}
	}
	public Active_Handgun_Freedom( StatusScript stat, int spriteId, int _idx ) : base( stat, spriteId, PlanetUtil.WeaponType.Handgun, _idx )
	{
		skillName = "풀 버스트";
		skillExplain = "스킬 시전 시 고오오급 멀티 레이더 추적 시스템을 이용해 몬스터를 포착합니다. 재시전시 포착된 적을 향해 공격합니다. 시전 시간동안 피격 시 스킬이 취소됩니다. 유니티쨩~ 이키마쓰!";

		ActiveSkillHandgun( );

		coolTime = 0f;
		if( stat != null )
			scr = stat.Hg_Target.GetComponent<Skill_Gunner_FreedomTargetScript>( );
		SkilPathSetting( GetFloat( 15f, 4 ), GetFloat( 333f, 4 ), GetFloat( 0f, 4 ) );
		curCoolTime[2] += -2.5f;
		curCoolTime[3] += -5f;
		damage[2] += +111f;
		damage[3] += +222f;
		playTime[2] += +0f;
		playTime[3] += +0f;
	}

	public override void SetStat( StatusScript _stat )
	{
		base.SetStat( _stat );
		scr = _stat.Hg_Target.GetComponent<Skill_Gunner_FreedomTargetScript>( );
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
	}

	public override IEnumerator CoSkill( float time = 10.0f )
	{
		time = 10f;
		stat.SetStatable( PlanetUtil.StatableSet.SkillDontCtrl );
		Time.timeScale = 0.02f;
		//n초 동안 몬스터 위치에 띡 띡 띡 하고 타겟 생성 -> 완료되면 타겟에 들어간다.
		stat.Hg_Target.SetActive( true );
		while( time > 0f && !immediatlyCancel && !cancel )
		{
			if( !GameMgr.Instance.gamePause )
			{
				time -= Time.unscaledDeltaTime;
				if( stat.bHit )
				{
					cancel = true;
					break;
				}
			}
			yield return 0;
		}
		if( cancel )
		{
			Debug.Log( "공격받아 캔슬" );
			stat.StartCoroutine( CoCancel( ) );
		}
		else if( immediatlyCancel )
		{
			Debug.Log( "직접 캔슬" );
			//stat.StartCoroutine( CoCancel( ) );
			stat.StartCoroutine( CoFreedom( ) );
		}
		else if( time <= 0f )
		{
			Debug.Log( "시간 종료 캔슬" );
			//Time.timeScale = 1f;
			stat.StartCoroutine( CoFreedom( ) );
		}
		//캔슬이면 딜레이에 위임
	}

	public IEnumerator CoCancel( float time = 0.2f )
	{
		scr.finish = true;
		yield return stat.StartCoroutine( scr.Cancel( ) );
		Time.timeScale = 1f;
		if( stat.state == PlanetUtil.CharacterState.Skill )
			stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
		ResetCoolTime( );
	}

	public IEnumerator CoFreedom( float time = 0.2f )
	{
		stat.SetStatable( PlanetUtil.StatableSet.SuperMode );
		Time.timeScale = 0.02f;
		stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_Gun_R_L] );
		//1초 먼저 캔슬 시키기
		scr.finish = true;
		yield return new WaitForSeconds( 0.5f * 0.02f );
		Debug.Log( "프리덤 공격" );
		yield return stat.StartCoroutine( scr.Shoot( damage[level - 1] ) );
		Time.timeScale = 0.04f;
		while( time > 0f )
		{
			if( !GameMgr.Instance.gamePause )
			{
				Debug.Log( time );
				time -= Time.unscaledDeltaTime;
			}
			yield return 0;
		}
		Debug.Log( "프리덤 공격 끝" );
		Time.timeScale = 1f;
		stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
		ResetCoolTime( );
	}
}