﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PassiveSkill : MonoBehaviour
{
	public Buff buff;

	public void SkillCheck( )
	{
		//버프 적용 시킨다
	}
}

public abstract class ActiveSkill
{
	public StatusScript stat;
	public PlanetUtil.WeaponType weaponType;

	public int level = 0;
	public int maxLevel = 3;
	public float coolTime;

	public string skillName;
	public string skillExplain;

	public float[] curCoolTime;
	public float[] damage;
	public float[] playTime;

	public bool cancel; //피해등을 받아 스킬이 도중에 캔슬되었을때 true
	public bool finish; //스킬이 시작할 때 false 끝나면 true
	public bool immediatlyCancel; //스킬을 캔슬할 때 true

	public string name;
	public string disc;
	public Sprite icon;
	public int spriteID;
	public PlanetUtil.WeaponType type;
	public int idx;

	public ActiveSkill( StatusScript stat, int _spriteID, PlanetUtil.WeaponType _type, int _idx, string _name = "", string _disc = "aa" )
	{
		this.stat = stat;
		finish = true;
		cancel = false;
		immediatlyCancel = false;

		disc = _disc;
		if( _name == "" )
			name = this.ToString( );
		else
			name = _name;


		spriteID = _spriteID;
		icon = GameMgr.Instance.dbMgr.GetSprite( spriteID );
		type = _type;
		idx = _idx;
	}

	public virtual void SetStat( StatusScript _stat )
	{
		stat = _stat;
	}

	public virtual void SkilPathSetting( float[] curCoolTime, float[] damage, float[] playTime = null )
	{
		this.curCoolTime = curCoolTime;
		this.damage = damage;
		this.playTime = playTime;
	}

	public void ResetCoolTime( )
	{
		finish = true;
		cancel = false;
		immediatlyCancel = false;
		coolTime = curCoolTime[level] + Time.time;
		Debug.Log( "▶" + ToString( ) + " end" );
	}

	public void ActiveSkillHandgun( )
	{
		weaponType = PlanetUtil.WeaponType.Handgun;
	}
	public void ActiveSkillRiple( )
	{
		weaponType = PlanetUtil.WeaponType.Riple;
	}
	public void ActiveSkillShotgun( )
	{
		weaponType = PlanetUtil.WeaponType.Shotgun;
	}
	public void ActiveSkillTower( )
	{
		weaponType = PlanetUtil.WeaponType.Scv;
	}
	public float[] GetFloat( float value, int count )
	{
		float[] f = new float[count];
		for( int i = 0 ; i < f.Length ; ++i )
			f[i] = value;
		return f;
	}
	public virtual bool Able( )
	{
		if( Time.time > coolTime && finish && stat.state != PlanetUtil.CharacterState.Skill )
		{
			finish = false;
			immediatlyCancel = false;
			cancel = false;
			return true;
		}
		else
		{
			//Debug.Log( coolTime - Time.time + "초 남음" );
			return false;
		}
	}

	public abstract IEnumerator CoSkill( float time = 0.0f );

	public abstract void SkillCallback( StatusScript stat, object obj );
}

public abstract class PassiveConditionSkill : MonoBehaviour//조건형 패시브
{
	public Buff buff;
	public float maxCount; //스킬 발동 조건

	public void SkillCheck( ref float count )
	{
		if( count >= maxCount )
		{
			//버프 적용 시킨다
			count = 0f; //초기화 시킨다.
		}
		else
		{
			//버프 해제 시킨다
		}
	}
}

//버프형
public class ActiveBuffSkill : ActiveSkill
{
	public int buffType;

	public ActiveBuffSkill( StatusScript stat, int _spriteId, PlanetUtil.WeaponType _type, int _idx )
		: base( stat, _spriteId, _type, _idx )
	{
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
	}

	public override IEnumerator CoSkill( float time = 0.3f )
	{
		Debug.Log( "▶" + ToString( ) + " start" );
		stat.StartCoroutine( isSwap( ) );
		ResetCoolTime( );
		yield return 0;
	}

	//public virtual Buff GetBuff( )
	//{
	//	return null;
	//}

	//public virtual Performence GetPerfromence( PlanetUtil.CharacterStat type, float add, float scale )
	//{
	//	Performence p = new Performence();
	//	p.type = type;
	//	p.add = add;
	//	p.scale = scale;
	//	return p;
	//}
	public float[] GetPlayTime( int count = 1 )
	{
		float[] f = new float[count];
		for( int i = 0 ; i < f.Length ; ++i )
			f[i] = playTime[level];
		return f;
	}
	public virtual IEnumerator isSwap( )
	{
		float pTime = Time.time +  playTime[level];
		while( weaponType == GameMgr.Instance.Player.nowWeaponType && pTime > Time.time )
		{
			yield return null;
		}
		GameMgr.Instance.Player.RemoveBuff( buffType );
	}
}