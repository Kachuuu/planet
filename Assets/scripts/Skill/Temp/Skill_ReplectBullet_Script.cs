﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Skill_ReplectBullet_Script : MonoBehaviour
{
	public SpriteRenderer spren;
	public StatusScript stat;
	public ControlScript.DefCallback callback;
	public int Penetrate = 0;
	public float speed = 15f;
	public float angle;

	private float colorCount;
	private float rotSpeed = 680f;
	Vector3 direction;
	bool go;
	bool enter;
	public float damage;
	public float maxDamage;

	public void Go( )
	{
		colorCount = 0f;
		spren.color = Color.white;
		transform.localScale = Vector3.one;
		go = true;
		angle = Mathf.Deg2Rad * angle;
		direction = new Vector3( Mathf.Cos( angle ), Mathf.Sin( angle ), 0 );
		enter = false;
	}

	private void Update( )
	{
		if( go )
		{
			transform.position += direction * speed * Time.deltaTime;
			transform.Rotate( Vector3.forward * rotSpeed * Time.deltaTime );
			if( Penetrate <= 0 )
			{
				go = false;
				GetComponent<GOScript>( ).DoDistroy( );
			}
		}
	}

	private void OnCollisionEnter2D( Collision2D collision )
	{
		if( enter )
			return;
		if( collision.gameObject.tag == "Ground" )
		{
			Penetrate -= 1;

			damage = Mathf.Min( damage * 2f, maxDamage );
			colorCount += 0.025f;
			spren.color = new Vector4( 1f, 1f - colorCount, 1f - colorCount, 1f );

			Vector3 incommingVector = direction;
			incommingVector = incommingVector.normalized;
			Vector3 myNormal = Vector3.Cross(collision.transform.forward,collision.transform.right);
			Vector3 rVector = incommingVector + 2 * myNormal * (Vector3.Dot(-incommingVector,myNormal));
			direction = rVector.normalized;
			enter = true;
		}
	}

	private void OnCollisionExit2D( Collision2D collision )
	{
		//localSize += 0.08f;
		//transform.localScale = Vector3.one * Mathf.Min( 1f + localSize, 3f );
		enter = false;
	}

	private void OnTriggerEnter2D( Collider2D collision )
	{
		if( collision.tag == "Enemy" )
		{
			collision.GetComponent<StatusScript>( ).cs.Demage( GameMgr.Instance.Player, damage );
			go = false;
			GetComponent<GOScript>( ).DoDistroy( );
		}
	}
}