﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dronbltScript : MonoBehaviour
{
	public float speed; //발사 속도
	public float damage; //공격력

	public GameObject effectPrefab; //폭발할 때 이펙트
	public AudioClip audioClip; //폭발소리
	public SpriteRenderer dronSprite;

	private AudioSource audioSource;
	private Collider2D monsterCol;
	private StatusScript monsterStat;

	private void Awake( )
	{
		audioSource = GetComponent<AudioSource>( );
	}

	public void Shoot( Vector2 target, float damage, float speed )
	{
		gameObject.SetActive( true );
		this.damage = damage;
		this.speed = speed;
		//		Debug.Log( transform.rotation );
		//transform.
		//Debug.Log( target );
		transform.LookAt( target );
		transform.right = transform.forward;
		//transform.right = new Vector2( target.x, target.y );
		//Debug.Log( transform.rotation );
		monsterCol = null;
		effectPrefab.SetActive( false );
		dronSprite.enabled = true;
		GetComponent<Collider2D>( ).enabled = true;
		GetComponent<GOScript>( ).DoDistroy( 10f );
	}

	private void Update( )
	{
		if( dronSprite.enabled )
			transform.position += transform.right * speed * Time.deltaTime;
	}

	private void OnTriggerEnter2D( Collider2D other )
	{
		if( other.tag == "Enemy" )
		{
			monsterCol = other;
			monsterStat = other.gameObject.GetComponent<StatusScript>( );
			StartCoroutine( EffectEnd( ) );
		}
	}

	private IEnumerator EffectEnd( )
	{
		//Debug.Log( "dronBlt bomb" );
		dronSprite.enabled = false;
		GetComponent<Collider2D>( ).enabled = false;
		monsterStat.cs.Demage( GameMgr.Instance.Player, damage );
		audioSource.PlayOneShot( audioClip );
		effectPrefab.SetActive( true );
		CancelInvoke( );
		while( audioSource.isPlaying )
		{
			yield return 0;
		}
	}
}

