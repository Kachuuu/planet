﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_DronMoveScript : MonoBehaviour
{
	bool ready;
	bool movePattern;
	float speedY = 0.7f;
	float limitY = 0.15f;

	float damage;
	float speed;
	float delay;
	float time;

	float currTime;
	float currDelay;
	public Transform shootPos;

	public GameObject breakEff;
	public SpriteRenderer spRen;
	public AudioClip shootSound,breakSound;
	public Sprite[] dronSprites;
	private AudioSource audioSource;

	public IEnumerator DronStart( float damage, float speed, float delay, float time )
	{
		audioSource = GetComponent<AudioSource>( );
		Upgrade( 0 );
		spRen.enabled = true;
		this.damage = damage;
		this.speed = speed;
		this.delay = delay;
		this.time = time;
		ready = false;
		currDelay = delay;
		currTime = time + Time.time;
		yield return 0;
	}
	private void Shoot( Transform target )
	{
		audioSource.PlayOneShot( shootSound );
		//Debug.Log( "dron shoot" );
		var pos = shootPos.position;
		var rot = shootPos.rotation;
		var cmd = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.DronBlt, pos, rot );
		var bs = cmd.GetComponent<dronbltScript>( );
		bs.Shoot( target.transform.position, damage, speed );
	}

	public void Upgrade( int index = 1, float delayCount = 1f )
	{
		spRen.sprite = dronSprites[index];
		if( index == 1 )
		{
			currTime = time + Time.time;
			delay = delay * delayCount;
		}
	}

	public IEnumerator DronBreak( float time = 1f )
	{
		audioSource.PlayOneShot( breakSound );
		ready = true;
		spRen.enabled = false;
		breakEff.SetActive( true );
		yield return new WaitForSeconds( time / 2f );
		yield return new WaitForSeconds( time / 2f );
		breakEff.SetActive( false );
		gameObject.SetActive( false );
	}
	void Update( )
	{
		if( ready )
			return;
		transform.position += Vector3.up * ( movePattern ? 1 : -1 ) * speedY * Time.deltaTime;
		if( movePattern )
		{
			if( transform.localPosition.y >= limitY )
				movePattern = false;
		}
		else
		{
			if( transform.localPosition.y <= -limitY )
				movePattern = true;
		}
		if( monsterColls != null && currDelay <= Time.time )
		{
			if( currTime <= Time.time && !ready )
			{
				StartCoroutine( DronBreak( ) );
			}
			else
			{
				Shoot( monsterStat.centerPos );
				currDelay = Time.time + delay;
			}
		}
	}

	private Collider2D monsterColls;
	private StatusScript monsterStat;
	private void OnTriggerStay2D( Collider2D collision )
	{
		if( monsterColls == null )
		{
			if( collision.tag == "Enemy" )
			{
				monsterColls = collision;
				monsterStat = monsterColls.gameObject.GetComponent<StatusScript>( );
			}
		}
	}
	private void OnTriggerExit2D( Collider2D collision )
	{
		if( monsterColls == collision )
		{
			monsterColls = null;
			monsterStat = null;
		}
	}
}
