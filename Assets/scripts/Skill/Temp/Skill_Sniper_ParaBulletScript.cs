﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Skill_Sniper_ParaBulletScript : MonoBehaviour
{
	public float speed = 500f;
	public StatusScript stat;
	public ControlScript.DefCallback callback;

	public float damage;
	private float liveTime = 1f;
	private float acc = 0f;
	public int Penetrate = 0;

	RaycastHit2D[] hits = new RaycastHit2D[10];
	ContactFilter2D filter = new ContactFilter2D();

	private void Awake( )
	{
		filter.layerMask = LayerMask.GetMask( "Ground" ) | LayerMask.GetMask( "Enemy" );
		filter.useLayerMask = true;
	}

	private void OnEnable( )
	{
		//rightOverBullet = gameObject.transform.localPosition.x + totalDistance;
		//leftOverBullet = gameObject.transform.localPosition.x - totalDistance;
		acc = Time.time + liveTime;
	}

	private void Update( )
	{
		if( acc < Time.time )
		{
			GetComponent<GOScript>( ).DoDistroy( );
			return;
		}

		FireBullet( );
	}

	private void FireBullet( )
	{
		var dir = transform.right * speed * Time.deltaTime;

		int cnt = Physics2D.Raycast(transform.position, transform.right, filter, hits, dir.magnitude );

		if( cnt == 0 )
		{
			gameObject.transform.Translate( transform.right * speed * Time.deltaTime, Space.World );
		}
		else
		{
			List<RaycastHit2D> hitList = new List<RaycastHit2D>();
			for( int i = 0 ; i < cnt ; ++i )
				hitList.Add( hits[i] );
			hitList.OrderBy( h => h.distance ).ToArray( );

			float listDist = dir.magnitude;
			for( int i = 0 ; i < cnt ; ++i )
			{
				if( hitList[i].collider.tag == "Ground" )
				{
					listDist = hitList[i].distance;
					GetComponent<GOScript>( ).DoDistroy( );
					break;
				}

				hitList[i].collider.GetComponent<ControlScript>( ).Demage( stat, damage, 1f, PlanetUtil.AttackType.Range, true, 2f );
				if( null != callback )
					callback( hitList[i].collider.GetComponent<StatusScript>( ) );

				--Penetrate;
				if( Penetrate <= 0 )
				{
					GetComponent<GOScript>( ).DoDistroy( );
					break;
				}
			}
			gameObject.transform.position += dir * ( listDist / dir.magnitude );
		}
	}

	//private void OnTriggerEnter2D( Collider2D collision )
	//{
	//	//CancelInvoke( );
	//	//GetComponent<GOScript>( ).DoDistroy( );

	//	if( collision.tag == "Enemy")
	//	{
	//		if( Penetrate <= 0 )
	//			return;

	//		if (collision.GetComponent<ControlScript>().Demage( stat ))
	//		{
	//			if( null != callback )
	//				callback( collision.GetComponent<StatusScript>( ) );

	//			--Penetrate;
	//			if( Penetrate <= 0 )
	//				GetComponent<GOScript>( ).DoDistroy( );
	//		}
	//	}

	//	if( collision.tag == "Ground" )
	//	{
	//           GetComponent<GOScript>().DoDistroy();
	//       }
	//}


}