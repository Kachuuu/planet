﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Skill_FlashBomb : MonoBehaviour
{
	public Rigidbody2D rigid;
	public SpriteRenderer spren;
	public Collider2D hitcol;
	public GameObject bomb;
	public Collider2D bombcol;

	public StatusScript stat;
	public ControlScript.DefCallback callback;
	public int Penetrate = 0;
	public float speed = 10f;
	public float angle;
	Vector3 direction;
	public float damage = 155f;
	bool bombReady;

	private void OnEnable( )
	{
		rigid.isKinematic = false;
		hitcol.enabled = true;
		spren.color = Color.white;
		spren.enabled = true;
		bombReady = false;
		bombcol.enabled = false;
		rigid.AddForce( Vector3.up * speed / 2.4f );
		rigid.AddForce( transform.right * speed * Time.deltaTime * 15f );
	}

	private void Update( )
	{
		if( !bombReady )
		{
			rigid.AddForce( transform.right * speed * 0.5f * Time.deltaTime );
		}
	}

	private IEnumerator Explosion( float time = 0.8f )
	{
		bombReady = true;
		spren.color = Color.red;
		yield return new WaitForSeconds( time );
		spren.enabled = false;
		Debug.Log( "bomb!!" );
		bomb.SetActive( true );
		rigid.velocity = Vector2.zero;
		rigid.isKinematic = true;
		hitcol.enabled = false;
		bombcol.enabled = true;
		yield return new WaitForSeconds( time * 2f );
		bomb.SetActive( false );
		StartCoroutine( DoDestroy( ) );
	}

	private IEnumerator DoDestroy( float time = 0f )
	{
		yield return new WaitForSeconds( time );
		GetComponent<GOScript>( ).DoDistroy( );
	}

	private void OnCollisionEnter2D( Collision2D collision )
	{
		if( collision.gameObject.tag == "Ground" && !bombReady )
		{
			StartCoroutine( Explosion( ) );
		}
		if( collision.gameObject.tag == "Enemey" && !bombReady )
		{
			StartCoroutine( Explosion( ) );
		}
	}

	private void OnTriggerEnter2D( Collider2D collision )
	{
		if( collision.tag == "Enemy" )
		{
			//그로기! 지금은 데미지
			collision.GetComponent<StatusScript>( ).cs.Demage( GameMgr.Instance.Player, damage, 1f, PlanetUtil.AttackType.Range, true, 4f );
		}
	}
}