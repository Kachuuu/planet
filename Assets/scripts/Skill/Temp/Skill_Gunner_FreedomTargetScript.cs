﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_Gunner_FreedomTargetScript : MonoBehaviour
{
	public Collider2D coll;
	public float scaleMin = 0.5f;
	public float scaleMax = 20f;
	//	public float scaleSpeed = 5f;
	public bool finish;

	private AudioSource audioSource;

	public List<Collider2D> colList = new List<Collider2D>();
	public Queue<StatusScript> statQueue = new Queue<StatusScript>();
	private List<Skill_Gunner_FreedomScript> statList = new List<Skill_Gunner_FreedomScript>();

	private SimpleFollowCam cam;

	private IEnumerator AudioEnd( float time = 0.5f )
	{
		while( audioSource.volume > 0f )
		{
			if( !GameMgr.Instance.gamePause )
			{
				audioSource.volume -= Time.unscaledDeltaTime / time;
			}
			yield return new WaitForSecondsRealtime( 0.02f );
		}
		audioSource.Stop( );
		audioSource.volume = 1f;
	}

	private void OnEnable( )
	{
		audioSource = GetComponent<AudioSource>( );
		audioSource.Play( );
		colList.Clear( );
		statQueue.Clear( );
		statList.Clear( );
		StartCoroutine( Search( ) );
		StartCoroutine( CameraSize( true ) );
	}

	public IEnumerator CameraSize( bool change = true )
	{
		cam = Camera.main.GetComponent<SimpleFollowCam>( );
		yield return StartCoroutine( cam.CameraScale( change ) );
	}

	public IEnumerator Search( bool start = true, float time = 1.0f )
	{
		if( start )
		{
			transform.localScale = Vector3.one * scaleMin;
			finish = false;
			coll.enabled = true;
			StartCoroutine( SearchLockOn( ) );
			float ctime = 0f;
			while( ctime < time )
			{
				if( !GameMgr.Instance.gamePause )
				{
					transform.localScale = Vector3.Lerp( Vector3.one * scaleMin, Vector3.one * scaleMax, ctime / time );
					ctime += Time.unscaledDeltaTime;
				}
				yield return new WaitForSecondsRealtime( 0.02f );
			}
		}
		else
		{
			time /= 1.5f;
			transform.localScale = Vector3.one * scaleMin;
			finish = true;
			coll.enabled = false;
			StartCoroutine( SearchLockOn( ) );
			float ctime = 0f;
			while( ctime < time )
			{
				if( !GameMgr.Instance.gamePause )
				{
					transform.localScale = Vector3.Lerp( Vector3.one * scaleMax, Vector3.one * scaleMin, ctime / time );
					ctime += Time.unscaledDeltaTime;
				}
				yield return new WaitForSecondsRealtime( 0.02f );
			}
		}
	}

	float nextDelay = 0.25f;
	public IEnumerator SearchLockOn( )
	{
		float delay = 0f;

		float currTime = 0f;
		while( currTime < nextDelay )
		{
			if( !GameMgr.Instance.gamePause )
			{
				currTime += Time.unscaledDeltaTime;
			}
			yield return 0;
		}
		while( !finish )
		{
			if( !GameMgr.Instance.gamePause )
			{
				if( delay > nextDelay && statQueue.Count > 0 )
				{
					var a = statQueue.Dequeue();
					Debug.Log( "enemy 타겟생성 : " + a.name );
					var pos = a.gameObject.transform.position + Vector3.up * 1f;
					var rot = Quaternion.identity;
					var cmd = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.FreedomBullet, pos, rot );
					var bs = cmd.GetComponent<Skill_Gunner_FreedomScript>( );
					statList.Add( bs );
					bs.TargetLockOn( a, 100f );
					delay = 0f;
				}
				delay += Time.unscaledDeltaTime;
			}
			yield return new WaitForSecondsRealtime( 0.02f );
		}
	}

	public IEnumerator Shoot( float damage )
	{
		finish = true;
		for( int i = 0 ; i < statList.Count ; i++ )
		{
			if( !GameMgr.Instance.gamePause )
			{
				statList[i].damage = damage;
				StartCoroutine( statList[i].CoTargetShoot( ) );
				yield return StartCoroutine( cam.CameraAction( statList[i].transform ) );
				yield return new WaitForSecondsRealtime( nextDelay );
			}
			else
			{
				i -= 1;
			}
			yield return null;
		}
		//yield return StartCoroutine( cam.CameraAction( cam.tg ) );
		//yield return new WaitForSecondsRealtime( nextDelay );
		StartCoroutine( AudioEnd( ) );
		//yield return StartCoroutine( cam.CameraAction( cam.tg ) );
		//		yield return new WaitForSecondsRealtime( nextDelay * 1f );
		yield return StartCoroutine( cam.CameraNormal( ) );
		yield return new WaitForSecondsRealtime( nextDelay * 2f );
		yield return StartCoroutine( Search( false ) );
		//		yield return new WaitForSecondsRealtime( nextDelay );
		StartCoroutine( CameraSize( false ) );
		gameObject.SetActive( false );
	}

	public IEnumerator Cancel( )
	{
		foreach( var a in statList )
			StartCoroutine( a.Cancel( ) );
		yield return StartCoroutine( AudioEnd( 0.2f ) );
		StartCoroutine( Search( false ) );
		StartCoroutine( CameraSize( false ) );
		gameObject.SetActive( false );
		yield return null;
	}

	private void OnTriggerEnter2D( Collider2D collision )
	{
		if( collision.tag == "Enemy" && !colList.Contains( collision ) && !finish )
		{
			colList.Add( collision );
			Debug.Log( "enemy 들어옴 : " + collision.name );
			statQueue.Enqueue( collision.gameObject.GetComponent<StatusScript>( ) );
			//Debug.Log( collision.gameObject.transform.position );
			//Debug.Log( collision.gameObject.GetComponent<MonsterStatusScript>( ) );
		}
	}
}