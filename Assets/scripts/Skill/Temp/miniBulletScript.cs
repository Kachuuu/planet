﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class miniBulletScript : MonoBehaviour
{
    public float speed;
    private float count = 5f;
    private Vector3 currLocalPos;

    private void Awake()
    {
        currLocalPos = transform.localPosition;
    }

    void OnEnable()
    {
        count = 5f;
    }

    private void Update()
    {
        transform.Translate( -transform.right * speed * Time.deltaTime, Space.World );
        count -= Time.deltaTime;
        if ( count <= 0f )
        {
            ActiveFalse( );
        }
    }

    private void OnTriggerEnter2D( Collider2D collision )
    {
        if ( collision.tag != "Player" )
        {
            ActiveFalse( );
        }
    }

    private void ActiveFalse()
    {
        gameObject.SetActive( false );
        transform.localPosition = currLocalPos;
    }
}