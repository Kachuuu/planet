﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_DronScript : MonoBehaviour
{
	public Skill_DronMoveScript dronScript;
	private float speed = 5f;

	public void LateUpdate( )
	{
		transform.position = Vector2.Lerp( transform.position,
			new Vector2( GameMgr.Instance.Player.Scv_Target.transform.position.x, GameMgr.Instance.Player.Scv_Target.transform.position.y ),
			Time.deltaTime * speed );
	}

	public void DronStart( float damage, float speed, float delay, float time )
	{
		dronScript.gameObject.SetActive( true );
		StartCoroutine( dronScript.DronStart( damage, speed, delay, time ) );
	}

	public void OnDisable( )
	{
		this.enabled = true;

	}
}