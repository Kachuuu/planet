﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_Sniper_SnipingTargetScript : MonoBehaviour
{
	public Collider2D col;
	private StatusScript stat;
	public GameObject redZone;
	public SpriteRenderer[] bullets;

	public int bltCount;
	public float damage;
	public bool isShoot;
	public bool isEnd;

	public IEnumerator CoStart( StatusScript stat, float time = 0.5f )
	{
		this.stat = stat;
		isEnd = false;
		Debug.Log( transform.rotation );
		gameObject.transform.Rotate( new Vector3( 0, ( stat.lookLeft ? 180 : 0 ), 0 ) );
		Debug.Log( transform.rotation );
		transform.position = stat.transform.position;
		var sp = GetComponent<SpriteRenderer>();
		Color c = sp.color;
		c.a = 0f;
		sp.color = c;
		sp.enabled = true;
		float currTime = time;

		for( int i = 0 ; i < bullets.Length ; ++i )
		{
			bullets[i].color = c;
			bullets[i].enabled = true;
		}
		while( time > 0f )
		{
			time -= Time.deltaTime;
			yield return 0;
			c.a += Time.deltaTime / currTime;
			sp.color = c;
			for( int i = 0 ; i < bullets.Length ; ++i )
				bullets[i].color = c;
		}
	}

	public IEnumerator CoMove( StatusScript stat )
	{
		this.stat = stat;
		var sp = GetComponent<SpriteRenderer>();
		Color c = sp.color;
		c.a = 1f;
		sp.color = c;
		for( int i = 0 ; i < bullets.Length ; ++i )
			bullets[i].color = c;
		float speed = 8f;
		Vector3 currTrans = transform.position;
		while( bltCount > 0 && !isEnd )
		{
			if( !isShoot )
			{
				float v = stat.GetComponent<PlayerInputScript>().axies[(int)PlayerInputScript.Aixes.MoveVertical].GetAxis();
				float h = stat.GetComponent<PlayerInputScript>().axies[(int)PlayerInputScript.Aixes.MoveHorizontal].GetAxis();
				if( transform.position.x < currTrans.x + 10f && transform.position.x > currTrans.x - 10f )
					transform.position += new Vector3( h, 0f, 0f ) * Time.deltaTime * speed;
				else
				{
					transform.position = new Vector3( currTrans.x >= transform.position.x ? currTrans.x - 9.9f : currTrans.x + 9.9f, transform.position.y, transform.position.z );
				}
				if( transform.position.y < currTrans.y + 6f && transform.position.y > currTrans.y - 6f )
					transform.position += new Vector3( 0f, v, 0f ) * Time.deltaTime * speed;
				else
				{
					transform.position = new Vector3( transform.position.x, currTrans.y >= transform.position.y ? currTrans.y - 5.9f : currTrans.y + 5.9f, transform.position.z );
				}

			}
			yield return 0;
		}
	}

	public IEnumerator CoShoot( StatusScript stat )
	{
		redZone.SetActive( true );
		Vector3 currTrans = transform.position;
		this.stat = stat;
		collist.Clear( );
		col.enabled = true;
		isShoot = true;
		yield return new WaitForSeconds( 0.03f );
		transform.position += Vector3.up * 0.25f;
		yield return new WaitForSeconds( 0.03f );
		transform.position += Vector3.left * 0.25f;
		yield return new WaitForSeconds( 0.03f );
		transform.position += Vector3.down * 0.35f;
		yield return new WaitForSeconds( 0.03f );
		transform.position += Vector3.up * 0.35f;
		yield return new WaitForSeconds( 0.03f );
		transform.position = currTrans;
		yield return new WaitForSeconds( 0.05f );
		col.enabled = false;
		redZone.SetActive( false );
		collist.Clear( );
		isShoot = false;
		for( int i = 0 ; i < bullets.Length ; ++i )
			if( i >= bltCount )
				bullets[i].enabled = false;
	}

	public IEnumerator CoShootEnd( float time = 1f )
	{
		Debug.Log( "coshootend" );
		isEnd = true;
		var sp = GetComponent<SpriteRenderer>();
		Debug.Log( sp );
		float currTime = time;
		while( time > 0f )
		{
			time -= Time.deltaTime;
			yield return 0;
			Color c = sp.color;
			c.a -= Time.deltaTime / currTime;
			sp.color = c;
		}
		sp.enabled = false;
		gameObject.SetActive( false );
	}

	public  List<Collider2D> collist = new List<Collider2D>();

	private void OnTriggerEnter2D( Collider2D collision )
	{
		if( collision.tag == "Enemy" && !collist.Contains( collision ) )
		{
			collist.Add( collision );
			collision.gameObject.GetComponent<StatusScript>( ).cs.Demage( stat, damage, 1f, PlanetUtil.AttackType.Range );
			Debug.Log( "★저격 펑펑!★" );
		}
	}
}