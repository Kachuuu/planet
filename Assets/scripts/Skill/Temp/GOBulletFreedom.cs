﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOBulletFreedom : GOScript
{
	public ObjectPoolManager.PoolType _type;
	public override bool Init( )
	{
		return true;
	}

	protected override void Awake( )
	{
		type = _type;
	}
}
