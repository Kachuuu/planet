﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_Gunner_FreedomScript : MonoBehaviour
{
	public GameObject freedomTarget;
	public GameObject shootPrefab;
	public Animator ani;
	public SpriteRenderer sp;

	private AudioSource audioSource;
	public AudioClip lockon,lockonshoot;

	public float damage;

	StatusScript targetStat;

	public void OnEnable( )
	{
		audioSource = GetComponent<AudioSource>( );
		if( GameMgr.Instance != null )
			StartCoroutine( CoTargetSpin( ) );
	}

	public void TargetLockOn( StatusScript _stat, float _dmg = 100f )
	{
		audioSource.PlayOneShot( lockon );
		targetStat = _stat;
		damage = _dmg;
		StartCoroutine( CoTargetSpin( ) );
	}

	public void CoTargetEnd( )
	{
		ani.speed = 1f;
		sp.color = new Vector4( 1f, 1f, 1f, 0.2f );
	}

	public IEnumerator CoTargetSpin( float time = 0.6f, float rot = 70f, float scale = 1.5f )
	{
		ani.speed = 1f / Time.timeScale;

		float currTime = 0f;
		while( currTime < time )
		{
			if( !GameMgr.Instance.gamePause )
			{
				freedomTarget.transform.localScale = Vector3.one * ( scale - ( scale - 1f ) * ( currTime / time ) );
				freedomTarget.transform.rotation = Quaternion.Euler( Vector3.forward * ( rot - ( rot * currTime / time ) ) );
				currTime += Time.unscaledDeltaTime;
				sp.color = new Vector4( 1.7f - ( currTime / time ), 1f - ( currTime / time ), 1f - ( currTime / time ), 0.2f + ( currTime / time ) );
			}
			yield return 0;
		}
		sp.color = new Vector4( 0.7f, 0f, 0f, 1f );
		freedomTarget.transform.localScale = Vector3.one;
		freedomTarget.transform.rotation = Quaternion.Euler( Vector3.zero );
		//yield return new WaitForSecondsRealtime( 1f );
		//StartCoroutine( Destroy( ) );
	}

	public IEnumerator CoTargetShoot( float time = 0.3f )
	{
		audioSource.PlayOneShot( lockonshoot );
		targetStat.cs.Demage( GameMgr.Instance.Player, damage );
		yield return StartCoroutine( Destroy( ) );
	}

	public IEnumerator Cancel( float time = 0.55f, float rot = 70f, float scale = 0.8f )
	{
		float currTime = 0f;
		while( currTime < time )
		{
			if( !GameMgr.Instance.gamePause )
			{
				freedomTarget.transform.localScale = Vector3.one * ( scale - ( scale - 1f ) * ( currTime / time ) );
				freedomTarget.transform.rotation = Quaternion.Euler( Vector3.forward * ( rot - ( rot * currTime / time ) ) );
				currTime += Time.unscaledDeltaTime;
				sp.color = new Vector4( 0.7f, ( currTime / time ), ( currTime / time ), 1f - ( currTime / time ) * 2f );
			}
			yield return 0;
		}
		sp.color = new Vector4( 0.7f, 0f, 0f, 1f );
		ani.speed = 1f;
	}

	public IEnumerator Destroy( float time = 0.55f, float rot = 70f, float scale = 0.8f )
	{
		//파괴연출
		shootPrefab.SetActive( true );
		ani.speed = 0f;
		sp.color = new Vector4( 1f, 1f, 1f, 0f );

		float currTime = 0f;
		while( currTime < time )
		{
			if( !GameMgr.Instance.gamePause )
			{
				currTime += Time.unscaledDeltaTime;
			}
			yield return 0;
		}
		shootPrefab.SetActive( false );
		GetComponent<GOScript>( ).DoDistroy( );
	}
}
