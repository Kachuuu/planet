﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class miniManyBulletScript : MonoBehaviour
{
    public GameObject[] gbList;
    public float delay;
    private float count = 0f;
    private int gbNum = 0;
    private void OnEnable()
    {
        count = 0f;
        gbNum = 0;
    }
    private void Update()
    {
        bool des = false;
        count += Time.deltaTime;
        if ( count >= delay && gbNum < gbList.Length )
        {
            gbList[gbNum].SetActive( true );
            gbNum += 1;
            count = 0f;
        }
        if ( gbNum == gbList.Length )
        {
            foreach ( var a in gbList )
            {
                if ( a.activeSelf )
                {
                    des = true;
                }
            }
            if ( !des )
            {
                GetComponent<GOScript>( ).DoDistroy( );
            }
        }
    }
}
