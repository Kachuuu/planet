﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bigBulletScript : MonoBehaviour
{
    public float damage; //공격력

    public float speed; //발사 속도
    public int penetrateCount; //최대 관통 횟수

    public GameObject effectPrefab; //폭발할 때 이펙트
    public AudioClip[] audioClip; //0: 통과소리 , 1: 폭발소리

    private AudioSource audioSource;
    private List<Collider2D> colList = new List<Collider2D>( ); //관통한 콜라이더 리스트
    private int count; //관통한 콜라이더 수

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>( );
    }

    private void OnEnable()
    {
        count = 0;
        colList.Clear( );
        effectPrefab.SetActive( false );
        GetComponent<SpriteRenderer>( ).enabled = true;
        GetComponent<Collider2D>( ).enabled = true;
        GetComponent<GOScript>( ).DoDistroy( 20f );
    }

    private void Update()
    {
        if ( count < penetrateCount )
            transform.Translate( -transform.right * speed * Time.deltaTime, Space.World );
    }

    private void OnTriggerEnter2D( Collider2D other )
    {
        if ( other.tag != "Player" && !colList.Contains( other ) )
        {
            colList.Add( other );
            count += 1;
            if ( count < penetrateCount )
            {
                audioSource.PlayOneShot( audioClip[0] );
            }
            else
            {
                StartCoroutine( EffectEnd( ) );
            }
        }
    }

    private IEnumerator EffectEnd()
    {
        audioSource.PlayOneShot( audioClip[1] );
        effectPrefab.SetActive( true );
        GetComponent<SpriteRenderer>( ).enabled = false;
        GetComponent<Collider2D>( ).enabled = false;
        CancelInvoke( );
        while ( audioSource.isPlaying )
        {
            yield return 0;
        }
        GetComponent<GOScript>( ).DoDistroy( );
    }
}