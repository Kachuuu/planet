﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillCtrl : MonoBehaviour
{
	public StatusScript stat;
	private bool swapStart;
	private IEnumerator Start( )
	{
		yield return new WaitForSeconds( 1f );
		//
		stat = GameMgr.Instance.Player;
		Debug.Log( stat );
		//		stat = GameObject.FindGameObjectWithTag( "Player" ).GetComponent<StatusScript>( );
		hg_Guard = new Active_Handgun_Guard( stat, 1, 0 );
		hg_nansa = new Active_Handgun_Nansa( stat, 1, 1 );
		hg_freedom = new Active_Handgun_Freedom( stat, 1, 2 );

		rf_paralysis = new Active_Riple_ParalysisShoot( stat, 1, 0 );
		rf_sniping = new Active_Riple_Sniping( stat, 1, 1 );
		rf_upgrade = new Active_Riple_Upgrade( stat, 1, 2 );

		sg_flash = new Active_Shotgun_FlashBomb( stat, 1, 0 );
		sg_fire = new Active_Shotgun_FireBomb( stat, 1, 1 );
		sg_upgrade = new Active_Shotgun_Upgrade( stat, 1, 2 );

		scv_tower = new Active_Scv_Tower( stat, 1, 0 );
		scv_dron = new Active_Scv_Dron( stat, 1, 1 );
		scv_upgrade = new Active_Scv_Upgrade( stat, 1, 2 );

		swapStart = true;
	}

	Active_Handgun_Guard hg_Guard;
	Active_Handgun_Nansa hg_nansa;
	Active_Handgun_Freedom hg_freedom;

	Active_Riple_ParalysisShoot rf_paralysis;
	Active_Riple_Sniping rf_sniping;
	Active_Riple_Upgrade rf_upgrade;

	Active_Shotgun_FlashBomb sg_flash;
	Active_Shotgun_FireBomb sg_fire;
	Active_Shotgun_Upgrade sg_upgrade;

	Active_Scv_Tower scv_tower;
	Active_Scv_Dron scv_dron;
	Active_Scv_Upgrade scv_upgrade;

	void skillable( )
	{

	}
	void Update( )
	{
		if( !swapStart )
			return;

		////권총
		//if( GameMgr.Instance.Player.nowWeaponType == PlanetUtil.WeaponType.Handgun )
		//{
		//	//1.가드
		//	if( Input.GetKeyDown( KeyCode.Alpha1 ) )
		//	{
		//		var a = hg_Guard;
		//		if( a.Able( ) )
		//			StartCoroutine( ( a.CoSkill( ) ) );
		//	}
		//	//2.난사
		//	if( Input.GetKeyDown( KeyCode.Alpha2 ) )
		//	{
		//		var a = hg_nansa;
		//		if( a.Able( ) )
		//			StartCoroutine( ( a.CoSkill( ) ) );
		//	}
		//	//3.프리덤
		//	if( Input.GetKeyDown( KeyCode.Alpha3 ) )
		//	{
		//		var a = hg_freedom;
		//		if( a.Able( ) )
		//			StartCoroutine( ( a.CoSkill( ) ) );
		//	}
		//}

		//소총
		//if( GameMgr.Instance.Player.nowWeaponType == PlanetUtil.WeaponType.Riple )
		{
			//1.마비샷
			if( Input.GetKeyDown( KeyCode.Alpha1 ) )
			{
				var a = rf_paralysis;
				if( a.Able( ) )
					StartCoroutine( ( a.CoSkill( ) ) );
			}
			//2.저격모드
			if( Input.GetKeyDown( KeyCode.Alpha2 ) )
			{
				var a = rf_sniping;
				if( a.Able( ) )
					StartCoroutine( ( a.CoSkill( ) ) );
			}
			//3.강화
			if( Input.GetKeyDown( KeyCode.Alpha3 ) )
			{
				var a = rf_upgrade;
				if( a.Able( ) )
					StartCoroutine( ( a.CoSkill( ) ) );
			}
		}

		//샷건
		if( GameMgr.Instance.Player.nowWeaponType == PlanetUtil.WeaponType.Shotgun )
		{
			//1.섬광탄
			if( Input.GetKeyDown( KeyCode.Alpha1 ) )
			{
				var a = sg_flash;
				if( a.Able( ) )
					StartCoroutine( a.CoSkill( ) );
			}
			//2.화염탄
			if( Input.GetKeyDown( KeyCode.Alpha2 ) )
			{
				var a = sg_fire;
				if( a.Able( ) )
					StartCoroutine( a.CoSkill( ) );
			}
			//3.강화
			if( Input.GetKeyDown( KeyCode.Alpha3 ) )
			{
				var a = sg_upgrade;
				if( a.Able( ) )
					StartCoroutine( a.CoSkill( ) );
			}
		}

		//타워
		if( GameMgr.Instance.Player.nowWeaponType == PlanetUtil.WeaponType.Scv )
		{
			//1.타워설치
			if( Input.GetKeyDown( KeyCode.Alpha4 ) )
			{
				var a = scv_tower;
				if( a.Able( ) )
					StartCoroutine( a.CoSkill( ) );
			}
			//2.드론설치
			if( Input.GetKeyDown( KeyCode.Alpha5 ) )
			{
				var a = scv_dron;
				if( a.Able( ) )
					StartCoroutine( a.CoSkill( ) );
			}
			//3.강화
			if( Input.GetKeyDown( KeyCode.Alpha6 ) )
			{
				var a = scv_upgrade;
				if( a.Able( ) )
					StartCoroutine( a.CoSkill( ) );
			}
		}
	}
}