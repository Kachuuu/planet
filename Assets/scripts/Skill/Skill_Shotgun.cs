﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//1.섬광탄
//2.화염탄
//3.강화

//?.반사슛


//1.섬광탄
public class Active_Shotgun_FlashBomb : ActiveSkill
{
	public Active_Shotgun_FlashBomb( StatusScript stat, int spriteId, int _idx ) : base( stat, spriteId, PlanetUtil.WeaponType.Shotgun, _idx )
	{
		skillName = "Flash Bang!";
		skillExplain = "적을 향해 섬광탄을 던집니다. 피격된 적들은 데미지를 입고 일정시간 동안 스턴 상태에 빠집니다.";

		ActiveSkillShotgun( );

		coolTime = 0f;
		SkilPathSetting( GetFloat( 6f, 4 ), GetFloat( 88f, 4 ), GetFloat( 0f, 4 ) );
		curCoolTime[2] += -0.5f;
		curCoolTime[3] += -1f;
		damage[2] += +22f;
		damage[3] += +44f;
		playTime[2] += +0f;
		playTime[3] += +0f;
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
		//맞은 적은 공격력 피해 입고 40% 이동속도 감소
	}

	public override IEnumerator CoSkill( float time = 0.2f )
	{
		stat.State |= PlanetUtil.CharacterState.Skill; //공격과 같은 취급
		stat.am.Play( "ContinuosAttack_start" );
		stat.am.SetBool( "Attack", true );
		stat.StartCoroutine( ShootMove( ) );
		while( time > 0f && !cancel )
		{
			time -= Time.deltaTime;
			yield return 0;
			if( stat.bHit )
				cancel = true;
		}
		if( !cancel )
		{
			stat.bAttack = true;
			var pos = stat.shotPos.position;
			var rot = stat.shotPos.rotation;

			var cmd = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.FlashBomb, pos, rot );
			var bs = cmd.GetComponent<Skill_FlashBomb>( );
			bs.stat = stat;
			bs.Penetrate = (int)stat[PlanetUtil.CharacterStat.Penetrate];
			bs.damage = damage[level];

			yield return 0;
			stat.StartCoroutine( ShootEnd( ) );
		}
		else
		{
			stat.am.SetBool( "Attack", false );
			//stat.State &= ~PlanetUtil.CharacterState.Skill;
			//stat.SetStatable( (PlanetUtil.StatableSet)int.MaxValue, true );
		}
		ResetCoolTime( );
	}

	public IEnumerator ShootMove( float time = 0.5f, float speed = 0.1f )
	{
		while( time > 0f && !cancel )
		{
			time -= Time.deltaTime;
			stat.transform.position += ( stat.lookLeft ? Vector3.left : Vector3.right ) * Time.deltaTime * speed;
			yield return 0;
		}
	}

	public IEnumerator ShootEnd( float time = 0f )
	{
		stat.am.SetBool( "Attack", false );
		stat.state = PlanetUtil.CharacterState.Idle;
		yield return 0;
	}
}

//2.화염탄
public class Active_Shotgun_FireBomb : ActiveSkill
{
	public Active_Shotgun_FireBomb( StatusScript stat, int spriteId, int _idx ) : base( stat, spriteId, PlanetUtil.WeaponType.Shotgun, _idx )
	{
		skillName = "Grenade";
		skillExplain = "적을 향해 강력한 수류탄을 던집니다. 피격된 적들은 데미지를 입습니다.";

		ActiveSkillShotgun( );

		coolTime = 0f;
		SkilPathSetting( GetFloat( 8f, 4 ), GetFloat( 444f, 4 ), GetFloat( 0f, 4 ) );
		curCoolTime[2] += -0.5f;
		curCoolTime[3] += -1f;
		damage[2] += +222f;
		damage[3] += +444f;
		playTime[2] += +0f;
		playTime[3] += +0f;
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
		//맞은 적은 공격력 피해 입고 40% 이동속도 감소
	}

	public override IEnumerator CoSkill( float time = 0.2f )
	{
		stat.State |= PlanetUtil.CharacterState.Skill; //공격과 같은 취급
		stat.am.Play( "ContinuosAttack_start" );
		stat.am.SetBool( "Attack", true );
		stat.StartCoroutine( ShootMove( ) );
		yield return 0;
		while( time > 0f && !cancel )
		{
			time -= Time.deltaTime;
			yield return 0;
			if( stat.bHit )
				cancel = true;
		}
		if( !cancel )
		{
			stat.bAttack = true;
			var pos = stat.shotPos.position;
			var rot = stat.shotPos.rotation;

			var cmd = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.FireBomb, pos, rot );
			var bs = cmd.GetComponent<Skill_FireBomb>( );
			bs.stat = stat;
			bs.Penetrate = (int)stat[PlanetUtil.CharacterStat.Penetrate];
			bs.damage = damage[level];
			stat.StartCoroutine( ShootEnd( ) );
			stat.am.SetBool( "Attack", false );
		}
		else
			stat.am.SetBool( "Attack", false );
		stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
		ResetCoolTime( );
	}

	public IEnumerator ShootMove( float time = 0.5f, float speed = 0.1f )
	{
		while( time > 0f && !cancel )
		{
			time -= Time.deltaTime;
			stat.transform.position += ( stat.lookLeft ? Vector3.left : Vector3.right ) * Time.deltaTime * speed;
			yield return 0;
		}
	}

	public IEnumerator ShootEnd( float time = 0f )
	{
		yield return 0;
	}
}

//3.Get Excited!
public class Active_Shotgun_Upgrade : ActiveBuffSkill
{
	public Active_Shotgun_Upgrade( StatusScript stat, int spriteId, int _idx ) : base( stat, spriteId, PlanetUtil.WeaponType.Shotgun, _idx )
	{
		skillName = "Get Excited!";
		skillExplain = "유니티쨩이 적들을 향해 샷것을 신나게 발사합니다. 일정시간 동안 공격 속도가 대폭 증가하며 총알 소모량이 0이 됩니다.";

		ActiveSkillShotgun( );
		coolTime = 0f;
		buffType = (int)PlanetUtil.SkillBuffType.shotgun_Upgrade;
		SkilPathSetting( GetFloat( 12f, 4 ), GetFloat( 0f, 4 ), GetFloat( 6f, 4 ) );
		curCoolTime[2] += -1f;
		curCoolTime[3] += -2f;
		damage[2] += +0f;
		damage[3] += +0f;
		playTime[2] += +2f;
		playTime[3] += +4f;
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
	}

	public override IEnumerator CoSkill( float time = 0.3f )
	{
		Debug.Log( "▶" + ToString( ) + " start" );
		var pt = GetPlayTime();

		//버프
		Performence[] p = new Performence[2];
		p[0] = new Performence( );
		p[0].type = PlanetUtil.CharacterStat.ReloadDelay;
		p[0].add = -100f;
		p[1] = new Performence( );
		p[1].type = PlanetUtil.CharacterStat.AttackDelay;
		p[1].add = -0.25f;
		TimeLimitBuff t = new TimeLimitBuff(buffType,p,pt);
		//생성

		GameMgr.Instance.Player.AddBuff( t );
		stat.StartCoroutine( isSwap( ) );
		ResetCoolTime( );
		yield return 0;
	}
}

//?.반사슛
//public class Active_Shotgun_ReplectShoot : ActiveSkill
//{
//	public int bltCount;
//	public int bltMaxCount;
//	public override bool Able( )
//	{
//		if( bltCount > 0 && finish )
//		{
//			return true;
//		}
//		else
//		{
//			//Debug.Log( coolTime - Time.time + "초 남음" );
//			return false;
//		}
//	}

//	private IEnumerator PlusBlt( )
//	{
//		while( bltMaxCount != bltCount )
//		{
//			if( Time.time > coolTime )
//			{
//				coolTime = curCoolTime + Time.time;
//				bltCount += 1;
//			}
//			yield return 0;
//		}
//		while( bltMaxCount == bltCount )
//		{
//			yield return 0;
//		}
//		coolTime = curCoolTime + Time.time;
//		stat.StartCoroutine( PlusBlt( ) );
//	}

//	public Active_Shotgun_ReplectShoot( StatusScript stat ) : base( stat )
//	{
//		coolTime = 0f;
//		curCoolTime[level-1] = 5f;

//		bltMaxCount = 3;
//		bltCount = bltMaxCount;
//		stat.StartCoroutine( PlusBlt( ) );
//	}

//	public override void SkillCallback( StatusScript stat, object obj )
//	{
//	}

//	public override IEnumerator CoSkill( float time = 0.2f )    //public IEnumerator Shoot( float time = 0.2f )
//	{
//		cancel = false;
//		finish = false;
//		bltCount -= 1;
//		stat.State |= PlanetUtil.CharacterState.Skill; //공격과 같은 취급
//		stat.am.Play( "ContinuosAttack_start" );
//		stat.am.SetBool( "Attack", true );
//		stat.StartCoroutine( ShootMove( ) );
//		while( time > 0f && !cancel )
//		{
//			time -= Time.deltaTime;
//			yield return 0;
//			if( stat.bHit )
//				cancel = true;
//		}
//		if( !cancel )
//		{
//			stat.bAttack = true;
//			var pos = stat.shotPos.position;
//			var rot = stat.shotPos.rotation;

//			Debug.Log( "shoot skill" );
//			var cmd = new CreateCommand(ObjectPoolManager.PoolType.ReplectBullet, pos, rot );
//			Debug.Log( rot );
//			GameMgr.Instance.cmdMgr.ExcuteAndPush( cmd );
//			var bs = cmd.GetObject( ).GetComponent<Skill_ReplectBullet_Script>( );
//			bs.angle = stat.lookLeft ? 150f : 30f;
//			bs.stat = stat;
//			bs.Penetrate = 30;
//			bs.damage = 1.5f;
//			bs.maxDamage = 1000f;
//			bs.speed = 5f;
//			bs.Go( );
//			yield return 0;
//			stat.StartCoroutine( ShootEnd( ) );
//		}
//		else
//		{
//			stat.am.SetBool( "Attack", false );
//			//stat.State &= ~PlanetUtil.CharacterState.Skill;
//		}
//		finish = true;
//	}

//	public IEnumerator ShootMove( float time = 0.5f, float speed = 0.5f )
//	{
//		while( time > 0f && !cancel )
//		{
//			time -= Time.deltaTime;
//			stat.transform.position += ( stat.lookLeft ? Vector3.left : Vector3.right ) * Time.deltaTime * speed;
//			yield return 0;
//		}
//	}

//	public IEnumerator ShootEnd( float time = 0f )
//	{
//		stat.am.SetBool( "Attack", false );
//		stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
//		yield return 0;
//	}
//}