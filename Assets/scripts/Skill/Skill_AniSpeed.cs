﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_AniSpeed : MonoBehaviour
{
	public Animator ani;
	private void OnEnable( )
	{
		ani.speed = 1f / Time.timeScale;
	}
}
