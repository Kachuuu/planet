﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//1.마비샷
//2.저격 
//3.강화


//1.마비샷
public class Active_Riple_ParalysisShoot : ActiveSkill
{
	public Active_Riple_ParalysisShoot( StatusScript stat, int spriteId, int _idx ) : base( stat, spriteId, PlanetUtil.WeaponType.Riple, _idx )
	{
		skillName = "패럴라이즈";
		skillExplain = "성능이 좋은 마비 탄환을 발사합니다. 피격된 적은 일정 시간 동안 마비 상태가 됩니다.";

		ActiveSkillRiple( );
		coolTime = 0f;
		SkilPathSetting( GetFloat( 8f, 4 ), GetFloat( 99f, 4 ), GetFloat( 0f, 4 ) );
		curCoolTime[2] += -0.5f;
		curCoolTime[3] += -1f;
		damage[2] += +33f;
		damage[3] += +66f;
		playTime[2] += +0f;
		playTime[3] += +0f;
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
		//맞은 적은 공격력 피해 입고 40% 이동속도 감소
	}

	public override IEnumerator CoSkill( float time = 0.2f )
	{
		finish = false;
		stat.State |= PlanetUtil.CharacterState.Skill; //공격과 같은 취급
		stat.am.Play( "ContinuosAttack_start" );
		stat.am.SetBool( "Attack", true );
		stat.StartCoroutine( ShootMove( ) );
		while( time > 0f && !cancel )
		{
			time -= Time.deltaTime;
			yield return 0;
			if( stat.bHit )
				cancel = true;
		}
		if( !cancel )
		{
			stat.bAttack = true;
			var pos = stat.shotPos.position;
			var rot = stat.shotPos.rotation;

			stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_Gun_R_Shot] );

			Debug.Log( "shoot skill" );
			var cmd = GameMgr.Instance.objMgr.BringObject(ObjectPoolManager.PoolType.ParalysisBullet, pos, rot );
			var bs = cmd.GetComponent<Skill_Sniper_ParaBulletScript>( );
			//			Debug.Log( stat );
			bs.stat = stat;
			bs.Penetrate = (int)stat[PlanetUtil.CharacterStat.Penetrate];
			bs.damage = damage[level - 1];
			yield return 0;
			stat.StartCoroutine( ShootEnd( ) );
		}
		else
			stat.am.SetBool( "Attack", false );
		stat.State &= ~PlanetUtil.CharacterState.Skill;
		stat.SetStatable( (PlanetUtil.StatableSet)int.MaxValue, true );
		ResetCoolTime( );
	}

	public IEnumerator ShootMove( float time = 0.5f, float speed = 0.5f )
	{
		while( time > 0f && !cancel )
		{
			time -= Time.deltaTime;
			stat.transform.position += ( stat.lookLeft ? Vector3.left : Vector3.right ) * Time.deltaTime * speed;
			yield return 0;
		}
	}

	public IEnumerator ShootEnd( float time = 0f )
	{
		stat.am.SetBool( "Attack", false );
		Debug.Log( "★난사 종료★" );
		stat.state = PlanetUtil.CharacterState.Idle;
		yield return 0;
	}
}

//2.비장의 여러발
public class Active_Riple_Sniping : ActiveSkill
{
	private Skill_Sniper_SnipingTargetScript script;
	public Active_Riple_Sniping( StatusScript stat, int spriteId, int _idx ) : base( stat, spriteId, PlanetUtil.WeaponType.Riple, _idx )
	{
		skillName = "비장의 여러발";
		skillExplain = "공을 들인 완벽한 사격을 '여러번' 하여 적에게 큰 피해를 입힙니다.";

		ActiveSkillRiple( );
		coolTime = 0f;
		SkilPathSetting( GetFloat( 12f, 4 ), GetFloat( 444f, 4 ), GetFloat( 0f, 4 ) );
		curCoolTime[2] += -1f;
		curCoolTime[3] += -2f;
		damage[2] += +222f;
		damage[3] += +222f;
		playTime[2] += +0f;
		playTime[3] += +0f;
	}
	public override bool Able( )
	{
		if( Time.time > coolTime && finish )
		{
			//Debug.Log( "ddd" );
			return true;
		}
		else if( Time.time > coolTime && !finish && !immediatlyCancel )
		{
			immediatlyCancel = true;
			return false;
		}
		else
		{
			Debug.Log( coolTime - Time.time + "초 남음" );
			return false;
		}
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
	}

	public override IEnumerator CoSkill( float time = 0.3f )
	{
		finish = false;
		Debug.Log( "저격 대기!" );
		script = stat.Rf_Target.GetComponent<Skill_Sniper_SnipingTargetScript>( );
		stat.State |= PlanetUtil.CharacterState.Skill;
		stat.SetStatable( PlanetUtil.StatableSet.SkillDontCtrl );

		stat.StartCoroutine( script.CoStart( stat ) );
		while( time > 0f && !cancel )
		{
			time -= Time.deltaTime;
			yield return 0;
			if( stat.bHit )
				cancel = true;
		}
		if( !cancel )
		{
			stat.StartCoroutine( Sniping( ) );
		}
		else
		{
			stat.StartCoroutine( script.CoShootEnd( ) );
			Debug.Log( "저격 실패!" );
			ResetCoolTime( );
		}
	}

	public IEnumerator Sniping( float time = 0f )
	{
		Debug.Log( "★저격 모드 진입!★" );
		stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_Gun_R_L] );

		stat.Rf_Target.SetActive( true );
		script.bltCount = 6;
		script.damage = damage[level - 1];
		stat.StartCoroutine( script.CoMove( stat ) );
		Debug.Log( script.bltCount + "," + cancel + "," + immediatlyCancel );
		while( script.bltCount != 0 && !cancel && !immediatlyCancel )
		{
			time -= Time.deltaTime;
			yield return 0;

			if( stat.GetComponent<PlayerInputScript>( ).GetKeyDown( PlayerInputScript.Inputs.Attack ) && !script.isShoot )
			{
				stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_Gun_R_LS] );
				script.StartCoroutine( script.CoShoot( stat ) );
				script.bltCount -= 1;
			}

			if( stat.bHit ) //힛하면 캔슬 + 추가로 스킬 또누르면 캔슬
			{
				cancel = true;
				Debug.Log( "너캔슬됐어" );
			}
			else
			{
				Debug.Log( "응 아니야" );
			}
		}
		Debug.Log( script.bltCount + "," + cancel + "," + immediatlyCancel );
		if( !cancel || immediatlyCancel )
		{
			Debug.Log( cancel );
			Debug.Log( immediatlyCancel );
			Debug.Log( "여기니1" );
			while( script.isShoot )
				yield return 0;
			yield return stat.StartCoroutine( SnipingEnd( ) );
		}
		else //맞았을 때는 힛에서 처리
		{
			Debug.Log( "여기니2" );
			stat.StartCoroutine( script.CoShootEnd( 0.0f ) );
		}
		ResetCoolTime( );
	}

	public IEnumerator SnipingEnd( float time = 0.5f )
	{
		stat.SetStatable( PlanetUtil.StatableSet.DelayMode );
		//stat.state = PlanetUtil.CharacterState.Delay;
		stat.StartCoroutine( script.CoShootEnd( ) );
		while( stat.Rf_Target.activeSelf )
		{
			if( stat.bHit )
				cancel = true;
			yield return 0;
		}
		if( !cancel )
		{
			//stat.State &= ~PlanetUtil.CharacterState.Skill;
			stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
		}
		Debug.Log( "★저격 종료★" );
	}
}

//3.쇼타임
public class Active_Riple_Upgrade : ActiveBuffSkill
{
	public Active_Riple_Upgrade( StatusScript stat, int spriteId, int _idx ) : base( stat, spriteId, PlanetUtil.WeaponType.Riple, _idx )
	{
		skillName = "쇼타임";
		skillExplain = "유니티쨩이 일시적으로 잠재된 재능을 일깨워 크리티컬 확률과 공격속도가 대폭 상승합니다. A.K.A OverSoul";

		ActiveSkillRiple( );

		coolTime = 0f;

		buffType = (int)PlanetUtil.SkillBuffType.riple_Upgrade;
		SkilPathSetting( GetFloat( 12f, 4 ), GetFloat( 0f, 4 ), GetFloat( 6f, 4 ) );
		curCoolTime[2] += -1f;
		curCoolTime[3] += -2f;
		damage[2] += +0f;
		damage[3] += +0f;
		playTime[2] += +1f;
		playTime[3] += +2f;
	}

	public override void SkillCallback( StatusScript stat, object obj )
	{
	}

	public override IEnumerator CoSkill( float time = 0.2f )
	{
		Debug.Log( "▶" + ToString( ) + " start" );

		var pt = GetPlayTime();
		stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_Gun_R_U] );
		Performence[] p = new Performence[2];
		//버프
		p[0] = new Performence( );
		p[0].type = PlanetUtil.CharacterStat.CriticalRate;
		p[0].add = 100f;
		p[1] = new Performence( );
		p[1].type = PlanetUtil.CharacterStat.AttackDelay;
		p[1].add = -0.15f;
		TimeLimitBuff t = new TimeLimitBuff(buffType,p,pt);
		//생성

		GameMgr.Instance.Player.AddBuff( t );
		stat.StartCoroutine( isSwap( ) );
		ResetCoolTime( );
		stat.SetStatable( PlanetUtil.StatableSet.IdleMode );
		yield return 0;
	}
}