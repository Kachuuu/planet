﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionPotal : ActionObject
{
	public Transform targetPos;
	public BoxCollider2D camRect;

	protected override void ToggleOnAction( )
	{
		Telleport( );
		Debug.Log( "ob\n" );
	}
	protected override void ToggleOffAction( )
	{
		Telleport( );
		Debug.Log( "off" );
	}

	void Telleport()
	{
		GameMgr.Instance.Player.transform.position = targetPos.position;
		Camera.main.GetComponent<SimpleFollowCam>( ).CalcRect( camRect );
	}
}
