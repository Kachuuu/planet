﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionDoor : ActionObject
{
	protected override void ToggleOnAction( )
	{
		gameObject.SetActive( false );
	}

	protected override void ToggleOffAction( )
	{
		gameObject.SetActive( true );
	}
}
