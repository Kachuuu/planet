﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionMovingPlatform : ActionObject
{
	public Transform start;
	public Transform end;

	private Vector3 sPos, ePos;
    public float time;
	private float acc = 0f;

	private void Start( )
	{
		sPos = start.position;
		ePos = end.position;
	}

	protected override void OnAction( )
	{
		acc += Time.deltaTime;
		acc %= time * 2f;

		if( acc < time )
			transform.position = Vector3.Lerp( sPos, ePos, acc / time );
		else
			transform.position = Vector3.Lerp( sPos, ePos, 2f - acc / time );
	}

}
