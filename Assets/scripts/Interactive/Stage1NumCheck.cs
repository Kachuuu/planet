﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stage1NumCheck : MonoBehaviour
{
	private Text starString;
	private Text levorString;

	public int startNum;
	public int levorNum;

	private GameObject numCheckPanel;

	private void Start( )
	{
		if( GameMgr.Instance.nowScene == 1 )
		{
			numCheckPanel = UIMgr.Instance.numCheck;
			starString = UIMgr.Instance.starText;
			levorString = UIMgr.Instance.lavorText;
			numCheckPanel.SetActive( true );
		}
		else
		{
			numCheckPanel.SetActive( false );
		}
	}

	private void Update( )
	{
		starString.text = startNum.ToString( );

		levorString.text = levorNum.ToString( );
	}

}
