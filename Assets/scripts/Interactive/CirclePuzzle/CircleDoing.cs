﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleDoing : ObjectInteraction
{
    public Sprite[] spriteArray = new Sprite[2];
    public SpriteRenderer sr;
    public int onCount = 0;

    private CirclePuzzle mother;

    private void Start()
    {
        sr = gameObject.GetComponent<SpriteRenderer>();
        mother = gameObject.transform.parent.GetComponent<CirclePuzzle>();
    }

    private new void Update()
    {
        if (mother.totalCount >= 4)
        {
            onCount = 2;
            sr.sprite = spriteArray[1];
        }
    }

    protected override void HiddenSet()
    {
        if (onCount == 0)
        {
            sr.sprite = spriteArray[1];

            onCount = 1;
        }

        else if (onCount == 1)
        {
            sr.sprite = spriteArray[0];

            onCount = 0;
        }
    }
}
