﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CirclePuzzle : MonoBehaviour
{
   
    public int totalCount = 0;
    private CircleDoing[] circleArray;
    public GameObject[] hiddenDoor;

    public float checkTime;
    private float amount = 0f;
    private bool start = false;

    private void Start()
    {
        circleArray = gameObject.GetComponentsInChildren<CircleDoing>();
	
    }

    private void Update()
    {
        for (int i = 0; i < circleArray.Length; ++i)
        {
            if (circleArray[i].onCount == 1)
            {
                totalCount += 1;
                start = true;
                
            }

            else if (totalCount == 0)
            {
                start = false;
            }
        }

        if (start)
        {
            amount += Time.deltaTime;
			

			if (amount > checkTime)
            {
                ChangePosition();
                amount = 0f;
            }
        }

        if (totalCount == 4)
        {
			for( int i = 0 ; i < hiddenDoor.Length ; ++i )
			{ hiddenDoor[i].SetActive( false ); }
		}

        else
        {
            totalCount = 0;
        }
    }
    private void ChangePosition()
    {
        var changeNumber = Random.Range(0, circleArray.Length - 1);
        var changeNumger2 = Random.Range(0, circleArray.Length - 1);

        if(changeNumber == changeNumger2)
        {
            if(changeNumber == 0)
            {
                changeNumber = Random.Range(1, circleArray.Length -1);
            }

            else if (changeNumber == circleArray.Length - 1)
            {
                changeNumber = Random.Range(0, circleArray.Length - 2);
            }

            else
            {
                changeNumber = Random.Range(1, circleArray.Length - 3);
            }
        }

        var temp = circleArray[changeNumber].transform.position;

        circleArray[changeNumber].transform.position = circleArray[changeNumger2].transform.position;
        circleArray[changeNumger2].transform.position = temp;
    }

	private int count = 0;
	private Text center;
	public string inputString;
	private void OnTriggerStay2D( Collider2D other )
	{
		if( other.tag == "Player" )
		{
			if( totalCount == 4 && count == 0 )
			{
				center = UIMgr.Instance.centerText;
				center.gameObject.SetActive( true );

				center.text = inputString;
				count = 1;
			}
		}
	}

	private void OnTriggerExit2D( Collider2D other )
	{
		if( other.tag == "Player" )
		{
			center = UIMgr.Instance.centerText;
			center.gameObject.SetActive( false );

			center.text = inputString;

		}
	}

}
