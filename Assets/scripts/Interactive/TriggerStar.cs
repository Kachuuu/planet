﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerStar : TriggerObject
{
	public Stage1NumCheck numCheck;
	protected override void HiddenSet( )
	{
		on = true;
		numCheck.startNum += 1;
		gameObject.SetActive( false );
	}
}
