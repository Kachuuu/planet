﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOPKill : MonoBehaviour
{
	private bulletScript playerBullet;
	public GameObject spawnZone;

	private int a = 1;

	void Update( )
	{
		if( gameObject.transform.localPosition.x < 38.0f )
		{
			a = 1;
		}
		else if( gameObject.transform.localPosition.x > 40.0f )
		{
			a = -1;
		}

		gameObject.transform.Translate( Vector3.left * 2.0f * Time.deltaTime * a );
	}


	private void OnTriggerEnter2D( Collider2D other )
	{
		if( other.tag != "Ground" )
		{
			if( playerBullet == null )
			{
				playerBullet = other.GetComponent<bulletScript>( );
			}
		}

		if( playerBullet != null )
		{
			if( playerBullet.stat.tag == "Player" )
			{
				Destroy( spawnZone );
				Destroy( playerBullet.gameObject );
				Destroy( gameObject );
			}
		}
	}
}
