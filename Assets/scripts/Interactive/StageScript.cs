﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageScript : MonoBehaviour
{
	[System.Serializable]
	public struct MonsterSpawnData
	{
		public Transform rootTransform;
		public ObjectPoolManager.PoolType type;
	}

	[System.Serializable]
	public struct StageObject
	{
		public bool and; // false : or
		public TriggerObject[] trigger;
		public ActionObject[] action;
	}

	[Header("Init( Create )")]
	public bool isDungeon = false;
	public Transform playerSpawn;
	public MonsterSpawnData Boss;
	public BoxCollider2D camRect;
	public List<MonsterSpawnData> sponList = new List<MonsterSpawnData>();

	public AudioClip bgm;

	private GameObject bossGo;

	[Space]
	[Header("Objects")]
	public List<StageObject> stageObjs;

	public GameObject Toko;

	void Start( )
	{
		// Create Player
		GameMgr.Instance.MakePlayer( playerSpawn.position );

		// Set Camera Setting
		Camera.main.GetComponent<SimpleFollowCam>( ).CalcRect( camRect );

		// Create Boss
		if( Boss.rootTransform != null)
		{
			bossGo = PlanetMonster.CreateMonster( Boss.type, Boss.rootTransform.position ).gameObject;
		}

		// Create Monsters
		for( int i = 0 ; i < sponList.Count ; ++i )
		{
			var list = sponList[i].rootTransform.GetComponentsInChildren<Transform>();
			for( int j = 1 ; j < list.Length ; ++j )
				PlanetMonster.CreateMonster( sponList[i].type, list[j].position );
		}

		GameMgr.Instance.uiMgr.inDungoen = isDungeon;

		GameMgr.Instance.SceneFade( );

		GameMgr.Instance.PlayBGM( bgm );

		// NPC enable
		if( Toko != null )
		{
			if( GameMgr.Instance.nowScene == 4 ) //town
			{
				if( PlayerData.Instance.questInfoList[1].state == QuestState.complete )
					Toko.SetActive( PlayerData.Instance.playerDic["YukoSQuest"] == 1 ? true : false );
				else
					Toko.SetActive( false );
			}
			else if( GameMgr.Instance.nowScene == 2 ) //스테이지2
			{
				if( PlayerData.Instance.questInfoList[1].state == QuestState.ing )
					Toko.SetActive( PlayerData.Instance.playerDic["YukoSQuest"] == 1 ? false : true );
				else
					Toko.SetActive( false );
			}
		}
	}

	private void Update( )
	{
		for( int i = 0 ; i < stageObjs.Count ; ++i )
			Set( stageObjs[i], IsOn( stageObjs[i] ) );
	}

	bool IsOn( StageObject _data )
	{
		for( int j = 0 ; j < _data.trigger.Length ; ++j )
		{
			if( _data.and && !_data.trigger[j].on )
				return false;
			if( !_data.and && _data.trigger[j].on )
				return true;
		}
		return _data.and;
	}

	void Set( StageObject _data, bool _on )
	{
		for( int i = 0 ; i < _data.action.Length ; ++i )
			_data.action[i].on = _on;
	}
}
