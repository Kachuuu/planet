﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerText : TriggerInteraction
{
	private Text centerText;
	public string touchString;
	public string afterString;

	protected override void HiddenSet( )
	{
		base.HiddenSet( );
		PrintText( );
	}

	protected override void OutColli( )
	{
		base.OutColli( );
		if( centerText == null )
			centerText = GameMgr.Instance.uiMgr.centerText;
		centerText.gameObject.SetActive( false );
	}

	protected override void ToggleOffAction( )
	{
		base.ToggleOffAction( );
		PrintText( );
	}

	protected override void ToggleOnAction( )
	{
		base.ToggleOnAction( );
		PrintText( );
	}


	void PrintText()
	{
		if( centerText == null )
			centerText = GameMgr.Instance.uiMgr.centerText;
		centerText.gameObject.SetActive( true );
		if( !on )
			centerText.text = touchString;
		else
			centerText.text = afterString;
	}

	/*protected override void NpcSet( )
	{
		centerText.text = afterString;
	}
	protected override void OnTriggerEnter2D( Collider2D other )
	{

		if( other.tag == TargetTag )
		{
			base.OnTriggerEnter2D( other );
			this.other = other.gameObject;
			tGo.SetActive( true );
			centerText.text = touchString;
			//HiddenSet( );
		}

	}

	protected override void OnTriggerExit2D( Collider2D other )
	{
		if( other.tag == TargetTag )
		{
			base.OnTriggerEnter2D( other );
			this.other = null;
			tGo.SetActive( false );
			//OutColli( );

		}

	}*/
}
