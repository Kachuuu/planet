﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeHiddenTrriger : ObjectInteraction
{
  
    public TimeMonsterSpawn mobKill;
    public GameObject spawnTrriger;
    private TimeMonsterSpawn deathCount;

    public GameObject[] hiddenObject;

	
    private new void Start()
    {
        gameObject.SetActive(true);
        spawnTrriger.SetActive(false);
        for (int i = 0; i < hiddenObject.Length; ++i)
        { hiddenObject[i].SetActive(false); }
        deathCount = mobKill.GetComponent<TimeMonsterSpawn>();
	
    }

    private new void Update()
    {
        if(deathCount.killCount == deathCount.endCount)
        {
            gameObject.SetActive(false);
            for (int i = 0; i < hiddenObject.Length; ++i)
            { hiddenObject[i].SetActive(false); }
            Debug.Log("end");
        }
    }

    protected override void HiddenSet()
    {
  
        for (int i = 0; i < hiddenObject.Length; ++i)
        { hiddenObject[i].SetActive(true); }

        spawnTrriger.SetActive(true);
    }


}
