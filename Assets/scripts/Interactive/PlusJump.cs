﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlusJump : MonoBehaviour
{
    private StatusScript player;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (gameObject.tag == "HIDDEN" && other.tag == "Player")
        {
            player = other.GetComponent<StatusScript>();

            player.status[(int)PlanetUtil.CharacterStatus.NowJumpableCount] += 1;
        }
    }
}

