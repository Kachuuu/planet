﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenTheBossRoom : MonoBehaviour
{
    private void Update()
    {
        var detect = GameObject.FindGameObjectWithTag("Star");

        if ( detect == null)
        {
            Destroy(gameObject);
        }
    }
}
