﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownPotal : ObjectInteraction
{
	public GameObject stageSelectUI;


	protected override void NpcSet( )
	{
		if( !GameMgr.Instance.uiMgr.stageUI.gameObject.activeInHierarchy )
			GameMgr.Instance.uiMgr.stageUI.Show( );
	}

	protected override void HiddenSet( )
	{
	}

	protected override void OutColli( )
	{
	}
}
