﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropTheObject : MonoBehaviour
{
    public TimeMonsterSpawn killCheck;

    private TimeMonsterSpawn rKillCheck;
    //private Rigidbody2D rb2d;
    private SpriteRenderer sr;
    public Sprite[] boxSprite;
	private Collider2D myColli;
	private int RewardId = 2201;
    private void Start()
    {
        rKillCheck = killCheck.GetComponent<TimeMonsterSpawn>();
        //rb2d = gameObject.GetComponent<Rigidbody2D>();
        sr = gameObject.GetComponent<SpriteRenderer>();
		myColli = gameObject.GetComponent<Collider2D>( );
		myColli.enabled = false;
    }
    private void Update()
    {
        if (rKillCheck.killCount == rKillCheck.endCount)
        {
			//rb2d.gravityScale = 1f;
			myColli.enabled = true;
			sr.sprite = boxSprite[1];
        }
    }

	private void OnTriggerEnter2D( Collider2D collision )
	{
		int mineral;
		var items = GameMgr.Instance.dbMgr.GetDropItems( RewardId, out mineral );
		GameMgr.CreateDropItem( gameObject.transform.position, items, mineral, true);
		Destroy( gameObject );
	}
}
