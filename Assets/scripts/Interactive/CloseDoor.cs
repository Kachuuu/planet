﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseDoor : MonoBehaviour
{
	[System.Serializable]
	public struct MonsterSpawnData
	{
		public Transform rootTransform;
		public ObjectPoolManager.PoolType type;
	}

	[Header("Init( Create )")]
	public MonsterSpawnData Boss;

	public GameObject door;
	private StageScript stageBoss;
	private MonsterStatusScript stat;
	private Collider2D myCollider;

	private void Start( )
	{
		myCollider = gameObject.GetComponent<Collider2D>( );
	}
	private void OnTriggerEnter2D( Collider2D other)
	{
		if(other.tag == "Player")
		{
			door.SetActive( true );
			PlanetMonster.CreateMonster( Boss.type, Boss.rootTransform.position );
			myCollider.enabled = false;
		}
	}
}