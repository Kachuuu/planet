﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeTouchMgr : MonoBehaviour
{
	private TimeTouchObject[] timeObject;
	public int total;

	public GameObject door;
	private Text center;
	public string inputString;
	private void Start( )
	{
		timeObject = gameObject.GetComponentsInChildren<TimeTouchObject>( );
	}

	private void Update( )
	{
		for( int i = 0 ; i < timeObject.Length ; ++i )
		{
			if( timeObject[i].flag3 == 1 )
			{
				total += 1;
			}
		}

		if( total == 2 )
		{
			door.SetActive( false );
		}

		else
		{
			total = 0;
		}
	}
	private int count = 0;

	private void OnTriggerStay2D( Collider2D other )
	{
		if( other.tag == "Player" )
		{
			if( total == 2 && count == 0 )
			{
				center = UIMgr.Instance.centerText;
				center.gameObject.SetActive( true );
			
				center.text = inputString;
				count = 1;
			}
		}
	}

	private void OnTriggerExit2D( Collider2D other )
	{
		if( other.tag == "Player" )
		{
			center = UIMgr.Instance.centerText;
			center.gameObject.SetActive( false );

			center.text = inputString;

		}
	}
}
