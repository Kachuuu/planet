﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeTouchObject : ObjectInteraction
{
    public float timeCheck;
    public Sprite[] spriteArray;

    private float amount;

    public int flag3 = 0;
    private bool start = false;
    private SpriteRenderer sr;
	public TimeTouchMgr touchMgr;
	private Collider2D myCollider;
	protected override void Start( )
	{
		base.Start( );
		sr = gameObject.GetComponent<SpriteRenderer>();
		myCollider = gameObject.GetComponent<Collider2D>( );
    }

    protected override void Update()
    {
		base.Update( );
		if( touchMgr.total >= 2 )
		{
			sr.sprite = spriteArray[1];
			myCollider.enabled = false;
			start = false;
		}

		if (start)
        {
            amount += Time.deltaTime;

            if (amount > timeCheck)
            {
                sr.sprite = spriteArray[0];
                flag3 = 0;

                amount = 0;
            }
        }

		
	}

    protected override void HiddenSet()
    {
        sr.sprite = spriteArray[1];
        start = true;
        flag3 = 1;
    }



}
