﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerLevor : TriggerText
{
	public Sprite[] sprite;
	private int oneChange = 0;

	public Stage1NumCheck levorNumCheck;

	protected override void ToggleOffAction( )
	{
		if( oneChange == 0 )
		{
			base.ToggleOffAction( );
			GetComponent<SpriteRenderer>( ).sprite = sprite[0];
		}
	}

	protected override void ToggleOnAction( )
	{
		if( oneChange == 0 )
		{
			base.ToggleOnAction( );
			GetComponent<SpriteRenderer>( ).sprite = sprite[1];
			if( levorNumCheck != null)
			{ levorNumCheck.levorNum += 1; }
			oneChange = 1;
		}
	}
}
