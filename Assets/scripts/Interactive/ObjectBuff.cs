﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBuff : ObjectInteraction
{
    private StatusScript player;
	public int buffID;
	private Buff buff;

	protected override void Start( )
	{
		buff = GameMgr.Instance.dbMgr.GetBuff( buffID );
	}

	protected override void NpcSet( )
	{
		player = other.GetComponent<StatusScript>( );
		player.AddBuff( buff );

		gameObject.SetActive( false );
	}
}
