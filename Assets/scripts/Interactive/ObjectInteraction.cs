﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectInteraction : MonoBehaviour
{
	public string TargetTag = "Player";

	protected GameObject other;

	protected int flag = 0; //진입시 플래그를 올리고해당 플래그에서 키를 입력하면 대화창 오픈되게하기위한 변수
	private float timer = 0.4f; // 키입력 재입력 대기 시간 (안하면 프레임당 호출됨)
	private float acc = 0f;

    protected virtual void Start()
    {
     
    }

    protected virtual void Update( )
	{
		if( flag == 1 )
			NpcInPutKey( );
		
		acc += Time.deltaTime;
	}

	private void NpcInPutKey( )
	{
		if( acc >= timer && GameMgr.Instance.Player.cs.pis.axies[(int)PlayerInputScript.Aixes.MoveVertical].GetAxis( ) > 0.7f )
		{
			acc = 0f;
			NpcSet( );
		}
	}

	protected virtual void OnTriggerEnter2D( Collider2D other )
	{

		if( other.tag == TargetTag )
		{
			this.other = other.gameObject;
			flag = 1;
			HiddenSet( );
		}

	}

	protected virtual void OnTriggerExit2D( Collider2D other )
	{
		if( other.tag == TargetTag )
		{
			this.other = null;
			flag = 0;
            
            OutColli( );

		}

	}

	protected virtual void NpcSet( )
	{

	}

	protected virtual void HiddenSet( )
	{
 
	}

	protected virtual void OutColli( )
	{

	}
}
