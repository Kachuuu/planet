﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenTrriger : ObjectInteraction
{
    public Transform[] Stage1HiddenMobTrans;
    private int flag2 = 0;

    protected override void HiddenSet()
    {
        if(flag2 == 0)
        {
            HiddenMobCreate();
        }      
    }

    private void HiddenMobCreate()
    {
        for (var i = 0; i < Stage1HiddenMobTrans.Length; ++i)
        {
            Vector3 pos = new Vector3(Stage1HiddenMobTrans[i].position.x, Stage1HiddenMobTrans[i].position.y, Stage1HiddenMobTrans[i].position.z);
			PlanetMonster.CreateRedRepTail(pos);
        }

        flag2 = 1;
    }
}
