﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossKillorLive : MonoBehaviour
{
	private MonsterStatusScript monster;
	private GameObject clearPanel;
	private void Start( )
	{
		clearPanel = GameMgr.Instance.uiMgr.clearPanel;
	}

	private void OnTriggerEnter2D( Collider2D other)
	{
		if(other.tag == "Enemy")
		{
			monster = other.GetComponent<MonsterStatusScript>( );

			if(monster.type == PlanetUtil.CharacterTypes.bossmonster)
			{
				monster = other.GetComponent<MonsterStatusScript>( );
			}
		}
	}

	private void OnTriggerExit2D( Collider2D other )
	{
		if( other.tag == "Enemy" )
		{
			monster = other.GetComponent<MonsterStatusScript>( );

			if( monster.type == PlanetUtil.CharacterTypes.bossmonster )
			{
				monster = other.GetComponent<MonsterStatusScript>( );
				if (monster.State == PlanetUtil.CharacterState.Death )
				{
					clearPanel.gameObject.SetActive( true );
				}
			}
		}
	}

}
