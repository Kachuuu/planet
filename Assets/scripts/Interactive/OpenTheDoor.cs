﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenTheDoor : ObjectInteraction
{
    private SpriteRenderer sr;

    public GameObject[] doors;
    public Sprite[] lavor;
    private void Start()
    {
        sr = gameObject.GetComponent<SpriteRenderer>();
		sr.sprite = lavor[0];
	}

    protected override void HiddenSet()
    {
        sr.sprite = lavor[1];

        for (var i = 0; i < doors.Length; ++i)
            doors[i].SetActive( !doors[i].activeSelf );
    }
}
