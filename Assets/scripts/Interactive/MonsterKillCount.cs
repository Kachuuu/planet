﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterKillCount : MonoBehaviour
{
    public int killCount;
    public int endCount;
    private int amount = 1;

    public bool end = false;

    private void OnTriggerExit2D(Collider2D other)
    {
		if( end == false )
		{
			if( other.tag == "Enemy" )
			{
				var stat = other.GetComponent<MonsterStatusScript>();

				if( stat.State == PlanetUtil.CharacterState.Death )
				{
					killCount++;

				}
			}
		}
    }

    private void Update()
    {
        if (killCount == endCount)
        {
            end = true;
        }
    }

}
