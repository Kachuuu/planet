﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeMonsterSpawn : MonoBehaviour
{
	public Transform[] Stage1HiddenMobTrans;
	public MonsterKillCount killScript;
	//private MonsterKillCount killCountw;

	private int creat = 0;
	private int count = 0;
	//public int spawnCount;
	//private Collider2D myCollider;
	
	public int  killCount = 0;
	public int endCount;
	private void Start( )
	{
		//killCountw = killScript.GetComponent<MonsterKillCount>( );
		//TimeHiddenMobCreate( );
		//myCollider = gameObject.GetComponent<Collider2D>( );
		//Debug.Log( Stage1HiddenMobTrans.Length );
	}

	private void Update( )
	{
		for( int i = 0 ; i < monsters.Count ; ++i )
		{
			if( monsters[i].state == PlanetUtil.CharacterState.Death )
			{
				monsters.RemoveAt( i-- );
				killCount += 1;
			}
		}
		Debug.Log( monsters.Count );

		if( monsters.Count == 0 )
		{
			//isit = true;
			TimeHiddenMobCreate( );
			//Debug.Log( "몬스터가 없어" );
		}

		if( killCount == endCount )
		{
			Destroy( gameObject );
		}

		/*if( PlanetUtil.RangeIn( myCollider, "Enemy" ) == null )
		{
			if( creat == 0 )
			{
				TimeHiddenMobCreate( );

				count += 1;
			}
		}

		Invoke( "ColliAble", 0.3f );*/

	

	}

	private void ColliAble( )
	{
		creat = 0;
	}
	/*private void DetectMob( )
	{
		if(PlanetUtil.RangeIn( myCollider, "Enemy" ) == null )
		{
			dieOrLive = false;
			//creat = 0;
		}

	}*/
	private List<MonsterStatusScript> monsters = new List<MonsterStatusScript>();

	private void TimeHiddenMobCreate( )
	{

		for( var i = 0 ; i < Stage1HiddenMobTrans.Length ; ++i )
		{
			Vector3 pos = new Vector3(Stage1HiddenMobTrans[i].position.x, Stage1HiddenMobTrans[i].position.y, Stage1HiddenMobTrans[i].position.z);
			var monster = PlanetMonster.CreateFemaleNagaBlack( pos );
			var monsterStat = monster.GetComponent<MonsterStatusScript>();
			monsters.Add( monsterStat );
		}

		creat = 1;
	}
}
