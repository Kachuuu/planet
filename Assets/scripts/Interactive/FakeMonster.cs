﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeMonster : ObjectInteraction
{

    public GameObject gop;
    public Transform[] spawnPoint;
	

	protected override void HiddenSet()
    {
        for (int i = 0; i < spawnPoint.Length; ++i)
        {
            Vector3 pos = new Vector3(spawnPoint[i].transform.position.x, spawnPoint[i].transform.position.y, spawnPoint[i].transform.position.z);
            PlanetMonster.CreateFemaleNaga(pos);

        }
		Destroy( gop );
        Destroy(gameObject);
    }
}
