﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerInteraction : TriggerObject
{
	bool set;

	protected override void Start( )
	{
		base.Start( );
		set = on;
	}

	protected override void NpcSet( )
	{
		base.NpcSet( );
		set = !on;
	}

	protected override bool Trigging( )
	{
		base.Trigging( );
		return set;
	}
}
