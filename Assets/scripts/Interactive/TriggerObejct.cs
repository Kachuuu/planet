﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerObject : ObjectInteraction
{	public bool disible = false; //true며 동작하지 않는다.
	public bool on = false;
	private bool oldOn = false;

	protected override void Update( )
	{
		base.Update( );

		if( !disible )
		{
			on = Trigging( );

			if( on == oldOn )
			{
				if( on )	OnAction( );
				else		OffAction( );
			}
			else
			{
				if( on )	ToggleOnAction( );
				else		ToggleOffAction( );
			}

			oldOn = on;
		}
	}

	protected virtual bool Trigging()
	{
		return on;
	}

	protected virtual void ToggleOnAction( )
	{
	}

	protected virtual void ToggleOffAction( )
	{
	}

	protected virtual void OnAction( )
	{
	}

	protected virtual void OffAction( )
	{
	}
}
