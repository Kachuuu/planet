﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasDamage : MonoBehaviour
{
	private StatusScript player3;
	public float gasDamage;
	private float amount;
	public float checkTime;


	private void OnTriggerEnter2D( Collider2D other )
	{
		if( other.tag == "Player" )
		{
			player3 = other.GetComponent<StatusScript>( );
		}

	}

	private void OnTriggerStay2D( Collider2D other )
	{

		if( other.tag == "Player" )
		{
			amount += Time.deltaTime;

			if(player3 == null)
			{
				player3 = other.GetComponent<StatusScript>( );
			}

			if( amount > checkTime )
			{
				//Debug.Log( "dfdf" );
				//Debug.Log( "nowHp" + player3.status[(int)PlanetUtil.CharacterStatus.NowHp] );
				//player3.status[(int)PlanetUtil.CharacterStatus.NowHp] -= gasDamage;
				//player3.status[(int)PlanetUtil.CharacterStatus.NowHp] -= gasDamage;
				var dmg = player3[PlanetUtil.CharacterStat.MaxHp] * gasDamage;

				player3.status[(int)PlanetUtil.CharacterStatus.NowHp] -= dmg;

				player3.cs.DemagePrint( dmg, new Color( 0.9f, 0.1f, 0.1f ), false );
				GameMgr.Instance.HitEffect( 1f - player3.HpRate );
				amount = 0f;
			}
		}
	}

	private void OnTriggerExit2D( Collider2D other )
	{
		amount = 0f;
	}
}
