﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionObject : MonoBehaviour
{
	public bool disible = false; // true면 동작 안함
	public bool on = false;		 // 외부에서 조작함
	private bool oldOn = false;

	protected virtual void Update( )
	{
		if( !disible )
		{
			if( on == oldOn )
			{
				if( on ) OnAction( );
				else OffAction( );
			}
			else
			{
				if( on ) ToggleOnAction( );
				else ToggleOffAction( );
			}
			oldOn = on;
		}
		
	}

	protected virtual void OnAction( )
	{
	}

	protected virtual void OffAction( )
	{

	}

	protected virtual void ToggleOnAction( )
	{
	}

	protected virtual void ToggleOffAction( )
	{

	}
}
