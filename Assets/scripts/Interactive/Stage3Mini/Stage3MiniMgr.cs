﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stage3MiniMgr : MonoBehaviour
{
	[System.Serializable]
	public struct MonsterSpawnData
	{
		public Transform rootTransform;
		//public ObjectPoolManager.PoolType type;
	}

	public enum State
	{
		TowerTime,
		Wave1,
		Wave2,
		Wave3,
		Wave4,
		End
	}

	public State state;

	public GameObject towers;
	private TowerConstruct[] tower;
	public Transform batSpawnPosition;
	public GameObject batMob;
	public GameObject stageClearCheck;

	[Header("Wave1 Data")]
	public int wave1SpawnMonsterCount;
	public float wave1CheckTime;
	public int wave1Count;
	[Space]
	[Header("Wave2 Data")]
	public int wave2SpawnMonsterCount;
	public float wave2CheckTime;
	public int wave2Count;
	[Space]
	[Header("Wave3 Data")]
	public int wave3SpawnMonsterCount;
	public float wave3CheckTime;
	public int wave3Count;

	[Header("Init( Create )")]
	public List<MonsterSpawnData> sponList = new List<MonsterSpawnData>();

	public List<Transform> BossSpawnPosition = new List<Transform>();
	private Transform[] list;
	private List<ObjectPoolManager.PoolType> whatMob = new List<ObjectPoolManager.PoolType>();
	private int waveStart = 0;
	private float checkTime;
	private float amount = 0f;
	private int start = 0;
	public bool waveEnd = false;

	public int wave = 0;
	private void Start( )
	{
		tower = towers.GetComponentsInChildren<TowerConstruct>( );

		whatMob.Add( ObjectPoolManager.PoolType.HellHound );
		whatMob.Add( ObjectPoolManager.PoolType.Wizard );
		whatMob.Add( ObjectPoolManager.PoolType.Skeleton);
	}
	private float startAmount = 0f;
	private float startCheckTime = 3f;
	private void Update( )
	{
		//Debug.Log( waveStart );
		switch( state )
		{
		case State.TowerTime:
			waveStart = 0;
			start = 0;
			waveEnd = false;
			//startAmount = 0;
			/*for( int i = 0 ; i < tower.Length ; ++i )
			{
				tower[i].gameObject.SetActive( true );
			}*/
			break;

		case State.Wave1:
			/*for( int i = 0 ; i < tower.Length ; ++i )
			{
				tower[i].gameObject.SetActive( false );
			}*/

			//startAmount += Time.deltaTime;

			if( start == 0 )
			{

				SpawnMob( wave1SpawnMonsterCount );

			}

			amount += Time.deltaTime;
			checkTime = wave1CheckTime;

			if( waveStart < wave1Count && start == 1 )
			{
				if( amount > checkTime )
				{
					SpawnMob( wave1SpawnMonsterCount );
					++waveStart;

					amount = 0f;
				}
			}

			if( waveStart >= wave1Count )
			{
				waveEnd = true;
				wave = 1;
			}

			break;

		case State.Wave2:
			/*for( int i = 0 ; i < tower.Length ; ++i )
			{
				tower[i].gameObject.SetActive( false );
			}*/
			//if( startAmount > 0 )
			//{ startAmount = 0; }

			//startAmount += Time.deltaTime;

			if( start == 0 )
			{

				SpawnMob( wave2SpawnMonsterCount );

			}

			amount += Time.deltaTime;
			checkTime = wave2CheckTime;

			if( waveStart < wave2Count && start == 1 )
			{
				if( amount > wave2CheckTime )
				{
					SpawnMob( wave2SpawnMonsterCount );
					BatCreat( 3 );
					++waveStart;

					amount = 0f;
				}
			}

			if( waveStart >= wave2Count )
			{
				waveEnd = true;
				wave = 2;
			}

			break;

		case State.Wave3:
			/*for( int i = 0 ; i < tower.Length ; ++i )
			{
				tower[i].gameObject.SetActive( false );
			}*/

			//if( startAmount > 0 )
			//{ startAmount = 0; }

			//startAmount += Time.deltaTime;

			if( start == 0 )
			{

				SpawnMob( wave3SpawnMonsterCount );

			}

			amount += Time.deltaTime;
			checkTime = wave3CheckTime;

			if( waveStart < wave3Count && start == 1 )
			{
				if( amount > wave3CheckTime )
				{
					SpawnMob( wave2SpawnMonsterCount );
					BatCreat( 5 );
					++waveStart;

					amount = 0f;
				}
			}

			if( waveStart >= wave3Count )
			{
				waveEnd = true;
				wave = 3;
			}

			break;

		case State.Wave4:
			{
				/*for( int i = 0 ; i < tower.Length ; ++i )
				{
					tower[i].gameObject.SetActive( false );
				}*/
				
				if( start == 0 )
				{
					SpawnBoss( );
				}

				stageClearCheck.gameObject.SetActive( true );

			}
			break;
		case State.End:

			break;

		}
	}

	private void SpawnMob( int oneSpawnCount )
	{
		var randomSpawns = new List<int>();
		var preRandom = 0;

		for( int k = 0 ; k < sponList.Count ; ++k )
		{
			list = sponList[k].rootTransform.GetComponentsInChildren<Transform>( );
		}

		for( int j = 0 ; j < oneSpawnCount ; ++j )
		{
			var randomSpawn = Random.Range(0, list.Length -1);

			var randomMob = Random.Range(0, whatMob.Count);
			var spawnMob = whatMob[randomMob];

			PlanetMonster.CreateMonster( spawnMob, list[randomSpawn].position );
			preRandom = randomSpawn;
		}

		start = 1;
	}

	private void SpawnBoss( )
	{
		var radomBossPosi = Random.Range(0, BossSpawnPosition.Count -1);

		PlanetMonster.CreateMonster( ObjectPoolManager.PoolType.FemaleNagaBoss, BossSpawnPosition[radomBossPosi].position );
		start = 1;
	}

	private void BatCreat( int batSpawnCount )
	{
		var batSpawn = batSpawnPosition.GetComponentsInChildren<Transform>();

		for( int i = 0 ; i < batSpawnCount ; ++i )
		{
			var randomNumber = Random.Range(0, batSpawn.Length -1);

			Instantiate( batMob, batSpawn[randomNumber].position, Quaternion.identity );
		}
	}
}
