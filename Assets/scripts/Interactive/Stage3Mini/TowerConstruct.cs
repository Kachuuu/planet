﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerConstruct : MonoBehaviour
{
	private float acc = 0f;
	private float timer = 0.4f;
	private float amount = 0f;
	private float checkTime = 3f;

	private int complete = 0;
	private Text explain;

	public string construcString;
	public string completeString;
	private SpriteRenderer sr;
	private Collider2D myColli;
	public Stage3MiniMgr stage3mini;
	public GameObject realTower;
	private void Start( )
	{
		explain = GameMgr.Instance.uiMgr.centerText;
		explain.gameObject.SetActive( true );
		sr = gameObject.GetComponent<SpriteRenderer>( );
		myColli = gameObject.GetComponent<Collider2D>( );
	}
	private void Update( )
	{
		acc += Time.deltaTime;

		if(complete == 1)
		{
			amount += Time.deltaTime;
			explain.text = completeString;

			if(amount > checkTime)
			{
				explain.text = null;
				amount = 0;
				complete = 0;
			}
		}

		if(stage3mini.state == Stage3MiniMgr.State.TowerTime)
		{
			sr.enabled = true;
			myColli.enabled = true;
		}

		if( stage3mini.state == Stage3MiniMgr.State.Wave1 )
		{
			sr.enabled = false;
			myColli.enabled = false;
		}

		if( stage3mini.state == Stage3MiniMgr.State.Wave2 )
		{
			sr.enabled = false;
			myColli.enabled = false;
		}

		if( stage3mini.state == Stage3MiniMgr.State.Wave3 )
		{
			sr.enabled = false;
			myColli.enabled = false;
		}

		if( stage3mini.state == Stage3MiniMgr.State.Wave4 )
		{
			sr.enabled = false;
			myColli.enabled = false;
		}
	}

	private void OnTriggerStay2D( Collider2D other )
	{
		if(other.tag == "Player")
		{
			explain.text = construcString;

			if( acc >= timer && GameMgr.Instance.Player.cs.pis.axies[(int)PlayerInputScript.Aixes.MoveVertical].GetAxis( ) > 0.7f )
			{
				acc = 0f;
				realTower.SetActive( true );
				complete = 1;
				
				

				if( stage3mini.wave == 0 )
				{
					stage3mini.state = Stage3MiniMgr.State.Wave1;
					
				}
				if( stage3mini.wave == 1 )
				{
					stage3mini.state = Stage3MiniMgr.State.Wave2;
					
				}
				if( stage3mini.wave == 2 )
				{
					stage3mini.state = Stage3MiniMgr.State.Wave3;
				
				}
				if( stage3mini.wave == 3 )
				{
					stage3mini.state = Stage3MiniMgr.State.Wave4;
					
				}
			}
		}
	}

	private void OnTriggerExit2D( Collider2D other )
	{
		explain.text = null;
	}




}
