﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatMove : MonoBehaviour
{
	//int a = 1;
	public float speed;

	private float amount;
	public float checkTime;

	public GameObject bullet;

	private bulletScript playerBullet;
	void Update( )
	{
		/*if( transform.localPosition.x < -3.0f )
		{
			a = -1;
		}
		else if( transform.localPosition.x > 3.0f )
		{
			a = 1;
		}*/

		transform.Translate( Vector3.left * speed * Time.deltaTime);

		amount += Time.deltaTime;
		if(amount > checkTime)
		{
			Instantiate( bullet, gameObject.transform.position, Quaternion.identity );
			amount = 0;
		}
	}

	private void OnTriggerEnter2D( Collider2D other )
	{

		if( other.tag != "Ground" )
		{
			if( playerBullet == null )
			{
				playerBullet = other.GetComponent<bulletScript>( );
			}
		}

		if( playerBullet != null )
		{
			if( playerBullet.stat.tag == "Player" )
			{
				Destroy( playerBullet.gameObject );
				Destroy( gameObject );
			}
		}
	}
}
