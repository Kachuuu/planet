﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillORDie : MonoBehaviour
{
	private Collider2D myCollider;
	public Stage3MiniMgr stage3mgr;

	private void Start( )
	{
		//stage3mgr = GetComponent<Stage3MiniMgr>( );
		myCollider = gameObject.GetComponent<Collider2D>();
	}

	private void Update( )
	{
		if( stage3mgr.waveEnd == true )
		{
			if( null == PlanetUtil.RangeIn( myCollider, "Enemy" ) )
			{

				stage3mgr.state = Stage3MiniMgr.State.TowerTime;
			}
		}

	}
}
