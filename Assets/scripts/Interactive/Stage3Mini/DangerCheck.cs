﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DangerCheck : MonoBehaviour
{
	private Slider danger;
	private Slider dff;
	private void Start( )
	{
		danger = GameMgr.Instance.uiMgr.dangerPercent;
		danger.gameObject.SetActive( true );

	}
	private void OnTriggerEnter2D( Collider2D other )
	{
		if(other.tag == "Enemy")
		{
			danger.value++;
		
		}


	}
	private void OnTriggerExit2D( Collider2D other )
	{
		if( other.tag == "Enemy" )
		{
			danger.value--;
		}
	}

	private void Update( )
	{
		if(danger.value == danger.maxValue)
		{
			var player2 = GameMgr.Instance.Player;
			player2.status[(int)PlanetUtil.CharacterStatus.NowHp] = 0;
			//player2.cs.Demage( null, 10000 );
			UIMgr.Instance.Die( );
		}
	}
}
