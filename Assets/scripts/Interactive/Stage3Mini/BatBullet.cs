﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatBullet : MonoBehaviour
{
	private StatusScript stat;
	public int batDamage;

	private void OnTriggerEnter2D( Collider2D other )
	{
		if(other.tag == "Player")
		{
			stat = other.GetComponent<StatusScript>( );

			stat.cs.Demage( null, batDamage);
			Destroy( gameObject );
		}

		if( other.tag == "Ground" )
		{
			Destroy( gameObject );
		}
	}
}
