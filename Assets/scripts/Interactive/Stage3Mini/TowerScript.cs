﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerScript : MonoBehaviour
{
	public Transform shotPos;
	public float damage;
	public float speed;
	public float delay;
	public int bltCount;
	public BoxCollider2D towerCol;
	public GameObject breakEff;

	public SpriteRenderer spRen;
	public AudioClip shootSound,breakSound;

	//public Sprite[] towerSprites;

	private bool ready;
	private int maxBltCount;
	private float currDelay = 1f;
	private Active_Scv_Tower scvScript;
	private AudioSource audioSource;

	private void Awake( )
	{
		audioSource = GetComponent<AudioSource>( );
	}

	/*public void Tower( float damage, float speed, float delay, int bltCount)
	{
		audioSource = GetComponent<AudioSource>( );
		Upgrade( 0 );
		this.damage = damage;
		this.speed = speed;
		this.delay = delay;
		this.bltCount = bltCount;

		currDelay = 1f;
		maxBltCount = bltCount;
		spRen.enabled = true;
		ready = false;
	
	}*/

	/*	public void Upgrade( int index = 1, float delayCount = 1f )
		{
			spRen.sprite = towerSprites[index];
			if( index == 1 )
			{
				maxBltCount = bltCount;
				delay = delay * delayCount;
			}
		}
		*/
	void Update( )
	{
		if( ready )
			return;
		if( monsterColls != null && currDelay <= Time.time )
		{
			if( bltCount == 0 && !ready )
			{
				//StartCoroutine( TowerBreak( ) );
			}
			else
			{
				if( monsterStat != null )
				{
					Shoot( monsterStat.centerPos );
					currDelay = Time.time + delay;
				}
			}
		}
	}
	public IEnumerator TowerBreak( float time = 1f )
	{
		audioSource.PlayOneShot( breakSound );
		ready = true;
		spRen.enabled = false;
	
	    breakEff.SetActive( true );
		yield return new WaitForSeconds( time / 2f );
		yield return new WaitForSeconds( time / 2f );
		breakEff.SetActive( false );
		GetComponent<GOScript>( ).DoDistroy( );
	}
	private void Shoot( Transform target )
	{
		audioSource.PlayOneShot( shootSound );
		bltCount -= 1;
	
		Debug.Log( "rocekt shoot" );
		var pos = shotPos.position;
		var rot = shotPos.rotation;
		var bs = GameMgr.Instance.objMgr.BringObject( ObjectPoolManager.PoolType.Rocket, pos, rot )
			.GetComponent<rocketScript>();
		bs.Shoot( target.transform.position, damage, speed );
	}

	private Collider2D monsterColls;
	private StatusScript monsterStat;
	private void OnTriggerStay2D( Collider2D collision )
	{
		if( monsterColls == null )
		{
			if( collision.tag == "Enemy" )
			{
				monsterColls = collision;
				monsterStat = monsterColls.gameObject.GetComponent<StatusScript>( );
			}
		}
	}
	private void OnTriggerExit2D( Collider2D collision )
	{
		if( monsterColls == collision )
		{
			monsterColls = null;
			monsterStat = null;
		}
	}
}
