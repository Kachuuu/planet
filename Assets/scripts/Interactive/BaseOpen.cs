﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseOpen : ObjectInteraction
{
    public GameObject[] doorObject;
    private SpriteRenderer sr;

    public Sprite[] lavorSprite2;

    protected override void HiddenSet()
    {
        sr = gameObject.GetComponent<SpriteRenderer>();
        sr.sprite = lavorSprite2[1];

		for( int i = 0 ; i < doorObject.Length ; ++i )
		{
			doorObject[i].SetActive( false );
		}
    }
}
