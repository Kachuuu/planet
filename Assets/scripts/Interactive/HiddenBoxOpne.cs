﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenBoxOpne : MonoBehaviour
{

	private int RewardId = 2201;

	private void OnTriggerEnter2D( Collider2D other )
	{
		if( other.tag == "Player" )
		{
			int mineral;
			var items = GameMgr.Instance.dbMgr.GetDropItems( RewardId, out mineral );
			GameMgr.CreateDropItem( gameObject.transform.position, items, mineral, true );
			Destroy( gameObject );
		}
	}
}
