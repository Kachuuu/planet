﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BombDestroy : ObjectInteraction
{
	public GameObject[] bombs;

	public Sprite[] lavorSprite;
	private SpriteRenderer sr;
	private Text center;
	public string destroyString;
	public string touchString;
	private int click = 0;

	protected override void HiddenSet( )
	{
		if( click == 0)
		{
			base.HiddenSet( );
			center = UIMgr.Instance.centerText;
			center.gameObject.SetActive( true );
			center.text = touchString;

		}

		if(click == 1)
		{
			center.text = destroyString;
		}

	}

	protected override void NpcSet( )
	{
		if( click == 0 )
		{
			sr = gameObject.GetComponent<SpriteRenderer>( );
			center = UIMgr.Instance.centerText;
			sr.sprite = lavorSprite[1];
			center.text = destroyString;

			for( int i = 0 ; i < bombs.Length ; ++i )
			{
				bombs[i].SetActive( false );
				Destroy( bombs[i] );
				click = 1;
			}
		}
		//Destroy(gameObject);
	}

	protected override void OutColli( )
	{
		base.OutColli( );
		center.gameObject.SetActive( false );

	}

}
