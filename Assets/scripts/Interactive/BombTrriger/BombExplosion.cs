﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombExplosion : MonoBehaviour
{
    private StatusScript player2;

    public int bombDamage;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            player2 = other.GetComponent<StatusScript>();

            player2.status[(int)PlanetUtil.CharacterStatus.NowHp] -= bombDamage;
            Debug.Log(player2.status[(int)PlanetUtil.CharacterStatus.NowHp]);
            Destroy(gameObject);
        }
    }
}
