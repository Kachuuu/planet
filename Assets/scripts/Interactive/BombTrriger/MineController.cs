﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineController : MonoBehaviour
{
	public GameObject explosion;
	private StatusScript player2;
	private Rigidbody2D rb2d;
	public float bombDamage;


	private void OnTriggerEnter2D( Collider2D other )
	{
		if( other.tag == "Player" )
		{
			player2 = other.GetComponent<StatusScript>( );
			var bombDamage2 = player2[PlanetUtil.CharacterStat.MaxHp];

			player2.cs.Demage( null, player2[PlanetUtil.CharacterStat.MaxHp] * bombDamage );
			rb2d = player2.GetComponent<Rigidbody2D>( );

			if( rb2d != null )
			{
				rb2d.velocity = new Vector2( 0f, 0f );
				rb2d.AddRelativeForce( new Vector3( -1f, 0.3f, 0f ), ForceMode2D.Force );
			}
			Instantiate( explosion, gameObject.transform.position, gameObject.transform.rotation );
			Destroy( gameObject );
		}

	}
}
