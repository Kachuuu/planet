﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMonsterDieORLive : TriggerObject
{
	bool isit = false;

	public StageScript.MonsterSpawnData[] monsterSpawnDatas;
	private List<GameObject> monsters = new List<GameObject>();

	protected override void Start( )
	{
		//Create Monster
		for( int i = 0 ; i < monsterSpawnDatas.Length ; ++i )
		{
			var list = monsterSpawnDatas[i].rootTransform.GetComponentsInChildren<Transform>();
			for( int j = 1 ; j < list.Length ; ++j )
				monsters.Add( PlanetMonster.CreateMonster( monsterSpawnDatas[i].type, list[j].position ).gameObject );
		}
	}

	protected override void Update( )
	{
		for( int i = 0 ; i < monsters.Count ; ++i )
			if( false == monsters[i].activeSelf )
				monsters.RemoveAt( i-- );

		if( monsters.Count == 0 )
			isit = true;

		base.Update( );
	}

	protected override bool Trigging( )
	{
		return isit;
	}

}
