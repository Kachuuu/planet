﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item
{
	public int id;
	public int spriteid;
	public int nowCount;
	public int maxCount;

	public string name;
	public string disc;
	public int useableLevel;

	public int enchanceCount;

	//public StatusScript stat; // 장착된 캐릭터의 status
	public Performence[] pfm;
	public Performence[] addPfm; // 추가 옵션

	public PlanetUtil.ItemType type;
	public PlanetUtil.ItemRate rate;

	public Item()
	{
	}

	public Item( PlanetUtil.ItemType _type, int _maxCount = 1 )
	{
		type = _type;
		nowCount = 1;
		maxCount = _maxCount;
		enchanceCount = 0;
		useableLevel = 1;
	}

	public Item( Item _item )
	{
		id = _item.id;
		spriteid = _item.spriteid;
		nowCount = _item.nowCount;
		maxCount = _item.maxCount;
		name = _item.name;
		disc = _item.disc;
		type = _item.type;
		rate = _item.rate;
		useableLevel = _item.useableLevel;
		enchanceCount = _item.enchanceCount;
		//stat = null;

		if( _item.pfm != null )
		{
			pfm = new Performence[_item.pfm.Length];
			for( int i = 0 ; i < _item.pfm.Length ; ++i )
				pfm[i] = _item.pfm[i].Clone( );
		}

		if( _item.addPfm != null )
		{
			addPfm = new Performence[_item.addPfm.Length];
			for( int i = 0 ; i < _item.addPfm.Length ; ++i )
				addPfm[i] = _item.addPfm[i].Clone( );
		}
	}

	public virtual Item Clone( )
	{
		return new Item( this );
	}
}
[System.Serializable]
public class DummyItem : Item
{
	public DummyItem( int _maxCount = 1 )
		: base( PlanetUtil.ItemType.Raw, _maxCount )
	{
	}

	public DummyItem( DummyItem _item )
	:base (_item)
	{

	}

	public override Item Clone( )
	{
		return new DummyItem( this );
	}
}

[System.Serializable]
public class EquipItem : Item
{
	public bool isApplyed;

	public EquipItem( PlanetUtil.ItemType _type )
		: base( _type, 1 )
	{
		isApplyed = false;
	}

	public EquipItem( EquipItem _item )
	:base (_item)
	{
		isApplyed = _item.isApplyed;
	}

	public override Item Clone( )
	{
		return new EquipItem( this );
	}
}

/// <summary>
///  Weapon 기본 클래스형
/// </summary>
[System.Serializable]
public class Weapon : EquipItem
{
	public PlanetUtil.WeaponType wpType;
	ObjectPoolManager.PoolType bltType;
	protected ControlScript.DefCallback atkHitCallback; // 블릿에 넣는다.

	public int shotSoundId = -1;
	public int reloadSoundId = -1;

	//protected Buff buff; // 공격중일때 적용되는 버프

	public Weapon( ObjectPoolManager.PoolType _bltType, PlanetUtil.WeaponType _type )
		: base( PlanetUtil.ItemType.Weapon )
	{
		wpType = _type;
		bltType = _bltType;
	}

	public Weapon( Weapon _item )
	:base (_item)
	{
		wpType = _item.wpType;
		bltType = _item.bltType;
		shotSoundId = _item.shotSoundId;
		reloadSoundId = _item.reloadSoundId;
}

	public override Item Clone( )
	{
		return new Weapon( this );
	}

	public virtual GameObject AttackCallback( StatusScript _stat, ControlScript.DefCallback _cb )
	{
		var pos = _stat.shotPos.position;
		var rot = _stat.shotPos.rotation;

		if( -1 != shotSoundId )
		{
			_stat.ado.clip = GameMgr.Instance.dbMgr.audios[shotSoundId];
			_stat.ado.Play( );
		}
		var go = GameMgr.Instance.objMgr.BringObject( bltType, pos, rot );
		var bs = go.GetComponent<bulletScript>( );
		bs.Init( false, _stat, null, _stat[PlanetUtil.CharacterStat.BulletRange], 20f, 0f, (int)_stat[PlanetUtil.CharacterStat.Penetrate], 0f, _cb );
		return go;
	}

	public virtual IEnumerator CoAttack( StatusScript stat, float scale = 1f, bool attack = true )
	{
		yield break;
	}
}

[System.Serializable]
public class Handgun : Weapon
{
	public Handgun( )
		: base( ObjectPoolManager.PoolType.Bullet, PlanetUtil.WeaponType.Handgun )
	{

		shotSoundId = PlanetUtil.S_Gun_H_Shot;
		reloadSoundId = PlanetUtil.S_Gun_H_Reload;
	}

	public Handgun( Handgun _item )
	:base (_item)
	{

		shotSoundId = PlanetUtil.S_Gun_H_Shot;
		reloadSoundId = PlanetUtil.S_Gun_H_Reload;
	}

	public override Item Clone( )
	{
		return new Handgun( this );
	}

	public Handgun( Performence[] _pfm )
	: base( ObjectPoolManager.PoolType.Bullet, PlanetUtil.WeaponType.Handgun )
	{
		pfm = _pfm;

		Performence[] perf = new Performence[]
			{   new Performence(){ type = PlanetUtil.CharacterStat.MoveSpeed, scale = 0.8f }    };
	}


	public override IEnumerator CoAttack( StatusScript stat, float scale = 1f, bool attack = true )
	{
		if( attack )
			stat.State |= PlanetUtil.CharacterState.Attack;

		// 애니메이션
		stat.am.Play( "ContinuosAttack_start" );
		stat.am.SetBool( "Attack", true );
		stat.am.speed = 1f / scale;
		stat.bAttack = true;



		// 선딜
		yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.AttackPrevDelay] * scale );

		bool stop = false;
		do
		{
			//공격 콜백
			{
				stat.am.SetTrigger( "Shot" );
				AttackCallback( stat, atkHitCallback ); //Todo 임시로 작업함
				--stat.nowBullet[stat.nowWeapon];
				stat.UpdateBulletUI( );
			}
			// 공격간 딜레이
			yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.AttackDelay] * scale );
			if( stat.nowBullet[stat.nowWeapon] == 0 )
				break;

			if( attack )
				stop = !stat.cs.pis.inputs[(int)PlayerInputScript.Inputs.Attack].GetKey( );

		} while( !stop );

		stat.am.SetBool( "Attack", false );

		//후딜
		yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.AttackPostDelay] * scale );

		stat.am.speed = 1f;

		stat.coAttack = null;
		if( attack )
			stat.State &= ~PlanetUtil.CharacterState.Attack;
	}
}
[System.Serializable]
public class Riple : Weapon
{
	public Riple( )
		: base( ObjectPoolManager.PoolType.Bullet, PlanetUtil.WeaponType.Riple )
	{
		shotSoundId = PlanetUtil.S_Gun_R_Shot;
		reloadSoundId = PlanetUtil.S_Gun_R_Reload;
	}

	public Riple( Performence[] _pfm )
		: base( ObjectPoolManager.PoolType.Bullet, PlanetUtil.WeaponType.Riple )
	{
		shotSoundId = PlanetUtil.S_Gun_R_Shot;
		reloadSoundId = PlanetUtil.S_Gun_R_Reload;
	}

	public Riple( Riple _item )
	:base (_item)
	{
	}

	public override Item Clone( )
	{
		return new Riple( this );
	}


	public override IEnumerator CoAttack( StatusScript stat, float scale = 1f, bool attack = true )
	{
		if( ( stat.State & PlanetUtil.CharacterState.Air ) != 0 )
			yield break;
		
		if( attack )
			stat.State |= PlanetUtil.CharacterState.Attack;

		stat.SetStatable( PlanetUtil.StatableSet.Movement, false );
		Camera.main.GetComponent<SimpleFollowCam>( ).bSizing = true;

		// 애니메이션
		stat.am.SetBool( "Sniping", true );

		// 선딜
		yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.AttackPrevDelay] * scale );

		while( false == stat.cs.pis.inputs[(int)PlayerInputScript.Inputs.Attack].GetKeyDown( ) && 
			   0f == stat.cs.pis.axies[(int)PlayerInputScript.Aixes.MoveHorizontal].GetAxis( ) &&
			   0f == stat.cs.pis.axies[(int)PlayerInputScript.Aixes.MoveVertical].GetAxis( ) )
		{
			Vector2 screenPos;

#if UNITY_ANDROID
			if( stat.cs.pis.GetScreenTouch( out screenPos ) )
#else
			if( Input.GetMouseButton(0) )
#endif
			{
				screenPos = Input.mousePosition;
				Vector2 tgPos = Camera.main.ScreenToWorldPoint( screenPos );
				if( stat.transform.position.x < tgPos.x )	stat.cs.FlipX( false );
				else										stat.cs.FlipX( true );

				stat.am.SetTrigger( "Shot" );
				var blt = AttackCallback( stat, null );
				--stat.nowBullet[stat.nowWeapon];

				//UI 업데이트
				stat.UpdateBulletUI( );

				// 불릿 회전
				Vector2 bltPos = blt.transform.position;
				float digree;
				if( !stat.lookLeft )
					digree = Mathf.Atan2( tgPos.y - bltPos.y, tgPos.x - bltPos.x ) * Mathf.Rad2Deg;
				else
					digree = Mathf.Atan2( bltPos.y - tgPos.y, bltPos.x - tgPos.x ) * Mathf.Rad2Deg;
				blt.transform.Rotate( 0f, 0f, digree, Space.World );

				//yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.AttackDelay] * scale );
				for( float acc = 0f ; acc < stat[PlanetUtil.CharacterStat.AttackDelay] * scale ; acc += Time.deltaTime )
				{
					stat.cs.FlipX( stat.lookLeft );
					yield return null;
				}

				if( 0 == stat.nowBullet[stat.nowWeapon] )
					break;
			}
			yield return null;
		}

		stat.am.SetBool( "Sniping", false );

		//후딜
		Camera.main.GetComponent<SimpleFollowCam>( ).bSizing = false;
		yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.AttackPostDelay] * scale );

		stat.SetStatable( PlanetUtil.StatableSet.Movement, true );
		stat.coAttack = null;

		if( attack )
			stat.State &= ~PlanetUtil.CharacterState.Attack;
	}
}
[System.Serializable]
public class Shotgun : Weapon
{
	public Shotgun( )
		: base( ObjectPoolManager.PoolType.Bullet, PlanetUtil.WeaponType.Shotgun )
	{

		shotSoundId = PlanetUtil.S_Gun_S_Shot;
		reloadSoundId = PlanetUtil.S_Gun_S_Reload;
	}

	public Shotgun( Performence[] _pfm )
		: base( ObjectPoolManager.PoolType.Bullet, PlanetUtil.WeaponType.Shotgun )
	{
		pfm = _pfm;

		shotSoundId = PlanetUtil.S_Gun_S_Shot;
		reloadSoundId = PlanetUtil.S_Gun_S_Reload;
	}

	public Shotgun( Shotgun _item )
	:base (_item)
	{
		pfm = _item.pfm;


	}

	public override Item Clone( )
	{
		return new Shotgun( this );
	}

	public override IEnumerator CoAttack( StatusScript stat, float scale = 1f, bool attack = true )
	{
		if( attack )
			stat.State |= PlanetUtil.CharacterState.Attack;



		// 애니메이션
		stat.am.SetTrigger( "Attack3" );

		// 선딜
		yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.AttackPrevDelay] * scale );

		for( int i = 0 ; i < 6 ; ++i )
		{
			var blt = AttackCallback( stat, atkHitCallback ); //Todo 임시로 작업함
															
			Vector2 bltPos = blt.transform.position;
			var digree = Random.Range(-10f,10f);
			blt.transform.Rotate( 0f, 0f, digree, Space.World );
		}
		--stat.nowBullet[stat.nowWeapon];

		//UI 업데이트
		stat.UpdateBulletUI( );

		//후딜
		yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.AttackPostDelay] * scale );

		stat.coAttack = null;
		if( attack )
			stat.State &= ~PlanetUtil.CharacterState.Attack;
	}
}
[System.Serializable]
public class SCV : Weapon
{
	public SCV( )
		: base( ObjectPoolManager.PoolType.Bullet, PlanetUtil.WeaponType.Scv )
	{
		shotSoundId = PlanetUtil.S_Gun_V_Shot;
		reloadSoundId = PlanetUtil.S_Gun_V_Reload;
	}

	public SCV( Performence[] _pfm )
		: base( ObjectPoolManager.PoolType.Bullet, PlanetUtil.WeaponType.Scv )
	{
		pfm = _pfm;

		shotSoundId = PlanetUtil.S_Gun_V_Shot;
		reloadSoundId = PlanetUtil.S_Gun_V_Reload;
	}

	public SCV( SCV _item )
	:base (_item)
	{
	}

	public override Item Clone( )
	{
		return new SCV( this );
	}

	public override IEnumerator CoAttack( StatusScript stat, float scale = 1f, bool attack = true )
	{
		if( attack )
			stat.State |= PlanetUtil.CharacterState.Attack;

		// 애니메이션
		stat.am.Play( "ContinuosAttack_start" );
		stat.am.SetBool( "Attack", true );
		stat.am.speed = 1f / scale;
		stat.bAttack = true;

		////// 종료 모션 시작 직전 콜백
		//if( null != atkPrevCallback )
		//	atkPrevCallback( stat );



		// 선딜
		yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.AttackPrevDelay] * scale );

		bool stop = false;
		do
		{
			//공격 콜백
			{
				stat.am.SetTrigger( "Shot" );
				AttackCallback( stat, atkHitCallback ); //Todo 임시로 작업함
				--stat.nowBullet[stat.nowWeapon];
				stat.UpdateBulletUI( );
			}
			// 공격간 딜레이
			yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.AttackDelay] * scale );
			if( stat.nowBullet[stat.nowWeapon] == 0 )
				break;

			if( attack )
				stop = !stat.cs.pis.inputs[(int)PlayerInputScript.Inputs.Attack].GetKey( );

		} while( !stop );

		stat.am.SetBool( "Attack", false );

		//후딜
		yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.AttackPostDelay] * scale );


		stat.am.speed = 1f;
		// 종료 모션 종료 후 콜백
		//if( null != atkPostCallback )
		//atkPostCallback( stat );
		stat.coAttack = null;
		if( attack )
			stat.State &= ~PlanetUtil.CharacterState.Attack;
	}
}

[System.Serializable]
public class Armor : EquipItem
{
	public Armor( )
		: base( PlanetUtil.ItemType.Armor )
	{
	}

	public Armor( Armor _item )
	:base (_item)
	{
	}

	public override Item Clone( )
	{
		return new Armor( this );
	}
}
