﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpbarScript : MonoBehaviour
{

	private bool bRunning = false;
	public float time = 1f;
	private float acc;
	private SpriteRenderer sr;

	public Transform zeroPos;
	public Vector3 maxPos = Vector3.zero;
	public Transform hp;
	public Transform follow;

	private Vector3 oldRight;
	void Start( )
	{
		sr = GetComponent<SpriteRenderer>( );
		oldRight = new Vector3( 1f, 0f, 0f );
		follow.SetAsLastSibling( );
		hp.SetAsLastSibling( );
	}

	void Update( )
	{
		if( acc < time )
		{
			acc += Time.deltaTime;
			var lr = follow.localScale;
			lr.x = Mathf.Lerp( follow.localScale.x, hp.localScale.x, acc / time );
			follow.localScale = lr;

			follow.localPosition = Vector3.Lerp( zeroPos.localPosition, Vector3.zero, lr.x );
		}

		if( transform.right != oldRight )
		{
			transform.rotation *= Quaternion.Euler( 0f, 180f, 0f );
			oldRight = transform.right;
		}
	}

	public void SetHp( float _percent )
	{
		var lr = hp.localScale;
		lr.x = _percent;
		hp.localScale = lr;
		hp.localPosition = Vector3.Lerp( zeroPos.localPosition, Vector3.zero, lr.x );
		acc = 0f;
	}

	public void Reload( float _time )
	{
		gameObject.SetActive( true );
		time = _time;
		acc = 0f;
		bRunning = true;
	}

	public void Cancel( )
	{
		bRunning = false;
		gameObject.SetActive( false );
	}

	public void Show( bool _show )
	{
		gameObject.SetActive( _show );
	}
}
