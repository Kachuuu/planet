﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlScript : MonoBehaviour
{
	public ReloadScript ReloadGage;
	public CapsuleCollider2D col;

	public PlayerInputScript pis;
	protected StatusScript stat;
	protected Rigidbody2D rb;
	protected Animator am;
	protected SpriteRenderer sr;

	public delegate void DefCallback( StatusScript _stat );
	public delegate void AttackCallback( StatusScript _stat, DefCallback _cb );

	private event AttackCallback atkCallback;
	private event DefCallback atkPrevCallback;
	private event DefCallback atkPostCallback;
	private event DefCallback atkHitCallback;

	protected virtual void Awake( )
	{
		pis = GetComponent<PlayerInputScript>( );
		stat = GetComponent<StatusScript>( );
		rb = GetComponent<Rigidbody2D>( );
		col = GetComponent<CapsuleCollider2D>( );
		am = transform.GetComponentInChildren<Animator>( );
		sr = transform.GetComponentInChildren<SpriteRenderer>( );
		rb.freezeRotation = true;
	}

	public void ChangeAttackCallback( AttackCallback _atk, DefCallback _prev = null, DefCallback _post = null, DefCallback _hit = null )
	{
		atkPrevCallback = _prev;
		atkCallback = _atk;
		atkPostCallback = _post;
		atkHitCallback = _hit;
	}

	public virtual void Idle( )
	{
		FlipX( stat.lookLeft );
	}

	public virtual bool Attack( )
	{
		if( pis.inputs[(int)PlayerInputScript.Inputs.Attack].GetKey( ) == false )
			return false;

		if( stat.nowBullet[stat.nowWeapon] > 0f )
		{
			if( stat.coReload != null )
			{
				StopCoroutine( stat.coReload );
				ReloadGage.Cancel( );
				stat.statable[(int)PlanetUtil.CharacterStatables.Reload].Reset( true );
				stat.State &= ~PlanetUtil.CharacterState.Reload;
				stat.coReload = null;
			}
			if( stat.coAttack == null )
				stat.coAttack = StartCoroutine( ( (Weapon)stat.GetItem( stat.nowWeapon ) ).CoAttack( stat ) );
			return true;
		}
		else
		{
			if( stat.coReload == null &&
				( 0 < stat.nowMagazine[stat.nowWeapon] ||
				  -1 == stat.nowMagazine[stat.nowWeapon] ) )
				stat.coReload = StartCoroutine( CoReload( ) );
			return false;
		}
	}

	public virtual bool Skill( int _num )
	{
		var skill = stat.playerActiveSkill[(int)((Weapon)stat.GetItem(stat.nowWeapon)).wpType, _num];

		if( skill == null )
			return false;

		if( skill.Able( ) )
		{
			AllStopCoroutine( );
			stat.coSkill = StartCoroutine( skill.CoSkill( ) );
			skillCallback = skill.SkillCallback;

			return true;
		}
		return false;
	}

	public bool Reload( )
	{
		if( pis.inputs[(int)PlayerInputScript.Inputs.ReLoad].GetKeyDown( ) )
		{
			if( stat.coReload == null &&
				stat.nowBullet[stat.nowWeapon] != stat[PlanetUtil.CharacterStat.MaxBullet] &&
				( 0 < stat.nowMagazine[stat.nowWeapon] ||
				  -1 == stat.nowMagazine[stat.nowWeapon] ) )
			{
				AllStopCoroutine( );
				stat.coReload = StartCoroutine( CoReload( ) );
				return true;
			}
		}
		return false;
	}


	public IEnumerator CoReload( )
	{
		stat.statable[(int)PlanetUtil.CharacterStatables.Reload].Reset( false );
		stat.State |= PlanetUtil.CharacterState.Reload;
		var wpType = ((Weapon)stat.GetItem(stat.nowWeapon)).wpType;
		var reloadDelay = stat[PlanetUtil.CharacterStat.ReloadDelay];
		var reloadCnt = (int)stat[PlanetUtil.CharacterStat.ReloadCnt];
		var maxBullet = (int)stat[PlanetUtil.CharacterStat.MaxBullet];

		while( stat.nowBullet[stat.nowWeapon] < maxBullet &&
				( 0 < stat.nowMagazine[stat.nowWeapon] ||
				-1 == stat.nowMagazine[stat.nowWeapon] ) )
		{
			ReloadGage.Reload( reloadDelay );

			var reloadsountID = ((Weapon)stat.GetItem(stat.nowWeapon)).reloadSoundId;
			if( reloadsountID != -1 )
				stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[reloadsountID] );

			yield return new WaitForSeconds( reloadDelay );

			var bltCnt = reloadCnt;
			if( -1 != stat.nowMagazine[stat.nowWeapon] ) //기본 이외 일때
			{
				if( bltCnt > maxBullet - stat.nowBullet[stat.nowWeapon] )
					bltCnt = maxBullet - stat.nowBullet[stat.nowWeapon];

				stat.nowMagazine[stat.nowWeapon] -= bltCnt;
				if( stat.nowMagazine[stat.nowWeapon] < 0 ) // 탄창의 잔량이 부족할 경우
				{
					bltCnt += stat.nowMagazine[stat.nowWeapon];
					stat.nowMagazine[stat.nowWeapon] = 0;
				}
			}

			stat.nowBullet[stat.nowWeapon] += bltCnt;
			stat.nowBullet[stat.nowWeapon] = (int)Mathf.Min( stat.nowBullet[stat.nowWeapon], stat[PlanetUtil.CharacterStat.MaxBullet] );
			stat.UpdateBulletUI( );
		}

		stat.State &= ~PlanetUtil.CharacterState.Reload;
		stat.statable[(int)PlanetUtil.CharacterStatables.Reload].Reset( true );
		stat.coReload = null;
	}

	float footStepDelay = 0.3f;
	float footStepAcc = 0f;
	public virtual bool Move( )
	{
		var x = pis.axies[(int)PlayerInputScript.Aixes.MoveHorizontal].GetAxis( );
		var absX = Mathf.Abs( x );
		if( absX < 0.25f )
		{
			footStepAcc = 0f;
			return false;
		}

		footStepAcc += Time.deltaTime;
		if( footStepDelay < footStepAcc && ( stat.State & PlanetUtil.CharacterState.Air ) == 0 )
		{
			footStepAcc = 0f;
			stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_P_Step] );
		}

#if UNITY_ANDROID
		var moveX = (x / absX ) * stat[PlanetUtil.CharacterStat.MoveSpeed];
#else
		var moveX = x * stat[PlanetUtil.CharacterStat.MoveSpeed];
#endif

		stat.velocity.x = moveX;

		FlipX( stat.velocity.x < 0 );

		return true;
	}

	public virtual void Death( )
	{
		AllStopCoroutine( );
		StartCoroutine( CoDeath( ) );
		GetComponentInChildren<StunEffectScript>( ).SetStun( 0f );
	}

	IEnumerator CoDeath( )
	{
		stat.State = PlanetUtil.CharacterState.Death;
		col.enabled = false;

		am.SetTrigger( "Death" );

		yield return new WaitForSeconds( 2f );
		var oldClr = sr.color;
		var oldAlpha = oldClr.a;

		float acc = 0f;
		while( acc < 1f )
		{
			acc += Time.deltaTime;
			oldClr.a = Mathf.Lerp( 1f, 0f, acc );
			sr.color = oldClr;
			yield return null;
		}

		gameObject.SetActive( false );
		oldClr.a = oldAlpha;
		sr.color = oldClr;
		col.enabled = true;

		stat.State = PlanetUtil.CharacterState.Idle;
		GetComponent<GOScript>( ).DoDistroy( );
	}



	public virtual bool Jump( )
	{
		if( pis.inputs[(int)PlayerInputScript.Inputs.Jump].GetKeyDown( ) == false )
			return false;



		if( stat.NowStanding )
		{
			stat.velocity.y = stat[PlanetUtil.CharacterStat.JumpPower];
			stat.status[(int)PlanetUtil.CharacterStatus.NowJumpableCount] = stat[PlanetUtil.CharacterStat.JumpCount];

			stat.State |= PlanetUtil.CharacterState.Air;

			CanselAttack( );
			am.Play( "Jumping" );
			stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_P_Jump] );

			jumping = 2;
			return true;
		}
		else if( stat.status[(int)PlanetUtil.CharacterStatus.NowJumpableCount] > 0 )
		{
			stat.velocity.y = stat[PlanetUtil.CharacterStat.SecJumpPower];
			stat.status[(int)PlanetUtil.CharacterStatus.NowJumpableCount] -= 1f;

			stat.State |= PlanetUtil.CharacterState.Air;

			CanselAttack( );
			am.Play( "Jumping" );
			stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_P_Jump] );

			return true;
		}

		return false;
	}

	//public virtual void Crouch(bool on )
	//{
	//	am.SetBool( "IsCrouch", on );
	//	if( on )
	//		stat.State = PlanetUtil.CharacterState.Crouch;
	//}

	public virtual bool Dash( )
	{
		if( pis.inputs[(int)PlayerInputScript.Inputs.Dash].GetKey( ) )
		{
			CanselAttack( );

			var x = pis.axies[(int)PlayerInputScript.Aixes.MoveHorizontal].GetAxis( );
			if( x != 0f )
				FlipX( x < 0 );

			StartCoroutine( CoDash( ) );
			return true;
		}
		return false;
	}

	public IEnumerator CoDash( )
	{

		stat.State = PlanetUtil.CharacterState.Dash;
		am.SetTrigger( "Gurugi" );
		col.isTrigger = true;
		stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_P_Avoid] );

		stat.statable[(int)PlanetUtil.CharacterStatables.Hit].Reset( false );

		var velX = stat[PlanetUtil.CharacterStat.DashSpeed] * transform.right.x;
		float acc = 0f;
		while( acc < stat[PlanetUtil.CharacterStat.DashOpTime] )
		{
			acc += Time.deltaTime;
			stat.velocity.x = velX;
			FlipX( stat.lookLeft );
			yield return null;
		}

		stat.State &= ~PlanetUtil.CharacterState.Dash;
		stat.statable[(int)PlanetUtil.CharacterStatables.Hit].Reset( true );
		acc = 0f;
		while( acc < stat[PlanetUtil.CharacterStat.DashTime] )
		{
			acc += Time.deltaTime;
			stat.velocity.x = velX;
			yield return null;
		}

		col.isTrigger = false;
		stat.State = PlanetUtil.CharacterState.Idle;
		stat.statable[(int)PlanetUtil.CharacterStatables.Dash].SetTimer( 0.5f );
	}

	// 적용된다면 true. 아니라면 false
	public virtual bool Demage( StatusScript _stat, float add = 0f, float scale = 1f, PlanetUtil.AttackType type = PlanetUtil.AttackType.Melee, bool stuck = true, float _stun = 0f )
	{
		if( _stat != null && ( stat.State & PlanetUtil.CharacterState.Dash ) != 0 )
			//SuperVoid( );

			if( !stat.statable[(int)PlanetUtil.CharacterStatables.Hit].Able( ) )
				return false;

		// Todo 데미지 공식
		bool cri = false;
		float dmg;
		if( _stat != null )
		{
			cri = Random.Range( 0f, 1f ) < _stat[PlanetUtil.CharacterStat.CriticalRate];
			dmg = ( _stat[PlanetUtil.CharacterStat.Attack] + add ) * scale * Random.Range( 0.935f, 1.065f )
						* ( ( cri ) ? 2f : 1f );
		}
		else
			dmg = add * scale;

		dmg -= stat[PlanetUtil.CharacterStat.Armor];
		dmg = Mathf.Max( 0, dmg );

		//Debug.Log( "Cir:" + cri.ToString( ) +
		//	", defAtk:" + _stat[PlanetUtil.CharacterStat.Attack] +
		//	", Add:" + add +
		//	", scale:" + scale +
		//	", hap: " + dmg );


		//핸드건 전용 가드 스킬
		if( ( (int)stat.State & (int)PlanetUtil.CharacterState.Guard ) != 0 &&
			type != PlanetUtil.AttackType.NonGuard )
		{
			skillCallback( _stat, dmg );
			return false;
		}

		stat.ado.PlayOneShot( GameMgr.Instance.dbMgr.audios[PlanetUtil.S_P_Hit] );

		stat.Demage( dmg );
		if( dmg > 0f )
		{
			DemagePrint( dmg, new Color( 0.9f, 0.1f, 0.1f ), cri );
			GameMgr.Instance.HitEffect( 1f - stat.HpRate );
			//GameMgr.Instance.SuperVoid( 0.3f, 0.3f ); // 피격시 슬로우
		}

		if( stat.HpRate == 0f )
		{
			AllStopCoroutine( );
			GetComponentInChildren<StunEffectScript>( ).SetStun( 0f );
			return true;
		}

		// if Stun?
		if( _stun != 0f && stat.HpRate != 0f )
		{
			stat.State = PlanetUtil.CharacterState.Stun;
			stat.statable[(int)PlanetUtil.CharacterStatables.Stun].SetTimer( _stun );
			GetComponentInChildren<StunEffectScript>( ).SetStun( _stun );
		}
		// if not Stuck?
		if( ( stat.State & ( PlanetUtil.CharacterState.Attack | PlanetUtil.CharacterState.Skill ) ) != 0 && // 스킬 또는 공격중일때
			( ( stat.State & PlanetUtil.CharacterState.SuperArmor ) != 0 || stuck == false || dmg == 0 ) )  // 슈퍼아머가 있거나 데미지가 0이거나
		{                                                                                                   // stuck 공격이 아닐때
			return true;                                                                                    // 경직 없음
		}

		stat.State = PlanetUtil.CharacterState.Idle;
		AllStopCoroutine( );

		if( null != stat.coStuck )
			StopCoroutine( stat.coStuck );
		if( stat.HpRate != 0f )
			stat.coStuck = StartCoroutine( CoStuck( _stat ) );
		return true;
	}

	public virtual IEnumerator CoStuck( StatusScript _stat )
	{
		stat.State |= PlanetUtil.CharacterState.Stuck;
		stat.bHit = true;
		col.enabled = false;
		stat.StatableResetAll( false );

		am.SetTrigger( "Damage" );
		yield return new WaitForSeconds( stat[PlanetUtil.CharacterStat.ReHitTime] );

		stat.State &= ~PlanetUtil.CharacterState.Stuck;
		stat.bHit = false;
		col.enabled = true;
		stat.StatableResetAll( true );

		stat.coStuck = null;
	}

	public bool WeaponSwap( )
	{
		if( pis.inputs[(int)PlayerInputScript.Inputs.Swap].GetKeyDown( ) )
		{
			if( ( stat.State & PlanetUtil.CharacterState.Skill ) == 0 )
			{
				AllStopCoroutine( );
				stat.SwapWeapon( );
				return true;
			}
		}
		return false;
	}

	public bool DownJump( )
	{
		if( pis.inputs[(int)PlayerInputScript.Inputs.Jump].GetKeyDown( ) == false )
			return false;

		StartCoroutine( CoDownJump( ) );
		jumping = 2;
		return true;
	}

	private IEnumerator CoDownJump( )
	{
		stat.State |= PlanetUtil.CharacterState.Air;
		stat.velocity.y = -5f;
		var defCol = stat.col;
		defCol.enabled = false;
		yield return new WaitForSeconds( 0.12f );
		defCol.enabled = true;
	}

	private void SuperVoid( )
	{
		GameMgr.Instance.SuperVoid( );
	}

	public void FlipX( bool _flip )
	{
		stat.lookLeft = _flip;
		transform.rotation = Quaternion.Euler( 0f, stat.lookLeft ? 180f : 0f, 0f );
	}

	public void FlipXToggle( )
	{
		stat.lookLeft = !stat.lookLeft;
		transform.rotation = Quaternion.Euler( 0f, stat.lookLeft ? 180f : 0f, 0f );
	}

	public void DemagePrint( float _dmg, Color _clr, bool _cri )
	{
		var pos = stat.centerPos.position + new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f), 0f );
		var go = GameMgr.Instance.objMgr.BringObject( ObjectPoolManager.PoolType.DemagePrint, pos );
		if( _cri ) go.GetComponent<GODemagePrint>( ).tm.characterSize = 0.08f;
		else go.GetComponent<GODemagePrint>( ).tm.characterSize = 0.05f;
		go.GetComponent<GODemagePrint>( ).tm.text = _dmg.ToString( "0" );
		go.GetComponent<GODemagePrint>( ).tm.color = _clr;
	}

	public void AllStopCoroutine( )
	{
		CanselAttack( );
		CanselReload( );
		//StopAllCoroutines( );
		//stat.StopAllCoroutines( );
		stat.State = PlanetUtil.CharacterState.Idle;
		stat.coStuck = null;
		stat.coSkill = null;
		stat.StatableResetAll( true );
		if( stat.name == "Player" )
			Camera.main.GetComponent<SimpleFollowCam>( ).bSizing = false;
	}

	public void CanselAttack( )
	{
		if( stat.coAttack != null )
		{
			stat.State &= ~PlanetUtil.CharacterState.Attack;
			StopCoroutine( stat.coAttack );
			stat.coAttack = null;

			am.SetBool( "Attack", false );
			am.SetBool( "Sniping", false );
			am.Play( "Stand" );
			stat.RemoveBuff( 556 ); // Todo 중화기 이속 저하 버프 제거. 불안정하기 때문에 꼭 수정
			am.speed = 1f;
		}
	}

	public void CanselReload( )
	{
		if( stat.coReload != null )
		{
			stat.State &= ~PlanetUtil.CharacterState.Reload;
			StopCoroutine( stat.coReload );
			stat.coReload = null;

			ReloadGage.Cancel( );
		}
	}


	protected virtual void Update( )
	{
		if( Input.GetKeyDown( KeyCode.Space ) )
		{
			//stat.SwapWeapon( );
		}

		var distanceFromGround = Physics2D.Raycast (transform.position- new Vector3(0f,-0.1f,0f), Vector3.down, 2, LayerMask.GetMask("Ground"));
		var jumpDist = distanceFromGround.collider == null ? 99 : distanceFromGround.distance;
		if( ( rb.velocity.y < 0.05f && stat.NowStanding == true ) )
		{
			am.SetFloat( "GroundDistance", 0f );
			am.SetFloat( "FallSpeed", 0f );
		}
		// Todo 랜딩 시 2연속 랜딩 함. 
		else
		{
			am.SetFloat( "GroundDistance", jumpDist );
			am.SetFloat( "FallSpeed", rb.velocity.y );
		}
		if( ( stat.State & PlanetUtil.CharacterState.Air ) != 0 && stat.NowStanding == true )
			stat.State &= ~PlanetUtil.CharacterState.Air;

		//Debug.DrawLine( transform.position, transform.position - Vector3.up , Color.red, 5f );
		//Debug.Log( transform.position.x.ToString("0.000") + ", " + rb.velocity );
		// Todo 여기서  액션 커맨드 캡처 (이동 과 회전 )

	}
	// Todo 임시. 모두 스킬에서 처리하고 반드시 지울 것
	private delegate void SkillCallback( StatusScript _stat, object _obj );
	private SkillCallback skillCallback;

	public void LateUpdate( )
	{
		am.SetFloat( "Speed", Mathf.Abs( stat.velocity.x / stat[PlanetUtil.CharacterStat.MoveSpeed] ) );
	}

	protected int jumping = 0; // jump, downJump를 할 때 3가 된다.
	protected virtual void FixedUpdate( )
	{
		if( collisions.Count != 0 )
		{
			--jumping;
			for( int i = 0 ; i < collisions.Count ; ++i )
			{
				if( !stat.NowStanding && collisions[i].contacts.Length != 0 )
				{
					var nor = collisions[i].contacts[0].normal;

					if( ( nor.x < -0.9f && stat.velocity.x > 0f ) || ( nor.x > 0.9f && stat.velocity.x < 0f ) )
					{
						stat.velocity.x = 0f;
						stat.velocityAcc.x = 0f;
						break;
					}
					else if( jumping < 0 && collisions[i].gameObject.tag == "Ground" )
					{
						if( nor.y < -0.9f && stat.velocity.y > 0f )
						{
							stat.velocity.y = 0f;
							stat.velocityAcc.y = 0f;
						}
						else if( nor.y > 0.9f && stat.velocity.y <= 0f )
						{
							stat.velocity.y = 0f;
							stat.velocityAcc.y = 0f;
						}
					}
				}
				else if( jumping < 0 && stat.velocity.y <= 0f )
				{
					stat.velocity.y = 0f;
					stat.velocityAcc.y = 0f;
				}
			}
		}
		collisions.Clear( );

		var sub = Physics2D.gravity * rb.gravityScale * Time.fixedDeltaTime;

		var x = stat.velocity.x + sub.x + stat.velocityAcc.x;
		var y = Mathf.Max( stat.velocity.y, -20f ) + sub.y + stat.velocityAcc.y;


		rb.velocity = new Vector2( x, y );
		stat.velocity.y += sub.y;
		stat.velocity.x = 0f;

		// 가속도 감속
		if( stat.velocityAcc.magnitude > 1f )
		{
			stat.velocityAcc = Vector2.Lerp( stat.velocityAcc, Vector2.zero, 0.1f );
		}
		else if( stat.velocityAcc != Vector2.zero )
		{
			stat.velocityAcc = Vector2.zero;
		}
	}


	private int cntGround = 0;
	private List<Collision2D> collisions = new List<Collision2D>();
	private void OnCollisionStay2D( Collision2D collision )
	{
		collisions.Add( collision );
	}

	protected virtual void OnTriggerEnter2D( Collider2D collision )
	{
		if( collision.tag == "Ground" || collision.tag == "GroundPlatform" )
		{
			//if( collision.transform.position.y < stat.transform.position.y - 0.1f )
			stat.listStandingBlock = collision.gameObject;

			//	if( cntGround == 0 )
			{
				stat.status[(int)PlanetUtil.CharacterStatus.NowJumpableCount] = (int)stat[PlanetUtil.CharacterStat.JumpCount];
				stat.NowStanding = true;
				//if( stat.State == PlanetUtil.CharacterState.Jump )
				//	stat.State = PlanetUtil.CharacterState.Idle;
			}
			//	++cntGround;
		}
	}

	protected virtual void OnTriggerStay2D( Collider2D collision )
	{
		if( collision.tag == "Ground" || collision.tag == "GroundPlatform" )
		{
			//if( collision.transform.position.y < stat.transform.position.y - 0.1f && stat.State == PlanetUtil.CharacterState.Jump )
			{
				//stat.State = PlanetUtil.CharacterState.Idle;
				stat.NowStanding = true;
			}
		}
	}

	protected virtual void OnTriggerExit2D( Collider2D collision )
	{
		if( collision.tag == "Ground" || collision.tag == "GroundPlatform" )
		{
			if( collision.transform.position.y < stat.transform.position.y - 0.1f )
			{
				//if( stat.State != PlanetUtil.CharacterState.Attack )
				//{
				//	stat.State = PlanetUtil.CharacterState.Jump;
				//	//stat.NowStanding = false;
				//}
			}
			//--cntGround;
			//if( cntGround == 0 )
			stat.NowStanding = false;
		}
	}
}
