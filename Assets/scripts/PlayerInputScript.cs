﻿//#define UNITY_ANDROID

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CnControls;
using System;

public class PlayerInputScript : MonoBehaviour
{
	public enum Inputs
	{
		Attack,
		Dash,
		Jump,
		Skill1,
		Skill2,
		Slot1,
		Slot2,
		Swap,   // 재스처
		ReLoad, // 재스처
		Move,
		none    
	}

	public enum Aixes
	{
		MoveHorizontal,
		MoveVertical,
		AimHorizontal,
		AimVertical,
	}

	[System.Serializable]
	public class KeyMapping
	{
		public Inputs input; // 반드시 enum의 순서와 일치해야 한다.
		public KeyCode key;
		public CommandButton btn;
		public bool gesture;
		public bool btnPressed;
		public bool btnToggled;

		public void Init( )
		{
#if UNITY_ANDROID
			if ( btn != null )
				btn.keymapping = this;
#elif UNITY_STANDALONE
#endif
			btnPressed = false;
			btnToggled = false;
		}

		public void Uninit( )
		{
#if UNITY_ANDROID
			if( btn != null )
				btn.keymapping = null;
#elif UNITY_STANDALONE
#endif
			btnPressed = false;
			btnToggled = false;
		}

		public bool GetKeyDown( )
		{
#if UNITY_ANDROID
			return btnPressed && btnToggled;
#elif UNITY_STANDALONE
			return Input.GetKeyDown( key );
#endif
		}
		public bool GetKey( )
		{
#if UNITY_ANDROID
			return btnPressed;
#elif UNITY_STANDALONE
			return Input.GetKey( key );
#endif
		}
		public bool GetKeyUp( )
		{
#if UNITY_ANDROID
			return !btnPressed && btnToggled;
#elif UNITY_STANDALONE
			return Input.GetKeyUp( key );
#endif
		}

		public void OnDown( )
		{
			btnPressed = true;
			btnToggled = true;
		}
		public void OnUp( )
		{
			btnPressed = false;
			btnToggled = true;
		}
		public void OffToggle( )
		{
			btnToggled = false;
		}
	}


	// 터치시 배제되는 인덱스 들
	public static List<int> touchExclude = new List<int>(); // 버튼 터치 한 핑거 아이디 들.(무브버튼만 들어간다)
	public static int GestureFingerID = -1; // 리스트에 없는 핑거아이디가 여기들어감

	public float minSlidDist = 50f;

	private void Update( )
	{
		if( Input.touches.Length == 0 )
		{
			touchExclude.Clear( );
			GestureFingerID = -1;
		}
		else if( GestureFingerID == -1 )
		{
			for( var i = 0 ; i < Input.touches.Length ; ++i )
			{
				bool isIt = false;

				for( var j = 0 ; j < touchExclude.Count ; ++j )
				{
					if( Input.touches[i].fingerId == touchExclude[j] )
					{
						isIt = true;
						break;
					}
				}

				if( !isIt )
				{
					GestureFingerID = Input.touches[i].fingerId;
					break;
				}
				else
					continue;
			}
		}

		if( GestureFingerID != -1 )
			for( var i = 0 ; i < Input.touches.Length ; ++i )
				if( Input.touches[i].fingerId == GestureFingerID )
					TracingGesture( i );
	}

	private void TracingGesture( int _idx ) // 한프레임에 델타값만을 검사 원래대로 변수 -1 마지막위치를 가지고있는 변수를 만들고
	{
		if( !inputs[(int)Inputs.ReLoad].GetKey( ) &&
			Mathf.Abs(Input.touches[_idx].deltaPosition.x) > minSlidDist )
		{
			inputs[(int)Inputs.ReLoad].OnDown( );
			return;
		}
		else if( inputs[(int)Inputs.ReLoad].GetKey( ) &&
			Input.touches[_idx].phase == TouchPhase.Ended )
		{
			inputs[(int)Inputs.ReLoad].OnUp( );
			GestureFingerID = -1;
			return;
		}

		if( !inputs[(int)Inputs.Swap].GetKey( ) &&
			Mathf.Abs( Input.touches[_idx].deltaPosition.y) > minSlidDist )
		{
			inputs[(int)Inputs.Swap].OnDown( );
			return;
		}
		else if( inputs[(int)Inputs.Swap].GetKey( ) &&
			Input.touches[_idx].phase == TouchPhase.Ended )
		{
			inputs[(int)Inputs.Swap].OnUp( );
			GestureFingerID = -1;
			return;
		}
	}

	public bool GetScreenTouch( out Vector2 _pos )
	{
		if( GestureFingerID != -1 )
		{
			for( var i = 0 ; i < Input.touches.Length ; ++i )
			{
				if( Input.touches[i].fingerId == GestureFingerID )
				{
					_pos = Input.touches[i].position;
					return true;
				}
			}
		}
		_pos = Vector2.zero;
		return false;
	}

	public static int GetNowTouchID( Vector2 pos )
	{
		foreach( var tcID in Input.touches )
		{
			if( pos == tcID.position )
				return tcID.fingerId;
		}
		return -1;
	}

	public static void RemoveExclude( int id )
	{
		touchExclude.Remove( id );
	}

	public static void AddExclude( int id )
	{
		touchExclude.Add( id );
	}

	[Serializable]
	public struct AixsMapping
	{
		public Aixes input; // 반드시 enum의 순서와 일치해야 한다.
		public KeyCode key1;
		public KeyCode key2;
		public string axisName;
		private VirtualAxis axis;

		public void Init( )
		{
			//CinputManager에 등록
			axis = axis ?? new VirtualAxis( axisName );
			CnInputManager.RegisterVirtualAxis( axis );
		}
		public void Uninit( )
		{
			//CinputManager에 등록 해지
			// 이미 삭제됨
			//CnInputManager.UnregisterVirtualAxis( axis );
		}

		public float GetAxis( )
		{
			return CnInputManager.GetAxis( axisName );
		}
	}

	[HideInInspector]
	public KeyMapping[] inputs;
	[HideInInspector]
	public AixsMapping[] axies;

	private void LateUpdate( )
	{
		for( int i = 0 ; i < inputs.Length ; ++i )
		{
			inputs[i].OffToggle( );
		}
	}

	private void OnDestroy( )
	{
		foreach( var it in inputs )
			it.Uninit( );
		foreach( var it in axies )
			it.Uninit( );
	}

	public void InitButtons( )
	{
		touchExclude.Clear( );
		GestureFingerID = -1;
#if UNITY_ANDROID
		var bts = GameMgr.Instance.uiMgr.controllerRoot.GetComponentsInChildren<CommandButton>();
		foreach( var it in inputs )
		{
			for( int i = 0 ; i < bts.Length ; ++i )
			{
				if( it.input == bts[i].input )
				{
					it.btn = bts[i];
					break;
				}
			}
			it.Init( );
		}
		foreach( var it in axies )
			it.Init( );
#elif UNITY_STANDALONE
#endif


	}


	public bool GetKeyDown( Inputs _input )
	{
		return inputs[(int)_input].GetKeyDown( );
	}
	public bool GetKey( Inputs _input )
	{
		return inputs[(int)_input].GetKey( );
	}
	public bool GetKeyUp( Inputs _input )
	{
		return inputs[(int)_input].GetKeyUp( );
	}


}
