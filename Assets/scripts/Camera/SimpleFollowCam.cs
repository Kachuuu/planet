﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraMode
{
	normal,
	action,
	effect,
}

public class SimpleFollowCam : MonoBehaviour
{
	protected Camera cam;
	protected Transform cachedTf;
	public Transform tg, tgA;
	public CameraMode mode;
	public float z;
	public float sizein;
	public float sizeout;
	public bool bSizing;
	public float sizeAcc;
	public float sizeTime = 0.8f;
	public bool normalMode;
	public bool gmaeMode;
	public float speed = 10f;


	private Vector2 movableH;
	private Vector2 movableV;

	public CanvasGroup cg;

	protected void Start( )
	{
		mode = CameraMode.normal;
		cam = GetComponent<Camera>( );
		cachedTf = transform;
		z = cachedTf.position.z;
		sizein = cam.orthographicSize;
		sizeout = sizein + 3f;
		bSizing = false;
		normalMode = true;
		gmaeMode = false;
	}

	public void CalcRect( BoxCollider2D _rect )
	{
		// Rect 설정
		//v = Camera.main.orthographicSize;
		//h = v * Camera.main.pixelWidth / Camera.main.pixelHeight;
		var rectPos = new Vector2 ( _rect.offset.x + _rect.transform.position.x,
									_rect.offset.y + _rect.transform.position.y );

		movableV = new Vector2( rectPos.y - ( _rect.size.y / 2 ), rectPos.y + ( _rect.size.y / 2 ) );
		movableH = new Vector2( rectPos.x - ( _rect.size.x / 2 ), rectPos.x + ( _rect.size.x / 2 ) );

		// 카메라보다 영역이 작을 경우 흔들림 제거를 위해
		//	movableH.y = Mathf.Max( movableH.x, movableH.y );
		//movableV.y = Mathf.Max( movableV.x, movableV.y );

		_rect.enabled = false;
	}

	// Update is called once per frame
	protected void LateUpdate( )
	{
		if( tg != null )
		{
			if( mode == CameraMode.normal )
			{
				if( !bSizing ) sizeAcc = Mathf.Clamp( sizeAcc - Time.deltaTime, 0f, sizeTime ); // 줌 인
				else sizeAcc = Mathf.Clamp( sizeAcc + Time.deltaTime, 0f, sizeTime ); // 중 아웃

				cam.orthographicSize = Mathf.Lerp( sizein, sizeout, sizeAcc / sizeTime );
				var v = Camera.main.orthographicSize;
				var h = v * Camera.main.pixelWidth / Camera.main.pixelHeight;

				var moveH = new Vector2 (movableH.x + h, movableH.y - h );
				var moveV = new Vector2 (movableV.x + v, movableV.y - v );
				moveH.y = Mathf.Max( moveH.x, moveH.y );
				moveV.y = Mathf.Max( moveV.x, moveV.y );

				var pos = Vector3.Lerp( cachedTf.position, tg.position, Time.deltaTime * speed );
				pos = new Vector3( Mathf.Clamp( pos.x, moveH.x, moveH.y ),
								   Mathf.Clamp( pos.y, moveV.x, moveV.y ),
								   z );

				cachedTf.position = pos;
			}
			else if( mode == CameraMode.action )
			{
				var pos = Vector3.Lerp(cachedTf.position, tgA.position, 1f);
				pos.z = z;
				cachedTf.position = pos;
			}
		}
	}

	public void ShakeCamera( float x, float y, float z, float time, bool change = true, float delay = 0f, float endDelay = 0f, bool islocal = false )
	{
		StartCoroutine( CoShakeCamera( x, y, z, time, change, delay, endDelay, islocal ) );
	}

	public IEnumerator CoShakeCamera( float x, float y, float z, float time, bool change = true, float delay = 0f, float endDelay = 0f, bool islocal = false )
	{
		// normalMode = false;
		mode = CameraMode.effect;
		var hash = iTween.Hash( "x", x, "y", y, "z", z, "time", time, "delay", delay, "islocal", islocal );
		iTween.ShakePosition( gameObject, hash );
		yield return new WaitForSeconds( time + delay + endDelay );
		if( change )
			//normalMode = true;
			mode = CameraMode.normal;
	}

	public IEnumerator Fade( float time )
	{
		cg.alpha = 1f;
		float count = time;
		while( count > 0f )
		{
			count -= Time.unscaledDeltaTime / time;
			cg.alpha = count;
			yield return 0;
		}
		cg.alpha = 0f;
	}

	float currSize;

	public IEnumerator CameraAction( Transform trans )
	{
		currSize = cam.orthographicSize;
		cam.orthographicSize = 3.5f;
		tgA = trans;
		mode = CameraMode.action;
		yield return new WaitForSecondsRealtime( 0.02f );
	}

	public IEnumerator CameraNormal( )
	{
		//cam.orthographicSize = currSize;
		mode = CameraMode.normal;
		yield return new WaitForSecondsRealtime( 0.02f );
	}

	public IEnumerator CameraScale( bool change = true, float per = 1.7f, float time = 0.35f )
	{
		if( change )
		{
			sizeout = sizein * per;
			Debug.Log( sizein + " " + sizeout );
			Debug.Log( sizeAcc + " " + sizeTime );

			float ctime = 0f;
			while( ctime < time )
			{
				sizeAcc = Mathf.Lerp( 0f, 1f / sizeTime, ctime / time );
				ctime += Time.unscaledDeltaTime;
				yield return new WaitForSecondsRealtime( 0.02f );
			}
			sizeAcc = 1f / sizeTime;
		}
		else
		{
			float ctime = 0f;
			while( ctime < time )
			{
				sizeAcc = Mathf.Lerp( sizeAcc, 0f, ctime / time );
				ctime += Time.unscaledDeltaTime;
				yield return new WaitForSecondsRealtime( 0.02f );
			}
			sizeAcc = 0f;
		}
	}
}
