﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System;

public static class PlanetUtil
{
	public static int TownSceneNumber = 4;

	public static string statsDataFileName = "Database/StatsDatas";
	public static int statsStartLine = 1;
	public static string spriteDataFileName = "Database/SpriteDatas";
	public static int spriteStartLine = 1;
	public static string QuestDataFileName = "Database/QuestDatas";
	public static int questStartLine = 2;
	public static string itemDataFileName = "Database/ItemDatas";
	public static int itemStartLine = 1;
	public static string itemOptionDataFileName = "Database/ItemAdditialOptionDatas";
	public static int itemOptionStartLine = 1;
	public static string rewardListDataFileName = "Database/RewardListDatas";
	public static int rewardListStartLine = 5;
	public static string buffDataFileName = "Database/BuffDatas";
	public static int buffStartLine = 1;

	public static bool newGame; //새로하기냐 이어하기냐 검사 해주기

	///////////SoundIndexs///////////////////////
	public static int S_Gun_H_Shot      = 0;
	public static int S_Gun_H_Reload    = 1;
	public static int S_Gun_S_Shot      = 3;
	public static int S_Gun_S_Reload    = 4;
	public static int S_Gun_R_Shot      = 2;
	public static int S_Gun_R_Reload    = 1;
	public static int S_Gun_V_Shot      = 0;
	public static int S_Gun_V_Reload    = 1;

	public static int S_P_Jump          = 5;
	public static int S_P_Hit           = 6;
	public static int S_P_Step          = 7;
	public static int S_P_Avoid         = 22;

	public static int S_M_RedTail_Atk   = 8;
	public static int S_M_RedTail_Hit   = 9;
	public static int S_M_RedTail_Phase = 10;
	public static int S_M_M_Naga_Atk    = 11;
	public static int S_M_M_Naga_Hit    = 12;
	public static int S_M_M_Naga_Phase  = 13;

	public static int S_EF_S1_Door_Open = 14;
	public static int S_EF_S2_Mine_Expl = 15;
	public static int S_EF_GetItem      = 16;
	public static int S_EF_UI_Pause     = 17;
	public static int S_EF_UI_Click     = 18;

	public static int S_Gun_R_L         = 24;
	public static int S_Gun_R_LS        = 25;
	public static int S_Gun_R_U         = 26;

	public static int S_Gun_V_T         = 27;
	public static int S_Gun_V_D         = 28;
	public static int S_Gun_V_U         = 29;

	public static int BGM_TITLE         = 0;
	public static int BGM_CALM1         = 1;
	public static int BGM_CALM2         = 2;
	public static int BGM_CALM3         = 3;
	public static int BGM_TOWN1         = 4;
	public static int BGM_TOWN2         = 5;
	public static int BGM_STAGE1_1      = 6;
	public static int BGM_STAGE1_2      = 7;
	public static int BGM_STAGE1_Boss   = 8;
	public static int BGM_STAGE2        = 9;
	public static int BGM_STAGE2_Boss   = 10;
	public static int BGM_STAGE3        = 11;
	public static int BGM_GAME_OVER     = 12;

	public delegate void MsgBoxCallBack( );


	public enum SkillBuffType
	{
		riple_Upgrade,
		shotgun_Upgrade
	}


	public enum AttackType
	{
		Melee,
		Range,
		NonGuard
	}

	public enum ItemType
	{
		Weapon,
		SubWeapon,
		DefaultWeapon, // 권총
		Armor, // 반드시 무기목록 바로 다음이여야 한다.(무기 종류 카운팅)
		Consume, //반드시 장비목록 바로 다음이여야 한다.(장비 가능 종류 카운팅)
		Core,
		Raw,
		None,
	}

	public enum ItemRate
	{
		Normal,
		Rare,
		Epic,
		Legendary,
		None // 반드시 마지막이여야 한다.
	}
	public static Color[] itemNameClr = new Color[(int)ItemRate.None]
	{
		new Color32(255,255,255,255),
		new Color32(56,176,255,255),
		new Color32(255,43,255,255),
		new Color32(255,211,41,255),
	};
	public static string[] itemNameClrText = new string[(int)ItemRate.None]
	{
		"ffffffff",
		"38b0ffff",
		"ff2bffff",
		"ffd329ff",
	};


	public enum WeaponType
	{
		Handgun,
		Riple,
		Shotgun,
		Scv,
		None
	}

	public enum CharacterTypes
	{
		player,
		monster,
		bossmonster
	}

	public enum CharacterStat
	{
		Level,
		MaxHp, // nowHp는 따로 관리한다.
		Attack,
		Armor,
		CriticalRate,
		Avoid, // 회피
		Acc, // 명중
		ReHitTime,
		AttackPrevDelay,
		AttackDelay,
		AttackPostDelay,
		DashCount, // 잔량은 따로 관리한다.
		DashSpeed,
		DashOpTime,
		DashTime,
		MoveSpeed,
		MoveRotate,
		JumpCount, // 잔량은 따로 관리한다.
		JumpPower,
		SecJumpPower,
		MaxBullet, // 1회 최대 장전 량
		MaxMagazine, // 총 탄창 량
		ReloadCnt, // 재장전 량
		ReloadDelay, // 재장전 소요 시간
		BulletRange, // 총알의 사거리
		Penetrate, // 관통
		MaxExp, // 최대 경험치. 도달시 레벨업
		None,// 반드시 마지막이여야 한다.
	}

	public enum CharacterStatus
	{
		AliveTime,
		GurigiCount,
		JumpCount,
		ReloadCount,
		ShotCount,
		AttackCount,
		HitCount,

		NowHp,
		NowGurugiableCount,
		NowJumpableCount,
		NowRemainBullet,
		NowExp,
		None, // 반드시 마지막이여야 한다.
	}

	[System.Flags]
	public enum CharacterState
	{
		Death = 0,
		Idle = 1 << 0,
		Move = 1 << 1,
		Air = 1 << 2,

		Dash = 1 << 3,
		Jump = 1 << 4,
		DownJump = 1 << 5,

		Attack = 1 << 6,
		Skill = 1 << 7,
		ReSkill = 1 << 8,
		Guard = 1 << 9,
		Reload = 1 << 10,   // 안씀
		Swap = 1 << 11,     // 안씀

		Hit = 1 << 12,  // 데미지 받은 여부 // 이 able값이 false면 데미지도 받지 않고 경직도 받지 않는다.
		Stuck = 1 << 13,    // 경직
		SuperArmor = 1 << 14,  // 경직 무시

		// 몬스터 전용
		Trace = 1 << 15,

		// 스턴
		Stun = 1 << 16,

	}


	public enum CharacterStatables
	{
		Move,
		Air,
		Dash,
		Jump,
		DownJump,

		Attack,
		Skill,
		ReSkill,
		Guard,
		Reload,
		Swap,

		Hit,  // 데미지 받은 여부 // 이 able값이 false면 데미지도 받지 않고 경직도 받지 않는다.
		Stuck,  // 경직
		SuperArmor,    // 경직 무시

		// 몬스터 전용
		Trace,
		Stun,
		None,// 반드시 마지막이여야 한다.
	}

	[System.Flags]
	public enum StatableSet
	{
		Movement = 1 << 0,  // 이동 점프 다운점프 대시
		Attack = 1 << 1,    // 공격 스킬
		Util = 1 << 2,  // 리로드 가드
		Hit = 1 << 3,   // 피격 
		HitAction = 1 << 4, // 피격액션

		SkillMoveAble = 1 << 5,
		SkillDontCtrl = 1 << 6,
		GuardMode = 1 << 7,
		SuperMode = 1 << 8,
		DelayMode = 1 << 9,
		IdleMode = 1 << 10
	}



	static Collider2D[] col2ds = new Collider2D[10];   // 충돌 중인 모든 콜리더들
	public static GameObject RangeIn( Collider2D _col, string _tag )
	{
		int cnt = _col.GetContacts( col2ds );
		if( cnt != 0 )
			for( int i = 0 ; i < cnt ; ++i )
			{
				if( col2ds[i].tag == _tag )
					return col2ds[i].gameObject;
			}
		return null;
	}

	public static string GetItemTypeString( Item _item )
	{
		switch( _item.type )
		{
		case PlanetUtil.ItemType.Weapon:
			switch( ( (Weapon)_item ).wpType )
			{
			case PlanetUtil.WeaponType.Handgun: return "권총";
			case PlanetUtil.WeaponType.Riple: return "저격총";
			case PlanetUtil.WeaponType.Shotgun: return "샷건";
			case PlanetUtil.WeaponType.Scv: return "중화기";
			default: return "???";
			}
		case PlanetUtil.ItemType.Armor: return "방어구";
		case PlanetUtil.ItemType.Consume: return "소비품";
		case PlanetUtil.ItemType.Raw: return "전리품";
		default: return "????";
		}
	}

	public static string GetStatTypeString( PlanetUtil.CharacterStat _type )
	{
		switch( _type )
		{
		case PlanetUtil.CharacterStat.Level: return "레벨";
		case PlanetUtil.CharacterStat.MaxHp: return "최대체력";
		case PlanetUtil.CharacterStat.Attack: return "공격력";
		case PlanetUtil.CharacterStat.Armor: return "방어력";
		case PlanetUtil.CharacterStat.CriticalRate: return "치명률";
		case PlanetUtil.CharacterStat.Avoid: return "회피";
		case PlanetUtil.CharacterStat.Acc: return "명중";
		case PlanetUtil.CharacterStat.ReHitTime: return "피격시무적시간";
		case PlanetUtil.CharacterStat.AttackPrevDelay: return "공격 선딜";
		case PlanetUtil.CharacterStat.AttackDelay: return "공격속도";
		case PlanetUtil.CharacterStat.AttackPostDelay: return "공격 후딜";
		case PlanetUtil.CharacterStat.DashCount: return "대쉬횟수";
		case PlanetUtil.CharacterStat.DashSpeed: return "대쉬속도";
		case PlanetUtil.CharacterStat.DashOpTime: return "대쉬무적시간";
		case PlanetUtil.CharacterStat.DashTime: return "대쉬시간";
		case PlanetUtil.CharacterStat.MoveSpeed: return "이동속도";
		case PlanetUtil.CharacterStat.MoveRotate: return "회전속도";
		case PlanetUtil.CharacterStat.JumpCount: return "점프횟수";
		case PlanetUtil.CharacterStat.JumpPower: return "점프력";
		case PlanetUtil.CharacterStat.SecJumpPower: return "공중점프력";
		case PlanetUtil.CharacterStat.MaxBullet: return "최대장전";
		case PlanetUtil.CharacterStat.MaxMagazine: return "최대탄창";
		case PlanetUtil.CharacterStat.ReloadCnt: return "재장전량";
		case PlanetUtil.CharacterStat.ReloadDelay: return "재장전속도";
		case PlanetUtil.CharacterStat.BulletRange: return "사거리";
		case PlanetUtil.CharacterStat.Penetrate: return "관통력";
		default: return "???";
		}
	}

	public static string GetPerfomenceString( Performence[] _pfm, bool _bulletInfo = false )
	{
		if( _pfm == null )
			return string.Empty;

		StringBuilder sb = new StringBuilder();
		for( int i = 0 ; i < _pfm.Length ; ++i )
		{
			if( !ChkBlockList( _pfm[i] ) )
				continue;
			if( _pfm[i].add != 0 )
			{
				if( _pfm[i].add < 0 ) sb.Append( "<color=orange>" );
				else sb.Append( "<color=#479EFFFF>" );

				sb.Append( GetStatTypeString( _pfm[i].type ) );
				sb.Append( _pfm[i].add.ToString( ) );
				sb.Append( "\n" );
				sb.Append( "</color>" );
			}

			if( _pfm[i].scale != 1.0f )
			{
				if( _pfm[i].scale < 1f ) sb.Append( "<color=orange>" );
				else sb.Append( "<color=#479EFFFF>" );
				sb.Append( GetStatTypeString( _pfm[i].type ) );
				sb.Append( " " );
				sb.Append( Mathf.RoundToInt( ( _pfm[i].scale - 1f ) * 100f ).ToString( ) );
				sb.Append( "%" );
				sb.Append( "\n" );
				sb.Append( "</color>" );
			}
		}

		if( _bulletInfo == true )
		{
			int maxBullet = 0, maxMagazine = 0 , reloadCnt = 0;
			for( int i = 0 ; i < _pfm.Length ; ++i )
			{
				if( _pfm[i].type == CharacterStat.MaxBullet )
					maxBullet = (int)_pfm[i].add;
				if( _pfm[i].type == CharacterStat.MaxMagazine )
					maxMagazine = (int)_pfm[i].add;
				if( _pfm[i].type == CharacterStat.ReloadCnt )
					reloadCnt = (int)_pfm[i].add;
			}
			sb.Append( maxBullet );
			sb.Append( "/" );
			sb.Append( maxMagazine );
			sb.Append( "(" );
			sb.Append( reloadCnt );
			sb.Append( ")" );
		}

		return sb.ToString( );
	}

	public static bool ChkBlockList( Performence _pfm )
	{
		if( _pfm.type == CharacterStat.BulletRange ||
			_pfm.type == CharacterStat.MaxBullet ||
			_pfm.type == CharacterStat.MaxMagazine ||
			_pfm.type == CharacterStat.AttackDelay ||
			_pfm.type == CharacterStat.AttackPrevDelay ||
			_pfm.type == CharacterStat.AttackPostDelay ||
			_pfm.type == CharacterStat.ReloadCnt )
			return false;
		return true;
	}


	public static float GetEnchancePercent( int _lvl )
	{
		return ( 15 - ( _lvl + 1 ) ) * 40 / 5.6f;
	}

	public static int GetEnchanceCost( int _lvl )
	{
		return 50 * ( _lvl + 1 );
	}
}


public class BitMaskAttribute : PropertyAttribute
{
	public System.Type propType;
	public BitMaskAttribute( System.Type _type )
	{
		propType = _type;
	}
}


[System.Serializable]
public class Stat
{
	public PlanetUtil.CharacterStat type;
	public float def; // 기본값
	public float add; // 추가값
	public float scale = 1f; // 배율값
	public float final; // 최종 계산값

	public void Calc( )
	{
		if( scale == float.NaN )
			scale = 1f;

		final = ( def + add ) * scale;
	}

	// 이것들을 한 뒤 반드시 Calc()를 호출해야한다.
	public void Reset( )
	{
		add = 0f;
		scale = 1f;
	}
	public void SetDef( float _value ) { def = _value; }
	public void ApplyAdd( float _value ) { add += _value; }
	public void DeApplyAdd( float _value ) { add -= _value; }
	public void ApplyScale( float _value ) { scale *= ( _value == 0 ) ? 1f : _value;	}
	public void DeApplyScale( float _value ) { scale /= ( _value == 0 ) ? 1f : _value;	}

	public Stat Clone( )
	{
		Stat clone = new Stat();
		clone.type = type;
		clone.def = def;
		clone.add = add;
		clone.scale = scale;
		clone.final = final;

		if( type == PlanetUtil.CharacterStat.MaxMagazine )
			Debug.Log( "clone : " + scale );

		return clone;
	}
}

[System.Serializable]
public class Performence // 아이템이나 스킬, 버프 등에 포함 된느 능력치를 올려주는 스탯
{
	public PlanetUtil.CharacterStat type;
	public float add; // 추가값
	public float scale = 1f; // 배율값

	public Performence( )
	{
		add = 0;
		scale = 1f;
	}

	public Performence( Performence pfm )
	{
		type = pfm.type;
		add = pfm.add;
		scale = pfm.scale;
		if( scale == 0 )
			scale = 1f;
	}

	public Performence Clone( )
	{
		return new Performence( this );
	}
}


[System.Serializable]
public class Statable
{
	private bool      bNever;   // false 라면 무조건 false. true일때 bTime에 의해 결정
	private bool      bTime;    // false 라면 무조건 true.	true일때 acc로 타임 아웃에 의해 결정(넘었을때 true, 아닐때 false )
	public float     resetDelay; // 이 시간이 지나면 다시...?
	private float    acc; // delay 계산을 위해 내부적으로만 사용


	// bNever	true	true	false
	// bTime	true	false	
	// result	acc?	true	false


	public void SetTimer( float _t = 0f )
	{
		bNever = true;
		bTime = true;
		if( _t == 0f )
			acc = Time.time + resetDelay;
		else
			acc = Time.time + _t;
	}

	public void Reset( bool _b )
	{
		bNever = _b;
		bTime = false;
	}

	public bool Able( )
	{
		if( bNever )
		{
			if( !bTime )
				return true;

			if( acc < Time.time )
			{
				bTime = false;
				return true;
			}
		}
		return false;
	}

}