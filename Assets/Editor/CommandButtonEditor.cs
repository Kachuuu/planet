﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor( typeof( CommandButton ) )]
public class CommandButtonEditor : Editor
{
	public override void OnInspectorGUI( )
	{
		DrawDefaultInspector( );
	}

}