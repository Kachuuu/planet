﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CanEditMultipleObjects]
[CustomEditor( typeof( PlayerInputScript ) )]
public class PlayerInputScriptEdiotr : Editor
{
	private ReorderableList keyList;
	private ReorderableList aixsList;

	public override void OnInspectorGUI( )
	{
		DrawDefaultInspector( );
		serializedObject.Update( );
		keyList.DoLayoutList( );
		aixsList.DoLayoutList( );
		serializedObject.ApplyModifiedProperties( );
	}

	private void OnEnable( )
	{
		keyList = new ReorderableList( serializedObject, serializedObject.FindProperty( "inputs" ), true, true, true, true );
		keyList.drawHeaderCallback = ( Rect rect ) =>
		{	EditorGUI.LabelField( rect, "Inputs (type)            Mobile    or    PC" );	};
		keyList.drawElementCallback = ( Rect rect, int index, bool active, bool focused ) =>
		{
			var element = keyList.serializedProperty.GetArrayElementAtIndex(index);
			EditorGUI.PropertyField( new Rect( rect.x      , rect.y, 100             , EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "input"), GUIContent.none );
			EditorGUI.PropertyField( new Rect( rect.x + 100, rect.y, 100             , EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "btn"), GUIContent.none );
			EditorGUI.PropertyField( new Rect( rect.x + 200, rect.y, rect.width - 200, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "key" ), GUIContent.none );
		};

		aixsList = new ReorderableList( serializedObject, serializedObject.FindProperty( "axies" ), true, true, true, true );
		aixsList.drawHeaderCallback = ( Rect rect ) =>
		{	EditorGUI.LabelField( rect, "Aixes (type)            Mobile    or    PC" );	};
		aixsList.drawElementCallback = ( Rect rect, int index, bool active, bool focused ) =>
		{
			var element = aixsList.serializedProperty.GetArrayElementAtIndex(index);
			EditorGUI.PropertyField( new Rect( rect.x					, rect.y, 100				, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "input" ), GUIContent.none );
			EditorGUI.PropertyField( new Rect( rect.x + 100				, rect.y, rect.width - 220	, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "axisName" ), GUIContent.none );
			EditorGUI.PropertyField( new Rect( rect.x + rect.width - 120, rect.y, 60				, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "key1" ), GUIContent.none );
			EditorGUI.PropertyField( new Rect( rect.x + rect.width - 60	, rect.y, 60				, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "key2" ), GUIContent.none );
		};
	}
}
