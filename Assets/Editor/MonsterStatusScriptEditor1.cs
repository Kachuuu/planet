﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CanEditMultipleObjects]
[CustomEditor( typeof( MonsterStatusScript ) )]
public class MonsterStatScriptEditor : Editor
{
	private ReorderableList statList;
	//private ReorderableList ableList;

	public override void OnInspectorGUI( )
	{
		DrawDefaultInspector( );
		serializedObject.Update( );
		statList.DoLayoutList( );
		//ableList.DoLayoutList( );
		serializedObject.ApplyModifiedProperties( );
	}

	private void OnEnable( )
	{
		statList = new ReorderableList( serializedObject, serializedObject.FindProperty( "stats" ), true, true, true, true );
		statList.drawHeaderCallback = ( Rect rect ) =>
		{ EditorGUI.LabelField( rect, "Stats (type)   def    add   scale  final" ); };
		statList.drawElementCallback = ( Rect rect, int index, bool active, bool focused ) =>
		{
			var element = statList.serializedProperty.GetArrayElementAtIndex(index);
			EditorGUI.PropertyField( new Rect( rect.x, rect.y, rect.width - 200, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "type" ), GUIContent.none );
			EditorGUI.PropertyField( new Rect( rect.x + rect.width - 200, rect.y, 50, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "def" ), GUIContent.none );
			EditorGUI.PropertyField( new Rect( rect.x + rect.width - 150, rect.y, 50, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "add" ), GUIContent.none );
			EditorGUI.PropertyField( new Rect( rect.x + rect.width - 100, rect.y, 50, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "scale" ), GUIContent.none );
			EditorGUI.PropertyField( new Rect( rect.x + rect.width - 50, rect.y, 50, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "final" ), GUIContent.none );
		};

		//ableList = new ReorderableList( serializedObject, serializedObject.FindProperty( "statable" ), true, true, true, true );
		//ableList.drawHeaderCallback = ( Rect rect ) =>
		//{ EditorGUI.LabelField( rect, "Stats (type)   able    delay" ); };
		//ableList.drawElementCallback = ( Rect rect, int index, bool active, bool focused ) =>
		//{
		//	var element = ableList.serializedProperty.GetArrayElementAtIndex(index);
		//	EditorGUI.PropertyField( new Rect( rect.x, rect.y, rect.width - 75, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "type" ), GUIContent.none );
		//	EditorGUI.PropertyField( new Rect( rect.x + rect.width - 75, rect.y, 25, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "able" ), GUIContent.none );
		//	EditorGUI.PropertyField( new Rect( rect.x + rect.width - 50, rect.y, 50, EditorGUIUtility.singleLineHeight ), element.FindPropertyRelative( "resetDelay" ), GUIContent.none );
		//};
	}
}
