﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Search
{
    public Graph graph;
    public List<Node> reachable;
    public List<Node> explored;
    public List<Node> path;
    public Node goalNode;
    public int iterations;
    public bool finished;

    public Search( Graph graph )
    {
        this.graph = graph;
    }

    public void Start( Node start, Node goal )
    {
        goalNode = goal;

        reachable = new List<Node>( );
        reachable.Add( start );

        path = new List<Node>( );
        explored = new List<Node>( );

        iterations = 0;

        for ( var i = 0 ; i < graph.nodes.Length ; ++i )
        {
            graph.nodes[i].Clear( );
        }

        finished = false;
    }

    public void Step()
    {
        if ( path.Count > 0 )
        {
            Debug.Log( "path.Count > n" );
            return;
        }
        if ( reachable.Count == 0 )
        {
            Debug.Log( "finished = true" );
            finished = true;
            return;
        }
        ++iterations;

        var node = ChoseNode( );

        if ( node == goalNode )
        {
            while ( node != null )
            {
                path.Insert( 0, node );
                node = node.previous;
            }
            finished = true;
            return;
        }

        reachable.Remove( node );
        explored.Add( node );

        for ( var i = 0 ; i < node.adjecent.Count ; ++i )
        {
            AddAjacent( node, node.adjecent[i] );
        }
    }

    public void AddAjacent( Node node, Node adjacent )
    {
        if ( FindNode( adjacent, explored ) || FindNode( adjacent, reachable ) )
            return;
        adjacent.previous = node;
        reachable.Add( adjacent );
    }

    public bool FindNode( Node node, List<Node> list )
    {
        for ( var i = 0 ; i < list.Count ; ++i )
        {
            if ( node == list[i] )
            {
                return true;
            }
        }
        return false;
    }

    public Node ChoseNode()
    {
        int ran = Random.Range( 0, reachable.Count );
        int ranran = Random.Range( ran, reachable.Count );
        return reachable[ranran];
        //return reachable[0];
    }
}