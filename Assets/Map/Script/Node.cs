﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public List<Node> adjecent = new List<Node>( );
    public Node previous = null;

    public int id;
    public int[] neibors = { 1000, 1000, 1000, 1000 }; //상하, 좌우

    public void Clear()
    {
        previous = null;
    }

    public void SetNeibors( int neiId )
    {
        if ( id == neiId - 1 )
            neibors[2] = neiId;
        else if ( id == neiId + 1 )
            neibors[3] = neiId;
        else
        {
            if ( id < neiId )
                neibors[0] = neiId;
            else
                neibors[1] = neiId;
        }
    }
}