﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MapMgr : MonoBehaviour
{
	public int rows;
	public int columns;
	public Vector2 roomSize;
	private int[,] map;

	public GameObject mapParent;
	public GameObject miniMapParent;
	public GameObject minimapPrefab;

	private GameObject[] mapPrefabs;
	private GameObject mapBossPrefab;
	private Sprite[] minimapSprites;

	public List<GameObject> mapList;
	public List<Transform> zenList;
	public Transform bossZen;
	public Transform startPortalTrans;
	public GameObject bossPortal;

	public string mapPath;
	public string minimapPath;
	bool awak = false;

	private void Awake( )
	{
		//StartCoroutine( MakeMap( ) );
	}

	public IEnumerator MakeMap( )
	{
		mapList = new List<GameObject>( );
		zenList = new List<Transform>( );
		minimapPath = "Sprite/minimap";
		minimapSprites = Resources.LoadAll<Sprite>( minimapPath );

		mapPath = "Prefab/Stage1/Normal";
		mapPrefabs = Resources.LoadAll<GameObject>( mapPath );

		mapPath = "Prefab/Stage1/Boss";
		var mb =Resources.LoadAll<GameObject>( mapPath );
		mapBossPrefab = mb[0];

		map = new int[rows, columns];
		for( int i = 0 ; i < rows ; ++i )
			for( int j = 0 ; j < columns ; ++j )
				map[i, j] = 0;

		if( !awak )
		{
			miniMapParent.GetComponent<GridLayoutGroup>( ).constraintCount = columns;
			for( int i = 0 ; i < rows * columns ; ++i )
			{
				var gb  = Instantiate( minimapPrefab, miniMapParent.transform );
				gb.GetComponent<Image>( ).sprite = minimapSprites[0];
			}
			awak = true;
		}
		else
		{
			for( int i = 0 ; i < rows * columns ; ++i )
			{
				miniMapParent.transform.GetChild( i ).GetComponent<Image>( ).sprite = minimapSprites[0];
				miniMapParent.transform.GetChild( i ).GetComponent<Image>( ).color = Color.white;
			}
		}

		int startRow = UnityEngine.Random.Range( 0, rows );
		int startColumn = UnityEngine.Random.Range( 0, columns );
		int endRow, endColumn;
		do
			endRow = UnityEngine.Random.Range( 0, rows );
		while( Mathf.Abs( startRow - endRow ) <= Mathf.RoundToInt( rows / 2f ) - 2 );
		do
			endColumn = UnityEngine.Random.Range( 0, columns );
		while( Mathf.Abs( startColumn - endColumn ) <= Mathf.RoundToInt( columns / 2f - 0.1f ) - 1 );

		var graph = new Graph( map );
		var search = new Search( graph );

		int startRoom = columns * startRow + startColumn;
		int endRoom = columns * endRow + endColumn;
		search.Start( graph.nodes[startRoom], graph.nodes[endRoom] );
		Debug.Log( startRoom );
		Debug.Log( endRoom );

		GetRoom( startRoom ).color = Color.blue;
		GetRoom( endRoom ).color = Color.red;

		int totalConnect = 15;

		//while( !Input.GetKeyDown( KeyCode.Alpha1 ) )
		//{
		//	yield return 0;
		//}

		while( !search.finished )
			search.Step( );

		var myMap = search.path;

		for( var i = 0 ; i < myMap.Count ; ++i )
		{
			if( i != myMap.Count - 1 )
				myMap[i].SetNeibors( myMap[i + 1].id );
			if( i != 0 )
				myMap[i].SetNeibors( myMap[i - 1].id );
		}
		Debug.Log( "최초 카운트 : " + myMap.Count );

		while( totalConnect > myMap.Count )
		{
			yield return 0;
			PlusMapGroup( graph, search );
		}
		//최종 출력
		Debug.Log( "이후 카운트 : " + myMap.Count );
		for( int i = 0 ; i < myMap.Count ; ++i )
		{
			GetRoom( myMap[i].id ).sprite = SetRoom( myMap[i].neibors );
			Vector3 v = new Vector3((myMap[i].id % columns ) *roomSize.x,(myMap[i].id /columns )*roomSize.y,0f);
			mapList.Add( Instantiate( SetRoomPrefab( myMap[i].neibors ), v, Quaternion.identity, mapParent.transform ) );
		}
		mapList.Add( Instantiate( mapBossPrefab, new Vector3( columns * 2f * roomSize.x, rows * 2f * roomSize.y, 0f ), Quaternion.identity, mapParent.transform ) );
		var portalPos = mapList[mapList.Count - 1].GetComponent<RoomScript>( ).playerSpawnZone.transform.position;

		//다음은 룸 처리
		for( int i = 0 ; i < mapList.Count ; ++i )
		{
			var m = mapList[i].GetComponent<RoomScript>( );
			m.roomNum = i;
			if( i == 0 )
			{
				startPortalTrans = m.playerSpawnZone.transform;
				m.playerSpawnZoneGb.SetActive( true );
				foreach( var a in m.monsterZenTrans )
					a.gameObject.SetActive( false );
			}
			else if( i == mapList.Count - 2 )
			{
				bossPortal = m.playerSpawnZone.gameObject;
				m.playerSpawnZoneGb.SetActive( true );
				bossPortal.layer = LayerMask.NameToLayer( "Ground" );
				bossPortal.AddComponent<BoxCollider2D>( ).isTrigger = false;
				bossPortal.AddComponent<PortalScript>( ).targetPos = portalPos;
				//보스방
				foreach( var a in m.monsterZenTrans )
					a.gameObject.SetActive( false );
			}
			else if( i == mapList.Count - 1 )
			{
				bossZen = m.monsterZenTrans[0].transform;
				m.playerSpawnZoneGb.SetActive( true );
			}
			else
			{
				foreach( var a in m.monsterZenTrans )
					zenList.Add( a );
				m.playerSpawnZoneGb.SetActive( false );
			}
		}

		foreach( var a in zenList )
		{
			a.gameObject.SetActive( false );
		}


		//zenList에서 랜덤으로 몬스터 소환

		//bosszen 에 보스 몬스터 소환

		//startPos에 캐릭터 소환
	}

	private GameObject SetRoomPrefab( int[] neibors )
	{
		int type = 0;
		int[] neis = new int[4] { 0, 0, 0, 0 };

		for( int i = 0 ; i < 4 ; ++i )
			neis[i] = neibors[i] == 1000 ? 0 : 1;

		string st = neis[0].ToString( );
		st += neis[1].ToString( );
		st += neis[2].ToString( );
		st += neis[3].ToString( );

		switch( st )
		{
		case "1000":
			type = 0; break;
		case "0010":
			type = 1; break;
		case "0100":
			type = 2; break;
		case "0001":
			type = 3; break;
		case "0011":
			type = 4; break;
		case "1011":
			type = 5; break;
		case "0111":
			type = 6; break;
		case "1100":
			type = 7; break;
		case "1110":
			type = 8; break;
		case "1101":
			type = 9; break;
		case "1010":
			type = 10; break;
		case "1001":
			type = 11; break;
		case "0101":
			type = 12; break;
		case "0110":
			type = 13; break;
		case "1111":
			type = 14; break;
		default:
			Debug.Log( "what: " + st );
			break;
		}
		return mapPrefabs[type];
	}
	private Sprite SetRoom( int[] neibors )
	{
		int type = 0;
		int[] neis = new int[4] { 0, 0, 0, 0 };

		for( int i = 0 ; i < 4 ; ++i )
			neis[i] = neibors[i] == 1000 ? 0 : 1;

		string st = neis[0].ToString( );
		st += neis[1].ToString( );
		st += neis[2].ToString( );
		st += neis[3].ToString( );

		switch( st )
		{
		case "1000":
			type = 0; break;
		case "0001":
			type = 1; break;
		case "0100":
			type = 2; break;
		case "0010":
			type = 3; break;
		case "0011":
			type = 4; break;
		case "1011":
			type = 5; break;
		case "0111":
			type = 6; break;
		case "1100":
			type = 7; break;
		case "1101":
			type = 8; break;
		case "1110":
			type = 9; break;
		case "1001":
			type = 10; break;
		case "1010":
			type = 11; break;
		case "0110":
			type = 12; break;
		case "0101":
			type = 13; break;
		case "1111":
			type = 14; break;
		default:
			Debug.Log( "what: " + st );
			break;
		}
		return minimapSprites[type + 1];
	}

	private Image GetRoom( int id )
	{
		var go = miniMapParent.transform.GetChild( rows  * columns - 1 - id).gameObject;
		return go.GetComponent<Image>( );
	}

	private void ResetMapGroup( Graph graph )
	{
		for( var i = 0 ; i < graph.nodes.Length ; ++i )
		{
			GetRoom( graph.nodes[i].id ).color = Color.white;
		}
	}

	private void PlusMapGroup( Graph graph, Search search )
	{
		var pa = search.path;
		int ran = UnityEngine.Random.Range( 0, pa.Count - 1 );// 보스방은 제외

		for( int i = 0 ; i < pa[ran].adjecent.Count ; ++i )
		{
			if( !pa.Contains( pa[ran].adjecent[i] ) )
			{
				pa[ran].SetNeibors( pa[ran].adjecent[i].id );
				pa[ran].adjecent[i].SetNeibors( pa[ran].id );
				pa.Insert( 1, pa[ran].adjecent[i] );
				break;
			}
		}
	}
}