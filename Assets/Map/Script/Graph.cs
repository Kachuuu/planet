﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph
{
    public int rows = 0;
    public int cols = 0;
    public Node[] nodes;

    public Graph( int[,] grid )
    {
        rows = grid.GetLength( 0 );
        cols = grid.GetLength( 1 );
        nodes = new Node[grid.Length];

        for ( var i = 0 ; i < nodes.Length ; ++i )
        {
            var node = new Node( );
            node.id = i;
            nodes[i] = node;
        }

        for ( var r = 0 ; r < rows ; ++r )
            for ( var c = 0 ; c < cols ; ++c )
            {
                if ( grid[r, c] == 9999 ) // 9999는 벽
                    continue;
                var node = nodes[cols * r + c];
                if ( r > 0 )
                    node.adjecent.Add( nodes[cols * (r - 1) + c] );
                if ( r < rows - 1 )
                    node.adjecent.Add( nodes[cols * (r + 1) + c] );
                if ( c > 0 )
                    node.adjecent.Add( nodes[cols * r + c - 1] );
                if ( c < cols - 1 )
                    node.adjecent.Add( nodes[cols * r + c + 1] );
            }
    }
}