﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainScreenScript : MonoBehaviour
{
	int state;
	public RectTransform main;
	public Image titleImage;
	public Image[] fades;
	public ImageCtrl[] words;

	public GameObject pressButton;
	public GameObject startButton;
	public GameObject loadButton;

	private AudioSource audioSource;

	private void Awake( )
	{
		state = 0;
		audioSource = GetComponent<AudioSource>( );
		pressButton.SetActive( false );
		startButton.SetActive( false );

		titleImage.color = Vector4.zero;
		foreach( var a in words )
			a.image.color = Vector4.zero;
	}

	IEnumerator Start( )
	{
		//yield return StartCoroutine( EffectMgr.Instance.iTweenCtrl( main.gameObject, iTweenType.scale, 1.1f, 1.1f, 0.8f, 2f ) );
		//StartCoroutine( EffectMgr.Instance.iTweenCtrl( main.gameObject, iTweenType.move, 0f, 100f, 0f, 2f ) );
		//yield return new WaitForSeconds( 99f );


		//시작
		yield return new WaitForSeconds( 0.5f );
		StartCoroutine( AudioStart( 3f ) );
		StartCoroutine( EffectMgr.Instance.Fade( true, 0, 2f ) );
		StartCoroutine( EffectMgr.Instance.iTweenCtrl( main.gameObject, iTweenType.scale, 2.7f, 2.7f, 2.7f, 7f, 1f, "linear" ) );
		yield return new WaitForSeconds( 1.5f );
		StartCoroutine( EffectMgr.Instance.FadeOut( titleImage, 4f ) );
		yield return new WaitForSeconds( 1f );
		StartCoroutine( EffectMoveAlpha( 4f, 4f ) );
		yield return new WaitForSeconds( 4.5f );

		//StartCoroutine( EffectMgr.Instance.iTweenCtrl( main.gameObject, iTweenType.move, 0f, -500f, 0f, 0.8f, 0f, "easeInOutBack" ) );
		//yield return new WaitForSeconds( 99f );

		//터치대기
		//pressButton.SetActive( true );
		state = 1;
		while( state == 0 )
			yield return null;
		//pressButton.SetActive( false );

		//메인화면
		yield return StartCoroutine( EffectMgr.Instance.iTweenCtrl( main.gameObject, iTweenType.scale, 1.65f, 1.65f, 1.65f, 0.6f ) );
		yield return StartCoroutine( EffectMgr.Instance.iTweenCtrl( main.gameObject, iTweenType.move, 0f, 220f, 0f, 1.2f ) );
		yield return new WaitForSeconds( 1f );
		yield return StartCoroutine( EffectMgr.Instance.iTweenCtrl( main.gameObject, iTweenType.scale, 1.55f, 1.55f, 1.55f, 10f, 0f, "linear" ) );
		yield return new WaitForSeconds( 0.5f );
		StartCoroutine( EffectColor( ) );

		yield return new WaitForSeconds( 4.5f );

		startButton.SetActive( true );
		loadButton.SetActive( true );
		while( state == 1 )
			yield return null;
		startButton.SetActive( false );
		loadButton.SetActive( false );

		//새로시작
		if( state == 2 )
		{
			StartCoroutine( EffectMoveAlpha( 2f, 3.5f, 0.02f, true, 1f, 0f ) );
			StartCoroutine( AudioEnd( 3f ) );
			StartCoroutine( EffectMgr.Instance.Fade( false, 0, 2f ) );
			yield return new WaitForSeconds( 3f );
			SceneManager.LoadScene( 5, LoadSceneMode.Single );
		}

		//이어하기
		else if( state == 3 )
		{
		}
	}

	public IEnumerator EffectMoveAlpha( float move = 5f, float time = 5f, float delay = 0.02f, bool revert = false, float a = 0f, float b = 1f )
	{
		for( int i = 0 ; i < words.Length ; ++i )
		{
			StartCoroutine( words[i].CoImageCtrl( Random.Range( -move, move ), Random.Range( -move, move ), a, b, Random.Range( time * 0.8f, time ), revert ) );
			yield return new WaitForSeconds( delay );
		}
	}

	public IEnumerator EffectColor( float time = 0.1f, float delay = 0.02f )
	{
		for( int i = 0 ; i < words.Length ; ++i )
		{
			StartCoroutine( words[i].CoImageColor( Vector4.one, Random.Range( 3f, 5f ) ) );
			yield return new WaitForSeconds( time );
		}
	}

	IEnumerator AudioStart( float time = 0.5f )
	{
		audioSource.volume = 0f;
		audioSource.Play( );
		while( audioSource.volume < 1f )
		{
			audioSource.volume += Time.deltaTime / time;
			yield return null;
		}
		audioSource.volume = 1f;
	}

	IEnumerator AudioEnd( float time = 0.5f )
	{
		while( audioSource.volume > 0f )
		{
			audioSource.volume -= Time.deltaTime / time;
			yield return null;
		}
		audioSource.Stop( );
		audioSource.volume = 1f;
	}

	public void ButtonClick( int state )
	{
		this.state = state;
	}
}
